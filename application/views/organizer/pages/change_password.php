<?php
//$change_pw_link = base_url('admin/change-password');
?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <div class="page-title-box">
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <?php
            $this->load->view('admin/includes/message');
            ?>
            </div>
            <div class="col-lg-1">
            </div>
        </div>

        <form class="form-validate"  method="post" action="<?php echo $change_pw_link; ?>">

            <div class="row">

                <div class="col-lg-1">
                </div>

                <div class="col-lg-10">

                    <div class="card m-b-20">
                        <div class="card-body">

                                <div class="form-group">
                                    <label>Current Password</label>
                                    <div>
                                    <?php
$field_value = NULL;
$temp_value = set_value('old');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                        <input type="password" name="old" class="form-control" id="old" placeholder="Enter current password" value="<?php echo $field_value; ?>" required> 
                                        <div class="validation-error-label">
                                            <?php echo form_error('old'); ?>
                                        </div>
                                    </div>
                                </div>
                           
                                <div class="form-group">
                                    <label>New Password</label>
                                    <div>
                                    <?php
$field_value = NULL;
$temp_value = set_value('new');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                        <input type="password" name="new" class="form-control" id="new" placeholder="Enter new password" value="<?php echo $field_value; ?>" required data-parsley-minlength="8">
                                        <div class="validation-error-label">
                                            <?php echo form_error('new'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Confirm New Password</label>
                                    <div>
                                        <?php
$field_value = NULL;
$temp_value = set_value('new_confirm');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                        <input type="password" name="new_confirm" class="form-control" id="new_confirm" placeholder="Enter confirm new password" value="<?php echo $field_value; ?>" required data-parsley-equalto="#new">
                                        <div class="validation-error-label">
                                            <?php echo form_error('new_confirm'); ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group m-b-0">
                                    <div>
                                        <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                            Reset
                                        </button>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div> 

                <div class="col-lg-1">
                </div>

            </div> <!-- end row -->
        </form>

    </div>
</div>
