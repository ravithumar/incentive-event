<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                     <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
               <?php
                $this->load->view('vendor/includes/message');
                ?>
            </div>
            <div class="col-sm-1">
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-1">
            </div>
            <div class="col-10">
                <div class="card m-b-20">
                    <div class="card-body">
                        <table class="table table-hover table-bordered mb-0">
                            
                                <tbody>
                                <?php
                                if (isset($settings) && !empty($settings) && count($settings) > 0){
                                ?>
                                <tr scope="row">
                                    <td>
                                    Email 
                                        <?php
                                        //echo ucfirst(str_replace('_', ' ', $settings[10]['path']));
                                        ?>
                                    </td>
                                    <td>
                                    <a href="mailto:<?php
                                        echo (str_replace('_', ' ', $settings[10]['value']));
                                        ?>" >
                                        <?php
                                        echo (str_replace('_', ' ', $settings[10]['value']));
                                        ?>
                                    </a>
                                    </td>
                                </tr>
                                <tr scope="row">
                                    <td>
                                    Phone
                                        <?php
                                        //echo ucfirst(str_replace('_', ' ', $settings[24]['path']));
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo (str_replace('_', ' ', $settings[24]['value']));
                                        ?>
                                    </td>
                                </tr>
                                <tr scope="row">
                                    <td>
                                        <?php
                                        echo ucfirst(str_replace('_', ' ', $settings[18]['path']));
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?php
                                        echo (str_replace('_', ' ', $settings[18]['value']));
                                        ?>" target="_blank">
                                        <?php
                                        echo (str_replace('_', ' ', $settings[18]['value']));
                                        ?>
                                    </a>
                                    </td>
                                </tr>
                                <tr scope="row">
                                    <td>
                                        <?php
                                        echo ucfirst(str_replace('_', ' ', $settings[19]['path']));
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?php
                                        echo (str_replace('_', ' ', $settings[19]['value']));
                                        ?>" target="_blank">
                                        <?php
                                        echo (str_replace('_', ' ', $settings[19]['value']));
                                        ?>
                                    </a>
                                    </td>
                                </tr>
                                <tr scope="row">
                                    <td>
                                        <?php
                                        echo ucfirst(str_replace('_', ' ', $settings[20]['path']));
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?php
                                        echo (str_replace('_', ' ', $settings[20]['value']));
                                        ?>" target="_blank">
                                        <?php
                                        echo (str_replace('_', ' ', $settings[20]['value']));
                                        ?>
                                    </a>
                                    </td>
                                </tr>
                                <tr scope="row">
                                    <td>
                                        <?php
                                        echo ucfirst(str_replace('_', ' ', $settings[21]['path']));
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?php
                                        echo (str_replace('_', ' ', $settings[21]['value']));
                                        ?>" target="_blank">
                                        <?php
                                        echo (str_replace('_', ' ', $settings[21]['value']));
                                        ?>
                                    </a>
                                    </td>
                                </tr>
                                <tr scope="row">
                                    <td>
                                        <?php
                                        echo ucfirst(str_replace('_', ' ', $settings[22]['path']));
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?php
                                        echo (str_replace('_', ' ', $settings[22]['value']));
                                        ?>" target="_blank">
                                        <?php
                                        echo (str_replace('_', ' ', $settings[22]['value']));
                                        ?>
                                    </a>
                                    </td>
                                </tr>
                                <tr scope="row">
                                    <td>
                                        <?php
                                        echo ucfirst(str_replace('_', ' ', $settings[23]['path']));
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?php
                                        echo (str_replace('_', ' ', $settings[23]['value']));
                                        ?>" target="_blank">
                                        <?php
                                        echo (str_replace('_', ' ', $settings[23]['value']));
                                        ?>
                                    </a>
                                    </td>
                                </tr>
                                
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
            <div class="col-1">
            </div>
        </div> <!-- end row -->

    </div> <!-- container-fluid -->

</div> <!-- content -->