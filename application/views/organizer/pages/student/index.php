<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>

        <?php
        $this->load->view('organizer/includes/message');
        ?>

        <div class="row">
            <div class="col-12 m-20 m-b-20">
                <a href="<?= base_url(); ?>organizer/student/add/" class="btn btn-primary float-right">Add +</a>
                
            </div>
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                        <?php echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
</div>

    

        