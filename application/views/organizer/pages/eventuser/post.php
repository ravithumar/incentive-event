<?php
$put_link = base_url('organizer/eventuser/add');
$back = base_url('organizer/eventuser');
$image  = BASE_URL().'assets/images/default.png';
//$product_data = $product_data->toArray();
//$product_style = array_column($product_data['style'], 'style_id');
// echo "<pre>";
// print_r($product_data);
// print_r($product_style);
// print_r($variants);
// print_r($category);
// exit();
?>
<style type="text/css">
.custom-control {
    display: inline-block;
    padding-right: 2rem;
}
.custom-control-input{
    position: absolute !important;
} 
.validation-error-label{
    margin-top: 0;
 }

.custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
    background-color: #2a3142;
}
input[switch] + label {
    width: 85px !important;
}
input[switch]:checked + label:after {
        left: 63px !important;
}

input[switch]:checked + label {
    background-color: #2a3142 !important;
    }
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                   <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('organizer/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <form class="form-validate"  method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Event</label>
                                        <div>
                                    
                                            <select class="form-group select2" data-parsley-errors-container="#event_id-error" name="event_id" required>
                                                <option value="">Select Event</option>
                                                <?php foreach($event as $data):?>
                                                <option value="<?php echo $data->id ?>"><?php echo $data->name; ?></option>

                                                <?php endforeach; ?>
                                            </select>
                                            <div class="validation-error-label" id="event_id-error">
                                                <?php echo form_error('event_id'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">User</label>
                                        <div>
                                    
                                            <select class="form-group select2" data-parsley-errors-container="#user_id-error" name="user_id[]"  required multiple="multiple">
                                                <option value="">Select Event</option>
                                                <?php foreach($user as $data):?>
                                                <option value="<?php echo $data->id ?>"><?php echo $data->first_name; ?></option>

                                                <?php endforeach; ?>
                                            </select>

                                            <div class="validation-error-label" id="user_id-error">
                                                <?php echo form_error('user_id'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              
                            </div>

                        
                            <hr>
                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Submit
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> 
        </div>

   </div>
</div>
<style type="text/css">
    .select2-selection__rendered{
        margin-left: 10px!important;
    }
</style>
<script src="<?php echo assets('js/custom/organizer/parsley_img_validate.js'); ?>"></script>
<script type="text/javascript">

    $(".demo1").TouchSpin({
        initval: 0.00,
        min: 0,
        max: 1000000,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $(".demo3").TouchSpin({
        initval: 00,
        min: 0,
        max: 1000000,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });
     $(".demo2").TouchSpin({
        forcestepdivisibility: 'none',
        max: 1000000000,
        decimals: 2,
        prefix: '$',
        step: 0.1,
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

     $("#picture").on("select2:selecting", function(e) {
        console.log($(this).val(), e.params.args.data);
        if($(this).val() && $(this).val().length >= 3) {
          e.preventDefault();
        }
      });


    $(".select2").select2({
        // maximumSelectionLength: 3,
    });
    $(document).on('click', '.upload', function(){
        $('#blah').attr('src', '<?php echo BASE_URL()."assets/images/default.png"; ?>');
    });
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
       }

     $(document).on('click','.remove-btn',function(){
        $(this).closest(".varient-row").remove();
    });

    var count = 1;
    $(document).on('click','#add_variant',function(){

        count = count + 1;

        var variant_row = 
        '<div class="varient-row">'+
        '<hr>'+
            '<div class="row">'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Name</label>'+
                        '<div>'+
                            '<input required="" type="text" name="event_prize_name[]" class="form-control name" placeholder="Prize Name">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Image</label>'+
                        '<div>'+
                            '<input type="file" name="image[]" class="form-control"  value="" data-parsley-errors-container="#image'+count+'"   required>'+
                            '<div id="image'+count+'"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Prize Rank</label>'+
                        '<div>'+
                            '<input required="" type="number" name="event_prize_rank[]" class="form-control name" placeholder="1">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group">'+
                    '<label >Remove</label>'+
                    '<div class="d-flex h-100">'+
                        '<div class="justify-content-center align-self-center ">'+
                            '<label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="ion-close-round"></i></label>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';

        $('#variants').append(variant_row);
        $(".select2").select2();
        $(".demo2").TouchSpin({
            forcestepdivisibility: 'none',
            max: 1000000000,
            decimals: 2,
            prefix: '$',
            step: 0.1,
            buttondown_class: 'btn btn-dark',
            buttonup_class: 'btn btn-dark'
        });

    });

    if ($('#inventory_status').is(':checked')){
        $('#quantity-div').removeClass("d-none");
    }else{
        $('#quantity-div').addClass("d-none");
    }

    $(document).on('change','#inventory_status',function(){
        if ($(this).is(':checked')){
            $('#quantity-div').removeClass("d-none");
        }else{
            $('#quantity-div').addClass("d-none");
        }
    });

</script>

    

        