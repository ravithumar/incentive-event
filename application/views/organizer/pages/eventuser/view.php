<?php
$back = base_url('organizer/eventuser');
$image  = BASE_URL().'assets/images/default.png';
?>
<style type="text/css" media="screen">
    .img-fluid {
        width: 100%;
        height: 250px;
        object-fit: contain;
    }
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo base_url('organizer/eventuser'); ?>" class="ml-2 btn btn-dark waves-effect waves-light float-right">
                        <i class="far fa-arrow-alt-circle-left m-r-10 "></i> Back 
                    </a>
                    <h4 class="page-title"><?php echo $title; ?></h4>
                   <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('organizer/includes/message');
        /*echo "<pre>";
        print_r($product);
        exit();*/
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                         <p><?php echo $eventuser->user->first_name; ?></p><br>
                         <p><?php echo $eventuser->event->name; ?></p><br>
                    </div>
                </div>
            </div> 
        
        </div>

   </div>
</div>     
<script src="<?php echo assets('pages/lightbox.js');?>"></script>  