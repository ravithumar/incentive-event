<?php
$put_link = base_url('vendor/product/edit/').$product['id'];;
$back = base_url('vendor/product');
$image  = BASE_URL().'assets/images/default.png';
?>
<style type="text/css">
.custom-control {
    display: inline-block;
    padding-right: 2rem;
}
.custom-control-input{
    position: absolute !important;
} 
.validation-error-label{
    margin-top: 0;
 }

.custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
    background-color: #2a3142;
}
input[switch] + label {
    width: 85px !important;
}
input[switch]:checked + label:after {
        left: 63px !important;
}

input[switch]:checked + label {
    background-color: #2a3142 !important;
    }
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                   <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('vendor/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <form class="form-validate"  method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Title</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('title');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = ($product['title']);
                                            }
                                            ?>
                                            <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('title'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Price</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('price');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = ($product['price']);
                                            }
                                            ?>
                                            <input type="text" name="price" class="form-control demo1" id="price" placeholder="Retail Value" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('price'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="required">Description</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('description');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = ($product['description']);
                                            }
                                            ?>
                                            <textarea required class="form-control" required maxlength="100" name="description" id="description"><?php echo $field_value; ?></textarea>
                                            <div class="validation-error-label">
                                                <?php echo form_error('description'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                   <div class="form-group">
                                        <label class="required" for="category_id">Category</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('category_id');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = ($product['category_id']);
                                            }
                                            ?>
                                            <select name="category_id" id="category_id" class="form-control select2" data-placeholder="Select category" required data-parsley-errors-container="#category_id_error">
                                                <option selected disabled></option>
                                                <?php 
                                                    
                                                foreach ($category as $key => $value) {
                                                    $selected = '';
                                                    if($field_value == $value['id']){
                                                        $selected = 'selected';
                                                    }
                                                    echo "<option value='".$value['id']."' ".$selected.">".$value['name']."</option>";
                                                }
                                                ?>
                                            </select>
                                            <div id="category_id_error"></div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('category_id'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Variant</label>
                                        <select id="variant_id" name="variant_id[]" class="form-control select2" multiple data-parsley-errors-container="#variant_id_error"  data-placeholder="Select Variants">
                                            <option value="" disabled > </option>
                                           <?php
                                            $field_value = NULL;

                                            $temp_value = set_value('variant_id');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $variant_ids = array_column($product['variant'], 'variant_id');
                                                $field_value = $variant_ids;
                                            }

                                            if(isset($variants) && !empty($variants)){
                                                foreach ($variants as $key => $value) {
                                                    if(in_array($value['id'], $field_value)){
                                                        $selected = 'selected';
                                                    }else{
                                                         $selected= '';
                                                    }
                                                ?>
                                                <option  value="<?php echo $value['id'];?>" <?php echo $selected; ?> > <?php echo ($value['name']);?> </option>
                                                <?php
                                                }
                                                }
                                                ?>
                                             
                                             <div class="validation-error-label">
                                                <?php echo form_error('variant_id'); ?>
                                            </div>
                                        </select>
                                        <div id="variant_id_error"></div>
                                    </div> 
                                </div>
                                
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label >Type</label>
                                        <div class="d-block mt-2">
                                            <?php
                                                $field_value = '';
                                                $temp_value = explode(',', $product['type']);
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                   if (in_array("1", $temp_value)){
                                                        $field_value = 'checked';
                                                   }
                                                }
                                                ?>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" data-parsley-errors-container="#error_service" id="customControlInline3" value="1" name="type" <?php echo $field_value; ?> >
                                                <label class="custom-control-label" for="customControlInline3">Alcoholic</label>
                                            </div>
                                        </div>
                                        <div id="error_service">
                                        </div>
                                        <div class="validation-error-label">
                                            <?php echo form_error('type'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group item">
                                         <label class="required">Inventory Status</label>
                                        <?php
                                        $checked = '';
                                        $field_value = NULL;
                                        $temp_value = set_value('inventory_status');
                                        if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                        }else{
                                                $field_value = ($product['inventory_status']);
                                            }
                                        if($field_value == 1){
                                            $checked = 'checked';
                                        }
                                        ?>
                                        <div>
                                        <input type="checkbox" switch="none" id="inventory_status" value="1" name="inventory_status" <?php echo $checked; ?> >
                                        <label class="mb-0 mt-1 " for="inventory_status" data-on-label="Inventory" data-off-label="Off" title="Inventory"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 d-none" id="quantity-div">
                                    <div class="form-group">
                                        <label class="required">Stock</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('stock');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = ($product['stock']);
                                            } 
                                            ?>
                                            <input type="text" name="stock" class="form-control demo3" id="stock" placeholder="Stock" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('stock'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group ">
                                    <?php
                                        $image  = BASE_URL().'assets/images/default.png';
                                        
                                    ?> 
                                    <div class="form-group ">
                                        <label class="">Image</label>
                                        <input type="file" accept="image/*" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png" onchange="readURL(this);" id="picture" name="picture"  class="form-control upload" />
                                    </div>
                                    <div class="form-group">
                                         <img class="border rounded p-0"  src="<?php echo base_url($product['image']);?>" onerror="this.src='<?php echo $image; ?>'" alt="your image" style="height: 130px;width: 130px" id="blah"/>
                                    </div>

                                </div>
                                </div>
                            </div>

                            <?php
                            if(isset($product['addon']) && count($product['addon']) > 0 ){
                                foreach ($product['addon'] as $key => $value) {
                            ?>
                                <div class="varient-row">
                                <hr>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="required">Name</label>
                                                <div>
                                                    <input required="" type="text" name="name[]" value="<?php echo $value['name']; ?>" class="form-control name" placeholder="Ex: Cheese">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="required">Price</label>
                                                <div>
                                                    <input type="text" name="vprice[]" class="form-control demo2 vprice" value="<?php echo $value['price']; ?>" placeholder="Ex: 10.99" value="" data-parsley-errors-container="#offer_price_errorcount" required>
                                                    <div id="offer_price_errorcount"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                            <label >Remove</label>
                                            <div class="d-flex h-100">
                                                <div class="justify-content-center align-self-center ">
                                                    <label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="ion-close-round"></i></label>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                }
                            }
                            ?>

                            <div id="variants">
                                <!-- Dynmaic  -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="button" name="add_variant" class="btn btn-primary waves-effect waves-light" id="add_variant" title="Add More Attribute">
                                            Add More Attribute
                                        </button>
                                    </div>
                                </div>
                            </div> 

                            <hr>
                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> 
        </div>

   </div>
</div>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script type="text/javascript">

    $(".demo1").TouchSpin({
        initval: 0.00,
        forcestepdivisibility: 'none',
        min: 0,
        max: 1000000,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $(".demo3").TouchSpin({
        initval: 00,
        forcestepdivisibility: 'none',
        min: 0,
        max: 1000000,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });
     $(".demo2").TouchSpin({
        forcestepdivisibility: 'none',
        max: 1000000000,
        decimals: 2,
        prefix: '$',
        step: 0.1,
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $(".select2").select2({
         maximumSelectionLength: 3
    });
    $("#variant_id").on("select2:selecting", function(e) {
        console.log($(this).val(), e.params.args.data);
        if($(this).val() && $(this).val().length >= 3) {
          e.preventDefault();
        }
    });

    $(document).on('click', '.upload', function(){
        $('#blah').attr('src', '<?php echo BASE_URL()."assets/images/default.png"; ?>');
    });
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
       }

     $(document).on('click','.remove-btn',function(){
        $(this).closest(".varient-row").remove();
    });

    var count = 1;
    $(document).on('click','#add_variant',function(){

        count = count + 1;

        var variant_row = 
        '<div class="varient-row">'+
        '<hr>'+
            '<div class="row">'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Name</label>'+
                        '<div>'+
                            '<input required="" type="text" name="name[]" class="form-control name" placeholder="Ex: Cheese">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Price</label>'+
                        '<div>'+
                            '<input type="text" name="vprice[]" class="form-control demo2 vprice" placeholder="Ex: 10.99" value="" data-parsley-errors-container="#offer_price_error'+count+'" required>'+
                            '<div id="offer_price_error'+count+'"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group">'+
                    '<label >Remove</label>'+
                    '<div class="d-flex h-100">'+
                        '<div class="justify-content-center align-self-center ">'+
                            '<label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="ion-close-round"></i></label>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';

        $('#variants').append(variant_row);
        $(".select2").select2();
        $(".demo2").TouchSpin({
            forcestepdivisibility: 'none',
            max: 1000000000,
            decimals: 2,
            prefix: '$',
            step: 0.1,
            buttondown_class: 'btn btn-dark',
            buttonup_class: 'btn btn-dark'
        });

    });
    if ($('#inventory_status').is(':checked')){
        $('#quantity-div').removeClass("d-none");
    }else{
        $('#quantity-div').addClass("d-none");
    }

    $(document).on('change','#inventory_status',function(){
        if ($(this).is(':checked')){
            $('#quantity-div').removeClass("d-none");
        }else{
            $('#quantity-div').addClass("d-none");
        }
    });
</script>

    

        