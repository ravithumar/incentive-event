<style type="text/css">
    .dot {
      height: 25px;
      width: 25px;
      background-color: #f05b4f;
      border-radius: 50%;
      display: inline-block;
    }
    .dot2{
        background-color: #d70206;
    }
    .dot3{
        background-color: #f4c63d;
    }
    
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Welcome to Event Organizer Dashbaord</a></li>
                    </ol>
                </div>
            </div>
        </div>

        <?php
        $this->load->view('organizer/includes/message');
        ?>

        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" onclick="window.location='<?php echo base_url('organizer/users'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-account-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Customers</h6>
                            <h4 class="mb-4"><?php echo (isset($total_users)) ? count($total_users) : '0' ;  ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" onclick="window.location='<?php echo base_url('organizer/events'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-store float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Events</h6>
                            <h4 class="mb-4">35</h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" >
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-chart-bar float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Winners</h6>
                            <h4 class="mb-4"><?php echo '0'; ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" >
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-currency-usd float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Revenue</h6>
                            <h4 class="mb-4"><?php echo number_format((float)12, 2, '.', ''); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>