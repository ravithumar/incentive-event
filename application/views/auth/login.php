<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo config('site_meta'); ?>">
        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png') ?>">
        <title><?php echo config('site_meta').'-'.ucfirst($user_type); ?></title>

        <link href="<?php echo assets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/metismenu.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/icons.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/style.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/custom.css'); ?>" rel="stylesheet" type="text/css">
    </head>
 
    <body>
        <!-- Begin page -->
        <div class="wrapper-page">
            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0 pt-2">
                        <a href="javascript:void(0)" class="logo logo-admin"><img src="<?php echo assets('images/logo-grey.png'); ?>" height="100" alt="logo"></a>
                        <!-- <a href="#" class="logo logo-admin">logo</a> -->
                    </h3>

                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Welcome Back !</h4>
                        <p class="text-muted text-center">Sign in to continue to <?php echo config('site_title'); ?> <?php echo $user_type; ?> panel.</p>
                        <?php
                        $this->load->view('admin/includes/message');
                        ?>
                        <form class="form-validate form-horizontal m-t-30" method="post" action="">



                            <div class="form-group">
                                <label class="">Email</label>
                                <?php echo lang('login_identity_label', 'identity');?>
                           
                                <?php
                                    $field_value = NULL;
                                    echo $temp_value = set_value('identity');
                                    if (isset($temp_value) && !empty($temp_value)) {
                                        $field_value = $temp_value;
                                    } 
                                ?>

                                <input type="email" name="identity" class="form-control" id="identity" placeholder="Enter email" autocomplete="off" value="<?php echo $field_value; ?>" required>
                                <div class="validation-error-label">
                                    <?php echo form_error('identity'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('login_password_label', 'password');?>
                                <?php
                                $field_value = NULL;
                                $temp_value = set_value('password');
                                if (isset($temp_value) && !empty($temp_value)) {
                                    $field_value = $temp_value;
                                }  ?>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Enter password" autocomplete="off" value="<?php echo $field_value; ?>" required>
                                <div class="validation-error-label">
                                    <?php echo form_error('password'); ?>
                                </div>
                            </div>

                            <div class="form-group row m-t-20">
                                <div class="col-12 text-center">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit" name="submit">
                                        <?php echo lang('login_submit_btn');?>
                                    </button>
                                </div>
                            </div>

                            <?php
                            if ($user_type == "vendor") {
                                ?>
                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="<?php echo base_url('vendor/forgot-password'); ?>" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                </div>
                            </div>
                            <?php
                            }else{
                                ?>
                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="<?php echo base_url('admin/forgot-password'); ?>" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                </div>
                            </div>
                            <?php
                            }?>
                            
                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">

                <p> Copyright &copy; <?php echo config('site_title')." ".date('Y'); ?></p>
                <!-- <p>&copy; <?php //echo date('Y').' '. config('site_title'); ?>. Crafted with <i class="mdi mdi-heart text-danger"></i> by Excellent WebWorld</p> -->
            </div>

        </div>


        
        <script src="<?php echo assets('js/jquery.min.js'); ?>"></script>
        <script src="<?php echo assets('js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo assets('js/metisMenu.min.js'); ?>"></script>
        <script src="<?php echo assets('js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo assets('js/waves.min.js'); ?>"></script>

        <!-- Js form validations -->
        <script src="<?php echo assets('js/validation/jquery.validate.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.min.js'); ?>"></script>



        <script src="<?php echo base_url('plugins/jquery-sparkline/jquery.sparkline.min.js'); ?>"></script>

        <!-- parsleyjs -->
        <script src="<?php echo base_url('plugins/parsleyjs/parsley.min.js'); ?>"></script>

    </body>
</html>
<script src="<?php echo base_url().'assets/js/mask/jquery.inputmask.bundle.js'; ?>"></script>
<script type="text/javascript">
    $('form').parsley();
   /* $("#identity").inputmask("(999) 999-9999",{"placeholder": ""});*/
</script>
