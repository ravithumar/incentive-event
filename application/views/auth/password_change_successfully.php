
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Change Password</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <link href="<?php echo assets('css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/metismenu.min.css');?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/icons.css');?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/style.css');?>" rel="stylesheet" type="text/css">
        <style>
            .alert-success {
                border-color: #2ab6b76e!important;
                background-color: #def3f3!important;
                color: #2ab6b7!important;
            }
        </style>
    </head>

    <body>

        <!-- Begin page -->
        <div class="wrapper-page">
            <div class="card">
                    <?php
                if($this->session->flashdata('message') != "") {
                    echo '<div class="alert alert-success">'
                    . '<button type="button" class="close theme-color" data-dismiss="alert" aria-hidden="true" style="line-height: 0.7;"><i class="fas fa-times"></i></button>'
                            .$this->session->flashdata("message").'</div>';
                }
                ?>  
                <div class="card-block">
                    <div class="ex-page-content text-center">
                        <img src="<?php echo base_url('assets/images/success_icon.png'); ?>" width="48" height="48" alt="" style="max-width: 48px;-ms-interpolation-mode: bicubic;vertical-align: middle;border: 0;line-height: 100%;height: auto;outline: none;text-decoration: none; margin-top: 5%;" data-crop="false">
                        <h2 class="text-dark">Password Change Successfully</h2><br>
                        <h5 class="">Please access your account in app.</h5><br>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p><?php echo $this->config->item('footer');?></p>
            </div>

        </div>
        
        <!-- jQuery  -->
        <script src="<?php echo assets('js/jquery.min.js');?>"></script>
        <script src="<?php echo assets('js/bootstrap.bundle.min.js');?>"></script>
        <script src="<?php echo assets('js/metisMenu.min.js');?>"></script>
        <script src="<?php echo assets('js/jquery.slimscroll.js');?>"></script>
        <script src="<?php echo assets('js/waves.min.js');?>"></script>

        <script src="<?php echo base_url('plugins/jquery-sparkline/jquery.sparkline.min.js');?>"></script>

        <!-- App js -->
        <script src="<?php echo assets('js/app.js');?>"></script>

    </body>

</html>