<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Account Verified</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo assets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">

  <style type="text/css">
    body {
        background: url(https://justborrowed.co/dashboard/assets/images/girlback.jpeg) no-repeat;
        background-color: #fdf4f1;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: 700px 800px;
        -o-background-size: cover;
        background-position-x: center;

    }
    
  </style>
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom" style="background-color: #894C48 !important;">
    <div class="container">
      <a class="navbar-brand" href="javascript:void(0)"><b>Just Borrowed</b></a>
    </div>
  </nav>

  <!-- Page Content -->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <h1 class="mt-5" style="color: #894C48;">Congratulations!</h1>
          <h4 style="color: #535a62fc;">Your email address has been successfully verified. If you have any questions about our apps, please <a href="mailto:hello@justborrowed.co" style="color: #535a62fc;"><u>reach out to our team</u></a>. </h4>
        </div>
      </div>
    </div>
  </section>

  <!-- Bootstrap core JavaScript -->
  <!-- jQuery  -->
    <script src="<?php echo assets('js/jquery.min.js');?>"></script>
    <script src="<?php echo assets('js/bootstrap.bundle.min.js');?>"></script>
    <script src="<?php echo assets('js/metisMenu.min.js');?>"></script>
    <script src="<?php echo assets('js/jquery.slimscroll.js');?>"></script>
    <script src="<?php echo assets('js/waves.min.js');?>"></script>

    <script src="<?php echo base_url('plugins/jquery-sparkline/jquery.sparkline.min.js');?>"></script>

    <!-- App js -->
    <script src="<?php echo assets('js/app.js');?>"></script>
</body>

</html>
