<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo base_url('admin/banner/add');?>" class="btn btn-primary waves-effect waves-light float-right">
                        <i class="ion-plus mr-1"></i> Add 
                    </a>
                        <h4 class="page-title"><?php echo $title; ?></h4>
                        <?php
                        echo $this->breadcrumbs->show();
                     ?>
                </div>
            </div>
        </div>
         <?php
        $this->load->view('admin/includes/message');

        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                         <table class="table table-hover dt-responsive nowrap banner" id="responseOptions" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th class="index sequence">Sequence</th>
                                <th >Id</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //_pre($banner);

                            $i = 1;
                            if (isset($banner) && !empty($banner) && count($banner) > 0){
                                foreach ($banner as $key => $value){
                                    $id = $value["id"];
                                    echo '<tr data-id="' . $id . '">';
                                    ?>

                                   
                                    <td class="index sequence  "><?php echo $i; ?></td>
                                    <td class="banner-id"><?php echo $id; ?></td>
                                    <?php
                                    $i++;
                                    echo "<td><a class='image-popup-no-margins blah1' href=".base_url().$value['image']."><img class='border rounded p-0'  src=".base_url().$value['image']." alt='your image' style='height: 75px;width: 120px;object-fit: cover;' id='blah'/></a></td>";
                                    echo "<td><a href=".base_url('admin/banner/edit/').$value['id']." class='btn btn-dark btn-sm waves-effect waves-light'>Edit</a> <button type='button' data-table='banner' data-id='".$id."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button> </td>";
                                    echo '</tr>';
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
</div>
<script src="<?php echo assets('pages/lightbox.js');?>"></script>
<!-- Required datatable js -->
<script src="<?php echo base_url('plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap4.min.js'); ?>"></script>
<!-- Buttons examples -->
<script src="<?php echo base_url('plugins/datatables/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/buttons.colVis.min.js'); ?>"></script>
<!-- Responsive examples -->
<script src="<?php echo base_url('plugins/datatables/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/responsive.bootstrap4.min.js'); ?>"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script type="text/javascript">
    
    var sequence_url = '<?php echo base_url().'admin/set-sequence'; ?>';

    var item_list = $('.banner').DataTable({
            keys: true,
            "order": [[0, "asc"]],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: -1,
            rowReorder: true,
            columnDefs: [{orderable: false, targets: [3]}],
    });

    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())

        });
        return $helper;
    },
    updateIndex = function(e, ui) {
            
            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
            UpdateSequence();
        };

    $("#responseOptions>tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex
    }).disableSelection();

    function UpdateSequence()
    {
        Notiflix.Loading.Standard();
        var seq = new Array();
        $('.banner-id').each(function (i, row) 
        {
            seq[i] = row.innerHTML;
        });

        console.log(seq);

        $.ajax({
            url: sequence_url,
            type: "POST",
            data:{seq:seq},
            success: function (returnData) {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Banner position updated.');
                console.log(returnData);
   
                
            }
        });    

        
    }

</script>