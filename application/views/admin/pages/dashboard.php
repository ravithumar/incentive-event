<style type="text/css">
    .dot {
      height: 25px;
      width: 25px;
      background-color: #f05b4f;
      border-radius: 50%;
      display: inline-block;
    }
    .dot2{
        background-color: #d70206;
    }
    .dot3{
        background-color: #f4c63d;
    }
    
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Welcome to Admin Dashbaord</a></li>
                    </ol>
                </div>
            </div>
        </div>

        <?php
       // $this->load->view('admin/includes/message');
        ?>

        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" onclick="window.location='<?php echo base_url('admin/users'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-account-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Customers</h6>
                            <h4 class="mb-4"><?php echo count($total_users); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" onclick="window.location='<?php echo base_url('admin/events'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-store float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Events</h6>
                            <h4 class="mb-4">35</h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" >
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-chart-bar float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Winners</h6>
                            <h4 class="mb-4"><?php echo number_format(15, 2, '.', ''); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" >
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-currency-usd float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Revenue</h6>
                            <h4 class="mb-4"><?php echo number_format((float)12, 2, '.', ''); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xl-12">
                <div class="card m-b-20">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-30 header-title">Recently Registered Users</h4>

                        <div class="table-responsive">
                            <table class="table table-vertical">

                                <tbody>
                                    <tr>
                                        <th>
                                            Profile
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        
                                        <th>
                                            Email
                                        </th>
                                        <th class="text-center">
                                            Action
                                        </th>
                                    </tr>
                                    <?php
                 /*                   echo "<pre>";
                                    print_r($total_users);
                                            exit;*/
                                    if(isset($users) && count($users) > 0){
                                        foreach ($users as $key => $value) {

                                            if($key == '8'){
                                                break;
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    
                                                    <img src="<?php echo base_url($value['profile_picture']); ?>" alt="user-image" class="thumb-md rounded-circle mr-2 border" style="object-fit: cover;"/>
                                                </td>
                                                <td>
                                                    
                                                    <?php echo ucfirst($value['first_name']).' '.$value['last_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo strtolower($value['email']);  ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="<?php echo base_url('admin/vendor/view/'.$value['id']); ?>" class="btn btn-secondary btn-sm waves-effect waves-light">View</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        </div>

    </div>
</div>