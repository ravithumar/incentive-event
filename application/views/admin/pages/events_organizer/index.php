<?php
$put_link = base_url('admin/events-organizer/add');
?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div> 
                <div class="btn-group float-right mt-2 mb-2">
                        <a href="<?php echo $put_link;?>" class="btn btn-sm btn-primary waves-effect waves-light">
                        <span class="btn-label">
                            <i class="fa fa-plus"></i>
                        </span>
                        Add
                    </a>
                </div>
            </div> 
        </div>

        <?php
        $this->load->view('admin/includes/message');
        ?>

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                        <?php echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div>
</div>

    

        