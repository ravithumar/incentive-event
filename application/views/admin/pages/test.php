<style type="text/css">
#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset .form-card {
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    padding: 20px 40px 30px 40px;
    box-sizing: border-box;
    width: 94%;
    margin: 0 3% 20px 3%;
    position: relative
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform fieldset .form-card {
    text-align: left;
    color: #9E9E9E
}

#msform input,
#msform textarea {
    padding: 0px 8px 4px 8px;
    border: none;
    border-bottom: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: none;
    font-weight: bold;
    border-bottom: 2px solid skyblue;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: skyblue;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#msform .action-button:hover,
#msform .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px skyblue
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
}

select.list-dt {
    border: none;
    outline: 0;
    border-bottom: 1px solid #ccc;
    padding: 2px 5px 3px 5px;
    margin: 2px
}

select.list-dt:focus {
    border-bottom: 2px solid skyblue
}

.card {
    z-index: 0;
    border: none;
    border-radius: 0.5rem;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #2C3E50;
    margin-bottom: 20px;
    font-weight: bold;
    text-align: left
}

#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #000000
}

#progressbar li {
    list-style-type: none;
    font-size: 12px;
    width: 32%;
    float: left;
    position: relative
}

#progressbar #account:before {
    font-family: FontAwesome;
    content: "1"
}

#progressbar #personal:before {
    font-family: FontAwesome;
    content: "2"
}

#progressbar #confirm:before {
    font-family: FontAwesome;
    content: "3"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 18px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: skyblue
}

.fit-image {
    width: 100%;
    object-fit: cover
}
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add New Course</h4>
                </div>
            </div>
        </div>

        <?php
        $this->load->view('admin/includes/message');
        ?>

        <!-- MultiStep Form -->
        <div id="grad1">
            <div class="row justify-content-center mt-0">
                <div class="col-11 col-sm-10 col-md-9 col-lg-9 text-center p-0 mt-3 mb-2">
                    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                        <!-- <h2><strong>Add New Course</strong></h2> -->
                        <!-- <p>Fill all form field to go to next step</p> -->
                        <div class="row">
                            <div class="col-md-12 mx-0">
                                <form id="msform">
                                    <!-- progressbar -->
                                    <ul id="progressbar" >
                                        <li class="active" id="account"><strong>Basic Information</strong></li>
                                        <li id="personal"><strong>Sub Topics</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                    </ul> <!-- fieldsets -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Basic Information</h2> 
                                            <input type="text" name="title" placeholder="Course Title" />
                                            <textarea name="desc" placeholder="Course Description"></textarea>
                                        </div> 
                                        <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Sub Topics</h2> 
                                            <button type="button" name="add" class="btn btn-dark waves-effect waves-light"  data-toggle="modal" data-target="#addtopics">
                                                Add Topics
                                            </button>

                                            <div id="addsubtopics">
                                                
                                            </div>
                                        </div> 
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title text-center">Success !</h2> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                            </div> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-7 text-center">
                                                    <h5>You Have Successfully Signed Up</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="addtopics" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form name="subtopicform">
                        
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel">Add Sub Topic</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                       <input type="text" name="subtopic"  class="subtopic form-control" placeholder="Sub Topic" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" id="savesubtopic" class="btn btn-primary waves-effect waves-light">Save changes</button>
                    </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div id="addsubtopicsmore" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form name="1subtopicform">
                        
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel">Add Sub Topic</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                       <input type="text" name="subtopic"  class="msubtopic form-control" placeholder="Sub Topic" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" id="1savesubtopic" class="btn btn-primary waves-effect waves-light">Save changes</button>
                    </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>





    </div>
</div>

<script type="text/javascript">

    $('#savesubtopic').on('click', function(e){
        var topicname = $(".subtopic").val();
       console.log($(".subtopic").val());

       var variant_row = 
        '<div class="varient-row">'+
        '<hr>'+
            '<div class="row">'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Name</label>'+
                        '<div>'+
                            '<input required="" type="text" value="'+topicname+'" name="name[]" class="form-control name" placeholder="Ex: Cheese">'+
                        '</div>'+
                    '</div>'+
                                    '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group">'+
                    '<label >Remove</label>'+
                    '<div class="d-flex h-100">'+
                        '<div class="justify-content-center align-self-center ">'+
                            '<label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="ion-close-round"></i></label>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group">'+
                    '<label >Add Sub Topic</label>'+
                    '<div class="d-flex h-100">'+
                        '<div class="justify-content-center align-self-center ">'+
                            '<button type="button" name="add" class="btn btn-dark waves-effect waves-light"  data-toggle="modal" data-target="#addsubtopicsmore">Add Sub Topic</button>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+


            '<div class="row">'+
                '<div class="col-2">'+
                '</div>'+
                '<div class="col-10">'+
                    '<div class="more">'+
                    '</div>'+
                '</div>'+
            '</div>'+


           
        '</div>';

        $('#addsubtopics').append(variant_row);
        $(".subtopic").val('');
        $('#addtopics').modal('toggle');
    });

    $('#1savesubtopic').on('click', function(e){
        var topicname = $(".msubtopic").val();
       console.log($(".msubtopic").val());

       var variant_row = 
        '<div class="varient-row">'+
        '<hr>'+
            '<div class="row">'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Name</label>'+
                        '<div>'+
                            '<input required="" type="text" value="'+topicname+'" name="name[]" class="form-control name" placeholder="Ex: Cheese">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group">'+
                    '<label >Remove</label>'+
                    '<div class="d-flex h-100">'+
                        '<div class="justify-content-center align-self-center ">'+
                            '<label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="ion-close-round"></i></label>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group">'+
                    '<label >Add Sub Topic</label>'+
                    '<div class="d-flex h-100">'+
                        '<div class="justify-content-center align-self-center ">'+
                            '<button type="button" name="add" class="btn btn-dark waves-effect waves-light"  data-toggle="modal" data-target="#addsubtopics">Add Sub Topic</button>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+


        '</div>';
        console.log( $(this).parentsUntil('.addsubtopics').find('.more'));
        $(this).parentsUntil('.addsubtopics').find('.more').append(variant_row);
        $(".msubtopic").val('');
        $('#addsubtopicsmore').modal('toggle');
    });

    $(document).on('click','.remove-btn',function(){
        $(this).closest(".varient-row").remove();
    });

    $(document).ready(function(){

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    $(".next").click(function(){

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
            'display': 'none',
            'position': 'relative'
            });
            next_fs.css({'opacity': opacity});
            },
        duration: 600
        });
    });

    $(".previous").click(function(){

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
        step: function(now) {
        // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
            'display': 'none',
            'position': 'relative'
            });
            previous_fs.css({'opacity': opacity});
        },
        duration: 600
        });
    });

    $(".submit").click(function(){
        return false;
    })

});
</script>