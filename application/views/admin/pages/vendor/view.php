<style type="text/css">
#cards img {
    height: 250px;
    width: auto;
    object-fit: contain;
}
.track .dot {
    height: 25px;
    width: 25px;
    background-color: #8bc34a;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
}
.track .not-first .dot:before {
    content: '';
    width: 0;
    height: 174%;
    position: absolute;
    border: 1px solid #8bc34a;
    bottom: 25px;
}
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo base_url('admin/vendors'); ?>" class="ml-2 btn btn-dark waves-effect waves-light float-right">
                        <i class="far fa-arrow-alt-circle-left m-r-10 "></i> Back 
                    </a>
                    <a href="<?php echo base_url('admin/vendor/edit/').$vendor['id']; ?>" class="btn btn-primary waves-effect waves-light float-right">
                        <i class="ion-edit mr-1"></i> Edit 
                    </a>
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                    echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div> 
        </div>

        <?php
        //_pre($vendor);
        $this->load->view('admin/includes/message');
        ?>
        <div class="row d-flex">
            <div class="col-lg-4">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="card-title font-16 mt-0">
                            <?php echo ($vendor['vendor']['name']); ?>
                            <p> <?php if ($vendor['active'] == 1) {
                                    echo '<span class="badge badge-success">Active</span>';
                                } elseif ($vendor['active'] == 2) {
                                    echo '<span class="badge badge-danger">Inactive</span>';
                                }else{
                                    echo '<span class="badge badge-warning">Pending</span>';
                                }?> </p>
                        </h4>
                        <h6 class="card-subtitle font-12 text-muted">
                            <p class="mb-1"><i class="fas fa-phone mr-2"></i><?php echo $vendor['phone']; ?></p>
                            <p class="mb-1"><a href="mailto:<?php echo $vendor['email']; ?>"><i class="fas fa-envelope mr-2"></i><?php echo $vendor['email']; ?></a></p>
                            <p class="mb-0"><i class="fas fa-map-pin mr-2"></i><?php echo $vendor['vendor']['street'].', '.$vendor['vendor']['city'].', '.$vendor['vendor']['state'].', '.$vendor['vendor']['country'].', '.$vendor['vendor']['zipcode']; ?></p>
                        </h6>
                    </div>
                   
                </div>

                 <div class="card m-b-30">
                    <div class="card-body"> 
                        <h4 class="card-title font-16 mt-0 mb-0">
                            <b>Basic Information</b>
                        </h4>
                    </div>
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td><b>Contact Person Name :</b></td>
                                <td><?php echo ($vendor['first_name']).' '.($vendor['last_name']);?></td>
                            </tr>
                            <tr>
                                <td><b>Alternative Phone No :</b></td>
                                <td><?php echo ($vendor['vendor']['phone_2']);?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="card m-b-30">
                    <div class="card-body"> 
                        <h4 class="card-title font-16 mt-0 mb-0">
                            <b>Store Information</b>
                        </h4>
                    </div>
                    <table class="table table-hover">
                        <tbody>
                            <!-- <tr>
                                <td >
                                   <b>Category</b> 
                                </td>
                                <td>
                                    <?php
                                   /*  $categorys = array_column($vendor['category'], 'category_id');
                                    foreach ($category as $key => $value) {
                                        if(in_array($value['id'], $categorys))
                                        {
                                            echo '<span class="badge badge-dark p-1 mr-1">'.$value['name'].'</span>';
                                        }
                                    } */
                                    ?>
                                </td>
                            </tr> -->
                            <tr>
                                <td >
                                    <b>Minimum Order Amount</b>
                                </td>
                                <td >
                                    <?php echo '$ '.$vendor['vendor']['minimum_order_amount'];?>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <b>Vendor Percentage</b>
                                </td>
                                <td >
                                    <?php echo $vendor['vendor']['percentage'].' %';?>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <b>Service Available</b>
                                </td>
                                <td >
                                    <?php 
                                    if ($vendor['vendor']['service_available'] == 1){
                                        echo '<span class="badge badge-dark p-1">Delivery</span>';

                                    }elseif($vendor['vendor']['service_available'] == 2){
                                        echo '<span class="badge badge-dark p-1">Takeaway</span>';
                                    }elseif ($vendor['vendor']['service_available'] == 3) {
                                        echo '<span class="badge badge-dark p-1 mr-1">Delivery</span>';
                                        echo '<span class="badge badge-dark p-1">Takeaway</span>';
                                    }else{
                                        echo '-';
                                    }
                                    ?>
                                    
                                </td>
                            </tr>
                           <!--  <tr>
                                <td >
                                   <b>Payment Mode</b> 
                                </td>
                                <td>
                                    <?php
                                    /* $payment_type = explode(',', $vendor['vendor']['payment_type']);
                                    if(in_array('1', $payment_type)){
                                        echo '<span class="badge badge-dark p-1 mr-1">COD</span>';
                                    }
                                    if(in_array('2', $payment_type)){
                                        echo '<span class="badge badge-dark p-1 mr-1">Debit/Credit Card</span>';
                                    }
                                    if(in_array('3', $payment_type)){
                                        echo '<span class="badge badge-dark p-1 mr-1">Payment Gateway</span>';
                                    } */
                                    
                                    ?>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>
                <?php if(isset($stripe_detail) && !empty($stripe_detail) && !isset($stripe_detail['error']) ){ ?>
                <div class="card m-b-30">
                    <div class="card-body"> 
                        <h4 class="card-title font-16 mt-0 mb-0">
                            <b>Bank Information</b>
                        </h4>
                    </div>
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td >
                                    <b>Account Id</b>
                                </td>
                                <td >
                                    <?php echo $stripe_detail['success']['id'];?>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <b>Account number</b>
                                </td>
                                <td >
                                    <?php echo $stripe_detail['success']['external_accounts']['data'][0]['last4'];?>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <b>Routing number</b>
                                </td>
                                <td >
                                    <?php echo $stripe_detail['success']['external_accounts']['data'][0]['routing_number'];?>
                                </td>
                            </tr><tr>
                                <td >
                                    <b>Postal code</b>
                                </td>
                                <td >
                                    <?php echo $stripe_detail['success']['company']['address']['postal_code'];?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php }?>
            </div>

            <div class="col-lg-3">
                <div class="card m-b-30" id="cards">
                    <h4 class="card-header font-16 mt-0">LOGO</h4>
                    <div class="card-body text-center"> 
                        <a class="image-popup-no-margins" href="<?php echo BASE_URL().$vendor['profile_picture'];?>">
                            <img class="img-fluid border" src="<?php echo BASE_URL().$vendor['profile_picture']; ?>" alt="Profile Picture">
                        </a>
                    </div>
                </div>
                <div class="card m-b-30" id="cards">
                    <h4 class="card-header font-16 mt-0">Backgroud Image</h4>
                    <div class="card-body text-center"> 
                        <a class="image-popup-no-margins" href="<?php echo BASE_URL().$vendor['vendor']['bg_img'];?>">
                            <img class="img-fluid border" src="<?php echo BASE_URL().$vendor['vendor']['bg_img']; ?>" alt="Profile Picture">
                        </a>
                    </div>
                </div>
               

            </div> <!-- end col -->

            <div class="col-lg-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card mini-stat bg-dark">
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-square-inc-cash float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Net Profit</h6> 
                                    <h4 class="mb-4"><?php echo number_format((float)$net_profit, 2, '.', ''); ?></h4>
                                    <!-- <h4 class="mb-4">0</h4> -->
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary ">
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-cash-multiple float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Revenue</h6>
                                    <h4 class="mb-4"><?php echo number_format((float)$revenue, 2, '.', ''); ?></h4>
                                    <!-- <h4 class="mb-4">0</h4> -->
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary">
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-briefcase-check float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Total Orders</h6> 
                                    <h4 class="mb-4"><?php echo $total_orders; ?></h4>
                                    <!-- <h4 class="mb-4">0</h4> -->
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-dark ">
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-food float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Total Products</h6>
                                    <h4 class="mb-4"><?php echo count($vendor['product']); ?></h4>
                                    <!-- <h4 class="mb-4">0</h4> -->
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card m-b-30">
                    <div class="card-body"> 
                        <h4 class="card-title font-16 mt-0 mb-0">
                            <b>Working Hours</b>
                        </h4>
                    </div>
                    <table class="table table-hover">
                        <tbody>
                            <?php foreach ($vendor['vendor_availability'] as $key => $value) {
                                
                            ?>
                            <tr>
                                <td class="py-1">
                                    <?php echo $value['day'];?>
                                </td>
                                <?php if($value['full_day'] == 1) { ?>
                                <td colspan="3">
                                   <span class="badge badge-success">24 Hours Open</span>
                                </td>
                                <?php } elseif($value['is_closed'] == 1) {?>
                                <td colspan="3">
                                   <span class="badge badge-danger">CLOSED</span>
                                </td>
                                <?php }else{ ?>
                                <td>
                                    <?php echo $value['from_time'];?>
                                </td>
                                <td>
                                    to
                                </td>
                                <td>
                                    <?php echo $value['to_time'];?>
                                </td>
                                <?php }?>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>

            
        </div> <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <h4 class="card-header font-16 mt-0">Orders</h4>
                    <div class="card-body">
                         <?php echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> 
        </div> 

        <div class="modal fade bs-example-modal-lg" id="trackmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content track">
              <div class="modal-header bg-primary">
                <h6 class="modal-title text-center m-0 font-weight-bold text-white">Track Order #<span id="order-id"></span></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               <h4 class="text-center">Tracking data fetching..</h4>
              </div>
              <div class="text-center text-white bg-primary py-2">
                <h6 class="text-uppercase font-weight-bold"><span id="order-final-status"></span></h6>
                <div class="pb-2"><span id="order-final-text"></span></div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('/assets/js/custom/admin/track_order.js');?>" type="text/javascript"></script>
<script src="<?php echo assets('pages/lightbox.js');?>"></script>

        