<?php
$setting_save_link = base_url('admin/setting-update');
$image  = BASE_URL().'assets/images/default.png';
//_pre($settings);
?>
<style type="text/css" media="screen">
.table .btn {
    padding: 0.4rem 0.75rem;
    width: unset;
}    
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                     <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
               <?php
                $this->load->view('admin/includes/message');
                ?>
            </div>
            <div class="col-sm-1">
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-1">
            </div>
            <div class="col-10">
                <div class="card m-b-20">
                    <div class="card-body">
                        <form class="form-validate"  method="post" action="<?php echo $setting_save_link; ?>"  enctype="multipart/form-data">
                            <table class="table table-hover table-bordered mb-0">
                               
                                 <tbody>
                                    <?php
                                    if (isset($settings) && !empty($settings) && count($settings) > 0){
                                    ?>
                                    <!-- <tr scope="row">
                                        <td>
                                            <?php
                                            //echo ucfirst(str_replace('_', ' ', $settings[0]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
/* 
                                                $field_value = NULL;
                                                $temp_value = set_value('');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                                }else{
                                                    $field_value = $settings[0]['value'];
                                                } */
                                                ?>
                                            <input type="text" name="<?php //echo $settings[0]['path']; ?>" class="form-control " id="<?php //echo $settings[0]['path']; ?>" data-parsley-errors-container="#error" placeholder="Enter stripe key" value="<?php //echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php //echo form_error($settings[0]['path']); ?>
                                            </div>
                                            <div id="error">
                                                
                                            </div>
                                        </td>
                                    </tr> -->
                                    <!-- <tr scope="row">
                                        <td>
                                            <?php
                                             //echo ucfirst(str_replace('_', ' ', $settings[1]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            /* $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[1]['value'];
                                            } */
                                            ?>
                                            <input type="text" name="<?php //echo $settings[1]['path']; ?>" class="form-control " id="<?php //echo $settings[1]['path']; ?>" placeholder="Enter stripe secret" value="<?php// echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php //echo form_error($settings[1]['path']); ?>
                                            </div>
                                        </td>
                                    </tr> -->
                                    <!-- <tr scope="row">
                                        <td>
                                            <?php
                                             //echo ucfirst(str_replace('_', ' ', $settings[2]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            /* $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[2]['value'];
                                            } */
                                            ?>
                                            <input type="text" name="<?php //echo $settings[2]['path']; ?>" class="form-control " id="<?php //echo $settings[2]['path']; ?>" placeholder="Enter stripe mode" value="<?php //echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php// echo form_error($settings[2]['path']); ?>
                                            </div>
                                        </td>
                                    </tr> -->
                                    
                                     <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[3]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[3]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[3]['path']; ?>" class="form-control " id="<?php echo $settings[3]['path']; ?>" placeholder="Enter site title" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[3]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                   <!--  <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[4]['path']));
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[4]['value'];
                                            }
                                            ?>
                                            <div class="row">
                                               <div class="col-lg-9">
                                                    <input type="file" accept="image/*" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png " onchange="readURL(this);" name="<?php echo $settings[4]['path']; ?>" class="form-control " id="<?php echo $settings[4]['path']; ?>" >
                                                    <input type="hidden" name="site_logo_old" value="<?php echo $settings[4]['value']; ?>">
                                                    <div class="validation-error-label">
                                                        <?php echo form_error($settings[4]['path']); ?>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                     <img src="<?php echo base_url().'/assets/files/'.$field_value ?>" class="border rounded p-0"  onerror="this.src='<?php echo $image; ?>'" alt="your image" style="height: 130px;width: 130px" id="blah">    
                                                </div> 
                                            </div>
                                        </td>
                                    </tr> -->
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[5]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                                }else{
                                                    $field_value = $settings[5]['value'];
                                                }
                                                ?>
                                            <input type="text" name="<?php echo $settings[5]['path']; ?>" class="form-control " id="<?php echo $settings[5]['path']; ?>" placeholder="Enter site meta" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[5]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[6]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[6]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[6]['path']; ?>" class="form-control " id="<?php echo $settings[6]['path']; ?>" placeholder="Enter site keyword" value="<?php echo $field_value; ?>" required>
                                             
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[6]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                   <!--  <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[7]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                    $field_value = NULL;
                                                    $temp_value = set_value('');
                                                    if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                    }else{
                                                        $field_value = $settings[7]['value'];
                                                    }
                                                    ?>
                                            <div class="row">
                                               <div class="col-lg-9">
                                                    <input type="file" accept="image/*" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png ,image/x-icon" onchange="readURL1(this);" name="<?php echo $settings[7]['path']; ?>" class="form-control " id="<?php echo $settings[7]['path']; ?>" >
                                                    <input type="hidden" name="favicon_old" value="<?php echo $settings[7]['value']; ?>">
                                                    <div class="validation-error-label">
                                                        <?php echo form_error($settings[7]['path']); ?>
                                                    </div>

                                                </div>
                                                <div class="col-lg-3">
                                                    ` <img src="<?php echo base_url().'/assets/files/'.$field_value ?>" class="border rounded p-0"  onerror="this.src='<?php echo $image; ?>'" alt="your image" style="height: 130px;width: 130px" id="blah1">    
                                                </div> 
                                            </div>
                                        </td>
                                       
                                    </tr> -->
                                    <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[8]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[8]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[8]['path']; ?>" class="form-control " id="<?php echo $settings[8]['path']; ?>" placeholder="Enter fcm key" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[8]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[9]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[9]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[9]['path']; ?>" class="form-control" id="<?php echo $settings[9]['path']; ?>" placeholder="Enter map key" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[9]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[10]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[10]['value'];
                                            }
                                            ?>
                                            <input type="email" name="<?php echo $settings[10]['path']; ?>" class="form-control" id="<?php echo $settings[10]['path']; ?>" placeholder="Enter from email" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[10]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[11]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[11]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[11]['path']; ?>" class="form-control" id="<?php echo $settings[11]['path']; ?>" placeholder="Enter smtp host" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[11]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[12]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[12]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[12]['path']; ?>" class="form-control" id="<?php echo $settings[12]['path']; ?>" placeholder="Enter smtp port" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[12]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[13]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[13]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[13]['path']; ?>" class="form-control" id="<?php echo $settings[13]['path']; ?>" placeholder="Enter smtp user" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[13]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[14]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[14]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[14]['path']; ?>" class="form-control" id="<?php echo $settings[14]['path']; ?>" placeholder="Enter smtp password" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[14]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[15]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[15]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[15]['path']; ?>" class="form-control" id="<?php echo $settings[15]['path']; ?>" placeholder="Enter from name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[15]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ','HST'));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[16]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[16]['path']; ?>" class="form-control demo1" id="<?php echo $settings[16]['path']; ?>" placeholder="Enter service charge" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[16]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[17]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[17]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[17]['path']; ?>" class="form-control" id="<?php echo $settings[17]['path']; ?>" placeholder="Enter api key" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[17]['path']); ?>
                                            </div>
                                        </td>
                                    </tr -->
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[18]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[18]['value'];
                                            }
                                            ?>
                                            <input type="url" name="<?php echo $settings[18]['path']; ?>" class="form-control" id="<?php echo $settings[18]['path']; ?>" placeholder="Enter twitter link" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[18]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[19]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[19]['value'];
                                            }
                                            ?>
                                            <input type="url" name="<?php echo $settings[19]['path']; ?>" class="form-control" id="<?php echo $settings[19]['path']; ?>" placeholder="Enter facebook link" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[19]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[20]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[20]['value'];
                                            }
                                            ?>
                                            <input type="url" name="<?php echo $settings[20]['path']; ?>" class="form-control" id="<?php echo $settings[20]['path']; ?>" placeholder="Enter instagram link" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[20]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[21]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[21]['value'];
                                            }
                                            ?>
                                            <input type="url" name="<?php echo $settings[21]['path']; ?>" class="form-control" id="<?php echo $settings[21]['path']; ?>" placeholder="Enter google link" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[21]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[22]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[22]['value'];
                                            }
                                            ?>
                                            <input type="url" name="<?php echo $settings[22]['path']; ?>" class="form-control" id="<?php echo $settings[22]['path']; ?>" placeholder="Enter pinterest link" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[22]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                            echo ucfirst(str_replace('_', ' ', $settings[23]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[23]['value'];
                                            }
                                            ?>
                                            <input type="url" name="<?php echo $settings[23]['path']; ?>" class="form-control" id="<?php echo $settings[23]['path']; ?>" placeholder="Enter youtube link" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[23]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[24]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[24]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[24]['path']; ?>" class="form-control number" id="<?php echo $settings[24]['path']; ?>" placeholder="Enter contact number" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[24]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[25]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[25]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[25]['path']; ?>" class="form-control simple" id="<?php echo $settings[25]['path']; ?>" placeholder="Enter total no feature product number" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[25]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- <tr scope="row">
                                        <td>
                                            <?php
                                             //echo ucfirst(str_replace('_', ' ', $settings[26]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                           /*  $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[26]['value'];
                                            } */
                                            ?>
                                            <input type="text" name="<?php //echo $settings[26]['path']; ?>" class="form-control demo1" id="<?php //echo $settings[26]['path']; ?>" placeholder="Enter tax" value="<?php //echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php //echo form_error($settings[26]['path']); ?>
                                            </div>
                                        </td>
                                    </tr> -->
                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[27]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[27]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[27]['path']; ?>" class="form-control demo1" id="<?php echo $settings[27]['path']; ?>" placeholder="Enter delivery person percentage" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[27]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- <tr scope="row"> -->
                                        <!-- <td> -->
                                            <?php
                                             //echo ucfirst(str_replace('_', ' ', $settings[28]['path']));
                                            ?>
                                        <!-- </td> -->
                                       <!--  <td>
                                            <?php
                                            /* $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[28]['value'];
                                            } */
                                            ?>
                                            <input type="text" name="<?php //echo $settings[28]['path']; ?>" class="form-control demo4" id="<?php //echo $settings[28]['path']; ?>" placeholder="Enter delivery miles available" value="<?php //echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php //echo form_error($settings[28]['path']); ?>
                                            </div>
                                         </td> -->
                                    <!-- </tr> -->

                                    <!-- <tr scope="row"> -->
                                        <!-- <td> -->
                                            <?php
                                             //echo ucfirst(str_replace('_', ' ', $settings[29]['path']));
                                            ?>
                                        <!-- </td>
                                        <td> -->
                                            <?php
                                            /* $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[29]['value'];
                                            } */
                                            ?>
                                            <!-- <input type="text" name="<?php //echo $settings[29]['path']; ?>" class="form-control demo5" id="<?php //echo $settings[29]['path']; ?>" placeholder="Enter delivery person response time" value="<?php //echo $field_value; ?>" required> -->
                                           <!--  <div class="validation-error-label">
                                                <?php //echo form_error($settings[29]['path']); ?>
                                            </div> -->
                                        <!-- </td> -->
                                    <!-- </tr> -->

                                    <tr scope="row">
                                        <td>
                                            <?php
                                             echo ucfirst(str_replace('_', ' ', $settings[30]['path']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                            $field_value = $temp_value;
                                            }else{
                                                $field_value = $settings[30]['value'];
                                            }
                                            ?>
                                            <input type="text" name="<?php echo $settings[30]['path']; ?>" class="form-control demo5" id="<?php echo $settings[30]['path']; ?>" placeholder="Enter store confirmation minitues" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error($settings[30]['path']); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>

                            <div class="form-group m-b-0 mt-3">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> <!-- end col -->
            <div class="col-1">
            </div>
        </div> <!-- end row -->

    </div> <!-- container-fluid -->

</div> <!-- content -->
<script src="<?php echo base_url().'assets/js/mask/jquery.inputmask.bundle.js'; ?>"></script>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script type="text/javascript">
  $(".number").inputmask("(999) 999-9999",{"placeholder": ""});
    $(".demo1").TouchSpin({
        initval: 0.00,
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    }); 

    $(".demo4").TouchSpin({
        initval: 0.00,
        min: 0,
        max: 100,
        step: 1.00,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: 'Miles',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $(".demo5").TouchSpin({
        initval: 0,
        min: 0,
        max: 100,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: 'Minutes',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $(".simple").TouchSpin({
        initval: 0.00,
        min: 0,
        max: 20,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

     $(document).on('click', '#favicon', function(){
        $('#blah1').attr('src', '<?php echo BASE_URL()."assets/images/default.png"; ?>');
    });
       function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
       }

    $(document).on('click', '#site_logo', function(){
        $('#blah').attr('src', '<?php echo BASE_URL()."assets/images/default.png"; ?>');
    });
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
       }
</script>