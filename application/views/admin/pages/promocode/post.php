<?php
$post_link = base_url('admin/promocode/add');
$back = base_url('admin/promocode');
?>
<style type="text/css">
    .select2-container--default .select2-selection--single .select2-selection__clear{
        float: left !important;
    }
    input[switch]:checked + label {
        background-color: #2a3142;
    }
</style>
<script type="module" src="<?php echo base_url('plugins/parsleyjs/extra/plugin/bind.js'); ?>"></script>
<script type="module" src="<?php echo base_url('plugins/parsleyjs/extra/validator/comparison.js'); ?>"></script>
<script type="module" src="<?php echo base_url('plugins/parsleyjs/extra/validator/date.js'); ?>"></script>
<script type="module" src="<?php echo base_url('plugins/parsleyjs/extra/validator/dateiso.js'); ?>"></script>
<script type="module" src="<?php echo base_url('plugins/parsleyjs/extra/validator/luhn.js'); ?>"></script>
<script type="module" src="<?php echo base_url('plugins/parsleyjs/extra/validator/notequalto.js'); ?>"></script>
<script type="module" src="<?php echo base_url('plugins/parsleyjs/extra/validator/words.js'); ?>"></script>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php //_pre($vendor);
                    echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                        <form class="form-validate"  method="post" action="<?php echo $post_link; ?>" >
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                <label class=" control-label">Vendor</label>
                                <select id="user_id" name="user_id" class="form-control select2"  data-parsley-errors-container="#user_id_error" data-placeholder="Select Vendor">
                                    <option value="" disabled selected> </option>
                                    <?php
                                    $field_value = NULL;
                                    $temp_value = set_value('user_id');
                                    if (isset($temp_value) && !empty($temp_value)) {
                                    $field_value = $temp_value;
                                    }
                                    foreach ($vendor as $key => $value) 
                                    {   
                                       if($value['vendor']['user_id'] == $field_value){
                                            $selected = 'selected';
                                       }else{
                                            $selected = '';
                                       }
                                    ?>
                                        <option value="<?php echo $value['vendor']['user_id'];?>" <?php echo $selected; ?> > <?php echo $value['vendor']['name'];?></option>
                                    <?php
                                        } 
                                    ?>
                                     
                                     <div class="validation-error-label">
                                        <?php echo form_error('user_id'); ?>
                                    </div>
                                </select>
                                <div id="user_id_error"></div>
                            </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Promocode</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('promocode');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="text" name="promocode" class="form-control" id="promocode" placeholder="Enter Promocode" value="<?php echo $field_value; ?>" data-parsley-type="alphanum" required style="text-transform:uppercase">
                                            <div class="validation-error-label">
                                                <?php echo form_error('promocode'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Minimum Order Amount</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('promo_min_order_amount');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input required type="text" name="promo_min_order_amount" class="form-control demo1" id="promo_min_order_amount" data-parsley-errors-container="#error_min_amount" placeholder="Enter minimum order amount" value="<?php echo $field_value; ?>" >
                                            <div class="validation-error-label">
                                                <?php echo form_error('promo_min_order_amount'); ?>
                                            </div>
                                            <div id="error_min_amount"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="amount">Discount Amount</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('amount');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <div class="input-group">
                                                <input  required type="text" name="amount" class="form-control" id="amount" placeholder="Ex: 40.00" value="<?php echo $field_value; ?>" data-parsley-type="number" data-parsley-errors-container="#error_disc_amount">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-percent" id="amount-type-icon"></i></span>
                                                </div>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('amount'); ?>
                                            </div>
                                            <div id="error_disc_amount"></div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-1">
                                    <div class="form-group">
                                        <label class="required" for="">Type</label>
                                        <div>
                                        <?php
                                            $checked = '';
                                            $field_value = NULL;
                                            $temp_value = set_value('discount_type');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            if($field_value == 1){
                                                $checked = 'checked';
                                            }
                                            ?>
                                        <input type="checkbox" switch="none" id="discount_type" value="1" name="discount_type" <?php echo $checked; ?> >
                                        <label class="mb-0 mt-1" for="discount_type" data-on-label="%" data-off-label="$"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5" id="max-disc">
                                    <div class="form-group">
                                        <label class="required" for="max_disc">Maximum Discount</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('max_disc');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="number" name="max_disc" class="form-control demo1 max-disc" id="max_disc" placeholder="Ex: 50.00" value="<?php echo $field_value; ?>" data-parsley-errors-container="#errors_max_disc">
                                            <div class="validation-error-label">
                                                <?php echo form_error('max_disc'); ?>
                                            </div>
                                            <div id="errors_max_disc"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="usage_limit">Usage Limit</label>
                                        <div>
                                        <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('usage_limit');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                                ?>
                                            <input required type="text" name="usage_limit" class="form-control demo2" id="usage_limit" placeholder="Ex: 3" value="<?php echo $field_value; ?>" data-parsley-errors-container="#error_limit">
                                            <div class="validation-error-label">
                                                <?php echo form_error('usage_limit'); ?>
                                            </div>
                                            <div id="error_limit"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="description">Description</label>
                                        <div>
                                        <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('description');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                                ?>
                                            <textarea required class="form-control" name="description" id="description"><?php echo $field_value; ?></textarea>
                                            <div class="validation-error-label">
                                                <?php echo form_error('description'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="from_date">From Date</label>
                                        <div>
                                            <div class="input-group">
                                                <?php
                                                    $field_value = NULL;
                                                    $temp_value = set_value('from_date');
                                                    if (isset($temp_value) && !empty($temp_value)) {
                                                        $field_value = $temp_value;
                                                    } 
                                                    ?>
                                                <input required type="text" class="form-control datepicker-autoclose" placeholder="Ex. 10 September , 2020" name="from_date" id="from_date" data-parsley-errors-container="#error_from" autocomplete="off" value="<?php echo $field_value; ?>" readonly> 
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('from_date'); ?>
                                            </div>
                                            <div id="error_from"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="to_date">To Date</label>
                                        <div>
                                            <div class="input-group">
                                                <?php
                                                    $field_value = NULL;
                                                    $temp_value = set_value('to_date');
                                                    if (isset($temp_value) && !empty($temp_value)) {
                                                        $field_value = $temp_value;
                                                    } 
                                                    ?>
   
                                                <input required type="text" class="form-control datepicker-autoclose" placeholder="Ex. 23 September , 2020" name="to_date" id="to_date" data-parsley-errors-container="#error_to" autocomplete="off" value="<?php echo $field_value; ?>" disabled readonly>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('to_date'); ?>
                                            </div>
                                            <div id="error_to"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Submit
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
</div>
<script type="text/javascript">
    $('.select2').select2({
        allowClear: true
      });
     $(".demo1").TouchSpin({
        forcestepdivisibility: 'none',
        initval : 0.00,
        max: 1000000000,
        decimals: 2,
        prefix: '$',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });
     $(".demo2").TouchSpin({
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });
    if ($('#discount_type').is(':checked')){
        $('#max-disc').removeClass("d-none");

        $('#amount-type-icon').removeClass("mdi-currency-usd");
        $('#amount-type-icon').addClass("mdi-percent");
        $('#amount').attr('max', '100');
        $('.max-disc').attr('min', '1');

    }else{
        $('#max-disc').addClass("d-none");

        $('#amount-type-icon').addClass("mdi-currency-usd");
        $('#amount-type-icon').removeClass("mdi-percent");
        $("#amount").removeAttr('max');
        $(".max-disc").removeAttr('min');

    }
     $(document).on('change','#discount_type',function(){
        if ($(this).is(':checked')){
            $('#max-disc').removeClass("d-none");

            $('#amount-type-icon').removeClass("mdi-currency-usd");
            $('#amount-type-icon').addClass("mdi-percent");
            $('#amount').attr('max', '100');
            $('.max-disc').attr('min', '1');

        }else{
            $(".max-disc").val('0.00'); 
            $("#discount_type").val('0'); 
            $('#max-disc').addClass("d-none");

            $('#amount-type-icon').addClass("mdi-currency-usd");
            $('#amount-type-icon').removeClass("mdi-percent");
            $("#amount").removeAttr('max');
            $(".max-disc").removeAttr('min');
        }
    });

    $("#from_date").datepicker({
        todayBtn:  1,
        autoclose: true,
        format: 'dd MM, yyyy',
        startDate: '-0d',   
    }).on('changeDate', function (selected) {
        $("#to_date").removeAttr('disabled');
        var minDate = new Date(selected.date.valueOf());
        $('#to_date').datepicker('setStartDate', minDate);
    });

    $("#to_date").datepicker({
        autoclose: true,
        format: 'dd MM, yyyy',
    }).on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            $('#from_date').datepicker('setEndDate', maxDate);
    });
    
</script>


    

        