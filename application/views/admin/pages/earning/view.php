<style type="text/css" media="screen">
.track .dot {
    height: 25px;
    width: 25px;
    background-color: #8bc34a;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
}
.track .not-first .dot:before {
    content: '';
    width: 0;
    height: 174%;
    position: absolute;
    border: 1px solid #8bc34a;
    bottom: 25px;
}
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">  
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Order Detail</h4>
                    <?php
                    // _pre($order);
                        echo $this->breadcrumbs->show();
                     ?>
                    <div class="state-information d-none d-sm-block">
                        <a href="<?php echo $back_url; ?>" class="btn btn-primary waves-effect waves-light"> <i class="far fa-arrow-alt-circle-left m-r-10"></i> <span>Back</span>  </a>
                        <?php if(!isset($hide_track)){ ?>
                        <button type="button" id="track-order" data-id ="<?php echo $order['id']; ?>" class="btn-edit btn btn-primary waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" ><i class="far fa-map m-r-10"></i><span>Track</span></button>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <?php if($order['alcoholic'] == 1 && $order['id_proof'] != null){ ?>
        <p>
          <u><a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Document Proof Of Customer
          </a></u>
        </p>

        <div class=" row collapse" id="collapseExample">
            <div class="col-12">
                
                <div class="card m-b-30">
                    <h4 class="card-header font-16 mt-0">Document Proof Of Customer</h4>
                    <div class="card-body">
                        <div class="">
                            <img class="img-thumbnail" alt="200x200" width="200" src="<?php echo BASE_URL().$this->config->item("user_document_path").$order['id_proof'];?>" height="50" data-holder-rendered="true">
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <?php }?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-12">
                                <div class="invoice-title">
                                    <h4 class="float-right font-16 mt-3"><strong>Order #<?php echo $order['id']; ?></strong></h4>
                                    <h3 class="mt-0">
                                        <img src="<?php echo base_url().'assets/images/logo_vector.png';?>" alt="" height="50">
                                    </h3>
                                </div>
                                <hr>
                                <div class="row">
                                    <?php if(!isset($hide_track)){ ?>
                                    <div class="col-12 text-center ">
                                        <?php
                                         // _pre($order);
                                        if($order['status'] == 1){
                                            echo $order_status = '<span class="badge badge-primary">Pending</span>';
                                        }else if($order['status'] == 2){
                                            echo $order_status = '<span class="badge badge-info">Accepted by store</span>';
                                        }else if($order['status'] == 3){
                                            echo $order_status = '<span class="badge badge-danger">Rejected by store</span>';
                                        }else if($order['status'] == 4){
                                            echo $order_status = '<span class="badge badge-danger">Cancelled</span>';
                                        }else if($order['status'] == 5){
                                            echo $order_status = '<span class="badge badge-info">Delivery Person Assigned</span>';
                                        }else if($order['status'] == 6){
                                            echo $order_status = '<span class="badge badge-info">Accepted by Delivery Person</span>';
                                        }else if($order['status'] == 7){
                                            echo $order_status = '<span class="badge badge-danger">Rejected by Delivery Person</span>';
                                        }else if($order['status'] == 8){
                                            echo $order_status = '<span class="badge badge-info">Reached at restaurant</span>';
                                        }else if($order['status'] == 9){
                                            echo $order_status = '<span class="badge badge-info">On the Way</span>';
                                        }else if($order['status'] == 10){
                                            echo $order_status = '<span class="badge badge-success">Delivered</span>';
                                        }else{
                                            echo $order_status ='';
                                        }
                                        ?>
                                    </div>
                                    <?php }?>
                                    <?php  if($order['status'] == 4){ ?>
                                        <div class="col-12 text-center ">
                                        <strong>Reason: </strong><?php echo $order['reason']; ?>
                                        </div>
                                    <?php }?>
                                    
                                    
                                    <div class="col-6">
                                        <address>
                                            <strong>Billed To:</strong><br>
                                            <?php echo $order['vendor']['name']; ?><br>
                                            <?php echo $order['vendor']['street']; ?>,<br>
                                            <?php echo $order['vendor']['city']; ?>,<br>
                                            <?php echo $order['vendor']['state']; ?>, <?php echo $order['vendor']['zipcode']; ?><br>
                                            <?php echo '+1 '.$order['user_vendor']['phone']; ?>
                                            <?php echo ($order['vendor']['phone_2'] != '')?'<br>'.'+1 '.$order['vendor']['phone_2']:''; ?>
                                        </address>
                                    </div>
                                    <div class="col-6 text-right">
                                        <address>
                                            <strong>Deliver To:</strong><br>
                                            <?php echo $order['user']['first_name'].' '.$order['user']['last_name']; ?><br>
                                        
                                            <?php echo $order['delivery_address'];?><br> 
                                            <?php echo$order['user']['email'];?><br> 
                                        
                                            <?php echo '+1 '.$order['user']['phone']; ?>
                                        </address>
                                    </div>
                                    <div class="col-6">
                                        <strong>Order Type:</strong>
                                        <?php
                                        
                                        if($order['order_type'] == 1){
                                            echo "Delivery".'<br>';
                                        }elseif($order['order_type'] == 2){
                                            echo "Takeout".'<br>';
                                        }elseif($order['order_type'] == 3){
                                            echo "Later Delivery".'<br>';
                                            $time = DateTime::createFromFormat('H:i:s',$order['later_time']);
                                            $later_time = $time->format('h:i A');
                                            echo "<strong>Later Time:</strong> ".$later_time.'<br>';
                                        }else{
                                            echo "Later Takeway".'<br>';
                                            $time = DateTime::createFromFormat('H:i:s',$order['later_time']);
                                            $later_time = $time->format('h:i A');
                                            echo "<strong>Later Time:</strong> ".$later_time.'<br>';
                                        }
                                        ?>
                                       
                                        <?php
                                        if(!isset($hide_track)){ 
                                        if(isset($order['delivery_boy']) && !empty($order['delivery_boy'])){
                                        ?>
                                            <br>
                                            <strong>Delivery Person:</strong><br>
                                            <?php
                                            echo $order['delivery_boy']['first_name'].' '.$order['delivery_boy']['last_name'];
                                            echo ' ( +1 '.$order['delivery_boy']['phone'].' )';
                                            echo "<br>";
                                            echo $order['delivery_boy']['email'];
                                        }}

                                        ?>
                                        

                                    </div>
                                    <?php if(!isset($hide_track)){ ?>
                                    <div class="col-6 text-right">
                                        <strong>Order Placed On:</strong><br>
                                        <?php
                                        $date = DateTime::createFromFormat('Y-m-d H:i:s',$order['created_at']);
                                        $created_at_date = $date->format('d M, Y');
                                        $created_at_time = $date->format('h:i A');
                                        echo $created_at_date.' '.'at'.' '.$created_at_time;
                                        ?>
                                        
                                    </div>
                                    <div class="col-6">
                                        <strong> Restaurant Amount:</strong><br>
                                        <?php echo "$".$order['store_amount'];?><br> 
                                        <strong> Admin Amount:</strong><br>
                                        <?php echo "$".$order['admin_amount'];?><br> 

                                    </div>
                                    <?php  if($order['status'] == 4){ ?>
                                    <div class="col-6 text-right">
                                        <strong> Refunded Amount:</strong><br>
                                        <?php echo "$".$order['total_amount'];?><br> 
                                    </div>
                                    <?php }?>
                                    <?php }?>
                                    <?php  if($order['special_instruction'] != null){ ?>
                                    <div class="col-12 ">
                                        <strong>Special Instruction:</strong><br>
                                        <?php echo $order['special_instruction'];?><br> 
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div>
                                    <div class="p-2">
                                        <h3 class="font-16"><strong>Order Summary</strong></h3>
                                    </div>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <td><strong>Product(s)</strong></td>
                                                    <td class="text-center"><strong>Price</strong></td>
                                                    <td class="text-center"><strong>Quantity</strong>
                                                    </td>
                                                    <td class="text-right"><strong>Total</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $sub_total = 0;
                                                foreach ($order_products as $key => $value) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        echo '<strong>'.$value['product_name'].'</strong>'.'<br>';

                                                        echo '<span class="varient-data">';
                                                        $name = '';
                                                        if(!empty($value['orderproductsaddon'])){
                                                            $name .= '( ';
                                                            foreach ($value['orderproductsaddon'] as $okey => $ovalue) {
                                                                if($okey == 0){
                                                                    $name .=  $ovalue['addon_name'] ;
                                                                }else{
                                                                    $name .=  ' , '.$ovalue['addon_name'] ;

                                                                }
                                                            }
                                                            $name .= ' )';
                                                        }
                                                        echo $name;
                                                        echo '</span>';
                                                        $product_total = $value['total_amount'] + $value['addon_total'];
                                                        ?>
                                                    </td>
                                                    <td class="text-center">$<?php echo number_format((float)$value['amount'], 2, '.', ''); ?></td>
                                                    <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                    <td class="text-right">$<?php echo number_format((float)$product_total, 2, '.', ''); ?></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                <tr>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line text-right">
                                                        <strong>Subtotal</strong></td>
                                                    <td class="thick-line text-right">$<?php echo number_format((float)$order['subtotal'], 2, '.', ''); ?></td>
                                                </tr>

                                                <?php 
                                                if($order['promo_id'] != '0'){ 
                                                    $promo_amount = $order['promo_amount'];
                                                    $sub_total = floatval($sub_total) - floatval($promo_amount);
                                                    $sub_total = number_format((float)$sub_total, 2, '.', '');
                                                ?>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>Promo - (<?php echo $order['promocode']['promocode']; ?>)</strong></td>
                                                    <td class="no-line text-right">-$<?php echo number_format((float)$promo_amount, 2, '.', ''); ?></td>
                                                </tr>
                                                <?php }else{
                                                    $promo_amount = 0;
                                                } ?>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>HST (<?php echo $order['service_charge']; ?>%)</strong></td>
                                                    <td class="no-line text-right">$<?php echo $order['service_charge_amount'] ; ?></td>
                                                </tr>

                                                <?php if($order['order_type'] == 1 || $order['order_type'] == 3){?>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>Delivery Fee</strong></td>
                                                    <td class="no-line text-right">$<?php echo number_format((float)$order['delivery_fee'], 2, '.', ''); ?></td>
                                                </tr>
                                            <?php }?>

                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>Total</strong></td>
                                                    <td class="no-line text-right"><h4 class="m-0">$<?php echo number_format((float)$order['total_amount'], 2, '.', ''); ?></h4></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="d-print-none">
                                            <div class="float-right">
                                                <a href="javascript:window.print()" class="btn btn-primary waves-effect"><i class="fa fa-print"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div>
                </div>
            </div> <!-- end col -->
        </div>     

         <div class="modal fade bs-example-modal-lg" id="trackmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content track">
              <div class="modal-header bg-primary">
                <h6 class="modal-title text-center m-0 font-weight-bold text-white">Track Order #<span id="order-id"></span></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               <h4 class="text-center">Tracking data fetching..</h4>
              </div>
              <div class="text-center text-white bg-primary py-2">
                <h6 class="text-uppercase font-weight-bold"><span id="order-final-status"></span></h6>
                <div class="pb-2"><span id="order-final-text"></span></div>
              </div>
            </div>
          </div>
        </div>   

        


    </div> <!-- container-fluid -->

</div> <!-- content -->

<script src="<?php echo base_url('/assets/js/custom/admin/track_order.js');?>" type="text/javascript"></script>