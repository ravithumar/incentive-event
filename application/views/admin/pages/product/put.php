<?php
$put_link = base_url('admin/product/edit/').$product_data['id'];
$back = base_url('admin/products');
$image  = BASE_URL().'assets/images/default.png';
$product_data = $product_data->toArray();
$product_style = array_column($product_data['style'], 'style_id');
// echo "<pre>";
// print_r($product_data);
// print_r($product_style);
// print_r($styles);
// print_r($category);
// exit();
?>
<style type="text/css" media="screen">
 #gallary img{
    height: 350px;
    width: auto;
    object-fit: cover;
 }  
 #gallary i{
    cursor: pointer;
 }     
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                   <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <form class="form-validate"  method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Title</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('title');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['title']);
                                            }
                                            ?>
                                            <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('title'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label >Added By</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('user_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = $product_data['user']['first_name'].' '.$product_data['user']['last_name'];
                                            }
                                            ?>
                                            <input type="text" name="user_name" disabled class="form-control" id="user_name" value="<?php echo $field_value; ?>" >
                                            <div class="validation-error-label">
                                                <?php echo form_error('user_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="form-group">
                                <label class="required">Description</label>
                                <div>
                                <?php
                                    $field_value = NULL;
                                    $temp_value = set_value('description');
                                    if (isset($temp_value) && !empty($temp_value)) {
                                        $field_value = $temp_value;
                                    } else{
                                        $field_value = ($product_data['description']);
                                    }
                                    ?>
                                    <textarea name="description" class="form-control" id="description" required><?php echo $field_value; ?></textarea>
                                    <div class="validation-error-label">
                                        <?php echo form_error('description'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label required">Style</label>
                                        <select id="style_id" name="style_id[]" class="form-control select2" multiple  required data-parsley-errors-container="#style_id_error" data-placeholder="Select Styles">
                                            <option value="" disabled > </option>
                                           <?php
                                            $field_value = NULL;

                                            $temp_value = set_value('style_id');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }

                                            if(isset($styles) && !empty($styles)){
                                                foreach ($styles as $key => $value) {
                                                    if(in_array($value['id'], $product_style)){
                                                        $selected = 'selected';
                                                    }else{
                                                         $selected= '';
                                                    }
                                                ?>
                                                <option  value="<?php echo $value['id'];?>" <?php echo $selected; ?> > <?php echo ($value['name']);?> </option>
                                                <?php
                                                }
                                                }
                                                ?>
                                             
                                             <div class="validation-error-label">
                                                <?php echo form_error('style_id'); ?>
                                            </div>
                                        </select>
                                        <div id="style_id_error"></div>
                                    </div> 
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label required">Category</label>
                                        <select id="category_id" name="category_id" required class="form-control select2" data-parsley-errors-container="#category_id_error" data-placeholder="Select Category">
                                            <option value="" disabled > </option>
                                           <?php
                                            $field_value = NULL;

                                            $temp_value = set_value('category_id');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                if(!empty($user_category)){
                                                    $field_value =$product_data['category_id'];
                                                }
                                            }

                                            if(isset($category) && !empty($category)){
                                                foreach ($category as $key => $value) {
                                                    if($value['id'] == $field_value){
                                                        $selected = 'selected';
                                                    }else{
                                                         $selected= '';
                                                    }
                                                ?>
                                                <option  value="<?php echo $value['id'];?>" <?php echo $selected; ?> > <?php echo ($value['name']);?> </option>
                                                <?php
                                                }
                                                }
                                                ?>
                                             
                                             <div class="validation-error-label">
                                                <?php echo form_error('category_id'); ?>
                                            </div>
                                        </select>
                                        <div id="category_id_error"></div>
                                    </div> 
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Brand</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('brand');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['brand']);
                                            }
                                            ?>
                                            <input type="text" name="brand" class="form-control" id="brand" placeholder="Brand" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('brand'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Size</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('size');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['size']);
                                            }
                                            ?>
                                            <input type="text" name="size" class="form-control" id="size" placeholder="Size" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('size'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Color</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('color');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['color']);
                                            }
                                            ?>
                                            <input type="text" name="color" class="form-control" id="color" placeholder="Color" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('color'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Retail Value</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('retail_value');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['retail_value']);
                                            }
                                            ?>
                                            <input type="text" name="retail_value" class="form-control demo1" id="retail_value" placeholder="Retail Value" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('retail_value'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Daily Rental Price</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('daily_rental_price');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['daily_rental_price']);
                                            }
                                            ?>
                                            <input type="text" name="daily_rental_price" class="form-control demo1" id="daily_rental_price" placeholder="Daily Rental Price" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('daily_rental_price'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Minimum Rental Days</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('minimum_rental_days');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['minimum_rental_days']);
                                            }
                                            ?>
                                            <input type="text" name="minimum_rental_days" class="form-control demo2" id="minimum_rental_days" placeholder="Minimum Rental Days" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('minimum_rental_days'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Weekly Rental Price</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('weekly_rental_price');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['weekly_rental_price']);
                                            }
                                            ?>
                                            <input type="text" name="weekly_rental_price" class="form-control demo1" id="weekly_rental_price" placeholder="Weekly Rental Price" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('weekly_rental_price'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="">Cleaning Fee</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('cleaning_fee');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['cleaning_fee']);
                                            }
                                            ?>
                                            <input type="text" name="cleaning_fee" class="form-control demo1" id="cleaning_fee" placeholder="Cleaning Fee" value="<?php echo $field_value; ?>" >
                                            <div class="validation-error-label">
                                                <?php echo form_error('cleaning_fee'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Delivery Option</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('delivery_option');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = explode(',',$product_data['delivery_option']);
                                            }
                                            ?>
                                            <select id="delivery_option" name="delivery_option[]" class="form-control select2" multiple required data-parsley-errors-container="#delivery_option_error" data-placeholder="Select Delivery Option">
                                                <option value="" disabled > </option>
                                                <option value="1" <?php if(in_array(1, $field_value)){echo 'selected';}?>> Meet-Up </option>
                                                <option value="2" <?php if(in_array(2, $field_value)){echo 'selected';}?>> Postage </option>
                                            </select>
                                           
                                            <div class="validation-error-label">
                                                <?php echo form_error('delivery_option'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Postage Fee</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('postage_fee');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = ($product_data['postage_fee']);
                                            }
                                            ?>
                                            <input type="text" name="postage_fee" class="form-control demo1" id="postage_fee" placeholder="Postage Fee" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('postage_fee'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Condition Product</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('condition_product');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } else{
                                                $field_value = $product_data['condition_product'];
                                            }
                                            ?>
                                            <select id="condition_product" name="condition_product" class="form-control select2" required data-parsley-errors-container="#condition_product_error" data-placeholder="Select Condition Product">
                                                <option value="" disabled > </option>
                                                <option value="1" <?php if($field_value == 1){echo 'selected';}?>> Excellent </option>
                                                <option value="2" <?php if($field_value == 2){echo 'selected';}?>> Very Good </option>
                                                <option value="3" <?php if($field_value == 3){echo 'selected';}?>> Good </option>
                                                <option value="4" <?php if($field_value == 4){echo 'selected';}?>> Fair </option>
                                            </select>
                                           
                                            <div class="validation-error-label">
                                                <?php echo form_error('condition_product'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="from_date">From Date</label>
                                        <div>
                                            <div class="input-group">
                                                <?php
                                                    $field_value = NULL;
                                                    $temp_value = set_value('from_date');
                                                    if (isset($temp_value) && !empty($temp_value)) {
                                                        $field_value = $temp_value;
                                                    }else{
                                                        if($product_data['from_date'] != '0000-00-00') {
                                                        $date = DateTime::createFromFormat('Y-m-d',  $product_data['from_date']);
                                                        $field_value = $date->format('d F, Y');
                                                        }
                                                    }
                                                    ?>
                                                <input required type="text" class="form-control datepicker-autoclose" placeholder="Ex. 10 September , 2020" name="from_date" id="from_date" data-parsley-errors-container="#error_from" autocomplete="off" value="<?php echo $field_value; ?>" readonly> 
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('from_date'); ?>
                                            </div>
                                            <div id="error_from"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="to_date">To Date</label>
                                        <div>
                                            <div class="input-group">
                                                <?php
                                                    $field_value = NULL;
                                                    $temp_value = set_value('to_date');
                                                    if (isset($temp_value) && !empty($temp_value)) {
                                                        $field_value = $temp_value;
                                                    }else{
                                                        if($product_data['to_date'] != '0000-00-00') {
                                                        $date = DateTime::createFromFormat('Y-m-d',  $product_data['to_date']);
                                                        $field_value = $date->format('d F, Y');
                                                        }
                                                    }
                                                    ?>
   
                                                <input required type="text" class="form-control datepicker-autoclose" placeholder="Ex. 23 September , 2020" name="to_date" id="to_date" data-parsley-errors-container="#error_to" autocomplete="off" value="<?php echo $field_value; ?>" disabled readonly>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('to_date'); ?>
                                            </div>
                                            <div id="error_to"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="form-group ">
                               
                                <label class="">Image</label>
                                <input type="file" accept="image/*" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png" onchange="readURL(this);" id="picture" name="picture"  class="form-control upload" />
                            </div>
                            <div class="form-group">
                                 <img class="border rounded p-0"  src="<?php echo base_url($product_data['image']);?>" onerror="this.src='<?php echo $image; ?>'" alt="your image" style="height: 130px;width: 130px" id="blah"/>
                            </div> -->

                            <input type="hidden" name="id" value="<?php echo $product_data['id'] ?>">
                            <!-- <input type="hidden" name="old_img" value="<?php echo $product_data['image'] ?>"> -->

                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> 
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo $add_url; ?>" class="btn btn-primary waves-effect waves-light float-right">
                            <i class="ion-plus mr-1"></i> Add Image
                        </a>
                    <h4 class="page-title">Post Image's</h4>
                </div>
            </div>
        </div>

        <div class="row pt-2" id="gallary">
            <?php foreach ($product_img as $key => $value) { ?>
            <div class="col-md-6 col-lg-6 col-xl-3 delete-row" id = "<?php echo $value['id']; ?>">
                <div class="card m-b-30 border">
                    <img class="card-img-top img-fluid" src="<?php echo base_url().$value['large_image']; ?>" alt="Card image cap">
                    <div class="card-body text-center">
                        <a data-id="<?php echo $value['id']; ?>" data-popup="tooltip" onclick="delete_image_notiflix(this);"><i class="fas fa-trash text-danger fa-2x"></i></a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>

   </div>
</div>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script type="text/javascript">
    function delete_image_notiflix(e)
    {
      var id = $(e).data('id');
      var div = $(e).closest('.delete-row');

      Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to delete this image?',
        'Yes',
        'No',
        function(){
          Notiflix.Loading.Standard();
          $.ajax({
            url: BASE_URL+'admin/img_delete',
            type: "POST",
            data:{
              id:id
            },
            success: function (returnData) {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Deleted');
                div.remove();
            }
          }); 
      });

    }

    $(".demo1").TouchSpin({
        initval: 0.00,
        min: 0,
        max: 1000000,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'btn btn-primary',
        buttonup_class: 'btn btn-primary'
    });

    $(".demo2").TouchSpin({
        initval: 00,
        min: 0,
        max: 1000000,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: 'Days',
        buttondown_class: 'btn btn-primary',
        buttonup_class: 'btn btn-primary'
    });

    $(".select2").select2();
    $(document).on('click', '.upload', function(){
        $('#blah').attr('src', '<?php echo BASE_URL()."assets/images/default.png"; ?>');
    });
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
       }
    $("#from_date").datepicker({
        todayBtn:  1,
        autoclose: true,
        format: 'dd MM, yyyy',
        startDate: '-0d',   
    }).on('changeDate', function (selected) {
        $("#to_date").removeAttr('disabled');
        var minDate = new Date(selected.date.valueOf());
        $('#to_date').datepicker('setStartDate', minDate);
    });

    $("#to_date").datepicker({
        autoclose: true,
        format: 'dd MM, yyyy',
    }).on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            $('#from_date').datepicker('setEndDate', maxDate);
    });
</script>

    

        