<?php
$back = base_url('admin/product');
$image  = BASE_URL().'assets/images/default.png';
?>
<style type="text/css" media="screen">
    .img-fluid {
        width: 100%;
        height: 250px;
        object-fit: contain;
    }
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo base_url('admin/product'); ?>" class="ml-2 btn btn-dark waves-effect waves-light float-right">
                        <i class="far fa-arrow-alt-circle-left m-r-10 "></i> Back 
                    </a>
                    <h4 class="page-title"><?php echo $title; ?></h4>
                   <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('admin/includes/message');
        /*echo "<pre>";
        print_r($product);
        exit();*/
        ?>
        <div class="row">
            <div class="col-3">
                <div class="card m-b-30" id="cards">
                    <h4 class="card-header font-16 mt-0">Image</h4>
                    <div class="card-body text-center"> 
                        <a class="image-popup-no-margins" href="<?php echo base_url().$product['image'];?>">
                            <img class="img-fluid border" src="<?php echo base_url().$product['image']; ?>" alt="Profile Picture">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-5">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">
                            <?php
                            echo $product['title'];
                            ?>
                        </h4>
                        <hr>
                        <p class="text-muted m-b-30"><?php echo $product['description'];?></p>
                        <dl class="row">
                            <dt class="col-sm-3">Restaurant Name</dt>
                            <dd class="col-sm-9"><u><a href="<?php echo base_url('admin/vendor/view/').$product['user_id'] ;?>" target="_blank"><?php echo $product['vendor']['name'];?></a></u></dd>

                            <dt class="col-sm-3">Category</dt>
                            <dd class="col-sm-9"><?php echo $product['category']['name'];?></dd>

                            <dt class="col-sm-3">Variants</dt>
                            <dd class="col-sm-9"><?php foreach ($product['variant'] as $key => $value) {
                                echo '<span class="badge badge-dark p-1 mr-1">'.$value['variant_name'].'</span>';
                                }?>
                            </dd>

                            <dt class="col-sm-3">Type</dt>
                            <dd class="col-sm-9"><?php if($product['type']  == 1) {
                                    echo '<span class="badge badge-dark p-1 mr-1">Alcoholic</span>';
                                }else{
                                    echo '<span class="badge badge-dark p-1 mr-1">Non-Alcoholic</span>';  
                                }?>
                            </dd>

                            <dt class="col-sm-3">Price</dt>
                            <dd class="col-sm-9"><?php echo  '$ '.$product['price'];?></dd>

                            <dt class="col-sm-3">Inventory Status</dt>
                            <dd class="col-sm-9"><?php if($product['inventory_status'] == 1) { echo "On"; }else{ echo "Off"; }?></dd>

                            <?php 
                            if($product['stock'] > 0) {
                            ?>
                            <dt class="col-sm-3">Stock</dt>
                            <dd class="col-sm-9"><?php echo $product['stock'];?></dd>
                            <?php }?>
                            

                            <dt class="col-sm-3">Added At</dt>
                            <dd class="col-sm-9"><?php echo $product['created_at_formatted'];?></dd>

                            <?php 
                            if($product['type'] == 1) {
                            ?>
                            <dt class="col-sm-3">Type</dt>
                            <dd class="col-sm-9"><?php echo "Alcoholic";?></dd>
                            <?php }?>

                        </dl>
        
                        <hr>

                       
                    </div>
                </div>
            </div> 
            

            
            <div class="col-4">
                 <div class="card m-b-20">
                    <div class="card-body">
                        <h4 class="mt-0 header-title"> Add-On </h4>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(isset($product['addon']) && !empty($product['addon'])){
                            ?>
                            <?php foreach ($product['addon'] as $key => $value) {?>
                                <tr>
                                    <td><?php echo $value['name'];?></td>
                                    <td><?php echo  '$ '.$value['price'];?></td>
                                </tr>
                            <?php }?>
                             <?php }else{?>
                                <tr>
                                    <td>No any Add-on found!</td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
           
        </div>

   </div>
</div>     
<script src="<?php echo assets('pages/lightbox.js');?>"></script>  