<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <!-- <li class="menu-title">Main Menu</li> -->
                <li class="mt-2">
                    <a href="<?php echo base_url('admin/dashboard'); ?>" class="waves-effect">
                        <i class="mdi mdi-view-dashboard"></i><span> Dashboard </span>
                    </a>
                </li>
                <li class="mt-2">
                    <a href="<?php echo base_url('admin/users'); ?>" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Users </span></a>
                </li>
                <li class="mt-2">
                    <a href="<?php echo base_url('admin/faq'); ?>" class="waves-effect"><i class="mdi mdi-help-circle"></i><span>FAQ </span></a>
                </li>

                <li class="mt-2">
                    <a href="<?php echo base_url('admin/contact-us'); ?>" class="waves-effect"><i class="mdi mdi-message-text"></i><span> Contact us </span></a>
                </li>
                <li class="mt-2">
                    <a href="<?php echo base_url('admin/setting'); ?>" class="waves-effect"><i class="mdi mdi-settings"></i><span> Settings </span></a>
                </li> 

            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->