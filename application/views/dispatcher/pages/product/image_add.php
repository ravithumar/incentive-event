<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-box">
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                    echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-12">
                <?php
                $this->load->view('admin/includes/message');
                ?>
            </div>
        </div>

        <form class="form-validate" method="post" action="<?php echo $post_link; ?>" enctype="multipart/form-data">

            <div class="row">

                <div class="col-lg-12">
                    <div class="card m-b-20">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                    <?php 
                                        $image  = BASE_URL().'assets/images/default.png';
                                        
                                    ?> 
                                    <label class="required">Images </label>
                                      <input type="file" data-parsley-file-mime-types="image/*" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" id="images" name="images[]"  class="form-control" required multiple />
                                      <div class="validation-error-label">
                                            <?php echo form_error('images'); ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-lg-12"  id="myImg">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-b-0">
                                        <div>
                                            <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit 
                                            </button>
                                            <a href="<?php echo $back_url; ?>" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> 

            </div> <!-- end row -->
        </form>

    </div>
</div>

<script type="text/javascript">
   //  function readURL(input) {
   //  if (input.files && input.files[0]) {
   //      var reader = new FileReader();

   //      reader.onload = function (e) {
   //          $('#blah').attr('src', e.target.result);
   //      };
   //      reader.readAsDataURL(input.files[0]);
   //  }
   // }
   $(function() {
      $(":file").change(function() {
        if (this.files && this.files[0]) {
          for (var i = 0; i < this.files.length; i++) {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[i]);
          }
        }
      });
    });

    function imageIsLoaded(e) {
      $('#myImg').append('<img class="border rounded p-1 mr-2 mb-1" src=' + e.target.result + ' style="height: 130px;width: 130px" >');
    };
</script>