<?php
$post_link = base_url('vendor/variant/add');
$back = base_url('vendor/variant');
?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                       <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                       <form class="form-validate"  method="post" action="<?php echo $post_link; ?>" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="required">Name</label>
                                <div>
                                <?php
                                    $field_value = NULL;
                                    $temp_value = set_value('name');
                                    if (isset($temp_value) && !empty($temp_value)) {
                                        $field_value = $temp_value;
                                    } 
                                ?>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Ex. Tops" value="<?php echo $field_value; ?>" required>
                                    <div class="validation-error-label">
                                        <?php echo form_error('name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Submit
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
</div>     