<style type="text/css" media="screen">
    .track .dot {
        height: 25px;
        width: 25px;
        background-color: #894c48;
        border-radius: 50%;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0 auto;
    }
    .track .not-first .dot:before {
        content: '';
        width: 0;
        height: 200%;
        position: absolute;
        border: 1px solid #894c48;
        bottom: 25px;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">  
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo $back_url; ?>" class="btn btn-primary waves-effect waves-light float-right">
                           <i class="far fa-arrow-alt-circle-left m-r-10"></i>Back 
                    </a>
                    <button type="button" id="track-order" data-id ="<?php echo $order_details['id']; ?>" class="btn-edit btn btn-primary waves-effect waves-light float-right mr-2"  data-toggle="modal" data-target="#trackmodal" ><i class="far fa-map m-r-10"></i><span>Track</span></button>
                    <h4 class="page-title">Order Detail</h4>
                    <?php
                        echo $this->breadcrumbs->show();
                     ?>
                </div>
            </div>
        </div>
        <!-- end row -->
<?php
$order_details = $order_details->toArray(); 
$order_products = $order_products->toArray(); 
/*echo "<pre>";
print_r ($order_details);
print_r ($order_products);
echo "</pre>";
exit();*/
?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="invoice-title">
                                    <h4 class="float-right font-16 mt-3"><strong>Order #<?php echo $order_details['id']; ?></strong></h4>
                                    <h3 class="mt-0">
                                        <img src="<?php echo base_url().'assets/images/logo_vector.png';?>" alt="" height="50">
                                    </h3>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-12 text-center ">
                                        <?php 
                                        if($order_details['status'] == 1){
                                            $today_obj = new DateTime('now');
                                            $today = $today_obj->format('Ymd');
                                            $from_date_obj = DateTime::createFromFormat('Y-m-d', $order_details['from_date']);
                                            $from_date = $from_date_obj->format('Ymd');
                                            $to_date_obj = DateTime::createFromFormat('Y-m-d', $order_details['to_date']);
                                            $to_date = $to_date_obj->format('Ymd');
                                            if($today >= $from_date && $today <= $to_date){
                                                echo '<span class="badge badge-warning">'.$order_status['on_rent'].'</span>';
                                            }else{
                                                echo '<span class="badge badge-success">'.$order_status['paid'].'</span>';
                                            }
                                        }else if($order_details['status'] == 2){
                                            echo '<span class="badge badge-info">'.$order_status['receive'].'</span>';
                                        }else if($order_details['status'] == 3){
                                            echo '<span class="badge badge-info">'.$order_status['completed'].'</span>';
                                        }else if($order_details['status'] == 4){
                                            echo '<span class="badge badge-success">'.$order_status['confirmed_completed'].'</span>';
                                        }else if($order_details['status'] == 5){
                                            echo '<span class="badge badge-danger">'.$order_status['fail'].'</span>';
                                        }else if($order_details['status']  == 6){
                                            echo '<span class="badge badge-danger">'.$order_status['cancel'].'</span>';
                                        }else{
                                            echo ' ';
                                        }?>
                                    </div>
                                    
                                    <div class="col-12">
                                        <address>
                                            <strong>Admin Earn:</strong><br>
                                            <?php echo '$ '.number_format((float)$order_details['admin_amount'], 2, '.', ''); ?><br>
                                        </address>
                                    </div>
                                    <div class="col-12">
                                        <address>
                                            <strong>Requested On:</strong><br>
                                           <?php
                                            if(isset($request_data['request']) && !empty($request_data['request'])){
                                                $date = DateTime::createFromFormat('Y-m-d H:i:s',$request_data['request']['created_at']);
                                                $created_at_date = $date->format('d M, Y');
                                                $created_at_time = $date->format('h:i A');
                                                echo $created_at_date.' '.'at'.' '.$created_at_time;
                                            }else{
                                                echo "-";
                                            }
                                            ?>
                                            <br>
                                           
                                        </address>
                                    </div>
                                    <div class="col-12">
                                        <address>
                                            <strong>Accepted On:</strong><br>
                                            <?php
                                            if(isset($request_data['request']) && !empty($request_data['request'])){
                                                $date = DateTime::createFromFormat('Y-m-d H:i:s',$request_data['request']['accepted_at']);
                                                $created_at_date = $date->format('d M, Y');
                                                $created_at_time = $date->format('h:i A');
                                                echo $created_at_date.' '.'at'.' '.$created_at_time;
                                            }else{
                                                echo "-";
                                            }
                                            ?>
                                            <br>
                                        </address>
                                    </div>
                                    <div class="col-6">
                                        <address>
                                            <strong>Billed To:</strong><br>
                                            <?php
                                            if(isset($order_details['seller']) && !empty($order_details['seller'])){
                                                echo '<a target="_blank" href="'.base_url('admin/user/view/').$order_details['seller_id'].'">'.$order_details['seller']['first_name']." ".$order_details['seller']['last_name'].'</a>';
                                            }else{
                                                echo "-";
                                            }
                                            ?>
                                            <br>
                                        </address>
                                    </div>
                                    <div class="col-6 text-right">
                                        <address>
                                            <strong>Deliver To:</strong><br>
                                                <?php
                                                if(isset($order_details['user']) && !empty($order_details['user'])){
                                                    echo '<a target="_blank" href="'.base_url('admin/user/view/').$order_details['user_id'].'">'.$order_details['user']['first_name']." ".$order_details['user']['last_name'].'</a>';
                                                }else{
                                                    echo "-";
                                                }
                                                ?><br>
                                        </address>
                                    </div>
                                    <div class="col-6">
                                        <strong>Order For:</strong>
                                        <?php
                                        
                                        if($order_details['delivery_type'] == 1){
                                            echo "Meetup".'<br>';
                                        }else{
                                            echo "Postage".'<br>';
                                        }?>
                                        
                                    </div>
                                     <div class="col-12">
                                        <strong>From date:</strong>
                                        <?php
                                        $date = DateTime::createFromFormat('Y-m-d',$order_details['from_date']);
                                        $from_date = $date->format('d M, Y');
                                        echo $from_date;
                                        ?>
                                        
                                    </div>
                                    <div class="col-12">
                                        <strong>To date:</strong>
                                        <?php
                                        $date = DateTime::createFromFormat('Y-m-d',$order_details['to_date']);
                                        $to_date = $date->format('d M, Y');
                                        echo $to_date;
                                        ?>
                                        
                                    </div>
                                     <div class="col-6">
                                        <strong>Total Days:</strong>
                                        <?php
                                        $earlier = new DateTime($order_details['from_date']);
                                        $later = new DateTime($order_details['to_date']);
                                        $diff = $later->diff($earlier)->format("%a");
                                        $numberDays =  $diff + 1;
                                        echo $numberDays;
                                        ?>
                                        
                                    </div>
                                    <div class="col-6 text-right">
                                        <strong>Order Placed On:</strong><br>
                                        <?php
                                        $date = DateTime::createFromFormat('Y-m-d H:i:s',$order_details['created_at']);
                                        $created_at_date = $date->format('d M, Y');
                                        $created_at_time = $date->format('h:i A');
                                        echo $created_at_date.' '.'at'.' '.$created_at_time;
                                        ?>
                                        
                                    </div>
                                    <div class="col-6">
                                    <?php if($order_details['product_rating'] != '' ) { ?>
                                        <address>
                                            <strong>Product Rate:</strong>
                                            <?php echo $order_details['product_rating']; ?><br>
                                            <strong>Product Review:</strong>
                                            <?php echo $order_details['product_review']; ?><br>
                                        </address>
                                    <?php }?>
                                    </div>
                                    <div class="col-6 text-right">
                                    <?php if($order_details['seller_rating'] != '' ) { ?>
                                        <address>
                                            <strong>Renter Rate:</strong>
                                            <?php echo $order_details['seller_rating']; ?><br>
                                            <strong>Renter Review:</strong>
                                            <?php echo $order_details['seller_review']; ?><br>
                                        </address>
                                    <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div>
                                    <div class="p-2">
                                        <h3 class="font-16"><strong>Order Summary</strong></h3>
                                    </div>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <td><strong>Product</strong></td>
                                                    <td class="text-center"><strong>Daily Rental Price</strong></td>
                                                    <td class="text-center"><strong>Day(s)</strong></td>
                                                    <td class="text-right"><strong>Total</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $sub_total = 0;
                                                foreach ($order_products as $key => $value) {
                                                  
                                                ?>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url().$value['cover_img']['image'];?>" height="70" width="70" style="object-fit: cover;" class="border rounded">
                                                        <?php
                                                        if(isset($value['product']) && !empty($value['product'])){
                                                            echo '<strong>'.'<a target="_blank" href="'.base_url('admin/product/view/').$value['product_id'].'">'.$value['product']['title'].'</a>'.'</strong>';
                                                        }else{
                                                            echo "-";
                                                        }
                                                        
                                                        echo '<span class="varient-data">';
                                                        echo '('.$value['size'].')' ;
                                                        echo '</span>';
                                                        ?>
                                                    </td>
                                                    <td class="text-center">$<?php echo number_format((float)$value['daily_rental_price'], 2, '.', ''); ?></td>
                                                    <td class="text-center"><?php echo $numberDays; ?></td>
                                                    <td class="text-right">$<?php echo number_format((float)$value['product_price'], 2, '.', ''); ?></td>
                                                </tr>
                                               
                                                <!-- <tr>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line text-right">
                                                        <strong>Subtotal</strong></td>
                                                    <td class="thick-line text-right">$<?php echo number_format((float)$value['product_price'], 2, '.', ''); ?></td>
                                                </tr> -->
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>Service Charge (<?php echo $service_charge; ?>%)</strong></td>
                                                    <td class="no-line text-right">$<?php echo $value['service_charge'] ; ?></td>
                                                </tr>
                                                <?php if($value['cleaning_fee'] != 0){?> 
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>Cleaning Fee</strong></td>
                                                    <td class="no-line text-right">$<?php echo $value['cleaning_fee'] ; ?></td>
                                                </tr>
                                                <?php }?>
                                                <?php if($order_details['delivery_type'] == 2){?>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>Delivery Fee</strong></td>
                                                    <td class="no-line text-right">$<?php echo number_format((float)$value['delivery_charge'], 2, '.', ''); ?></td>
                                                </tr>
                                                <?php }?>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right">
                                                        <strong>Total</strong></td>
                                                    <td class="no-line text-right"><h4 class="m-0">$<?php echo number_format((float)$value['total_amount'], 2, '.', ''); ?></h4></td>
                                                </tr>
                                                 <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="d-print-none">
                                            <div class="float-right">
                                                <a href="javascript:window.print()" class="btn btn-primary waves-effect"><i class="fa fa-print"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end row -->
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
  
      
        <div class="modal fade bs-example-modal-lg" id="trackmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content track">
              <div class="modal-header bg-primary">
                <h6 class="modal-title text-center m-0 font-weight-bold text-white">Track Order #<span id="order-id"></span></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               <h4 class="text-center">Tracking data fetching..</h4>
              </div>
              <div class="text-center text-white bg-primary py-2">
                <h6 class="text-uppercase font-weight-bold"><span id="order-final-status"></span></h6>
                <div class="pb-2"><span id="order-final-text"></span></div>
              </div>
            </div>
          </div>
        </div>  
           
    </div> <!-- container-fluid -->
</div> <!-- content -->

<script type="text/javascript">
    
    $(document).on('click',"#track-order", function(){
        Notiflix.Loading.Standard();
        var track_order_id = $(this).attr("data-id");
        //console.log(track_order_id);
        $('.track #order-id').html(track_order_id);
        $.ajax({
                url: BASE_URL+'admin/order/track',
                type: "POST",
                data:{order_id:track_order_id},
                success: function (returnData) {
                    returnData = jQuery.parseJSON(returnData);
                    console.log(returnData);
                    //return false;
                    $(".overlay").css("display", "none");

                    var str = '';
                    if (typeof returnData.data != "undefined" && returnData.data.length > 0){

                        var final_status = 'Pending';
                        var final_status_text = 'Order pending';

                        $.each(returnData.data, function (key, val){

                            if(key != 0){
                                var extra_class = ' not-first mt-3';
                            }else{
                                var extra_class = '';
                            } 

                            if(val.status == 'request'){
                                final_status_text = 'Request for a product';
                                final_status = val.status_title;
                            }else if(val.status == 'request_accepted'){
                                final_status_text = 'Request accepted for a product';
                                final_status = val.status_title;
                            }else if(val.status == 'request_rejected'){
                                final_status_text = 'Request rejected for a product';
                                final_status = val.status_title;
                            }else if(val.status == 'request_expired'){
                                final_status_text = 'Request expired for a product';
                                final_status = val.status_title;
                            }else if(val.status == 'request_cancelled'){
                                final_status_text = 'Request Canceled for a product';
                                final_status = val.status_title;
                            }else if(val.status == 'add_to_cart'){
                                final_status_text = 'Product added to cart';
                                final_status = val.status_title;
                            }else if(val.status == 'order_placed'){
                                final_status_text = 'Order has been placed';
                                final_status = val.status_title;
                            }else if(val.status == 'order_cancelled'){
                                final_status = 'Canceled';
                                final_status_text = 'Order has been canceled';

                            }else if(val.status == 'return_complete'){
                                final_status_text = 'Order completed and initiate to return';
                                final_status = val.status_title;
                            }else if(val.status == 'complete_confirmed'){
                                final_status = 'Fulfilled';
                                final_status_text = 'Order Fulfilled';
                            }else if(val.status == 'fail'){
                                final_status = 'Failed';
                                final_status_text = 'Order has been failed';
                            }else if(val.status == 'delete_cart'){
                                final_status_text = 'Product deleted from cart';
                                final_status = val.status_title;
                            }else{
                                final_status_text = 'Order Pending';
                            }
                            str += '<div class="row align-items-center '+extra_class+'">'
                                        +'<div class="col-md-2 text-left text-md-right status-time">'
                                          +val.created_at+'<br>'
                                        +'</div>'
                                        +'<div class="col-md-2">'
                                          +'<span class="dot"></span>'
                                        +'</div>'
                                        +'<div class="col-md-7 status">'
                                          +'<strong>'
                                            +val.status_title
                                          +'</strong>'
                                          +'<div class="text-muted">'
                                            +val.status_text
                                          +'</div>'
                                        +'</div>'
                                    +'</div>';

                        });

                        $('#trackmodal .modal-body').html(str);


                        $('#trackmodal #order-final-status').html(final_status);
                        $('#trackmodal #order-final-text').html(final_status_text);
                         Notiflix.Loading.Remove();
                    }else{

                        str += '<h4 class="text-center">Tracking Under Development</h4>';
                        $('#trackmodal .modal-body').html(str);

                        $('#trackmodal #order-final-status').html('');
                        $('#trackmodal #order-final-text').html('');
                        Notiflix.Loading.Remove();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });   
    });


</script>
