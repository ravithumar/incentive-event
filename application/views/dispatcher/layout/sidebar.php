<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Main Menu</li>
                <li>
                    <a href="<?php echo base_url('dispatcher/dashboard'); ?>" class="waves-effect">
                        <i class="mdi mdi-view-dashboard"></i><span> Dashboard </span>
                    </a>
                </li>

                <!-- <li>
                    <a href="<?php// echo base_url('dispatcher/user'); ?>" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Delivery Boy </span></a>
                </li> -->

            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->