
        <footer class="footer">
        Copyright &copy; <?php echo config('site_title')." ".date('Y'); ?>. <!-- <span class="d-none d-sm-inline-block"> Crafted with <i class="mdi mdi-heart text-danger"></i> by Excellent WebWorld</span>. -->
        </footer>
    
        </div>
</div>
        <!-- END wrapper -->

</body>

</html>

<?php
if(isset($datatable) && $datatable)
{
    $this->load->view('vendor/includes/datatable');
}
?>


<!-- App js -->
<script src="<?php echo assets('js/app.js'); ?>"></script>
<script type="text/javascript">
    $('form').parsley();
    var path = window.location.pathname;
    $(".metismenu li a[href*='"+path.split('/')[1]+'/'+path.split('/')[2] +"']").addClass("active").closest('li').addClass("active").closest('ul').addClass('in');
    
</script>
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
    var add_channel_url = "<?php echo base_url().'add-channel'; ?>";    
    console.log('add_channel_url:'+add_channel_url);
    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
        OneSignal.init({
            appId: "fe939676-d326-4f0a-9cbb-15b6f5bc2f77",
        });
        // OneSignal.showNativePrompt();
  });

  OneSignal.push(function() {
        // alert('here');
        OneSignal.on('subscriptionChange', function(isSubscribed) {
            // alert('here123');
            console.log('isSubscribed:'+isSubscribed);            
            if (isSubscribed === true) {

                console.log('The user subscription state is now:', isSubscribed);

                OneSignal.getUserId( function(userId) {
                    // Make a POST call to your server with the user ID
                    console.log('The player id is :', userId);                    

                    $.ajax({
                        url: add_channel_url,
                        type: "POST",
                        data:{
                          // user_id:user_id,
                          channel_id:userId,
                          // user:user_type
                        },
                        success: function (returnData) {
                            returnData = $.parseJSON(returnData);
                            if (typeof returnData != "undefined")
                            {
                                console.log(returnData);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log('error in saving channel');
                        }
                    });

                  });

            }
        });

        OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            console.log('isEnabled:'+isEnabled);
          if (isEnabled) {

              // user has subscribed
              OneSignal.getUserId( function(userId) {
                  console.log('player_id of the subscribed user is : ' + userId);
                  // Make a POST call to your server with the user ID   
                    $.ajax({
                        url: add_channel_url,
                        type: "POST",
                        data:{
                          // user_id:user_id,
                          channel_id:userId,    
                          // user:user_type
                        },
                        success: function (returnData) {
                            returnData = $.parseJSON(returnData);
                            if (typeof returnData != "undefined")
                            {
                                console.log(returnData);
                            } 
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log('error in saving channel');
                        }
                    });


              });
          }
        });
    });

</script>