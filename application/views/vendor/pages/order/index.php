<style type="text/css" media="screen">
 .reassign{
    width: 5rem !important;
 }   
 .track .dot {
		height: 25px;
		width: 25px;
		background-color: #8bc34a;
		border-radius: 50%;
		display: flex;
		align-items: center;
		justify-content: center;
		margin: 0 auto;
	}

	.track .not-first .dot:before {
		content: '';
		width: 0;
		height: 174%;
		position: absolute;
		border: 1px solid #8bc34a;
		bottom: 25px;
	}
</style>
<?php //  _pre($delivery_boy);?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                        <h4 class="page-title"><?php echo $title; ?></h4>
                        <?php
                        echo $this->breadcrumbs->show();
                     ?>
                </div>
            </div>
        </div>
         <?php
        $this->load->view('vendor/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                         <?php echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

         <!-- Modal HTML -->
       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assign Delivery Person</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="post" name="select_db" class="select-db" action="<?php echo base_url('vendor/assign_delivery_boy'); ?>">
              <div class="modal-body">
                    <input type="hidden" name="order_id" id="order_id">
                   
                    <select name="delivery_boy" id="delivery_boy" class="form-control select2" data-placeholder="Select Delivery Person " required data-parsley-errors-container="#delivery_boy_error">
                        <option selected disabled></option>
                        <?php 
                            
                        foreach ($delivery_boy as $key => $value) {
                            $selected = '';
                            // if($field_value == $value['id']){
                            //     $selected = 'selected';
                            // }
                            if($value['duty'] == 0){
								$duty = 'Off';
							}else{
								$duty = 'On';
							}
                            echo "<option value='".$value['id']."' ".$selected.">".$value['first_name']." ".$value['last_name']." "."-".$duty."</option>";
                        }
                        ?>
                    </select>
                    <div id="error">
                    </div>
               
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" >Save changes</button>
              </div>
               </form>
            </div>
          </div>
        </div>

          <div class="modal fade bs-example-modal-lg" id="trackmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content track">
              <div class="modal-header bg-primary">
                <h6 class="modal-title text-center m-0 font-weight-bold text-white">Track Order #<span id="order-id"></span></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               <h4 class="text-center">Tracking data fetching..</h4>
              </div>
              <div class="text-center text-white bg-primary py-2">
                <h6 class="text-uppercase font-weight-bold"><span id="order-final-status"></span></h6>
                <div class="pb-2"><span id="order-final-text"></span></div>
              </div>
            </div>
          </div>
        </div>

         <div class="modal fade" id="sendpush" tabindex="-1" role="dialog" aria-labelledby="sendpushLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="sendpushLabel">Send Push Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="post" name="select_user" class="select-user" action="<?php echo base_url('vendor/send_push_notification'); ?>">
              <div class="modal-body">
                    <input type="hidden" name="order_id_push" id="order_id_push">
                    <input type="hidden" name="url" id="url" value="<?php echo base_url(uri_string()); ?>">
                    <div class="form-group">
                        <select name="user" id="user" class="form-control select2" data-placeholder="Select User" required data-parsley-errors-container="#user_error">
                        </select>
                        <div id="user_error">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" required placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <textarea name="description" class="form-control" required placeholder="Enter Message"></textarea>
                    </div>
               
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" >Send Notification</button>
              </div>
               </form>
            </div>
          </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(".select2").select2();
    $('#exampleModal').on('hidden.bs.modal', function () {
        console.log('closed');
        $('.select-db').trigger("reset");
        $("#delivery_boy").val('').trigger('change');
    });

    function assign_delivery_boy(id, delivery_boy_id = 0){
        var id=$('#'+id).data('id');
        $('#order_id').val(id);
        console.log(delivery_boy_id);
        $('#delivery_boy').val(delivery_boy_id).trigger('change');
    }

    $('#sendpush').on('hidden.bs.modal', function () {
        console.log('closed');
        $('#user').find('option').remove();
        $('.select-user').trigger("reset");
        $("#user").val('').trigger('change');
    });


    var users = <?php echo json_encode($user); ?>; 
    function send_push_notification(id, user_id = 0){

        var id=$('#'+id).data('id');
        var userid=$('#'+id).data('userid');
        var deliveryid=$('#'+id).data('deliveryid');
        $('#order_id_push').val(id);

        console.log(users);
        console.log(userid);
        console.log(deliveryid);
        console.log(id);

        var ids = [userid,deliveryid];

        
        //destroy select2
        $('#user').select2('destroy');
        $("#user").val([]);


        var option = '<option selected disabled></option>';
        $('#user').append(option);

        $.each( users, function( id, val) {


            var user_type = '';
            if(val.group == 5){
                user_type = ' - Customer';
            }else if(val.group == 4){
                user_type = ' - Delivery Person';
            }else{
                user_type = '';
            }

            $.each( ids, function( i, v) {
                if(v == val.id){
                    $('#user').append($("<option></option>").attr("value", val.id).text(val.first_name+' '+val.last_name+user_type));
                }
          });
        });
        $(".select2").select2();

    //$('#user').val(user_id).trigger('change');
    }

</script>
<script src="<?php echo base_url('/assets/js/custom/admin/track_order.js');?>" type="text/javascript"></script>