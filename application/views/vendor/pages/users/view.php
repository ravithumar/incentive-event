<style type="text/css" media="screen">
    #product-imgs img{
        height: 350px;
        object-fit: cover;
    }
    #product-imgs .item-name{
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-decoration: underline;
    }
    #followers img, #following img, #followers-list img, #followings-list img{
        height: 40px;
        width: 40px;
        margin-right: 10px;
        object-fit: cover;
    }
    #profile img{
        object-fit: cover;
    }
</style>

<?php //echo "<pre>";
    $user = $user->toArray();
    $user['profile_picture'] = base_url().$user['profile_picture']; 
    //print_r($user);
    if(count($product) > 0){
        $product = $product->toArray();
        //print_r($product);
    }

?>
<!-- The Modal -->

<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-box">
                    <a href="<?php echo base_url('admin/users');?>" class="btn btn-primary waves-effect waves-light float-right">
                        <i class="far fa-arrow-alt-circle-left m-r-10"></i> Back 
                    </a>
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $this->load->view('admin/includes/message');
                ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4" id="profile">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="card-title font-16 mt-0"><b><?php echo $user['first_name']." ".$user['last_name']; ?> </b></h4>
                        <h6 class="card-subtitle font-12 text-muted">
                            <?php
                            if(trim($user['description']) != ''){
                            ?>
                            <p class="mb-2"><?php echo nl2br($user['description']); ?></p>
                            <?php
                            }
                            ?>
                            <p class="mb-1"><i class="fas fa-phone mr-2"></i><?php echo $user['phone']; ?></p>
                            <p class="mb-1"><a href="mailto:<?php echo $user['email']; ?>"><i class="fas fa-envelope mr-2"></i><?php if($user['email_verify'] == 1){
                            echo $user['email'].' '.'<i class="mdi mdi-verified text-success font-18"></i>';}else{ echo $user['email']; }  ?></a></p>
                            <?php if($user['address'] != ''){ ?>
                            <p class="mb-0"><i class="fas fa-map-pin mr-2"></i><?php echo $user['address']; ?></p>
                            <?php }?>
                        </h6>
                    </div>
                    <img class="img-fluid border" src="<?php echo $user['profile_picture'];?>" alt="Profile Picture" style="height: 378px;">
                </div>
            </div>  
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary" >
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-tshirt-crew float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Posts</h6>
                                    <h4 class="mb-4"><?php echo count($product); ?></h4>
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary">
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-tshirt-crew float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Orders</h6>
                                    <h4 class="mb-4"><?php echo count($orders_renter); ?></h4>
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary">
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-tshirt-crew float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Rental</h6>
                                    <h4 class="mb-4"><?php echo count($orders_lender); ?></h4>
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary" >
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-currency-usd float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Net amount</h6>
                                    <h4 class="mb-4"><?php echo number_format((float)$net_amount, 2, '.', ''); ?></h4>
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary" >
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-chart-bar float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Growth amount</h6>
                                    <h4 class="mb-4"><?php echo number_format((float)$growth_amount, 2, '.', ''); ?></h4>
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mini-stat bg-primary" >
                            <div class="card-body mini-stat-img">
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-currency-usd float-right"></i>
                                </div>
                                <div class="text-white">
                                    <h6 class="text-uppercase mb-3">Withdraw pending</h6>
                                    <h4 class="mb-4"><?php echo number_format((float)$withdraw_pending, 2, '.', ''); ?></h4>
                                    <span class="ml-2">Total added in system</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
           
            
            
        </div>

        <?php if(count($product) > 0){ ?>
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $user['first_name']." ".$user['last_name']; ?> Apparel's</h4>
                </div>
            </div>
        </div>
        <div class="row" id='product-imgs'>
            <?php 
            
            foreach ($product as $key => $value) {
                $value['cover_img']['large_image'] = base_url().$value['cover_img']['large_image']; 
                 ?>
           
            <div class="col-md-3">
                <div class="card m-b-30">
                    <img class="card-img-top img-fluid" src="<?php echo $value['cover_img']['large_image']; ?>" alt="Card image cap">
                    <?php if($value['on_rent'] == 1) {?>

                    <div class="card-img-overlay">
                        <h3 class="card-title text-white mt-0">
                            <span class="badge badge-pill badge-light">On Rent</span>
                        </h3>
                        </p>
                    </div>
                    <?php } ?>
                    <div class="card-body" style="z-index: 1;">
                        <h4 class="card-title font-16 mt-0 item-name"><a target="_blank" href="<?php echo base_url().'admin/product/view/'.$value['id'];?>"><?php echo $value['title']; ?></a></h4>
                        <p class="card-text">
                            <?php echo '$ '.$value['daily_rental_price']; ?>
                        </p>
                    </div>
                </div>
            </div>

            <?php } ?>
              
        </div> <!-- end row -->
        <?php }?>

        <div class="row">
            <div class="col-md-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#lender" role="tab">AS LENDER</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#renter" role="tab">AS RENTER</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active p-3" id="lender" role="tabpanel">
                                <p class="mb-0">
                                   <div class="col-md-12">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 m-b-30 header-title">Orders</h4>
                                                <!-- <input type="text" name="myInput" id="myInput" value="" placeholder="Search..." class="float-right form-control w-25 mb-2"> -->
                                                <div class="table-responsive">
                                                    <table class="table table-vertical" id="l_order">

                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    Order Id
                                                                </th>
                                                                <th>
                                                                    Request Id
                                                                </th>
                                                                <th>
                                                                    Renter Name
                                                                </th>
                                                                <th>
                                                                    Product Name
                                                                </th>
                                                                <th>
                                                                    Size
                                                                </th>
                                                                <th>
                                                                    Total
                                                                </th>
                                                                <th>
                                                                    From Date
                                                                </th>
                                                                <th>
                                                                    To Date
                                                                </th>
                                                                <th>
                                                                    Status
                                                                </th>
                                                                <th>
                                                                    Ordered On
                                                                </th>
                                                                <th class="text-center">
                                                                    Action
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                           if(isset($orders_lender) && count($orders_lender) > 0  ){
                                                                foreach ($orders_lender as $key => $value) {
                                                                    if(isset($value['order_products']['product']['title'])){
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $value['id'];?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if(isset($value['request_id'])){ echo $value['request_id'];}else{echo '-';}?>
                                                                        </td>
                                                                        <td>
                                                                           <a target="_blank" href="<?php echo base_url('admin/user/view/').$value['user_id'] ;?>"><?php echo $value['user']['first_name']." ".$value['user']['last_name'];?></a>
                                                                        </td>
                                                                        <td>
                                                                            <?php if(isset($value['order_products']['product']['title'])){     ?>
                                                                                <a target="_blank" href="<?php echo base_url('admin/product/view/').$value['order_products']['product_id'] ;?>"><?php echo ucfirst($value['order_products']['product']['title']); ?></a>
                                                                            <?php }else {echo '-';}?>
                                                                        </td>
                                                                        <td>
                                                                            <?php  
                                                                                if(isset($value['order_products']['size'])){
                                                                                    echo $value['order_products']['size']; 
                                                                                }else {
                                                                                    echo '-';
                                                                                } 
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo '$'.$value['total'];  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $from_date = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $date = $from_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $to_date = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $date = $to_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                           <?php if($value['status'] == 1){
                                                                                    $today_obj = new DateTime('now');
                                                                                    $today = $today_obj->format('Ymd');

                                                                                    $from_date_obj = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $from_date = $from_date_obj->format('Ymd');
                                                                                    $to_date_obj = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $to_date = $to_date_obj->format('Ymd');

                                                                                    if($today >= $from_date && $today <= $to_date){
                                                                                        echo '<span class="badge badge-warning">'.$order_status['on_rent'].'</span>';
                                                                                    }else{
                                                                                        echo '<span class="badge badge-success">'.$order_status['paid'].'</span>';
                                                                                    }

                                                                                }else if($value['status'] == 2){
                                                                                    echo '<span class="badge badge-info">'.$order_status['receive'].'</span>';
                                                                                }else if($value['status'] == 3){
                                                                                    echo '<span class="badge badge-info">'.$order_status['completed'].'</span>';
                                                                                }else if($value['status'] == 4){
                                                                                    echo '<span class="badge badge-success">'.$order_status['confirmed_completed'].'</span>';
                                                                                }else if($value['status'] == 5){
                                                                                    echo '<span class="badge badge-danger">'.$order_status['fail'].'</span>';
                                                                                }else if($value['status']  == 6){
                                                                                    echo '<span class="badge badge-danger">'.$order_status['cancel'].'</span>';
                                                                                }else{
                                                                                    echo ' ';
                                                                                }?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $date = DateTime::createFromFormat('Y-m-d H:i:s',$value['created_at']);
                                                                                $created_at_date = $date->format('d M, Y');
                                                                                $created_at_time = $date->format('h:i A');
                                                                                echo $created_at_date.' '.'at'.' '.$created_at_time;
                                                                            ?>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <a target="_blank" href="<?php echo base_url('admin/order/view/'.$value['id']); ?>" class="btn btn-secondary btn-sm waves-effect waves-light">View</a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }}}else{
                                                                    echo '<tr> <td colspan="11" class="text-center">'.'No data available in table'.'</td></tr>';
                                                                }
                                                           
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 m-b-30 header-title">Requests</h4>

                                                <div class="table-responsive">
                                                    <table class="table table-vertical" id="l_request">

                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    Request Id
                                                                </th>
                                                                <th>
                                                                    Order Id
                                                                </th>
                                                                <th>
                                                                    Renter Name
                                                                </th>
                                                                <th>
                                                                    Product Name
                                                                </th>
                                                                <th>
                                                                    Size
                                                                </th>
                                                                <th>
                                                                    From Date
                                                                </th>
                                                                <th>
                                                                    To Date
                                                                </th>
                                                                <th>
                                                                    Delivery Option
                                                                </th>
                                                                <th>
                                                                    Status
                                                                </th>
                                                                <th>
                                                                    Requested On
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if(isset($request_lender) && count($request_lender) > 0){
                                                                foreach ($request_lender as $key => $value) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $value['id'];  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if($value['order_id'] != 0){?>
                                                                            <a target="_blank" href="<?php echo base_url('admin/order/view/'.$value['order_id']); ?>"><?php echo $value['order_id'];?></a>
                                                                            <?php }else{ echo "-"; }?>
                                                                        </td>
                                                                        <td>
                                                                           <a target="_blank" href="<?php echo base_url('admin/user/view/').$value['user_id'] ;?>"><?php echo $value['user']->first_name." ".$value['user']->last_name;?></a>
                                                                        </td>
                                                                        <td>
                                                                           <a target="_blank" href="<?php echo base_url('admin/product/view/').$value['product_id'] ;?>"><?php echo ucfirst($value['products']['title']); ?></a>
                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $value['size'];  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $from_date = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $date = $from_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $to_date = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $date = $to_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if($value['delivery_option'] == 1){
                                                                               echo '<span class="badge badge-primary">Meetup</span>';
                                                                            }else{
                                                                               echo '<span class="badge badge-primary">Postage</span>';
                                                                            }  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if($value['status'] == 1){
                                                                                    echo '<span class="badge badge-primary">'.$request_status['pending'].'</span>';
                                                                                }else if($value['status'] == 2){
                                                                                    echo '<span class="badge badge-success">'.$request_status['accepted'].'</span>';
                                                                                }else if($value['status'] == 3){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['rejected'].'</span>';
                                                                                }else if($value['status'] == 4){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['expired'].'</span>';
                                                                                }else if($value['status'] == 5){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['expired'].'</span>';
                                                                                }else if($value['status'] == 6){
                                                                                    $from_date_obj = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $from_date = $from_date_obj->format('Ymd');
                                                                                    $to_date_obj = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $to_date = $to_date_obj->format('Ymd');

                                                                                    $today_obj = new DateTime('now');
                                                                                    $today = $today_obj->format('Ymd');
                                                                                    if($today >= $from_date && $today <= $to_date){
                                                                                        echo '<span class="badge badge-warning">'.$request_status['on_rent'].'</span>';
                                                                                    }else{
                                                                                        echo '<span class="badge badge-success">'.$request_status['paid'].'</span>';
                                                                                    }
                                                                                }else if($value['status'] == 7){
                                                                                    echo '<span class="badge badge-success">'.$request_status['fulfilled'].'</span>';
                                                                                }else if($value['status'] == 8){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['refunded'].'</span>';
                                                                                }else{
                                                                                    echo ' ';
                                                                                }?> 
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $date = DateTime::createFromFormat('Y-m-d H:i:s',$value['created_at']);
                                                                                $created_at_date = $date->format('d M, Y');
                                                                                $created_at_time = $date->format('h:i A');
                                                                                echo $created_at_date.' '.'at'.' '.$created_at_time;
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }else{
                                                                    echo '<tr> <td colspan="10" class="text-center">'.'No data available in table'.'</td></tr>';
                                                                }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </p>
                            </div>
                            <div class="tab-pane p-3" id="renter" role="tabpanel">
                                <p class="mb-0">
                                   <div class="col-md-12">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 m-b-30 header-title">Orders</h4>

                                                <div class="table-responsive">
                                                    <table class="table table-vertical" id="r_order">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    Order Id
                                                                </th>
                                                                <th>
                                                                    Request Id
                                                                </th>
                                                                <th>
                                                                    Lender Name
                                                                </th>
                                                                <th>
                                                                    Product Name
                                                                </th>
                                                                <th>
                                                                    Size
                                                                </th>
                                                                <th>
                                                                    Total
                                                                </th>
                                                                <th>
                                                                    From Date
                                                                </th>
                                                                <th>
                                                                    To Date
                                                                </th>
                                                                <th>
                                                                    Status
                                                                </th>
                                                                <th>
                                                                    Ordered On
                                                                </th>
                                                                <th class="text-center">
                                                                    Action
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                             if(isset($orders_renter) && count($orders_renter) > 0){ 
                                                                //_pre($orders_renter);
                                                                foreach ($orders_renter as $key => $value) {
                                                                    if(isset($value['order_products']['product']['title'])){
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $value['id'];  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if(isset($value['request_id'])){ echo $value['request_id'];}else{echo '-';}?>
                                                                        </td>
                                                                        <td>
                                                                           <a target="_blank" href="<?php echo base_url('admin/user/view/').$value['seller_id'] ;?>"><?php echo $value['seller']['first_name']." ".$value['seller']['last_name'];?></a>
                                                                        </td>
                                                                        <td>
                                                                            <?php if(isset($value['order_products']['product']['title'])){ ?>
                                                                                <a target="_blank" href="<?php echo base_url('admin/product/view/').$value['order_products']['product_id'] ;?>"><?php echo ucfirst($value['order_products']['product']['title']); ?></a>
                                                                            <?php }else {echo '-';}?>
                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <?php  
                                                                                if(isset($value['order_products']['size'])){
                                                                                    echo $value['order_products']['size']; 
                                                                                }else {
                                                                                    echo '-';
                                                                                } 
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo '$'.$value['total'];  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $from_date = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $date = $from_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $to_date = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $date = $to_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                           <?php if($value['status'] == 1){
                                                                                    $today_obj = new DateTime('now');
                                                                                    $today = $today_obj->format('Ymd');

                                                                                    $from_date_obj = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $from_date = $from_date_obj->format('Ymd');
                                                                                    $to_date_obj = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $to_date = $to_date_obj->format('Ymd');

                                                                                    if($today >= $from_date && $today <= $to_date){
                                                                                        echo '<span class="badge badge-warning">'.$order_status['on_rent'].'</span>';
                                                                                    }else{
                                                                                        echo '<span class="badge badge-success">'.$order_status['paid'].'</span>';
                                                                                    }

                                                                                }else if($value['status'] == 2){
                                                                                    echo '<span class="badge badge-info">'.$order_status['receive'].'</span>';
                                                                                }else if($value['status'] == 3){
                                                                                    echo '<span class="badge badge-info">'.$order_status['completed'].'</span>';
                                                                                }else if($value['status'] == 4){
                                                                                    echo '<span class="badge badge-success">'.$order_status['confirmed_completed'].'</span>';
                                                                                }else if($value['status'] == 5){
                                                                                    echo '<span class="badge badge-danger">'.$order_status['fail'].'</span>';
                                                                                }else if($value['status']  == 6){
                                                                                    echo '<span class="badge badge-danger">'.$order_status['cancel'].'</span>';
                                                                                }else{
                                                                                    echo ' ';
                                                                                }?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $date = DateTime::createFromFormat('Y-m-d H:i:s',$value['created_at']);
                                                                                $created_at_date = $date->format('d M, Y');
                                                                                $created_at_time = $date->format('h:i A');
                                                                                echo $created_at_date.' '.'at'.' '.$created_at_time;
                                                                            ?>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <a target="_blank" href="<?php echo base_url('admin/order/view/'.$value['id']); ?>" class="btn btn-secondary btn-sm waves-effect waves-light">View</a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }}}else{
                                                                    echo '<tr> <td colspan="11" class="text-center">'.'No data available in table'.'</td></tr>';
                                                                }
                                                           
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 m-b-30 header-title">Requests</h4>

                                                <div class="table-responsive">
                                                    <table class="table table-vertical" id="r_request">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                   Request Id
                                                                </th>
                                                                <th>
                                                                   Order Id
                                                                </th>
                                                                <th>
                                                                    Lender Name
                                                                </th>
                                                                <th>
                                                                    Product Name
                                                                </th>
                                                                <th>
                                                                    Size
                                                                </th>
                                                                <th>
                                                                    From Date
                                                                </th>
                                                                <th>
                                                                    To Date
                                                                </th>
                                                                <th>
                                                                    Delivery Option
                                                                </th>
                                                                <th>
                                                                    Status
                                                                </th>
                                                                <th>
                                                                    Requested On
                                                                </th>
                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if(isset($request_renter) && count($request_renter) > 0){
                                                                //$request_renter = $request_renter->toArray();
                                                                foreach ($request_renter as $key => $value) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $value['id'];  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if($value['order_id'] != 0){?>
                                                                            <a target="_blank" href="<?php echo base_url('admin/order/view/'.$value['order_id']); ?>"><?php echo $value['order_id'];?></a>
                                                                            <?php }else{ echo "-"; }?>
                                                                        </td>
                                                                        <td>
                                                                           <a target="_blank" href="<?php echo base_url('admin/user/view/').$value['products']['user']['id'] ;?>"><?php echo $value['products']['user']['first_name']." ".$value['products']['user']['last_name'];?></a>
                                                                        </td>
                                                                        <td>
                                                                           <a target="_blank" href="<?php echo base_url('admin/product/view/').$value['product_id'] ;?>"><?php echo ucfirst($value['products']['title']); ?></a>
                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $value['size'];  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $from_date = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $date = $from_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $to_date = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $date = $to_date->format('d M,Y'); 
                                                                                    echo $date; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if($value['delivery_option'] == 1){
                                                                               echo '<span class="badge badge-primary">Meetup</span>';
                                                                            }else{
                                                                               echo '<span class="badge badge-primary">Postage</span>';
                                                                            }  ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if($value['status'] == 1){
                                                                                    echo '<span class="badge badge-primary">'.$request_status['pending'].'</span>';
                                                                                }else if($value['status'] == 2){
                                                                                    echo '<span class="badge badge-success">'.$request_status['accepted'].'</span>';
                                                                                }else if($value['status'] == 3){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['rejected'].'</span>';
                                                                                }else if($value['status'] == 4){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['expired'].'</span>';
                                                                                }else if($value['status'] == 5){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['expired'].'</span>';
                                                                                }else if($value['status'] == 6){
                                                                                    $from_date_obj = DateTime::createFromFormat('Y-m-d', $value['from_date']);
                                                                                    $from_date = $from_date_obj->format('Ymd');
                                                                                    $to_date_obj = DateTime::createFromFormat('Y-m-d', $value['to_date']);
                                                                                    $to_date = $to_date_obj->format('Ymd');

                                                                                    $today_obj = new DateTime('now');
                                                                                    $today = $today_obj->format('Ymd');
                                                                                    if($today >= $from_date && $today <= $to_date){
                                                                                        echo '<span class="badge badge-warning">'.$request_status['on_rent'].'</span>';
                                                                                    }else{
                                                                                        echo '<span class="badge badge-success">'.$request_status['paid'].'</span>';
                                                                                    }
                                                                                }else if($value['status'] == 7){
                                                                                    echo '<span class="badge badge-success">'.$request_status['fulfilled'].'</span>';
                                                                                }else if($value['status'] == 8){
                                                                                    echo '<span class="badge badge-danger">'.$request_status['refunded'].'</span>';
                                                                                }else{
                                                                                    echo ' ';
                                                                                }?> 
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $date = DateTime::createFromFormat('Y-m-d H:i:s',$value['created_at']);
                                                                                $created_at_date = $date->format('d M, Y');
                                                                                $created_at_time = $date->format('h:i A');
                                                                                echo $created_at_date.' '.'at'.' '.$created_at_time;
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }else{
                                                                    echo '<tr> <td colspan="10" class="text-center">'.'No data available in table'.'</td></tr>';
                                                                }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="<?php echo base_url('plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap4.min.js'); ?>"></script>
<script type="text/javascript">
    var l_order = <?php echo json_encode($orders_lender); ?>;
    var l_request = <?php echo json_encode($request_lender);?>;
    var r_request = <?php echo json_encode($request_renter);?>;
    var r_order =<?php echo json_encode($orders_renter);?>;
    $(document).ready(function(){
        if(l_order.length > 0){
            $('#l_order').DataTable({
                "order": [[ 0, "desc" ]]
            });
        }
        if(l_request.length > 0){
            $('#l_request').DataTable({
                "order": [[ 0, "desc" ]]
            });
        }
        if(r_request.length > 0){
            $('#r_request').DataTable({
                "order": [[ 0, "desc" ]]
            });
        }
        if(r_order.length > 0){
            $('#r_order').DataTable({
                "order": [[ 0, "desc" ]]
            });  
        } 
    });
</script>