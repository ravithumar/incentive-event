<?php
$put_link = base_url('admin/style/edit/').$style_data['id'];
$back = base_url('admin/style');
$image  = BASE_URL().'assets/images/default.png';
?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                   <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <form class="form-validate"  method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="required">Name</label>
                                <div>
                                <?php
                                    $field_value = NULL;
                                    $temp_value = set_value('name');
                                    if (isset($temp_value) && !empty($temp_value)) {
                                        $field_value = $temp_value;
                                    } else{
                                        $field_value = htmlentities($style_data['name']);
                                    }
                                    ?>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Ex: Trendy" value="<?php echo $field_value; ?>" required>
                                    <div class="validation-error-label">
                                        <?php echo form_error('name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                               
                                <label class="">Image</label>
                                <input type="file" accept="image/*"  data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png" onchange="readURL(this);" id="picture" name="picture"  class="form-control upload" />
                                <!-- data-parsley-filemaxmegabytes="2" -->
                            </div>
                            <div class="form-group">
                                 <img class="border rounded p-0"  src="<?php echo base_url($style_data['image']);?>" onerror="this.src='<?php echo $image; ?>'" alt="your image" style="height: 130px;width: 130px;object-fit: cover;" id="blah"/>
                            </div>

                            <input type="hidden" name="id" value="<?php echo $style_data['id'] ?>">
                            <input type="hidden" name="old_img" value="<?php echo $style_data['image'] ?>">

                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> 
        </div>

   </div>
</div>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script type="text/javascript">

    $(document).on('click', '.upload', function(){
        $('#blah').attr('src', '<?php echo BASE_URL()."assets/images/default.png"; ?>');
    });
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
       }

</script>

    

        