
<script src="<?= assets('web/js/jquery.min.js'); ?>"></script>
<script src="<?= assets('web/js/bootstrap.min.js'); ?>"></script>
<script src="<?= assets('web/js/owl.carousel.min.js'); ?>"></script>
<script src="<?= assets('web/js/popper.min.js'); ?>"></script>

<script src="<?= assets('web/js/slick.min.js'); ?>"></script>
<script src="<?= assets('web/js/custom.js'); ?>"></script>

</body>

<!-- profile-popup  modal -->
<div class="modal fade menu-popup profile" id="profile-popup" tabindex="-1" role="dialog" aria-labelledby="profile-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content position-relative">
			<div class="modal-header justify-content-center bg-yellow">
				<button type="button" class="close bg-white px-1 position-absolute" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title py-2 font-40 font-futura-book">My Profile</h5>
			</div>
			<div class="modal-body p-3 bg-grey">
				
				<form action="" method="get" accept-charset="utf-8">
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">Full Name</label>
						<input type="text" name="" value="" placeholder="Steven" class="form-control">
					</div>
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">Contact Number</label>
						<input type="text" name="" value="" placeholder="+966 868 632 5221" class="form-control">
					</div>
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">Email Address</label>
						<input type="text" name="" value="" placeholder="johndoe@gmail.com" class="form-control">
					</div>
					<div class="text-center mt-5">
						<button type="submit" class="btn-red px-4 py-3 text-white font-futura-bq text-uppercase">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- change-password-popup  modal -->
<div class="modal fade menu-popup profile" id="change-password-popup" tabindex="-1" role="dialog" aria-labelledby="change-password-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content position-relative">
			<div class="modal-header justify-content-center bg-yellow">
				<button type="button" class="close bg-white px-1 position-absolute" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title py-2 font-40 font-futura-book">Change Password</h5>
			</div>
			<div class="modal-body p-3 bg-grey">
				
				<form action="" method="get" accept-charset="utf-8">
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">Old Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">

					</div>
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">New Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">

					</div>
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">Confirm Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">

					</div>
					<div class="text-center mt-5">
						<button type="submit" class="btn-red px-4 py-3 text-white font-futura-bq text-uppercase">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- forgot-password-popup  modal -->
<div class="modal fade menu-popup profile" id="forgot-password-popup" tabindex="-1" role="dialog" aria-labelledby="change-password-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content position-relative">
			<div class="modal-header justify-content-center bg-yellow">
				<button type="button" class="close bg-white px-1 position-absolute" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title py-2 font-40 font-futura-book">Forgot Password</h5>
			</div>
			<div class="modal-body p-3 bg-grey">
				
				<form action="" method="get" accept-charset="utf-8">
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">Old Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">

					</div>
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">New Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">

					</div>
					<div class="form-group mb-4">
						<label class="t-yellow text-left font-30">Confirm Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">

					</div>
					<div class="text-center mt-5">
						<button type="submit" class="btn-red px-4 py-3 text-white font-futura-bq text-uppercase">Reset Password</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- logout-popup  modal -->
<div class="modal fade menu-popup profile logout-popup" id="logout-popup" tabindex="-1" role="dialog" aria-labelledby="change-password-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content position-relative">
			<div class="modal-header justify-content-center bg-yellow">
				
			</div>
			<div class="modal-body p-3 bg-grey">
				<h5 class="modal-title py-2 font-30 font-futura-book text-white text-center">Are you sure you want to logout?</h5>
				<div class="btn-wrapper d-flex align-items-center justify-content-center">
					<a href="#" class="btn-red px-3 py-2 text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cancel</span></a>
					<a href="#" class="btn-red px-3 py-2 text-white"><span aria-hidden="true">Logout</span></a>

					<!-- 
					<button type="button" class="close bg-white px-1 position-absolute" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button> -->
				</div>
				
			</div>
		</div>
	</div>
</div>

<!-- responsive menu  modal -->
<div class="modal fade menu-popup" id="menu-popup" tabindex="-1" role="dialog" aria-labelledby="menu-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content position-relative">
			
			<div class="modal-body p-3 text-center bg-grey">
				<button type="button" class="close bg-white px-1" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="text-wrapper">
					<p class="text-uppercase t-yellow font-40 font-futura-bq">Settings</p>
					<ul>
						<li class="text-uppercase font-40"><a href="javascript:void(0);" class="p-2 d-inline-block t-yellow ">Notification</a></li>
						<li class="text-uppercase middle font-40"><a href="javascript:void(0);"  class="p-2 d-inline-block position-relative t-yellow">Faq</a></li>
						<li class="text-uppercase font-40"><a href="javascript:void(0);"  class="p-2 d-inline-block t-yellow">Logout</a></li>
					</ul>
					<a href="javascript:void(0);" class="need-help text-right t-yellow d-flex justify-content-end">Need Help?</a>
				</div>
			</div>
		</div>
	</div>
</div>

</html>