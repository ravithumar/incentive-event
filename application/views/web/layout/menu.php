<section class ="event-schedule eg-yellow position-relative" style="background-image: url(<?= assets('web/images/schedule-bg.jpg'); ?>)">

<div class="menu position-relative">
	<nav class="navbar navbar-expand-lg navbar-light flex-nowrap align-items-center justify-content-between">
		<div class="logo-wrapper">
			<a class="navbar-brand event-text position-relative" href="index.php"><span class="text-uppercase t-yellow">Event</span></a>
			<a class="navbar-brand event-prev-icon d-none" href="##" onClick="history.go(-1); return false;"></a>				
			<a class="navbar-brand event-grey position-relative d-flex align-items-center" href="index.php"><img src="<?= assets('web/images/logo-grey.png'); ?>" alt="" class="img-fluid"><span class="text-uppercase ml-3 pl-2 font-70 position-relative d-block font-futura-bq">Sunlife Acm</span></a>
			<a class="navbar-brand event-yellow position-relative d-flex align-items-center" href="index.php"><img src="<?= assets('web/images/logo-yellow.png'); ?>" alt="" class="img-fluid"><span class="text-uppercase ml-3 pl-2 t-yellow font-70 position-relative d-block font-futura-bq">Sunlife Acm</span></a>
			
		</div>
		
		<div class="right-menu text-center d-flex align-items-center">
			<ul class="ml-auto d-none d-md-flex mb-0">
				<li class="ml-2 ml-xl-4 mr-2 mr-xl-3"><a href="events.php" class="font-40 text-white btn-grey d-block px-3 px-xl-5 py-1 text-uppercase text-center position-relative font-futura-bq">Event <sup><img src="<?= assets('web/images/top-plus.png'); ?>" alt="" class="img-fluid"></sup></a></li>
				<li class="ml-2 ml-xl-4 mr-2 mr-xl-3"><a href="rewards.php" class="font-40 text-white btn-red d-block px-3 px-xl-5 py-1 text-uppercase text-center position-relative font-futura-bq">Reward <sup><img src="<?= assets('web/images/top-plus.png'); ?>" alt="" class="img-fluid"></sup></a></li>
		   </ul>
			<div class="right-container">
				<?php  $id = $this->session->userdata['users']['user_id'];
                $profile =get_session_data($id,'users');
                //_pre($profile); ?>
				 <h4 class="d-block  text-uppercase font-futura-bq mb-2">Welcome ! <?php echo ($profile['first_name']." ".$profile['last_name']) ;?></h4>
				<div class="account ml-lg-3 dropdown mb-0 mb-md-2">
					<a href="#" title="" class="d-inline-block btn-green dropdown-toggle" id="menu-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?= assets('web/images/mobile-white.png'); ?>" alt="" class="img-fluid white d-none mx-auto"><img src="<?= assets('web/images/mobile-grey.png'); ?>" alt="" class="img-fluid grey mx-auto"></a>
					<div class="dropdown-menu" aria-labelledby="menu-dropdown">
						<a href="#" class="dropdown-item dd-profile text-white" data-toggle="modal" data-target="#profile-popup">My Profile</a>
						<a href="#" class="dropdown-item dd-order text-white" data-toggle="modal" data-target="#change-password-popup">Change Password</a>
						<a href="#" class="dropdown-item dd-fav-res text-white">Notifications</a>
						<a href="#" class="dropdown-item dd-add-mang text-white">FAQ</a>
						<a href="#" class="dropdown-item dd-ch-pass text-white" data-toggle="modal" data-target="#logout-popup">Logout</a>							
					</div>
		   		</div>	                  
               <div class="bottom-part d-flex align-items-center justify-content-between">
               	  <div class="language-wrapper">
					<a href="javascript:void(0);" class="t-yellow text-uppercase font-30 d-flex align-items-center mr-2"><img src="<?= assets('web/images/change.png'); ?>" alt="" class="img-fluid">Eng</a>
					<a href="javascript:void(0);" class="t-grey text-uppercase font-30 d-flex align-items-center mr-2"><img src="<?= assets('web/images/change-grey.png'); ?>" alt="" class="img-fluid">Eng</a>
				  </div>
				  <a href="#" class="btn-grey logout d-block px-3 py-1 font-20 text-uppercase border-r8 text-white" data-toggle="modal" data-target="#logout-popup">Logout</a> 
               </div>
			</div>
			
		</div>
	
	</nav>
		<div class="log-in-area d-none align-items-center px-2">
			<div class="log-in-content d-flex  justify-content-between align-items-center">
				<a class="navbar-brand event-text position-relative" href="javascript:void(0);"><span class="text-uppercase t-yellow">Event</span></a>
				<a class="navbar-brand login-text position-relative" href="javascript:void(0);"><span class="text-uppercase t-grey d-block font-70 font-futura-bq">Welcome !</span><span class="user-name t-grey text-uppercase position-relative d-block font-90 font-futura-bq">Man Chan</span></a>
				<div class="date-time d-flex align-items-center">
					<p class="t-yellow text-right font-50 mb-0">22/APR</p>&nbsp;&nbsp;
					<p class="t-yellow text-right font-50 mb-0">19:08</p>
				</div>
				
			</div>
		</div>

</div>