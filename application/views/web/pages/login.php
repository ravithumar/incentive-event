<section class="event-center d-flex align-items-center justify-content-center position-relative p-3">
	<div class="language-wrapper position-absolute mobile">
		<a href="javascript:void(0);" class="t-yellow text-uppercase font-30"><img src="<?= assets('web/images/change-white.png'); ?>" alt="" class="img-fluid pr-5">Eng</a>
	</div>
	<div class="container">
		<div class="row">
			 <?php
                $this->load->view('admin/includes/message');
                ?>
			<div class="col-md-12">
				<div class="launch-block position-relative mx-auto">
					<span class="img-wrapper d-block mb-3">
						<img src="<?= assets('web/images/ready.png'); ?>" alt="" class="img-fluid">
					</span>
					<form method="post" action="">
						<?php
                            $field_value = NULL;
                            $temp_value = set_value('identity');
                            if (isset($temp_value) && !empty($temp_value)) {
                                $field_value = $temp_value;
                            } 
                        ?>
					  <div class="form-group">		    
					    <input type="email" class="form-control text-center bg-grey border-r5 font-28 text-uppercase" name="identity" id="identity" aria-describedby="emailHelp" placeholder="UserName / Email Address" value="<?php echo $field_value; ?>"> 
					    <div class="validation-error-label">
                            <?php echo form_error('identity'); ?>
                        </div> 
					  </div>
					  <div class="form-group position-relative">
					  	<?php
                        $field_value = NULL;
                        $temp_value = set_value('password');
                        if (isset($temp_value) && !empty($temp_value)) {
                            $field_value = $temp_value;
                        }  ?>
					    <div class="icon position-absolute"><img src="<?= assets('web/images/eye-light.png'); ?>" alt="" class="img-fluid"></div> 
					    <input type="password" class="form-control text-center bg-grey border-r5 font-28 text-uppercase" id="exampleInputPassword1" placeholder="Password" name="password" value="<?php echo $field_value; ?>">
					    <div class="validation-error-label">
                            <?php echo form_error('password'); ?>
                        </div>
					  </div>					  
					<a href="#" class="forgot-password text-right font-22" data-toggle="modal" data-target="#forgot-password-popup">Forgot Password</a>
					<div class="btn-wrapper d-flex align-items-center justify-content-between mb-3">
						<span class="img-wrapper pr-3">
							<img src="<?= assets('web/images/for-the.png'); ?>" alt="" class="img-fluid">
						</span>
						<button type="submit" name="submit" class="text-center bg-grey px-4 py-3 border-r20"><img src="<?= assets('web/images/next.png'); ?>" alt="" class="img-fluid btn-black"></button>
					</div>
					</form>
					<span class="img-wrapper d-block">
						<img src="<?= assets('web/images/event.png'); ?>" alt="" class="img-fluid">
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="language-wrapper position-absolute">
		<a href="javascript:void(0);" class="t-yellow text-uppercase font-30"><img src="<?= assets('web/images/change.png'); ?>" alt="" class="img-fluid pr-5">Eng</a>
	</div>
</section>
