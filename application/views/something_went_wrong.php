<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo assets('images/rad.ico'); ?>">

  <title>Something went wrong</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo assets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">

  <style type="text/css">
    body {
      background: url('http://50.18.114.231/assets/images/bg-img.jpg') no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
      -o-background-size: cover;
    }


    
  </style> 
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom" style="background-color: #8bc34a !important !important;">
    <div class="container">
      <a class="navbar-brand" href="javascript:void(0)"><b>RAD</b></a>
    </div>
  </nav>

  <!-- Page Content -->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <h1 class="mt-5" style="color: #8bc34a !important;">
            <?php
            if(isset($title) && $title != ''){
              echo $title;
            }else{
              echo "Sorry! Something went wrong.";
            }
            ?>
          </h1>
          <h4 class="">Try again later. If you have any questions about the application, <a href="mailto:info@radservices.ca" class=""><u>reach out to our team</u></a>. </h4>
        </div>
      </div>
    </div>
  </section>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  
</body>

</html>

