<?php

require_once APPPATH. 'third_party/twilio-php-master/src/Twilio/autoload.php';
use Twilio\Rest\Client;

class Twilio{


	protected $sid;
	protected $token;
	protected $client;
	protected $from;

	public function __construct(){

		error_reporting(0);
		$this->CI = & get_instance();
	 	$this->sid = $this->CI->config->config['twillio']['sid'];
	 	$this->token = $this->CI->config->config['twillio']['token'];
	 	$this->client = new Client($this->sid, $this->token);
	 	$this->from = $this->CI->config->config['twillio']['from'];

	 	

	}

	public function validate_phone($phone_number)
	{

		try{
			$phone_number = $this->client->lookups->v1->phoneNumbers($phone_number)->fetch(array( "Type" => array("carrier")));
			// echo "<pre>";
			// print_r($phone_number->carrier);
			// exit;
	        if($phone_number->carrier['error_code'] == '' && $phone_number->carrier['type'] != ''){
	        	$data['status'] = TRUE;
				$data['message'] = 'Verified';
	        }else{
	        	$data['status'] = FALSE;
				$data['error'] = 'Invalid phone number type.';
	        }
	        
		}catch (Exception $e){
			$data['status'] = FALSE;
			$data['error'] = $e->getMessage();
		}


		return $data;
		
	}	

	public function send($to = '', $message = ''){

		if($to != '' && $message != ''){

			try{

				$result = $this->client->messages->create(
				    // the number you'd like to send the message to
				    $to,
				    array(
				        // A Twilio phone number you purchased at twilio.com/console
				        'from' => $this->from,
				        // the body of the text message you'd like to send
				        'body' => $message
				    )
				);

				return TRUE;

				$data['status'] = TRUE;
				$data['data'] = $result;


			} catch (Exception $e) {

				return FALSE;

				$data['status'] = FALSE;
				$data['data'] = $e->getMessage();
		    }
				

		}else{

			return FALSE;
			$data['status'] = FALSE;
			$data['error'] = 'Parameter Missing';
		}

		return FALSE;
		return $data;
	}

}
?>