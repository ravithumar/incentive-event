<?php

class Onesignal{

	protected $onesignal_appid;

	public function __construct(){

		$this->CI = & get_instance();
	 	$this->onesignal_appid = "fe939676-d326-4f0a-9cbb-15b6f5bc2f77";

	}

	public function send($player_ids = array(), $message = '', $header = 'Naqsa'){
		
		if(isset($player_ids) && is_array($player_ids) && !empty($player_ids)){

			$content = array(
				"en" => $message
				);
			$heading = array(
			   "en" => $header
			);

			foreach ($player_ids as $key => $value) {
				
				$fields = array(
					'app_id' => $this->onesignal_appid,
					'include_player_ids' => array($value),
					'contents' => $content,
					'headings' => $heading
				);
				
				$fields = json_encode($fields);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

				$response = curl_exec($ch);
				curl_close($ch);

			}
			
			return $response;

		}else{
			return FALSE;
		}
	}

	public function send2($player_ids = array(), $message = '', $header = 'Naqsa'){
		
		if(isset($player_ids) && is_array($player_ids) && !empty($player_ids)){

			$content = array(
				"en" => $message
				);
			$heading = array(
			   "en" => $header
			);

			$response = array();

			foreach ($player_ids as $key => $value) {
				
				$fields = array(
					'app_id' => $this->onesignal_appid,
					'include_player_ids' => array($value),
					'contents' => $content,
					'headings' => $heading
				);
				
				$fields = json_encode($fields);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

				$result = curl_exec($ch);				
				$response[$value] = $result;
				curl_close($ch);

			}
			
			return $response;

		}else{
			return FALSE;
		}
	}
}
?>