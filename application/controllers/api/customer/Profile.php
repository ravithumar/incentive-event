<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class Profile extends REST_Controller {

	function __construct() {
        header('Content-Type: text/html; charset=utf-8');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
		$this->load->model('ContactUs');
		$this->load->model('ion_auth_model');
		$this->load->model('Token');
		$this->load->library(['ion_auth', 'form_validation']);

	}

    public function email_exist_check($str, $user_id){
        if($str != ''){
            $user = User::where('users.email', $str)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[5])
            ->where('users.id','!=',$user_id)
            ->first();
            if (empty($user)) {
                return TRUE;
            }
            $this->form_validation->set_message('email_exist_check', "Email is already exist.");
            return FALSE;
        }else{
            return TRUE; 
        }
        
        
    }

    function phone_exist_check($str, $user_id) {
        $user = User::where('users.phone', $str)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[5])
            ->where('users.id','!=',$user_id)
            ->first();
        if (empty($user)) {
            return TRUE;
        }
        $this->form_validation->set_message('phone_exist_check', "Phone number is already exist..");
        return FALSE;
    }

	public function profile_post(){

        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');

        $this->form_validation->set_rules('first_name', 'first name', 'trim');
        $this->form_validation->set_rules('last_name', 'last name', 'trim');

        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|callback_email_exist_check['.$this->input->post('user_id').']');
        $this->form_validation->set_rules('phone', 'phone', 'trim|callback_phone_exist_check['.$this->input->post('user_id').']');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{

            $request = $this->post();

            $user = User::find($request['user_id']);
            if(isset($user)){

            	if($user->active == '1'){


                    /*if(isset($request['full_name'])){
                        $names = explode(' ', $request['full_name'], 2);
                        $user->first_name =$names[0];
                        $user->last_name = $names[1];

                    }*/
                     if(isset($request['first_name'])){
                        $user->first_name = $request['first_name'];
                    }

                    if(isset($request['last_name'])){
                        $user->last_name = $request['last_name'];
                    }
                    
                    if(isset($request['email'])){
                        $user->email = $request['email'];
                    }

                    if(isset($request['phone'])){
                        $user->phone = $request['phone'];
                    }

                    if (isset($_FILES['profile_picture'])) {
                        // if ($_FILES['profile_picture']['name'] != "") {
                        if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0){

                            $file_upload = TRUE;

                            if($file_upload == TRUE){

                                $img_name = 'user_' . time().rand(1000, 9999).'.jpg';
                                $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
                                if(compress($_FILES['profile_picture']['tmp_name'], $destination_url, 30)){
                                    $user->profile_picture = $img_name;
                                }


                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Error on adding profile picture.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }
                           
                        }
                    }
                    
                    $user->save();

                    $api_key = Token::where('user_id',$request['user_id'])->first();
                    $user->api_key = $api_key['token'];


            		$response['user'] = $user;
                    $response['message'] = 'Profile details have been updated successfully';
                    $response['status'] = TRUE;
            		
            	}else if($user->active == '0'){
            		$response['message'] = 'Sorry! Your account is not verified, please activate it from your registered email';
            		$response['status'] = FALSE;
            	}else if($user->active == '2'){
            		$response['message'] = 'Account deactivated.';
            		$response['status'] = FALSE;
            	}else{
            		$response['message'] = 'Something went wrong';
            		$response['status'] = FALSE;
            	}
            	
            }else{
            	$response['message'] = 'Account not found.';
            	$response['status'] = FALSE;
            }

        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function contact_us_post(){
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('comment', 'comment', 'trim|required');


        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $conatct_us_data = array(
                'name' => $request['name'],
                'email' => $request['email'],
                'message' => $request['comment'],
            );

            $insert_id = ContactUs::insertGetId($conatct_us_data);

            if($insert_id){
                // $response['data'] = ContactUs::find($insert_id);
                $response['status'] = true;
                $response['message'] = "Thank you for contacting us! We'll get back to you shortly";
            }else{
                $response['status'] = false;
                $response['message'] = 'Server encountered an error. please try again';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
}