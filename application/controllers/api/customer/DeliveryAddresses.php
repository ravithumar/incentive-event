<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class DeliveryAddresses extends REST_Controller {

	function __construct() {
        header('Content-Type: text/html; charset=utf-8');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
		$this->load->model('DeliveryAddress');
		$this->load->model('ion_auth_model');
		$this->load->library(['ion_auth', 'form_validation']);

	}

	public function add_delivery_address_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('type', 'type', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('address', 'address', 'trim');
        $this->form_validation->set_rules('location', 'location', 'trim|required');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');


        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            

            $delivery_address_data = array(
                'user_id' => $request['user_id'],
                'type' => $request['type'],
                'location' => $request['location'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude']
            );

            if(isset($request['address']) && $request['address'] != ''){
                $delivery_address_data['address'] = $request['address'];
            }else{
                $delivery_address_data['address'] = '';
                
            }
            $default = DeliveryAddress::where('user_id',$request['user_id'])->where('default', '1')->first();
            if(!isset($default)){
                $delivery_address_data['default'] = 1;
            }

            $insert_id = DeliveryAddress::insertGetId($delivery_address_data);

            if($insert_id){
                $response['data'] = DeliveryAddress::find($insert_id);
                $response['status'] = true;
                $response['message'] = 'Delivery address added successfully';
            }else{
                $response['status'] = false;
                $response['message'] = 'Server encountered an error. please try again';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function validate_latlong($str){
        if (!preg_match('/([0-9.-]+).+?([0-9.-]+)/', $str)) {
                $this->form_validation->set_message('validate_latlong', 'The {field} field is invalid.');
                return false;
        }
        
        return TRUE;
    }

    public function update_delivery_address_post(){
        $this->form_validation->set_rules('delivery_address_id', 'delivery address id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('type', 'type', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('address', 'address', 'trim');
        $this->form_validation->set_rules('location', 'location', 'trim|required');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false; 
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $delivery_address = DeliveryAddress::find($request['delivery_address_id']);
            if (isset($delivery_address)){

                $delivery_address_data = array(
                    'type' => $request['type'],
                    'location' => $request['location'],
                    'latitude' => $request['latitude'],
                    'longitude' => $request['longitude']
                );
                if(isset($request['address']) && $request['address'] != ''){
                    $delivery_address_data['address'] = $request['address'];
                }else{
                    $delivery_address_data['address'] = '';
                    
                }

                DeliveryAddress::whereId($request['delivery_address_id'])->update($delivery_address_data);

                $response['data'] = DeliveryAddress::find($request['delivery_address_id']);
                $response['status'] = true;
                $response['message'] = 'Delivery address updated successfully';

            }else{

                $response['status'] = false;
                $response['message'] = 'Delivery address not found';
            }

            
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function delivery_addresses_list_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $delivery_address = DeliveryAddress::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();

            if(count($delivery_address) > 0){
            	 $response['data'] = $delivery_address;
            	 $response['status'] = TRUE;
            }else{
            	$response['message'] = 'No delivery address found.';
            	 $response['status'] = false;
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function delete_delivery_address_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('delivery_address_id', 'delivery address id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $delivery_address = [];
            if(DeliveryAddress::whereId($request['delivery_address_id'])->where('user_id',$request['user_id'])->delete()){
                $delivery_address = DeliveryAddress::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();
                $response['data'] = $delivery_address;
                $response['status'] = true;
                $response['message'] = 'Delivery address removed successfully';
            }else{
                $response['status'] = false;
                $response['message'] = 'Server encountered an error. please try again';
            }

        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function add_default_address_post(){
    	 $this->form_validation->set_rules('delivery_address_id', 'delivery address id', 'trim|required|is_natural_no_zero');
    	 $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
    	 if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $update_data = array('default' => 0);

            DeliveryAddress::where('user_id',$request['user_id'])->where('default', '1')->update($update_data);

            $data = array('default' => 1);
            
            if(DeliveryAddress::whereId($request['delivery_address_id'])->update($data)){

                $delivery_address = DeliveryAddress::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();

            	$response['data'] = $delivery_address;

                $response['status'] = true;
                $response['message'] = 'Default delivery address set successfully';
            }else{
                $response['status'] = false;
                $response['message'] = 'Server encountered an error. please try again';
            }

        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }


    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
}