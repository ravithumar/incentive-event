 <?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

require '/var/www/html/vendor/autoload.php';
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

class Order extends REST_Controller {
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Devices');
        $this->load->model('DeliveryAddress');
        $this->load->model('Product');
        $this->load->model('ProductAddOn');
        $this->load->model('Promocode');
        $this->load->model('Orders');
        $this->load->model('OrderProductAddOn');
        $this->load->model('OrderProducts');
        $this->load->model('TrackOrder');
        $this->load->model('Category');
        $this->load->model('Wallet');
        $this->load->model('WalletHistory');
        $this->load->model('PaymentCard');
        $this->load->model('Vendor');
        $this->load->model('Favourite');
        $this->load->model('ion_auth_model');
        $this->load->model('System_setting');
        $this->load->model('Cart');
        $this->load->model('CartProducts');
        $this->load->model('CartProductAddons');
        // $this->load->library(['ion_auth', 'form_validation']);
    }
    public function customAlpha($str) {
        if($str != ''){
            if (!preg_match('/^[0-9a-z .,\-]+$/i', $str)) {
                $this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
                return false;
            }
        }
        
        return TRUE;
    }
    public function validateDateFormat($str) {
        if($str != ''){
            if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$str)) {
                $this->form_validation->set_message('validateDateFormat', 'The {field} field contain invalid date format.');
                return false;
            }
        }
        return TRUE;
    }

    public function validate_latlong($str){
        if (!preg_match('/([0-9.-]+).+?([0-9.-]+)/', $str)) {
                $this->form_validation->set_message('validate_latlong', 'The {field} field is invalid.');
                return false;
        }
        
        return TRUE;
    }

    public function order_accept_reject_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('delivery_boy_id', 'delivery boy id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $orders = Orders::where('id',$request['order_id'])->first();
            $user = User::where('id',$orders['user_id'])->first();
            if (isset($orders)){
                $orders = $orders->toArray();

                //	1-Placed , 2-Store accepted & sent request to all db, 3-Store rejected , 4-Cancelled (refer reason) , 5-Order assign for dispatcher, 6-Delivery boy accepted, 7-Delivery boy rejected , 8-Delivery boy reached , 9-Picked & On the way / Ready to pick , 10-Delivered

                if($orders['status'] == '2'){
                   
                    $data['status'] = 6;
                    $data['delivery_boy_id'] = $request['delivery_boy_id'];

                    Orders::where('id', $request['order_id'])->update($data);
                    $current_datetime = date("Y-m-d H:i:s");
                    $track_data['order_id'] = $request['order_id'];
                    $track_data['status'] = 5;
                    $track_data['created_at'] = $current_datetime;
                    $track_data['status_title'] = 'Delivery Person Assigned';
                    $track_data['status_text'] = 'Delivery Person assigned for your order.';
                    TrackOrder::insert($track_data);

                    $order_data = Orders::with(['user','user_device','delivery_boy'])->where('id',$request['order_id'])->first();
                    if (isset($order_data)) {
                        $order_data = $order_data->toArray();
                    }

                    $phone_no = $order_data['delivery_boy']['phone'];
                    $user_phone_no = $order_data['user']['phone'];

                    $device_token = $order_data['user_device']['token'];
                    $device_type = $order_data['user_device']['type'];
                    $sms_text = $order_data['delivery_boy']['first_name'].' '.$order_data['delivery_boy']['last_name'].' has been assigned as a delivery person for your order #'.$request['order_id'].'. Call '.$phone_no.' for a real-time update.';
                    /*john doe has been assigned as a delivery person for your order #123. Call 677678677676 for a real-time update.*/
                    $push_title = 'Delivery Person assigned';
                    $push_data = array(
                            'order_id' => $request['order_id'],
                            'message' => $sms_text
                        );
                    $push_type = 'delivery_boy_accepted';

                    $message = $sms_text; 
                    
                    if($user['notification_status'] == 1 && $device_token != ''){
                        $SendNotification = SendNotification($device_type,$device_token, $push_title, $push_data, $push_type);
                    }
                    notification_add($order_data['user_id'],$message,$push_type,0,$request['order_id'],0);

                    send_SMS($user_phone_no,$sms_text);

                    //accept socket
                    $client = new Client(new Version2X($this->config->item('socket_url')));
                    $client->initialize();

                    $client->emit('accept_booking_request',$order_data);
                    $client->close();

                    $response['user_device'] = $order_data['user_device'];
                    $response['SendNotification'] = $SendNotification;
                    $response['status'] = true;
                    $response['message'] = 'Request accepted successfully.';
                    
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Order accepted by another delivery person.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Order not found.';
            }
            
        }
        
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function get_order_inprocess_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $status = [6,8,9];
            $request = $this->post();
            $orders = Orders::with(['orderproducts','vendor','user'])->where('delivery_boy_id',$request['user_id'])->orderBy('id', 'desc')->whereIn('status',$status)->get();
            if(count($orders) > 0){

                $orders = $orders->toArray();

                $response['data'] = $orders;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No order found.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    
    public function get_order_history_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();
            $orders = Orders::with(['orderproducts','vendor','user'])->where('delivery_boy_id',$request['user_id'])->where('status',10)->orderBy('id', 'desc')->get();
            if(count($orders) > 0){

                $orders = $orders->toArray();

                $response['data'] = $orders;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No order found.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function get_order_detail_post(){ 
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();

            $orders = $order_products = []; 
            $orders = Orders::with(['vendor','user','delivery_boy','user_vendor'])->where('id',$request['order_id'])->first();
            if(isset($orders)){
                $orders = $orders->toArray();
                $RAD_number = System_setting::select('value')->where('path','RAD_number')->first();

                $order_products = OrderProducts::where('order_id',$request['order_id'])->with(['orderproductsaddon'])->get();
                $subtotal = 0;
                if(count($order_products) > 0){
                    $order_products = $order_products->toArray();
                    foreach ($order_products as $key => $value) {
                        $addon_total = 0;
                        if(count($value['orderproductsaddon']) > 0){
                            foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
                                $addon_total += $cvalue['price'];
                            }
                        }
                        $addon_totals = $addon_total * $value['quantity'];
                        $order_products[$key]['addon_total'] = number_format((float)$addon_totals, 2, '.', '');
                        $total_amount = $addon_totals + $value['total_amount'];
                        $order_products[$key]['total_amount'] = number_format((float)$total_amount, 2, '.', '');
                    }
                    foreach ($order_products as $key => $value) {
                        $total_price  += $value['total_amount'];  
                        
                    }
                }
                if($orders['payment_mode'] == 1){
                    $orders['payment_type'] = 'Paid: Using card';
                }else{
                    $orders['payment_type'] = 'Paid: Using wallet';
                }
                $orders['sos'] = $RAD_number['value'];

                $orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
                $data = [];
                $data['order'] = $orders;
                $data['order_products'] = $order_products;

                $response['data'] = $data;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No order found.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function update_order_status_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('status', 'status', 'trim|required'); // 1-Reached at Restaurant 2-Picked , 3- Delivered
        $this->form_validation->set_rules('delivery_boy_id', 'delivery boy id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $order = Orders::with(['vendor','user'])->where('delivery_boy_id',$request['delivery_boy_id'])->where('id',$request['order_id'])->first();
            
            if (isset($order)){
                $order = $order->toArray();
                $store_name = $order['vendor']['name'];


                $device = Devices::where('user_id',$order['user_id'])->first();
                $user = User::where('id',$order['user_id'])->first();
                $delivery_boy = User::where('id',$order['delivery_boy_id'])->first();

                $push_type = 'order_reach_picked_up_delivered';

                if($request['status'] == 1 ) {

                    if($order['status'] == '6'){
                        $data['status'] = 8;
                        Orders::where('id', $request['order_id'])->update($data);
                        $message = 'Delivery Person is reached at your restaurant.';
                        $current_datetime = date("Y-m-d H:i:s");
                        $track_data['order_id'] = $request['order_id'];
                        $track_data['status'] = 6;
                        $track_data['created_at'] = $current_datetime;
                        $track_data['status_title'] = 'Reached at Restaurant';
                        $track_data['status_text'] = $message;
                        TrackOrder::insert($track_data);


                        $push_data = array(
                            'order_id' => $request['order_id'],
                            'message' => $message
                        );
                        $push_title = 'Reached at Restaurant';

                        if($user['notification_status'] == 1 && $device['token'] != ''){
                            $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
                        }
                        notification_add($order['user_id'],$message,$push_type,0,$request['order_id'],0);


                       /* $phone_no = $user['phone'];
                        $sms_text = 'Your order #'.$request['order_id'].' from '.$store_name.' is now ready for pickup. Call (999) 999-9999 with questions.';
                        send_SMS($phone_no,$sms_text);*/

                        $response['device'] = $device;  
                        $response['SendNotification'] = $SendNotification;  
                        $response['status'] = true;
                        $response['message'] = 'Reached at Restaurant.';
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Order should be accepted first.';
                    }
                    
                }elseif($request['status'] == 2){
                    if($order['status'] == '8'){
                        $data['status'] = 9;
                        Orders::where('id', $request['order_id'])->update($data);

                        $message = 'Delivery person has picked up your order. You will receive your order soon.';
                        $current_datetime = date("Y-m-d H:i:s");
                        $track_data['order_id'] = $request['order_id'];
                        $track_data['status'] = 7;
                        $track_data['created_at'] = $current_datetime;
                        $track_data['status_title'] = 'Order Picked';
                        $track_data['status_text'] = $message;
                        TrackOrder::insert($track_data);

                        $push_data = array(
                            'order_id' => $request['order_id'],
                            'message' => $message
                        );
                        $push_title = 'Order Picked';

                        if($user['notification_status'] == 1 && $device['token'] != ''){
                            $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
                        }
                        notification_add($order['user_id'],$message,$push_type,0,$request['order_id'],0);

                        $response['device'] = $device;  
                        $response['SendNotification'] = $SendNotification;
                        $response['status'] = true;
                        $response['message'] = 'Order picked successfully.';
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'You have to reach to restaurant first.';
                    }
                }elseif($request['status'] == 3 ){
                    if($order['status'] == '9'){
                        if($order['alcoholic'] == 1){
                            if (isset($_FILES['document']) && !empty($_FILES['document']) && strlen($_FILES['document']['name']) > 0) {

                                $img_name = 'document' . '_' . time().'.jpg';
                                $destination_url = FCPATH . $this->config->item("document").'/'.$img_name;
                                if(compress($_FILES["document"]["tmp_name"], $destination_url, 90)){
                                    $data['id_proof'] = $img_name;
                                }else{
                                    $response['status'] = false;
                                    $response['message'] = 'Unable to upload document.';
                                    //$this->response($response);
                                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                    return TRUE;
                                    exit;
                                }

                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Please upload Id Proof.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                                exit;
                            }
                        }
                        $data['status'] = 10;
                        $data['completed_at'] = date('Y-m-d H:i:s');
                        Orders::where('id', $request['order_id'])->update($data);
                        $current_datetime = date("Y-m-d H:i:s");
                        $track_data['order_id'] = $request['order_id'];
                        $track_data['status'] = 8;
                        $track_data['created_at'] = $current_datetime;
                        $track_data['status_title'] = 'Delivered';
                        $track_data['status_text'] = 'Your order has been delivered.';
                        TrackOrder::insert($track_data);

                        $push_title = 'Order Delivered'; 
                        $message = 'Thank you for using RAD - We hope you enjoyed your delivery. To leave a tip and rate your delivery, please access My Orders in the RAD App.'; 
                        $push_data = array(
                            'order_id' => $request['order_id'],
                            'message' => $message,
                            'status' => 10
                        );

                        if($user['notification_status'] == 1 && $device['token'] != ''){
                            $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
                        }
                        notification_add($order['user_id'],$message,$push_type,0,$request['order_id'],0);

                        $phone_no = $order['user']['phone'];
                        $sms_text = 'Thank you for using RAD - We hope you enjoyed your delivery. To leave a tip and rate your delivery, please access My Orders in the RAD App.';
                        send_SMS($phone_no,$sms_text);

                        $client = new Client(new Version2X($this->config->item('socket_url')));
                        $client->initialize();
    
                        $client->emit('delivery_boy_unset_busy',$order);
                        $client->close();

                        $response['device'] = $device;  
                        $response['SendNotification'] = $SendNotification;
                        $response['status'] = true;
                        $response['message'] = 'Order delivered successfully.';
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Order should be pickedup first';
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Something went wrong.';
                }  
            }else{
                $response['status'] = false;
                $response['message'] = 'Order not found.';
            }
            
               
        }
        
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function add_rating_review_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('rate', 'rate', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('review', 'review', 'trim');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $request = array_map('trim', $request);
            if($request['rate'] > 1){
                $stars = 'stars';
            }else{
                $stars = 'star';
            }
            $orders = Orders::with(['delivery_boy'])->where('delivery_boy_id',$request['user_id'])->where('status', 10)->where('id', $request['order_id'])->first();
            if (count($orders) > 0) {
                $orders = $orders->toArray();

                $device = Devices::where('user_id',$orders['user_id'])->first();
                $push_type = 'order_detail';
                $user = User::where('id',$orders['user_id'])->first();
                if ($orders['customer_rate'] == 0) {

                    $order_data['customer_rate'] = $request['rate'];
                    $order_data['customer_review'] = $request['review'];
                    Orders::whereId($request['order_id'])->update($order_data);

                    $push_title = 'Rate and Review for Order #'.$request['order_id'] ;
                    $message = $orders['delivery_boy']['first_name'].' '.$orders['delivery_boy']['last_name'].' gave '.$request['rate'].' '.$stars.' for Order #'.$request['order_id'].'.';
                    $push_data = array(
                        'order_id' => $request['order_id'],
                        'message' => $message 
                    );
                    
                    if($user['notification_status'] == 1 && $device['token'] != ''){
                        $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
                    }
                    notification_add($orders['user_id'],$message,$push_type,0,$request['order_id'],0);

                    $response['SendNotification'] = $SendNotification;
                    $response['message'] = 'Rate & Review successfully submitted.';
                    $response['status'] = true;    

                }else{
                    $response['message'] = 'Rating already submitted.';
                    $response['status'] = false;
                }

            }else{
            	$response['status'] = false;
                $response['message'] = 'No any order found.';
            }
              
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
   
}
?>