<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class WalletManage extends REST_Controller {

	function __construct() {
        header('Content-Type: text/html; charset=utf-8');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
        $this->load->model('PaymentCard');
        $this->load->model('Wallet');
        $this->load->model('Orders');
        $this->load->model('WalletHistory');
		$this->load->model('ion_auth_model');
		$this->load->library(['ion_auth', 'form_validation']);

	}
    public function customAlpha($str) {
        if($str != ''){
            if (!preg_match('/^[a-z \-]+$/i', $str)) {
                $this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
                return false;
            }
        }
        
        return TRUE;
    }
    /* public function add_to_wallet_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required');
        
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $payment_card = PaymentCard::where('user_id',$request['user_id'])->whereId($request['payment_card_id'])->first();
            if(isset($payment_card)){

                $wallet = Wallet::where('user_id',$request['user_id'])->first();

                if(isset($wallet)){
                    $wallet = $wallet->toArray();

                    $update_data = [];
                    $balance = $request['amount'] + $wallet['balance'] ;
                    $update_data['balance'] = number_format((float)$balance, 2, '.', '');

                    Wallet::where('user_id', $request['user_id'])->update($update_data);
                    
                    $wallet_balance = Wallet::where('user_id',$request['user_id'])->first();
                        
                    $transaction['user_id'] = $request['user_id'];
                    $transaction['amount'] = number_format((float)$request['amount'], 2, '.', '');
                    $transaction['type'] = 'plus';
                    $transaction['message'] = 'Money added to wallet';

                    WalletHistory::insert($transaction);

                    $response['wallet_balance'] = $wallet_balance['balance'];
                    $response['status'] = true;
                    $response['message'] = 'Amount added to wallet successfully';
                }else{
                $response['status'] = false;
                $response['message'] = 'Wallet not found.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Payment card not found.';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    } */

    public function my_earnings_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            
            $orders = Orders::select('id','delivery_boy_id','delivery_boy_amount','tip','status','created_at')->where('delivery_boy_id',$request['user_id'])->where('status', 10)->where('db_withdraw_id', 0)->orderBy('id','DESC')->get();

            if(count($orders) > 0){
                $total_earnings = 0;
                foreach ($orders as $key => $value) {
                    $total_earnings += $value['delivery_boy_amount'] + $value['tip'];
                }

                $response['data'] = $orders;
                $response['total_earnings'] = number_format((float)$total_earnings, 2, '.', '');
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No earnings!';
            }
           
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }
    public function add_to_wallet_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required');

        $this->form_validation->set_rules('type', 'type', 'trim|required'); // 1-payment_card_id 2- add_payment_card

        $this->form_validation->set_rules('save', 'save', 'trim'); // 1-save 2- Dont save

        $this->form_validation->set_rules('card_holder_name', 'card holder name', 'trim');
        $this->form_validation->set_rules('card_number', 'card number', 'trim');
        $this->form_validation->set_rules('expiry_date', 'expiry date', 'trim');
        $this->form_validation->set_rules('cvv', 'cvv', 'trim');
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            if($request['type'] == 1){
                if($request['payment_card_id'] != '' && isset($request['payment_card_id'])) {
                    $payment_card = PaymentCard::where('user_id',$request['user_id'])->whereId($request['payment_card_id'])->first();
                    if(isset($payment_card)){

                        
                        $wallet = Wallet::where('user_id',$request['user_id'])->first();
                        
                        if(isset($wallet)){
                            $wallet = $wallet->toArray();
                            
                            //Create Charge Here
                            $charge = TRUE;
                            if($charge == TRUE){
                                $update_data = [];
                                $balance = $request['amount'] + $wallet['balance'] ;
                                $update_data['balance'] = number_format((float)$balance, 2, '.', '');
    
                                Wallet::where('user_id', $request['user_id'])->update($update_data);
                                
                                $wallet_balance = Wallet::where('user_id',$request['user_id'])->first();
                                    
                                $transaction['user_id'] = $request['user_id'];
                                $transaction['amount'] = number_format((float)$request['amount'], 2, '.', '');
                                $transaction['type'] = 'plus';
                                $transaction['message'] = 'Money added to wallet';
    
                                WalletHistory::insert($transaction);
    
                                $response['wallet_balance'] = $wallet_balance['balance'];
                                $response['status'] = true;
                                $response['message'] = 'Amount added to wallet successfully';
                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Something went wrong with stripe payment card.';
                            }
                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Wallet not found.';
                        }
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Payment card not found.';
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Payment card id not found.';
                }
            }elseif($request['type'] == 2){
                if($request['card_holder_name'] != '' && $request['card_number'] != '' && $request['expiry_date'] != '' && $request['cvv'] != ''){
                    $card_number = $request['card_number'];
                    $card_number = str_replace(' ', '', $card_number);
        
                    $card_type = validate_customer_card($card_number);
                    // $card_type = 1;
        
                    if($card_type != ''){
        
                            $payment_card_data = array(
                                'user_id' => $request['user_id'],
                                'card_holder_name' => $request['card_holder_name'],
                                'card_number' => encrypt($card_number),
                                'display_number' => 'XXXX XXXX XXXX '.substr($card_number, -4),
                                'expiry_date' => encrypt($request['expiry_date']),
                                'cvv' => encrypt($request['cvv']),
                                'card_type' => $card_type,
                            );
        
                            $card_data = array(
                                'Fullname' => $request['card_holder_name'],
                                'credit_card_no' => $card_number,
                                'security_code' => $request['cvv'],
                                'expiry_date' => $request['expiry_date']
                                    );
        
                            $this->load->helper("stripe_helper");
        
                            $create_token = custom_create_token($card_data);
                            
                            //for saving card
                            if(isset($create_token['status']) && $create_token['status'] == TRUE){
                                    if($request['save'] ==  1){
                                        PaymentCard::insert($payment_card_data);
                                    }
                                    // Create Charge Here

                                    $charge = TRUE;

                                    $wallet = Wallet::where('user_id',$request['user_id'])->first();
                                    if(isset($wallet)){
                                        $wallet = $wallet->toArray();
                                        if($charge == TRUE){
                                            $update_data = [];
                                            $balance = $request['amount'] + $wallet['balance'] ;
                                            $update_data['balance'] = number_format((float)$balance, 2, '.', '');
                
                                            Wallet::where('user_id', $request['user_id'])->update($update_data);
                                            
                                            $wallet_balance = Wallet::where('user_id',$request['user_id'])->first();
                                                
                                            $transaction['user_id'] = $request['user_id'];
                                            $transaction['amount'] = number_format((float)$request['amount'], 2, '.', '');
                                            $transaction['type'] = 'plus';
                                            $transaction['message'] = 'Money added to wallet';
                
                                            WalletHistory::insert($transaction);
                
                                            $response['wallet_balance'] = $wallet_balance['balance'];
                                            $response['status'] = true;
                                            $response['message'] = 'Amount added to wallet successfully';
                                        }else{
                                            $response['status'] = false;
                                            $response['message'] = 'Something went wrong with stripe payment card.';
                                        }
                                        
                                    }else{
                                        $response['status'] = false;
                                        $response['message'] = 'Wallet not found.';
                                    }
                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Server encountered an error. please try again';
                            }
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Invalid card number';
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Please enter all the details to add the card';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Please enter valid type.';
            }
            

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function send_money_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required');
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $user = User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','4')->where('phone',$request['phone'])->first();
            $user_sender = User::whereId($request['user_id'])->first();

            if(isset($user)){
                $wallet = Wallet::where('user_id',$request['user_id'])->first();
                $userwallet = Wallet::where('user_id',$user['id'])->first();
                if(isset($wallet) && isset($userwallet)){
                    $wallet = $wallet->toArray();
                    $userwallet = $userwallet->toArray();
  
                    //for sender
                    $update_data = [];
                    $balance = $wallet['balance'] - $request['amount'] ;
                    $update_data['balance'] = number_format((float)$balance, 2, '.', '');

                    Wallet::where('user_id', $request['user_id'])->update($update_data);
                    
                    $wallet_balance = Wallet::where('user_id',$request['user_id'])->first();
                         
                    $transaction['user_id'] = $request['user_id'];
                    $transaction['amount'] = number_format((float)$request['amount'], 2, '.', '');
                    $transaction['type'] = 'minus';
                    $transaction['message'] = 'Send to '.$request['phone'];

                    WalletHistory::insert($transaction);

                    //for reciever
                    $update_data_receiver = [];
                    $balance = $userwallet['balance'] + $request['amount'] ;
                    $update_data_receiver['balance'] = number_format((float)$balance, 2, '.', '');

                    Wallet::where('user_id', $user['id'])->update($update_data_receiver);
                    
                    $transaction_receiver['user_id'] = $user['id'];
                    $transaction_receiver['amount'] = number_format((float)$request['amount'], 2, '.', '');
                    $transaction_receiver['type'] = 'plus';
                    $transaction_receiver['message'] = 'Received from '.$user_sender['phone'];

                    WalletHistory::insert($transaction_receiver);


                    $response['wallet_balance'] = $wallet_balance['balance'];
                    $response['status'] = true;
                    $response['message'] = 'Send money successfully';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Wallet not found.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'User not found.';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function wallet_history_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $data = [];
            $wallet_history = WalletHistory::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();
            if (count($wallet_history) > 0){
                $wallet_history = $wallet_history->toArray();
                
                $wallet = Wallet::where('user_id',$request['user_id'])->first();
                $data['wallet_balance'] = $wallet['balance'];
                $data['wallet_history'] = $wallet_history;

                $response['data'] = $data;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No any history.';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

	public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
}
?>