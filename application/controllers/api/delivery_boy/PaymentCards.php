<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class PaymentCards extends REST_Controller {

    function __construct() {
        header('Content-Type: text/html; charset=utf-8');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('User');
        $this->load->model('PaymentCard');
        $this->load->model('ion_auth_model');
        $this->load->library(['ion_auth', 'form_validation']);

    }
    public function customAlpha($str) {
        if($str != ''){
            if (!preg_match('/^[a-z \-]+$/i', $str)) {
                $this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
                return false;
            }
        }
        
        return TRUE;
    }
    public function add_payment_card_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('card_holder_name', 'card holder name', 'trim|required');
        $this->form_validation->set_rules('card_number', 'card number', 'trim|required');
        $this->form_validation->set_rules('expiry_date', 'expiry date', 'trim|required');
        $this->form_validation->set_rules('cvv', 'cvv', 'trim|required');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $card_number = $request['card_number'];
            $card_number = str_replace(' ', '', $card_number);

            $card_type = validate_customer_card($card_number);
            // $card_type = 1;

            if($card_type != ''){

                    $payment_card_data = array(
                        'user_id' => $request['user_id'],
                        'card_holder_name' => $request['card_holder_name'],
                        'card_number' => encrypt($card_number),
                        'display_number' => 'XXXX XXXX XXXX '.substr($card_number, -4),
                        'expiry_date' => encrypt($request['expiry_date']),
                        'cvv' => encrypt($request['cvv']),
                        'card_type' => $card_type,
                    );

                    $card_data = array(
                        'Fullname' => $request['card_holder_name'],
                        'credit_card_no' => $card_number,
                        'security_code' => $request['cvv'],
                        'expiry_date' => $request['expiry_date']
                            );

                    $this->load->helper("stripe_helper");

                    $create_token = custom_create_token($card_data);
                   
                    // $create_token['status'] = TRUE;

                    $data = PaymentCard::where('user_id',$request['user_id'])->where('default', '1')->first();
                    if(count($data) == 0){
                        $payment_card_data['default'] = 1; 
                    }
                    if(isset($create_token['status']) && $create_token['status'] == TRUE){
                        if(PaymentCard::insert($payment_card_data)){
                            $response['status'] = true;
                            $response['message'] = 'Card added successfully';
                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Server encountered an error. please try again';
                        }
                    }else{
                        $response['status'] = false;
                        $response['message'] = $create_token['error_title'];
                        $response['create_token'] = $create_token;
                    }
            }else{
                $response['status'] = false;
                $response['message'] = 'Invalid card number';
            }
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function get_payment_cards_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $payment_cards = [];
            $payment_cards = PaymentCard::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();
            if(count($payment_cards) > 0){
                foreach ($payment_cards as $key => $value) {
                    $payment_cards[$key]['expiry_date'] = decrypt($value['expiry_date']);
                }
                $response['data'] = $payment_cards;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No any payment card found.';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function delete_payment_card_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            
            // $data = array('deleted_at' => date('Y-m-d H:i:s'));

            if(PaymentCard::whereId($request['payment_card_id'])->where('user_id',$request['user_id'])->delete()){
                $response['status'] = true;
                $response['message'] = 'Card removed successfully';
            }else{
                $response['status'] = false;
                $response['message'] = 'Server encountered an error. please try again';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function add_default_payment_card_post(){
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $update_data = array('default' => 0);
            PaymentCard::where('user_id',$request['user_id'])->where('default', '1')->update($update_data);

            $data = array('default' => 1);
            
            if(PaymentCard::whereId($request['payment_card_id'])->update($data)){
                $payment_cards = PaymentCard::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();
                $response['data'] = $payment_cards;
                $response['status'] = true;
                $response['message'] = 'Default payment card set successfully';
            }else{
                $response['status'] = false;
                $response['message'] = 'Server encountered an error. please try again';
            }
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function get_default_payment_card_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $data = PaymentCard::where('user_id',$request['user_id'])->where('default', '1')->first();

            if(count($data) > 0){
                $data['expiry_date'] = decrypt($data['expiry_date']);
                
                $response['data'] = $data;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No any default payment card found.';
            }
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }



    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
}
?>