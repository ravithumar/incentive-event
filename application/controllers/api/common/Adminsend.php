<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class Adminsend extends REST_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Usergroup');
        $this->load->model('Chats');
        $this->load->model('Orders');
        $this->load->model('ion_auth_model');
        $this->load->library(['ion_auth', 'form_validation']);

    }


    public function send_push_post(){
        $group=$this->db->from('groups')->where('name','admin')->get()->row();
        $adminGroupId=$group->id;
        $userIds=Usergroup::where('group_id',$adminGroupId)->pluck('user_id')->toArray();
        $request =$this->post();
        $orderid = $request["order_id"];
      /*  _pre($orderid);*/
        for($i=0;$i<count($userIds);$i++){

            $user=User::where('id',$userIds[$i])->first();
            $userPhone=$user->phone;
            $userEmail=$user->email;    
           /* var_dump($userPhone,$userEmail);*/

          /*  $admin = User::where('id', $request['order_id'])->first(); */   
            $phone_no =  $userPhone;
            $sms_text = 'Order ID: #'.$_POST['order_id'].' requires delivery person to be assigned. Kindly assign a RAD Driver from the Assign Delivery Person located in the Admin Panel.';
            send_SMS($phone_no,$sms_text); 
            /*Order ID: #123 requires delivery person to be assigned. Kindly assign a RAD Driver from the Assign Delivery Person located in the Admin Panel.*/

        
            $template = file_get_contents(base_url('email_templates/order_assignment.html'));
            $message  = create_email_template_new($template);
            $orderiduser = $request["order_id"];
            $message = str_replace('##ORDERIDUSER##', $orderid, $message);
            $subject = $this->config->item('site_title', 'ion_auth').' Delivery Person';
            $to = $userEmail;
            $sent = send_mail_new($to,$subject, $message);
        
        }
      /*  exit();*/
        
        

    }


}