<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		//echo "ee";echo '<pre>'; print_r($this->session->all_userdata());exit;
		if (!$this->ion_auth->is_web()) {
			redirect('/organizer/login');
		}
		$this->load->model('Product');
		$this->load->model('Orders');
		$this->title = 'Dashboard';
	}

	public function index(){
		// echo '<pre>'; print_r($this->session->all_userdata());exit;
		$id = $this->session->userdata['organizer']['user_id'];
        $this->load->library('breadcrumbs');
        $data['title'] = $this->title;
        // $data['total_products'] = Product::where('user_id',$id)->count();
		
		$net_profit = $revenue = 0;

        $data['net_profit'] = $net_profit;
        $data['revenue'] = $revenue;
        // $data['total_orders'] = count($orders);

        $this->web_render('dashboard', $data);    
    }

}