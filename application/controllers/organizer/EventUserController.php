<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EventUserController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_organizer()) {
			redirect('/organizer/login');
		}
		
		$this->load->model('Event');
		$this->load->model('EventUser');
		$this->load->model('User');
		$this->title = 'Event User';
		
	}

	public function index()
	{

		// _pre('fd');
		$id = $this->session->userdata['organizer']['user_id'];
		// _pre($id);
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		// _pre($dt_data);
		// $dt_data->select('event_users.*', false)->from('event_users')->where('event_users.user_id', $id)->where('event_users.deleted_at', NULL);

		$dt_data->select('event_users.*,count(event_users.user_id) as num,events.name', false)
		->from('event_users')
		->group_by('event_users.event_id')
		->join('events', 'events.id = event_users.event_id')
		->where('event_users.deleted_at', NULL);


		$action['edit'] = base_url('organizer/eventuser/edit/');
		$action['view'] = base_url('organizer/eventuser/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Event', 'name')
			->column('Total User','num')
			
			->column('Action','id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">Edit</a>';
				$option .= " <button type='button' data-table='event_users' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				// $option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light ml-1">View</a>';
				return $option;
			});

		$dt_data->searchable('name');
		$dt_data->datatable('event_users');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'organizer/eventuser');
		$data['add_url'] = base_url('organizer/eventuser/add');
        
		$this->organizer_render('eventuser/index',$data);	
	}
	public function add(){
		
		// _pre('helo');
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				
				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					array('field' => 'event_id', 'label' => 'Event', 'rules' => 'required'),
					// array('field' => 'user_id', 'label' => 'User', 'rules' => 'required'),
					
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){
					
					$request = $this->input->post();
                	
                	foreach($request['user_id'] as $data){
                		$user_data['user_id'] = $data;
                		$user_data['event_id'] = $request['event_id'];
                		$user_data['status'] = 1;
                		$insert_event=EventUser::insertGetId($user_data);
                	}
                	// echo $insert_event;die;
              
					$this->session->set_flashdata('success','Event added successfully');
					redirect(base_url("organizer/eventuser"));

				}
			}
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'organizer/eventuser');
        $this->breadcrumbs->push('Add', '/', true);
        $output_data['event'] = Event::get(['name','id']);
        $output_data['user'] = User::get(['first_name','id']);
        // _pre($output_data['user']);
		$this->organizer_render('eventuser/post', $output_data);
	}
	public function edit($id = ''){

		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){

				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					
					// array('field' => 'user_id', 'label' => 'User', 'rules' => 'trim|required'),
					array('field' => 'event_id', 'label' => 'Event', 'rules' => 'trim|required'),
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {

                    $request = $this->input->post();
                    EventUser::where('event_id',$request['event_id'])->delete();
                    foreach($request['user_id'] as $data){
                		$user_data['user_id'] = $data;
                		$user_data['event_id'] = $request['event_id'];
                		$user_data['status'] = 1;
                		$insert_event=EventUser::insertGetId($user_data);
                	}

                    	$this->session->set_flashdata('success', 'Event User updated successfully');
						redirect(base_url('organizer/eventuser'));

				} 
			}	
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'organizer/eventuser');
        $this->breadcrumbs->push('Edit', '/', true);

		$output_data['eventuser'] = EventUser::find($id);
        $output_data['eventuserid'] =EventUser::where('event_id',$output_data['eventuser']->event_id)->pluck('user_id')->toArray();

		// _pre($output_data['eventuser']);
		$output_data['event'] = Event::get(['name','id']);
        $output_data['user'] = User::get(['first_name','id']);
		// _pre($output_data['event']);
		$this->organizer_render('eventuser/put', $output_data);
	}

	public function view($id = ''){
		
		$output_data['title'] = $this->title;
		$output_data['eventuser'] = EventUser::with(['user','event'])->find($id);
		// _pre($output_data['eventuser']->toArray());
		// if(isset($output_data['eventuser'])){
  //           $output_data['eventuser'] = $output_data['eventuser']->toArray();
  //       }

		if(!isset($output_data['eventuser']) && count($output_data['eventuser']) == 0){
			$this->session->set_flashdata('error', 'No any product found!');
			redirect(base_url('organizer/eventuser'));
		}
		// /_pre($output_data);		
		$this->breadcrumbs->push($this->title , 'organizer/eventuser');
        $this->breadcrumbs->push('View', '/', true);
		$this->organizer_render('eventuser/view', $output_data);
	}

}
?>
