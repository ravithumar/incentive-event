<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_organizer()) {
			redirect('/organizer/login');
		}
		$this->load->model('System_setting');
		$this->load->model('User');

		$this->title = 'Admin Contact';
	}

	public function index()
	{
		$settings = System_setting::all()->toArray();
		$data["settings"] = $settings;

		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'organizer/contact');
		$this->organizer_render('contact',$data);	
	}

}
?>
