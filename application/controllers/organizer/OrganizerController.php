<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class OrganizerController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		/* if (!$this->ion_auth->is_organizer()) {
			redirect('/organizer/login');
		} */
		$this->load->model('User');
		$this->load->model('Category');
		$this->load->model('VendorAvailibility');
		$this->load->model('Vendor');
		$this->load->model('ion_auth_model');

		$this->title = 'Event Organizer';
	}

	public function change_password(){
		if (!$this->ion_auth->is_organizer()) {
			redirect('/organizer/login');
		}
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
				$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]');
				$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

				 
				if ($this->form_validation->run() === true){
					$request = $this->input->post();


					$identity =  $this->session->userdata['organizer']['identity'];
					$id =  $this->session->userdata['organizer']['user_id'];

					// _pre($this->session->userdata());
					$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'),$id);

					if($change){
						$this->session->set_flashdata('success', $this->ion_auth->messages());
					}else{
						$this->session->set_flashdata('error', $this->ion_auth->errors());
					}
					redirect(base_url('organizer/change-password'));
				}

			}
		}

		$output_data['title'] = 'Change Password';
		$output_data['change_pw_link'] = base_url('organizer/change-password');
		$this->breadcrumbs->push('Change Password' , 'organizer/change-password');
		$this->organizer_render('change_password',$output_data);
	}

	public function my_profile_update($id = ''){
		
		if (!$this->ion_auth->is_organizer()) {
			$this->organizer_render('my_profile',$data);
		}
		$id =  $this->session->userdata['organizer']['user_id'];

		$data['title'] = 'Update Profile';
        $this->breadcrumbs->push($this->title , 'organizer/my-profile');
        $this->breadcrumbs->push('Edit', '/', true);
	 	$data['organizer'] = User::find($id);
		// _pre($_POST);
		$this->load->library(['ion_auth', 'form_validation']);
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){
				$validation_rules = array(
					
					array('field' => 'first_name', 'label' => 'first name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'last_name', 'label' => 'last name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					
				);
				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();
					
					$user_data = [
							'first_name' => ucfirst($request["first_name"]),
							'last_name' => ucfirst($request["last_name"]),
						];
					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
                		$img_name = 'organizer_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
					        $user_data['profile_picture'] = $img_name;
					        if($request["old_profile_picture"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_profile_picture"]);
					        }
					        
					    }
					    $file_upload = true;
                	}
                	

                	// _pre($user_data);

                	$availability = array();
				

					User::whereId($request['user_id'])->update($user_data);

					$data = array(
						'id' =>  $request["user_id"],
						'first_name' => ucfirst($request["first_name"]),
						'last_name' => ucfirst($request["last_name"]),
						
					);

					User::whereId($request['user_id'])->update($data);

					$this->session->set_flashdata('success','Profile updated successfully');
						redirect(base_url("organizer/my-profile"));
				} 
			}
		}else{
			// $this->session->set_flashdata('error', 'Something went wrong');
			// redirect(base_url() . "organizer/my-profile");
			$this->organizer_render('my_profile',$data);
		}
	}

	public function my_profile(){

		if (!$this->ion_auth->is_organizer()) {
			redirect('/organizer/login');
		}
		$id =  $this->session->userdata['organizer']['user_id'];

		$data['title'] = 'Update Profile';
        $this->breadcrumbs->push($this->title , 'organizer/my-profile');
        $this->breadcrumbs->push('Edit', '/', true);
	 	$data['organizer'] = User::find($id);
	 // _pre($data['organizer']);
		// $data['category'] = Category::get();

		if(isset($data['organizer'])){
            $data['organizer'] = $data['organizer']->toArray();
        }
        /* if(isset($data['category'])){
            $data['category'] = $data['category']->toArray();
        } */
		//_pre($data);
		$this->organizer_render('my_profile',$data);

	}

	public function forgot_password(){

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('identity', 'email', 'required');
				 
                if ($this->form_validation->run() === true) {
                    $request = $this->input->post();

                    $user = User::select('users.*')->where('email', $request['identity'])->join('users_groups', 'users_groups.user_id', 'users.id')->whereIn('users_groups.group_id', [2])->first();
                    //forgot pass

                    if (isset($user)) {
                        $forgotten = $this->ion_auth_model->forgotten_password_vendor($request['identity'], $user->id);
                        $data = [
							'identity'   => $user->email,
							'id'         => $user->id,
							'email'      => $user->email,
							'forgotten' => $forgotten ,
                    	];
						
						$this->lang->load('ion_auth');
                        $message = $this->load->view('email_templates/reset_password_vendor', $data, true);
						$subject = $this->config->item('site_title', 'ion_auth') . ' -  Reset Password';
						$to_email = $user->email;
						send_mail_new($to_email, $subject, $message);
						$this->session->set_flashdata('success', 'Reset password link sent to your email.');

                        /* $this->load->library(['email']);
                        $this->lang->load('ion_auth');
                    
                        $email_config = $this->config->item('email_config', 'ion_auth');

                        $this->email->initialize($email_config);
                        $message = $this->load->view('email_templates/reset_password_vendor', $data, true);
                    
                        $this->email->set_newline("\r\n");
                        $this->email->from('ruralareadelivery@gmail.com', "RAD");
                        $this->email->to($user->email);
                        $this->email->reply_to('info@radservices.ca', "RAD");
                        $this->email->subject($this->config->item('site_title', 'ion_auth') . ' -  Reset Password');
                        $this->email->message($message);

                        if ($this->email->send() === true) {
                            $this->session->set_flashdata('success', 'Reset password link sent to your email.');
                        } else {
                            $this->session->set_flashdata('error', 'Something went wrong');
                        } */
                        redirect(base_url('organizer/login'));
                    }else{
						$this->session->set_flashdata('error', 'This account does not Exists!');
						redirect(base_url('organizer/forgot-password'));
					}
                }
			}
		}
		$data['user_type'] = "vendor";
		$this->load->view('auth/forgot_password',$data);
	}
}