<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';
use ElephantIO\Client;
// use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Engine\SocketIO\Version2X;
use SebastianBergmann\Environment\Console;

class Common extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('Product');
		$this->load->model('Devices');
		$this->load->model('TrackOrder');
		$this->load->model('User');
		$this->load->model('System_setting');
		$this->load->model('Orders');
		$this->load->model('WalletHistory');
		$this->load->model('Wallet');
		$this->load->model('VendorChannels');

	}

	public function active_deactive(){
		if($_POST['table'] == 'users'){
			if($_POST['status'] == 1){
				$status = 2;
			}else{
				$status = 1;
			}
			$this->db->where('id',$_POST['id'])->update($_POST['table'],['active'=>$status]);

			$this->db->where('user_id', $_POST['id']);
			$this->db->delete('tokens');

			$user_data = $this->db->query("SELECT * FROM users_groups WHERE user_id = '".$_POST['id']."'")->row_array();
			/*_prea($user_data);*/
			if(!empty($user_data)) {
				if($user_data["group_id"] == "4") {
					if($status == "2") {
						$this->db->where('id',$_POST['id'])->update($_POST['table'],['duty'=>"0"]);

						$client = new Client(new Version2X($this->config->item('socket_url')));
		                $client->initialize();
		                
		                $client->emit('end_duty', ['driver_id' => (string)$_POST['id']]);
		                $client->close();

		                $user_device_data = $this->db->query("SELECT * FROM devices WHERE user_id = '".$_POST['id']."'")->row_array();
		                if(!empty($user_device_data)) {
                            $push_title = 'logout';
                            $push_data = array(
                                'user_id' => $_POST['id'],
                                'message' => 'logout'
                            );
                            $push_type = 'silent';

                            $SendNotification = SendNotification($user_device_data["type"],$user_device_data["token"], $push_title, $push_data, $push_type);
                        }
					}
				}
			}
			$this->db->where('user_id',$_POST['id'])->update('devices',['token'=>'']);
		}else{
			if($_POST['status'] == 1){
				$status = 0;
			}else{
				$status = 1;
			}
			$this->db->where('id',$_POST['id'])->update($_POST['table'],['status'=>$status]);
		}
		echo $status;
	}

	public function reject_db(){
		
		$this->db->where('id',$_POST['id'])->update('users',['active'=>3]);
		$sms_text = 'Your account has been rejected by admin. Contact team for more details.';
		send_SMS($_POST['phone'],$sms_text);
		echo 1;
	}

	public function accept_db(){
		
		$this->db->where('id',$_POST['id'])->update('users',['active'=>1]);
		$sms_text = 'Your account has been successfully activated. Thank you for choosing RAD!';
		send_SMS($_POST['phone'],$sms_text);

		echo 1;
	}
	public function change_status(){
		if($_POST['status'] == 1){
			$status = 2;
		}else{
			$status = 3;
		}
		$this->db->where('id',$_POST['id'])->update('report_issue',['status'=>$status]);
		echo 1;
	}
	
	public function featured(){

		$total_no = System_setting::select('value')->where('path','total_no_of_feature_product')->first();
		

		$feature_total = Product::where('user_id',$_POST['user_id'])->where('featured',1)->count();

		if($_POST['status'] == 1){
			$update_data = ['featured' => 0, 'featured_number' => 0];
			$status = 1;
			$this->db->where('id',$_POST['id'])->update('product', $update_data);
		}else{
			if($total_no['value'] > $feature_total){
				$sql_query = $this->db->query("SELECT MAX(featured_number) as max_num FROM `product` where deleted_at is null and featured = 1");
			    $max = 0;
			    if ($sql_query->num_rows() > 0){
			    	$result = $sql_query->row_array();
			    	$max = $result['max_num'] + 1;
			    }
				$update_data = ['featured' => 1, 'featured_number' => $max];
				$this->db->where('id',$_POST['id'])->update('product', $update_data);
				$status = 1;
			}else{
				$status = 0;
			}
		}
		echo json_encode(array('data' => $status ,'total_no' =>$total_no['value'] ,'feature_total' => $feature_total));
		return true;
	}

	public function delete()
	{

		if($_POST['table'] == 'product'){
			// Product::whereId($_POST['id'])->delete();
			// ProductRequest::whereProductId($_POST['id'])->whereIn('status',[1,2,3,4,5])->delete();
			// Cart::whereProductId($_POST['id'])->where('order_id',0)->delete();
		}else if($_POST['table'] == 'users'){
			User::whereId($_POST['id'])->delete();
			// $products = Product::where('user_id',$_POST['id'])->get();

			$this->db->where('user_id', $_POST['id']);
			$this->db->delete('tokens');

			$this->db->where('user_id',$_POST['id'])->update('devices',['token'=>'']);
			
			// if(count($products) > 0){
			// 	$products = $products->toArray();
			// 	$product_ids = array_column($products,'id');

			// 	// ProductRequest::whereIn('product_id',$product_ids)->whereIn('status',[1,2,3,4,5])->delete();
			// 	// Cart::whereIn('product_id',$product_ids)->where('order_id',0)->delete();

			// 	// Product::where('user_id',$_POST['id'])->delete();
			// }
			
			
		}else{
			if($_POST['table'] == 'users')
			{
				$this->db->where('id',$_POST['id'])->delete('users');
			}
			else
			{

			$this->db->where('id',$_POST['id'])->update($_POST['table'],['deleted_at'=>date('Y-m-d H:i:s')]);
			}
		}
		
		echo 1;
	}

	public function get_track_data(){
		$order_data = TrackOrder::where("order_id",$_POST['order_id'])->get();
		// _pre($order_data);
		echo json_encode(array('data' => $order_data));
		return true;
	}
	public function accept_order(){
		$delivery_person_response_time = System_setting::select('value')->where('path','delivery_person_response_time')->first();
		$add_time = $delivery_person_response_time['value'];
		$time = new DateTime();
        $time->add(new DateInterval('PT' . $add_time . 'M'));
        $created_time = $time->format('Y-m-d H:i:s'); 

		if($_POST['ordertype'] == 1){
			$update_data = ['status' => $_POST['status'], 'db_request_sent_at' => date('Y-m-d H:i:s'), 'accept_before' => $created_time];
		}else{
			$update_data = ['status' => $_POST['status']];
		}
		Orders::where('id', $_POST['id'])->update($update_data);

		$orders = Orders::with(['vendor','orderproducts','user','delivery_boy'])->where('id',$_POST['id'])->first();
        if (isset($orders)) {
            $orders = $orders->toArray();
			$store_name = $orders['vendor']['name'];
        }

		if($_POST['status'] == 2){
			
			$message = 'Order #'.$_POST['id'].' confirmed.';
			$push_data = array(
                'order_id' => $_POST['id'],
                'message' => 'Your order #'.$_POST['id'].' has been confirmed by '.$store_name.'.'

            );

            if($orders['order_type'] == '1' || $orders['order_type'] == '3'){
				if (isset($orders)) {
					$client = new Client(new Version2X($this->config->item('socket_url')));
					$client->initialize();	                
					$client->emit('forward_booking_request', $orders);
				}
            	$sms_text = 'Order #'.$_POST['id'].' - Thank you for ordering with '.$store_name.'. You will be updated as soon as your order is on its way.';
            }else{
            	$sms_text = 'Order #'.$_POST['id'].' - Thank you for ordering with '.$store_name.'. You will be updated as soon as your order is ready for pickup.';
            }
            $current_datetime = date("Y-m-d H:i:s");
            $track_data['order_id'] = $_POST['id'];
            $track_data['status'] = 2;
            $track_data['created_at'] = $current_datetime;
            $track_data['status_title'] = 'Accepted By Store';
            $track_data['status_text'] = 'Store has confirmed the order.';
            TrackOrder::insert($track_data);


			//SEND NOTIFICATION TO DELIVERY PERSONS

			$user = User::select('users.*')
			->join('users_groups', 'users_groups.user_id', 'users.id')
			->whereIn('users_groups.group_id',[4])
			->where('users.duty','1')
			->with(['device'])
			->get();
			
			$delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();
			
			/* if(count($user) > 0){
				$user = $user->toArray();
				foreach ($user as $key => $value) {
					
					$delivery_miles = get_delivery_miles($orders['vendor']['latitude'],$orders['vendor']['longitude'],$value['latitude'], $value['longitude']);
					
                    if ($delivery_miles['status'] == true) {
						$user[$key]['miles'] = $delivery_miles['distance'];
                        if ($delivery_miles['distance'] <= $delivery_miles_available['value']) {
                            $device_token = $value['device']['token'];
                            $device_type = $value['device']['type'];
                            $push_title_db = 'New request of delivery';
                            $push_data_msg = 'Hey '.$value['first_name'].'! ';
                            $push_data_msg .= 'You got new order delivery request.';
                            $push_data_db = array(
								'order_id' => $_POST['id'],
								'message' => $push_data_msg
							);
                            $push_type_db = 'request';

                            if ($value['notification_status'] == 1 && $device_token != '') {
                                $SendNotification = SendNotification($device_type, $device_token, $push_title_db, $push_data_db, $push_type_db);
                            }

                            notification_add($value['id'], $push_data_msg, $push_type_db, 0, $_POST['id'], 0);
                        }
                    }
				}
				
			} */

			
			

		}else{

			if($orders['payment_mode'] == 1){
				$this->load->helper("stripe_helper");
				$refund_stripe = refund_stripe($orders['transaction_id'],$orders['total_amount']);
                //refund_stripe($orders['transaction_id']);
            }else{
                $update_wallet = [];
                $balance = $orders['total_amount'] + $orders['user_balance'] ;
                $update_wallet['balance'] = number_format((float)$balance, 2, '.', '');

				Wallet::where('user_id', $orders['user_id'])->update($update_wallet);

				$transaction['user_id'] = $orders['user_id'];
				$transaction['order_id'] = $orders['id'];
				$transaction['amount'] = number_format((float)$orders['total_amount'], 2, '.', '');
				$transaction['type'] = 'plus';
				$transaction['message'] = 'Payment refunded to wallet for order #'.$orders['id'];
				WalletHistory::insert($transaction);  
                
                
            }

			$message = 'Order #'.$_POST['id'].' rejected.';
			$push_data = array(
                'order_id' => $_POST['id'],
                'message' => 'Your order #'.$_POST['id'].' has been rejected by '.$store_name.'.'
            );

			$sms_text = 'Your order #'.$_POST['id'].' has been canceled by '.$store_name.' . We apologize for the inconvenience caused. The payment will be auto refunded.';

			/*email cancel requst start*/
			$orderDetails = $this->db->from('orders')->where('id', $_POST['id'])->get()->row();
			$userDetailsGrab=User::whereId($orderDetails->user_id)->first();
			$userEmailGrab=$userDetailsGrab->email;
			$template = file_get_contents(base_url('email_templates/cancelation_order.html'));
			$mail_message  = create_email_template_new($template);
			$orderid = $_POST['id'];
			$mail_message = str_replace('##ORDERID##', $orderid, $mail_message);
			$subject = $this->config->item('site_title', 'ion_auth').' Request Cancelled';
			$to = $userEmailGrab;
			$sent = send_mail_new($to,$subject, $mail_message);
			/*email cancel requst end*/


			$current_datetime = date("Y-m-d H:i:s");
            $track_data['order_id'] = $_POST['id'];
            $track_data['status'] = 3;
            $track_data['created_at'] = $current_datetime;
            $track_data['status_title'] = 'Order Cancelled';
            $track_data['status_text'] = 'Your order has been cancelled by the store. We are sorry.';
            TrackOrder::insert($track_data);
		}
		$push_type = 'store_accept_reject';

		$device = Devices::where('user_id',$orders['user_id'])->first();
		$user = User::find($orders['user_id']);

		if($user['notification_status'] == 1 && $device['token'] != ''){
			$SendNotification = SendNotification($device['type'],$device['token'], $message, $push_data, $push_type);
		}
		notification_add($orders['user_id'],$message,$push_type,0,$orders['id'],0);

		$phone_no = $user['phone'];
        
        send_SMS($phone_no,$sms_text);

		echo $SendNotification;
		echo 1;
	}

	/* public function test_socket(){
		$orders = Orders::with(['vendor','orderproducts','user','delivery_boy'])->where('id','88')->first();
		if (isset($orders)) {
            $orders = $orders->toArray();
        }
		
	} */

	public function completed()
	{
		$this->db->where('id',$_POST['id'])->update('orders',['status'=> 10 , 'completed_at' => date('Y-m-d H:i:s')]);

        $message = 'Order Completed';
		$push_data = array(
            'order_id' => $_POST['id'],
            'message' => 'Thank you for using rad - We hope you enjoy your takeaway'
        );
        $push_type = 'order_reach_picked_up_delivered';

		$device = Devices::where('user_id',$_POST['userid'])->first();
		$user = User::find($_POST['userid']);

		if($user['notification_status'] == 1 && $device['token'] != ''){
			$SendNotification = SendNotification($device['type'],$device['token'], $message, $push_data, $push_type);
		}
		notification_add($_POST['userid'],$message,$push_type,0,$_POST['id'],0);

		$phone_no = $user['phone'];
		$sms_text = 'Thank you for using rad - We hope you enjoy your takeaway.';
        
        send_SMS($phone_no,$sms_text);
        $current_datetime = date("Y-m-d H:i:s");
		$track_data['order_id'] = $_POST['id'];
        $track_data['status'] = 9;
        $track_data['created_at'] = $current_datetime;
        $track_data['status_title'] = 'Picked Up';
        $track_data['status_text'] = 'Your order has been picked up successfully.';
        TrackOrder::insert($track_data);
		echo 1;
	}

	public function admin_cancel_order(){

		$orders = Orders::with(['vendor','orderproducts','user','delivery_boy'])->where('id',$_POST['id'])->first();
        if (isset($orders)) {
            $orders = $orders->toArray();
            $order_status_allowed = array('1', '2', '5', '6', '7', '8', '9');
            if(in_array($orders['status'], $order_status_allowed)){
            	$store_name = $orders['vendor']['name'];

            	$refunded = FALSE;
            	$error_msg = '';

            	if($orders['payment_mode'] == '1'){
					$this->load->helper("stripe_helper");
					$refund_stripe = refund_stripe($orders['transaction_id']);
					if($refund_stripe['status'] == TRUE){
						$refunded = TRUE;
					}else{
						$error_msg = $refund_stripe['error'];
					}
		        }else{
		            $update_wallet = [];
		            $balance = $orders['total_amount'] + $orders['user_balance'] ;
		            $update_wallet['balance'] = number_format((float)$balance, 2, '.', '');

					Wallet::where('user_id', $orders['user_id'])->update($update_wallet);

					$transaction['user_id'] = $orders['user_id'];
					$transaction['order_id'] = $orders['id'];
					$transaction['amount'] = number_format((float)$orders['total_amount'], 2, '.', '');
					$transaction['type'] = 'plus';
					$transaction['message'] = 'Payment refunded to wallet for order #'.$orders['id'];
					WalletHistory::insert($transaction);  

					$refunded = TRUE;
		    
		        }

		        if($refunded == TRUE){

		       		//Update order status
		        	$update_data = ['status' => 4, 'reason' => 'Admin cancelled', 'admin_amount' => 0, 'store_amount' => 0, 'delivery_boy_amount' => 0];
					Orders::where('id', $_POST['id'])->update($update_data);
					$current_datetime = date("Y-m-d H:i:s");
					//Add tracking data
					$track_data['order_id'] = $_POST['id'];
			        $track_data['status'] = 4;
			        $track_data['created_at'] = $current_datetime;
			        $track_data['status_title'] = 'Cancelled By RAD';
			        $track_data['status_text'] = 'Administrator cancelled order.';
			        TrackOrder::insert($track_data);

			        $message = 'Order #'.$_POST['id'].' has been cancelled by RAD.';
			        $push_title = 'Order #'.$_POST['id'].' Cancelled';
					$push_data = array(
			            'order_id' => $_POST['id'],
			            'message' => $message
			        );

					$push_type = 'order_cancel';

					$device = Devices::where('user_id',$orders['user_id'])->first();
					$user = User::find($orders['user_id']);

					if($user['notification_status'] == 1 && $device['token'] != ''){
						$SendNotification = SendNotification($device['type'], $device['token'], $push_title, $push_data, $push_type);
					}
					notification_add($orders['user_id'],$message,$push_type,0,$orders['id'],0);

					$phone_no = $user['phone'];

					$sms_text = 'Your order #'.$_POST['id'].' has been canceled by RAD. We apologize for the inconvenience caused. The payment will be auto refunded.';
			        
			        send_SMS($phone_no, $sms_text);

			        $vendor_channels_data = VendorChannels::where('vendor_id',$orders['vendor_id'])->get();
	                $description = $message;
	                $title = "Order Cancelled";
	                if(count($vendor_channels_data) > 0){
	                    $vendor_channels_data = $vendor_channels_data->toArray();
	                    $player_ids = array_column($vendor_channels_data, 'channel_id');
	                    $send_web_push = send_web_push($player_ids, $description, $title,$type = 3);
	                }

			        echo json_encode(array('status' => TRUE, 'message' => 'Order cancelled successfully. Customer got full refund.'));

        			return TRUE;

		        }else{

		        	echo json_encode(array('status' => FALSE, 'message' => $error_msg));
        			return TRUE;
		        }

            }else{

            	switch ($orders['status']) {
            		case '3':
            			$message = 'Restaurant already rejected this order.';
            			break;
            		case '4':
            			$message = 'Order already cancelled.';
            			break;
            		case '10':
            			$message = 'Order already delivered.';
            			break;
            		default:
            			$message = 'Something went wrong.';
            			break;
            	}

            	echo json_encode(array('status' => FALSE, 'message' => $message));
        		return TRUE;
            }
			
        }else{
        	
        	echo json_encode(array('status' => FALSE, 'message' => 'Order not found'));
        	return TRUE;
        }

	}
	
}