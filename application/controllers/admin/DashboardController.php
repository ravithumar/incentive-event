<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {

	public function __construct() {
		parent::__construct();
        //echo '<pre>'; print_r($this->session->all_userdata());exit;
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->model('User');
        // $this->load->model('Product');
        // $this->load->model('Orders');
        // $this->load->model('VendorChannels');
		$this->title = 'Admin Dashboard';
	}

	public function index(){

        //echo '<pre>'; print_r($this->session->all_userdata());exit;

		$this->load->library('breadcrumbs');
		$data['title'] = $this->title;
		$data['total_users'] = User::select('users.id', 'users.first_name', 'users.last_name', 'users.phone', 'users.email','users.profile_picture')->join('users_groups','users_groups.user_id','users.id')->where('users_groups.group_id',3)->orderBy('id', 'DESC')->get();
        $data['users'] = User::select('users.id', 'users.first_name', 'users.last_name', 'users.phone', 'users.email','users.profile_picture')->join('users_groups','users_groups.user_id','users.id')->where('users_groups.group_id',3)->orderBy('id', 'DESC')->get();
        $data['products'] = 0;
        // $net_profit = $revenue = 0;
        // $orders = Orders::where('status',10)->get();
        // if(isset($orders)){
        //     $orders = $orders->toArray();
            
        //     foreach ($orders as $key => $value) {
        //         $net_profit += $value['admin_amount'];
        //         $revenue += $value['total_amount'];
        //     }
        // }
        // $data['net_profit'] = $net_profit;
        // $data['revenue'] = $revenue;
        $id = $this->session->userdata['admin']['user_id'];
        // $vendor_channels_data = VendorChannels::where('vendor_id',$id)->get();
        $description = "This is a testing notification for web push Admin";
        $title = "Test Notification";
        // if(count($vendor_channels_data) > 0){
        //     $vendor_channels_data = $vendor_channels_data->toArray();
        //     // _pre($vendor_channels_data);
        //     $player_ids = array_column($vendor_channels_data, 'channel_id');
        //     // $send_web_push = send_web_push($player_ids, $description, $title);
        // }

		$this->admin_render('dashboard', $data);	
	}

    public function testing(){


        $this->load->helper("stripe_helper");

        /* $card_array['name'] = "test";
        $card_array['card_number'] = "4000 0000 0000 0077";
        $card_array['month'] = "12";
        $card_array['year'] = "2023";
        $card_array['cvc_number'] = "123";

        $stripeCardObject = create_token($card_array);

        $transact = array();
        $transact['description'] = 'My First Test Charge (created for API docs)';
        $transact['source'] = "tok_bypassPending";
        $transact['amount'] =1000000;
        $paymentResponse = addCharge($transact);  */

        /* $time=strtotime('1996-02-14');
        $month=date("m",$time);
        $year=date("Y",$time);
        $day=date("d",$time);

        $info = [
            'type' => 'custom',
            'country' => 'CA',
            'email' => 'vendor@yopmail.com',
            'business_type' => 'individual',
            'individual' => [
                'dob' => [
                    'day' => $day,
                    'month' => $month,
                    'year' => $year
                ],
               'first_name' => 'John',
               'last_name' => 'Chirs',
               'address' => [
                    'city' => 'Vancouver',
                    'country' =>  'CA',
                    'line1' =>  '1133 W 26th Ave',
                    'line2' =>  'Vancouver',
                    'postal_code' => 'M8V',
                    'state' => 'Alberta'
               ]
            ],
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true]
            ],
            'external_account' => [
                'object' => 'bank_account',
                'country' => 'CA',
                'currency' => 'CAD',
                'routing_number' =>  '11000-000',
                'account_number' =>  '000123456789'
            ],
            'tos_acceptance' => [
                'date' => time(),
                'ip' => $_SERVER['REMOTE_ADDR']
            ]
        ];
                
        $result = createCustomAccount($info);
        echo  $result['success']['id'];*/
        exit();

        $amount = 10*100;
        // $transferToConnectedAccount = transferToConnectedAccount($amount,"acct_1JI5AD2ShIiH0LZR");
        print_r($amount); 


        $card_array['name'] = 'Dhrumi Sw';
        $card_array['card_number'] = '4000 0000 0000 0077';
        $card_array['month'] = '11';
        $card_array['year'] = '2022';
        $card_array['cvc_number'] = '123';

        $stripeCardObject = create_token($card_array);
        if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true){
            $stripe_token = $stripeCardObject['token'];
        }else{
            $error = $stripeCardObject['error'];
            $response['message'] = $error['message'];
            $response['status'] = false;
            // $response['card_array'] = $card_array;
            //$this->response($response);
            echo json_encode($response, JSON_UNESCAPED_UNICODE);
            
            
        }

        
        $transact = array();
        $transact['description'] = "Testing 1";
        $transact['source'] = $stripe_token;
        $transact['amount'] = 1 * 100;
        $paymentResponse = addCharge($transact);

        if(isset($paymentResponse['status']) && $paymentResponse['status'] == true){
            $res['status'] = $paymentResponse['status'];
            $res['reference_id'] = $paymentResponse['success']->id;
            $res['id'] = $paymentResponse['success']->id;

            if($res['status'] == 1){
                    
                    echo "Success";
                    var_dump($res);

                
            }else{

                if(isset($paymentResponse['error']['message'])){
                    $error = $paymentResponse['error']['message'].' '.$paymentResponse['error']['code'];
                    // $error = $paymentResponse['error']['message'];
                    // $error = 'Please use testing payment card to make payment.';
                }else{
                    $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                }
                
                echo "fail";
                var_dump($paymentResponse);

                


            }

        }

        exit;


        $time=strtotime('1996-02-14');
        $month=date("m",$time);
        $year=date("Y",$time);
        $day=date("d",$time);

        $info = [
            // 'type' => 'custom',
            // 'country' => 'CA',
            // 'email' => 'dhrumi.m@gmail.com',
            // 'business_type' => 'individual',
            'individual' => [
                'dob' => [
                    'day' => $day,
                    'month' => $month,
                    'year' => $year
                ],
               'first_name' => 'Dhrumi',
               'last_name' => 'Soni',
               'address' => [
                    'city' => 'Vancouver',
                    'country' =>  'CA',
                    'line1' =>  '1133 W 26th Ave',
                    'line2' =>  'Vancouver',
                    'postal_code' => 'M8V',
                    'state' => 'Alberta'
               ]
            ],
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true]
            ],
            'external_account' => [
                'object' => 'bank_account',
                'country' => 'CA',
                'currency' => 'CAD',
                'routing_number' =>  '11000-000',
                'account_number' =>  '000123456789'
            ],
            'tos_acceptance' => [
                'date' => time(),
                'ip' => $_SERVER['REMOTE_ADDR']
            ]
        ];
                
        $result = updateCustomAccount('acct_1Iqxu12SZpBboKFe', $info);

        echo "<pre>";
        print_r($result);
        exit;


        //$create_token = transferToConnectedAccount(1 * 100, );

    }

}