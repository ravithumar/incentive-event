<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FaqController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		
		$this->load->model('Faq');
		$this->title = 'FAQ';
		
	}

	public function index()
	{
		
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('faq.*', false)->from('faq')->where('faq.deleted_at', NULL);
		$action['edit'] = base_url('admin/faq/edit/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Question', 'question',function($question,$row){
				 $out = strlen($question) > 50 ? substr($question,0,50)."..." : $question;
				 return $out;

			})
			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="faq" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="faq" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">Edit</a>';
				$option .= " <button type='button' data-table='faq' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('id,question');
		$dt_data->datatable('faq');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/faq');
		$data['add_url'] = base_url('admin/faq/add');
        
		$this->admin_render('faq/index',$data);	
	}
	public function add(){

		
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				
				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					array('field' => 'question', 'label' => 'question', 'rules' => 'trim|required|min_length[2]'),
					array('field' => 'answer', 'label' => 'answer', 'rules' => 'trim|required|min_length[2]'),
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){

					$request = $this->input->post();
                	$user_data['question'] = $request['question'];
                	$user_data['answer'] = $request['answer'];
                	Faq::insert($user_data);

					$this->session->set_flashdata('success','Faq added successfully');
					redirect(base_url("admin/faq"));

				}
			}
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/faq');
        $this->breadcrumbs->push('Add', '/', true);
		$this->admin_render('faq/post', $output_data);
	}
	public function edit($id = ''){

		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){

				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					
					array('field' => 'question', 'label' => 'question', 'rules' => 'trim|required|min_length[2]'),
					array('field' => 'answer', 'label' => 'answer', 'rules' => 'trim|required|min_length[2]'),
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {

                    	$request = $this->input->post();

                    	$user_data['question'] = $request['question'];
                		$user_data['answer'] = $request['answer'];
                    	Faq::whereId($id)->update($user_data);

                    	$this->session->set_flashdata('success', 'Faq updated successfully');
						redirect(base_url('admin/faq'));

				} 
			}	
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/faq');
        $this->breadcrumbs->push('Edit', '/', true);

		$output_data['faq'] = Faq::find($id);
		$this->admin_render('faq/put', $output_data);
	}

}
?>
