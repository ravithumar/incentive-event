<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppSettingController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}

		$this->load->model('AppSetting');
		$this->title = 'App Setting';

	}

	public function index(){
		$output_data["settings"] = AppSetting::all()->toArray();

		$this->breadcrumbs->push('App Setting', 'admin/appsetting');
        $this->breadcrumbs->push('Edit', '/', true);

		$this->admin_render('app_setting',$output_data);
	}

	public function app_setting_put(){

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				// _pre($_POST);

				foreach ($this->config->item("app_setting") as $key => $value){
					$app_setting = array();
					$app_version = $this->input->post("appname_".$key);
					$app_setting = array('app_version' => $app_version );
					// $app_setting['app_version'] = $app_version;
					// AppSetting::where('app_name',$value)->update($app_setting);

					$this->db->where('app_name', $value);
					$this->db->update("appsetting", $app_setting);
				}

				if ($this->input->post("customer_android")){
					$appsetting = array('updates' => 1 );	
				}else{
					$appsetting = array('updates' => 0 );
				}
				AppSetting::where('app_name','customer_android')->update($appsetting);

				if ($this->input->post("customer_ios")){
					$appsetting = array('updates' => 1 );
				}else{
					$appsetting = array('updates' => 0 );
				}
				AppSetting::where('app_name','customer_ios')->update($appsetting);

				if ($this->input->post("delivery_boy_android")){
					$appsetting = array('updates' => 1 );
				}else{
					$appsetting = array('updates' => 0 );
				}

				AppSetting::where('app_name','delivery_boy_android')->update($appsetting);

				if ($this->input->post("delivery_boy_ios")){
					$appsetting = array('updates' => 1 );
				}else{
					$appsetting = array('updates' => 0 );
				}

				AppSetting::where('app_name','delivery_boy_ios')->update($appsetting);


				if ($this->input->post("maintenance_mode")){
					$appsetting = array('updates' => 1 );
				}else{
					$appsetting = array('updates' => 0 );
				}
				AppSetting::where('app_name','maintenance_mode')->update($appsetting);

				$this->session->set_flashdata('success','App setting updated successfully');
				redirect(base_url("admin/appsetting"));
				
			}else{
				$this->session->set_flashdata('error','Error into updating data');
				redirect(base_url("admin/appsetting"));
			}
		}
				
	}
}