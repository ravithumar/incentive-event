<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BannerController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->model('Banner');

		$this->title = 'Banners';
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['banner'] = Banner::orderBy("sequence", 'ASC')->get();
		$this->breadcrumbs->push($this->title , 'admin/banner');
		$this->admin_render('banner/index',$data);	
	}
	public function add(){

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				
				/*$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					//array('field' => 'picture', 'label' => 'picture', 'rules' => 'required')
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){*/

					$img_name = 'banner_' . time().rand(1000, 9999).'.jpg';
				    $destination_url = FCPATH . $this->config->item("banner").$img_name;
				    if(compress($_FILES["picture"]["tmp_name"], $destination_url, 50)){
				        $user_data['image'] = $img_name;
				    }
					
					$request = $this->input->post();
                	// $user_data['sequence'] = $request['sequence'];
                	$user_data['sequence'] = 0;
                	Banner::insert($user_data);

					$this->session->set_flashdata('success','Banner added successfully');
					redirect(base_url("admin/banner"));

				//}
			}
		}
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/banner');
        $this->breadcrumbs->push('Add', '/', true);

		$this->admin_render('banner/post', $data);
	}

	public function set_sequence(){

		$i = 1;
		$new_sequence = array();

		foreach ($_POST['seq'] as $row) {
			$new_sequence[$i] = $row;
	 		$i++;
		}

		foreach ($new_sequence as $key => $value) {
			$data = array('sequence' => $key);
			$this->db->where('id', $value);
			$this->db->update("banner", $data);
		}
		echo 'success';
	}

	public function edit($id = ''){
		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){

				/*$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					
					array('field' => 'picture', 'label' => 'picture', 'rules' => 'required')
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {*/

                    	$request = $this->input->post();
                    	 //_pre(FCPATH.$request["old_img"]);

                    	if(isset($_FILES['picture']) && !empty($_FILES['picture']) && strlen($_FILES['picture']['name']) > 0) {
                    		$img_name = 'banner_' . time().rand(1000, 9999).'.jpg';
						    $destination_url = FCPATH . $this->config->item("banner").$img_name;
						    if(compress($_FILES["picture"]["tmp_name"], $destination_url, 50)){
						        $user_data['image'] = $img_name;
						        unlink(FCPATH.$request["old_img"]);
						    }
                    	}

                    	if(isset($user_data)){
                    		Banner::whereId($id)->update($user_data);
                    	}

                    	$this->session->set_flashdata('success', 'Banner updated successfully');
						redirect(base_url('admin/banner'));

				//} 
			}
		}
		$output_data['title'] = $this->title;
		$output_data['banner_data'] = Banner::find($id);
		$this->breadcrumbs->push($this->title , 'admin/banner');
        $this->breadcrumbs->push('Edit', '/', true);
         if(!isset($output_data['banner_data']) && count($output_data['banner_data']) == 0){
			$this->session->set_flashdata('error', 'No any banner found!');
			redirect(base_url('admin/banner'));
		}
		// _pre($output_data['banner_data']);
		$this->admin_render('banner/put', $output_data);
	}

}
?>
