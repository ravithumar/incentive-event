<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SystemSettingController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
	}

	public function setting(){

		$this->load->model('System_setting');

		$settings = System_setting::all()->toArray();
		$output_data["settings"] = $settings;
		$output_data['title'] = 'Settings';

		$this->breadcrumbs->push('Settings', 'admin/setting');
            $this->breadcrumbs->push('Edit', '/', true);

       	$this->admin_render('setting', $output_data);
	}

	public function setting_put(){

		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

			$this->load->model('System_setting');

			$request = $this->input->post();

				/* $user_data['value'] = $request['stripe_key'];
            	System_setting::where('path','stripe_key')->update($user_data);

            	$user_data['value'] = $request['stripe_secret'];
            	System_setting::where('path','stripe_secret')->update($user_data);

            	$user_data['value'] = $request['stripe_mode'];
            	System_setting::where('path','stripe_mode')->update($user_data); */

            	$user_data['value'] = $request['site_title'];
            	System_setting::where('path','site_title')->update($user_data);



            	/*if(isset($_FILES['site_logo']) && !empty($_FILES['site_logo']) && strlen($_FILES['site_logo']['name']) > 0) {
            		$img_name = 'site_logo_' . time().rand(1000, 9999).'.jpg';
				    $destination_url = FCPATH .'/assets/files/'.$img_name;
				    if(compress($_FILES["site_logo"]["tmp_name"], $destination_url, 90)){
				        $user_data['value'] = $img_name;
				        unlink(FCPATH.$request["site_logo_old"]);
				        System_setting::where('path','site_logo')->update($user_data);
				    }
            	}*/

            	$user_data['value'] = $request['site_meta'];
            	System_setting::where('path','site_meta')->update($user_data);

            	$user_data['value'] = $request['site_keyword'];
            	System_setting::where('path','site_keyword')->update($user_data);

            	//_pre($_FILES);
            	/*if(isset($_FILES['favicon']) && !empty($_FILES['favicon']) && strlen($_FILES['favicon']['name']) > 0) {
            		$img_name = 'favicon_' . time().rand(1000, 9999).'.jpg';
				    $destination_url = FCPATH .'/assets/files/'.$img_name;
				   
				    if(compress($_FILES["favicon"]["tmp_name"], $destination_url, 90)){
				        $user_data['value'] = $img_name;
				        unlink(FCPATH.$request["favicon_old"]);
				        System_setting::where('path','favicon')->update($user_data);
				    }
            	}*/

            	/* $user_data['value'] = $request['fcm_key'];
            	System_setting::where('path','fcm_key')->update($user_data);

            	$user_data['value'] = $request['map_key'];
            	System_setting::where('path','map_key')->update($user_data); */

            	$user_data['value'] = $request['from_email'];
            	System_setting::where('path','from_email')->update($user_data);

            	$user_data['value'] = $request['smtp_host'];
            	System_setting::where('path','smtp_host')->update($user_data);

            	$user_data['value'] = $request['smtp_port'];
            	System_setting::where('path','smtp_port')->update($user_data);

            	$user_data['value'] = $request['smtp_user'];
            	System_setting::where('path','smtp_user')->update($user_data);

            	$user_data['value'] = $request['smtp_pass'];
            	System_setting::where('path','smtp_pass')->update($user_data);

            	$user_data['value'] = $request['from_name'];
            	System_setting::where('path','from_name')->update($user_data);

            	$user_data['value'] = $request['service_charge'];
            	System_setting::where('path','service_charge')->update($user_data);

            	/*$user_data['value'] = $request['api_key'];
            	System_setting::where('path','api_key')->update($user_data);*/
				
            	$user_data['value'] = $request['twitter_link'];
            	System_setting::where('path','twitter_link')->update($user_data);

            	$user_data['value'] = $request['facebook_link'];
            	System_setting::where('path','facebook_link')->update($user_data);

            	$user_data['value'] = $request['instagram_link'];
            	System_setting::where('path','instagram_link')->update($user_data);

            	$user_data['value'] = $request['google_link'];
            	System_setting::where('path','google_link')->update($user_data);

            	$user_data['value'] = $request['pinterest_link'];
            	System_setting::where('path','pinterest_link')->update($user_data);

            	$user_data['value'] = $request['youtube_link'];
            	System_setting::where('path','youtube_link')->update($user_data);

            	$user_data['value'] = $request['RAD_number'];
            	System_setting::where('path','RAD_number')->update($user_data);

			$user_data['value'] = $request['total_no_of_feature_product'];
			System_setting::where('path','total_no_of_feature_product')->update($user_data);

            	/*$user_data['value'] = $request['tax'];
				System_setting::where('path','tax')->update($user_data);*/

				$user_data['value'] = $request['delivery_person_percentage'];
				System_setting::where('path','delivery_person_percentage')->update($user_data);
				
				/* $user_data['value'] = $request['delivery_miles_available'];
				System_setting::where('path','delivery_miles_available')->update($user_data);

				$user_data['value'] = $request['delivery_person_response_time'];
				System_setting::where('path','delivery_person_response_time')->update($user_data); */

				$user_data['value'] = $request['store_confirmation_minute'];
				System_setting::where('path','store_confirmation_minute')->update($user_data);


            	$this->session->set_flashdata('success','Setting updated successfully');
				redirect(base_url("admin/setting"));

			}
		}else{
			$this->session->set_flashdata('error', 'Something went wrong');
			redirect(base_url() . "admin/setting");
		}
	}
}