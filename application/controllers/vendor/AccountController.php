<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		// $this->load->model('System_setting');
		// $this->load->model('User');

		$this->title = 'Bank Details';
	}

	public function index()
	{
		// $settings = System_setting::all()->toArray();
		// $data["settings"] = $settings;

		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'vendor/variant');
		$this->vendor_render('account',$data);	
	}

}
?>
