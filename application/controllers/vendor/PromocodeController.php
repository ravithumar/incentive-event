<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PromocodeController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		
		$this->load->model('Promocode');
		$this->load->model('User');
		$this->title = 'Promocode';
		
	}

	public function index()
	{
		$id =  $this->session->userdata['vendor']['user_id'];
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('promocode.*', false)->from('promocode')->where('promocode.user_id', $id)->where('promocode.deleted_at', NULL);
		$action['edit'] = base_url('vendor/promocode/edit/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Promocode', 'promocode',function($promocode,$row){

				return ucfirst(htmlentities($promocode));

			})
			->column('Minimum Order Amount', 'promo_min_order_amount',function($promo_min_order_amount,$row){

				return '$ '.$promo_min_order_amount;

			})
			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="promocode" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="promocode" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">Edit</a>';
				$option .= " <button type='button' data-table='promocode' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('id,promocode');
		$dt_data->datatable('promocode');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$data['add_url'] = base_url('vendor/promocode/add');
        $this->breadcrumbs->push($this->title , 'vendor/promocode');
		$this->vendor_render('promocode/index',$data);	
	}
	public function isexists_promo($str = NULL, $id = NULL) {

			$this->db->select('*');
			if ($id != '') {
				$this->db->where('id !=', $id);
			}
			$this->db->where('promocode', $str);
			$this->db->where('deleted_at', NULL);
			$this->db->from('promocode');
			$sql_query = $this->db->get();
			if ($sql_query->num_rows() > 0) {
				$this->form_validation->set_message('isexists_promo', "This promocode field is already exists.");
				return FALSE;
			} else {
				return TRUE;
			}
	}
	public function add(){
		
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$validation_rules = array(
					array('field' => 'promocode', 'label' => 'promocode', 'rules' => 'trim|required|min_length[2]|max_length[50]|callback_isexists_promo')
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){
					
					$request = $this->input->post();
					$user_id =  $this->session->userdata['vendor']['user_id'];
					$promo_data['user_id'] = $user_id;

					if(isset($_POST['from_date']) && !empty($_POST['from_date']))
			        {
			            $from_date = $request['from_date']; 
						$date = DateTime::createFromFormat('d F, Y', $from_date);
				        $promo_data['from_date'] =  $date->format('Y-m-d');
			        }
			        if(isset($_POST['to_date']) && !empty($_POST['to_date']))
			        {
			            $to_date = $request['to_date']; 
						$date = DateTime::createFromFormat('d F, Y', $to_date);
				        $promo_data['to_date'] =  $date->format('Y-m-d');
			        }

					$promo_data['promocode'] = strtoupper($request['promocode']);
					$promo_data['promo_min_order_amount'] = $request['promo_min_order_amount'];
					$promo_data['amount'] = $request['amount'];
					if(isset($_POST['discount_type']) && !empty($_POST['discount_type']))
			        {
						$promo_data['discount_type'] = $request['discount_type'];
					}
					if(isset($_POST['max_disc']) && !empty($_POST['max_disc']))
			        {
						$promo_data['max_disc'] = $request['max_disc'];
					}
					$promo_data['usage_limit'] = $request['usage_limit'];
					$promo_data['description'] = $request['description'];
         
                	Promocode::insert($promo_data);

                	$this->session->set_flashdata('success','Promocode added successfully');
					redirect(base_url("vendor/promocode"));
					  	
				}
			}
		}

		$output_data['title'] = $this->title;
		
		$this->breadcrumbs->push($this->title , 'vendor/promocode');
        $this->breadcrumbs->push('Add', '/', true);
		$this->vendor_render('promocode/post', $output_data);
	}
	public function edit($id = ''){
		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'promocode', 'label' => 'promocode', 'rules' => 'trim|required|min_length[2]|max_length[50]|callback_isexists_promo[' . $this->input->post("id") . ']')
				);

				$this->form_validation->set_rules($validation_rules);
				 
				if ($this->form_validation->run() === true) {


					$request = $this->input->post();

					$user_id =  $this->session->userdata['vendor']['user_id'];
					$promo_data['user_id'] = $user_id;

                	$promo_data['promocode'] = strtoupper($request['promocode']);
					$promo_data['promo_min_order_amount'] = $request['promo_min_order_amount'];
					$promo_data['amount'] = $request['amount'];
					if(isset($_POST['discount_type']) && !empty($_POST['discount_type']))
			        {
						$promo_data['discount_type'] = $request['discount_type'];
					}
					$promo_data['max_disc'] = $request['max_disc'];
					$promo_data['usage_limit'] = $request['usage_limit'];
					$promo_data['description'] = $request['description'];

					if(isset($_POST['from_date']) && !empty($_POST['from_date']))
			        {
			            $from_date = $request['from_date']; 
						$date = DateTime::createFromFormat('d F, Y', $from_date);
				        $promo_data['from_date'] =  $date->format('Y-m-d');
			        }
			        if(isset($_POST['to_date']) && !empty($_POST['to_date']))
			        {
			            $to_date = $request['to_date']; 
						$date = DateTime::createFromFormat('d F, Y', $to_date);
				        $promo_data['to_date'] =  $date->format('Y-m-d');
			        }
                	Promocode::whereId($id)->update($promo_data);

                	$this->session->set_flashdata('success','Promocode updated successfully');
					redirect(base_url("vendor/promocode"));
						
				} 
			}
		}
		$output_data['title'] = $this->title;
		
		$output_data['promocode'] = Promocode::find($id);

		if(!isset($output_data['promocode']) && count($output_data['promocode']) == 0){
			$this->session->set_flashdata('error', 'No any promocode found!');
			redirect(base_url('vendor/promocode'));
		}
		
		$this->breadcrumbs->push($this->title , 'vendor/promocode');
        $this->breadcrumbs->push('Edit', '/', true);
		$this->vendor_render('promocode/put', $output_data);
	}

}
?>
