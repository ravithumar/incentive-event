<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		
		$this->load->model('Product');
		$this->load->model('Variant');
        $this->load->model('Category');
        $this->load->model('User');
        $this->load->model('ProductVariant');
        $this->load->model('ProductAddOn');
		$this->title = 'Product';
		
	}

	public function index()
	{
		$id = $this->session->userdata['vendor']['user_id'];
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('product.*', false)->from('product')->where('product.user_id', $id)->where('product.deleted_at', NULL);
		$action['edit'] = base_url('vendor/product/edit/');
		$action['view'] = base_url('vendor/product/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Image', 'image',function($image,$row){
				
				if($image != ''){
					$src = BASE_URL().$this->config->item("product_picture_path").$image;
				}else{
					$src = BASE_URL().'/assets/images/default.png';
				}
				return '<img class="border rounded p-0"  src="'.$src.'" alt="your image" style="height: 75px;width: 75px;object-fit: cover;" id="blah"/>';

			})
			->column('Title', 'title',function($title,$row){

				return ucfirst($title);

			})
			->column('Featured', 'featured', function ($featured, $row){
				if ($featured == 1) {
					return '<button onclick="featured(this);" data-id="' . $row['id'] . '" data-user_id="' . $row['user_id'] . '" class="btn btn-success btn-sm waves-effect waves-light" data-status="' . $featured . '">On</button>';
				} else{
					return '<button onclick="featured(this);" data-id="' . $row['id'] . '" data-user_id="' . $row['user_id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-status="' . $featured . '">Off</button>';
				}
				
			})

			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="product" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="product" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light mr-1 ">Edit</a>';
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">View</a>';
				$option .= " <button type='button' data-table='product' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('id,title');
		$dt_data->datatable('product');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$data['add_url'] = base_url('vendor/product/add');
        $this->breadcrumbs->push($this->title , 'vendor/product');
		$this->vendor_render('product/index',$data);	
	}
	
	public function add(){
		

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$validation_rules = array(
					array('field' => 'title', 'label' => 'title', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'price', 'label' => 'price', 'rules' => 'trim|required'),
					array('field' => 'category_id', 'label' => 'category_id', 'rules' => 'trim|required')
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){
					
					$request = $this->input->post();
					$user_id = $this->session->userdata['vendor']['user_id'];

					if(isset($_FILES['picture']) && !empty($_FILES['picture']) && strlen($_FILES['picture']['name']) > 0) {
                		$img_name = 'product_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("product_picture_path").$img_name;
					    if(compress($_FILES["picture"]["tmp_name"], $destination_url, 50)){
					        $product_data['image'] = $img_name;
					        //unlink(FCPATH.$request["old_img"]);
					    }
                	}
					$product_data['user_id'] = $user_id;
					$product_data['title'] = ($request['title']);
					$product_data['description'] = ($request['description']);
					$product_data['price'] = $request['price'];
					$product_data['category_id'] = $request['category_id'];
					$product_data['stock'] = $request['stock'];
					if(isset($_POST['type']) && !empty($_POST['type'])){
						$product_data['type'] = $request['type'];
					}else{
						$product_data['type'] = 0;
					}
					if(isset($_POST['inventory_status']) && !empty($_POST['inventory_status'])){
						$product_data['inventory_status'] = $request['inventory_status'];
						$product_data['stock'] = $request['stock'];
					}else{
						$product_data['inventory_status'] = 0;
						$product_data['stock'] = 0;
					}
                	$id = Product::insertGetId($product_data);
                	if(isset($request['variant_id']) && $request['variant_id'] != ''){
                        ProductVariant::where('product_id',$id)->delete();
                        foreach ($request['variant_id'] as $key => $value) {
                            $variants[$key]['product_id'] = $id;
                            $variants[$key]['variant_id'] = $value;
                        }
                        ProductVariant::insert($variants);
                    }

                    if(isset($_POST['name']) && isset($_POST['vprice'])){
						$group_array = array();

						foreach ($this->input->post("name") as $key => $value) {
							$group_array[$key]['name'] = $value;
						}
						foreach ($this->input->post("vprice") as $key => $value) {
							$group_array[$key]['price'] = $value;
							$group_array[$key]['product_id'] = $id;
						}
						
						ProductAddOn::insert($group_array);
						
					}

                	$this->session->set_flashdata('success','Product added successfully');
					redirect(base_url("vendor/product"));
					  	
				}
			}
		}

		$output_data['title'] = $this->title;
		$output_data['variants'] = Variant::get();
		$output_data['category'] = Category::get();


		if(isset($output_data['variants'])){
            $output_data['variants'] = $output_data['variants']->toArray();
        }
        if(isset($output_data['category'])){
            $output_data['category'] = $output_data['category']->toArray();
        }
		
		$this->breadcrumbs->push($this->title , 'vendor/product');
        $this->breadcrumbs->push('Add', '/', true);
		$this->vendor_render('product/post', $output_data);
	}
	public function edit($id = ''){
		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'title', 'label' => 'title', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'price', 'label' => 'price', 'rules' => 'trim|required'),
					array('field' => 'category_id', 'label' => 'category_id', 'rules' => 'trim|required')
				);

				$this->form_validation->set_rules($validation_rules);
				 
				if ($this->form_validation->run() === true) {


					$request = $this->input->post();

					$user_id = $this->session->userdata['vendor']['user_id'];

					if(isset($_FILES['picture']) && !empty($_FILES['picture']) && strlen($_FILES['picture']['name']) > 0) {
                		$img_name = 'product_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("product_picture_path").$img_name;
					    if(compress($_FILES["picture"]["tmp_name"], $destination_url, 50)){
					        $product_data['image'] = $img_name;
					        unlink(FCPATH.$request["old_img"]);
					    }
                	}
					$product_data['user_id'] = $user_id;
					$product_data['title'] = ($request['title']);
					$product_data['description'] = ($request['description']);
					$product_data['price'] = $request['price'];
					$product_data['category_id'] = $request['category_id'];
					if(isset($_POST['inventory_status']) && !empty($_POST['inventory_status'])){
						$product_data['inventory_status'] = $request['inventory_status'];
						$product_data['stock'] = $request['stock'];
					}else{
						$product_data['inventory_status'] = 0;
						$product_data['stock'] = 0;
					}
					if(isset($_POST['type']) && !empty($_POST['type'])){
						$product_data['type'] = $request['type'];
					}else{
						$product_data['type'] = 0;
					}
                	Product::whereId($id)->update($product_data);

                    ProductVariant::where('product_id',$id)->delete();
                	if(isset($request['variant_id']) && $request['variant_id'] != ''){
                        foreach ($request['variant_id'] as $key => $value) {
                            $variants[$key]['product_id'] = $id;
                            $variants[$key]['variant_id'] = $value;
                        }
                        ProductVariant::insert($variants);
                    }

                   	ProductAddOn::where('product_id',$id)->delete();
                    if(isset($_POST['name']) && isset($_POST['vprice'])){
						$group_array = array();

						foreach ($this->input->post("name") as $key => $value) {
							$group_array[$key]['name'] = $value;
						}
						foreach ($this->input->post("vprice") as $key => $value) {
							$group_array[$key]['price'] = $value;
							$group_array[$key]['product_id'] = $id;
						}
						
						ProductAddOn::insert($group_array);
						
					}

                	$this->session->set_flashdata('success','Product updated successfully');
					redirect(base_url("vendor/product"));
						
				} 
			}
		}
		$output_data['title'] = $this->title;

		$output_data['variants'] = Variant::get();
		$output_data['category'] = Category::get();


		if(isset($output_data['variants'])){
            $output_data['variants'] = $output_data['variants']->toArray();
        }
        if(isset($output_data['category'])){
            $output_data['category'] = $output_data['category']->toArray();
        }
		
		$output_data['product'] = Product::with(['variant','addon'])->find($id);

		if(isset($output_data['product'])){
            $output_data['product'] = $output_data['product']->toArray();
        }

		if(!isset($output_data['product']) && count($output_data['product']) == 0){
			$this->session->set_flashdata('error', 'No any product found!');
			redirect(base_url('vendor/product'));
		}
		//_pre($output_data);
		
		$this->breadcrumbs->push($this->title , 'vendor/product');
        $this->breadcrumbs->push('Edit', '/', true);
		$this->vendor_render('product/put', $output_data);
	}

	public function view($id = ''){
		
		$output_data['title'] = $this->title;
		$output_data['product'] = Product::with(['variant','addon','vendor'])->find($id);

		if(isset($output_data['product'])){
            $output_data['product'] = $output_data['product']->toArray();
        }

		if(!isset($output_data['product']) && count($output_data['product']) == 0){
			$this->session->set_flashdata('error', 'No any product found!');
			redirect(base_url('vendor/product'));
		}
		// /_pre($output_data);		
		$this->breadcrumbs->push($this->title , 'vendor/product');
        $this->breadcrumbs->push('View', '/', true);
		$this->vendor_render('product/view', $output_data);
	}

	public function featured_products()
	{
		$data['title'] = 'Featured Products';
		$id = $this->session->userdata['vendor']['user_id'];
		$data['products'] = Product::with(['vendor'])->where("featured", 1)->groupBy("id")->orderBy("featured_number", 'ASC')->where('user_id',$id)->get();
		//_pre($data['products']->toArray());
        $this->breadcrumbs->push('Featured Products' , 'vendor/product');
		$this->vendor_render('product/featured',$data);	
	}

	public function set_sequence(){

		$i = 1;
		$new_sequence = array();

		foreach ($_POST['seq'] as $row) {
			$new_sequence[$i] = $row;
	 		$i++;
		}

		foreach ($new_sequence as $key => $value) {
			$data = array('featured_number' => $key);
			$this->db->where('id', $value);
			$this->db->update("product", $data);
		}
		echo 'success';
	}

}
?>
