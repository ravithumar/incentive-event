<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class VendorController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		/* if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		} */
		$this->load->model('User');
		$this->load->model('Category');
		$this->load->model('VendorAvailibility');
		$this->load->model('Vendor');
		$this->load->model('ion_auth_model');

		$this->title = 'Vendor';
	}

	public function change_password(){
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
				$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]');
				$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

				 
				if ($this->form_validation->run() === true){
					$request = $this->input->post();


					$identity =  $this->session->userdata['vendor']['identity'];
					$id =  $this->session->userdata['vendor']['user_id'];

					// _pre($this->session->userdata());
					$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'),$id);

					if($change){
						$this->session->set_flashdata('success', $this->ion_auth->messages());
					}else{
						$this->session->set_flashdata('error', $this->ion_auth->errors());
					}
					redirect(base_url('vendor/change-password'));
				}

			}
		}

		$output_data['title'] = 'Change Password';
		$output_data['change_pw_link'] = base_url('vendor/change-password');
		$this->breadcrumbs->push('Change Password' , 'vendor/change-password');
		$this->vendor_render('change_password',$output_data);
	}

	public function my_profile_update($id = ''){
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		//_pre($_POST);
		$this->load->library(['ion_auth', 'form_validation']);
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'name', 'label' => 'name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'first_name', 'label' => 'first name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'last_name', 'label' => 'last name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'phone', 'label' => 'phone', 'rules' => 'trim|required'),
					array('field' => 'email', 'label' => 'email', 'rules' => 'trim|required'),
					array('field' => 'city', 'label' => 'city', 'rules' => 'trim|required'),
					array('field' => 'state', 'label' => 'state', 'rules' => 'trim|required'),
					array('field' => 'country', 'label' => 'country', 'rules' => 'trim|required'),
					array('field' => 'zipcode', 'label' => 'zipcode', 'rules' => 'trim|required'),
				);
				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();
					
					$user_data = [
							'first_name' => ucfirst($request["first_name"]),
							'last_name' => ucfirst($request["last_name"]),
							'phone' => $request["phone"],
							'email' => $request["email"],
						];
					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
                		$img_name = 'vendor_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
					        $user_data['profile_picture'] = $img_name;
					        if($request["old_profile_picture"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_profile_picture"]);
					        }
					        
					    }
					    $file_upload = true;
                	}
                	

                	// _pre($user_data);

                	$availability = array();
					foreach ($this->config->item("days") as $key => $value){
					  
					  	if (!empty($_POST[$value]) && sizeof($_POST[$value]) != 0){
					  		// If not closed

						  	if($_POST[$value][0] == 'fullday'){
						  		// If full day open
						  		//echo 'full day';
						  		$availability[$value]['full_day'] = '1';
						  		$availability[$value]['closed'] = '0';
						  		$availability[$value]['from'] = '';
						  		$availability[$value]['to'] = '';
						  	}else{
						  		// If custom time open
						  		$availability[$value]['full_day'] = '0';
						  		$availability[$value]['closed'] = '0';

						  		$availability[$value]['from'] = date('h:i A', strtotime($_POST[$value][0]));
						  		$availability[$value]['to'] = date('h:i A', strtotime($_POST[$value][1]));
						  	}
					  	}else{
					  		// echo 'closed';
					  		// If closed
					  		$availability[$value]['full_day'] = '0';
					  		$availability[$value]['closed'] = '1';
					  		$availability[$value]['from'] = '';
					  		$availability[$value]['to'] = '';
					  	}
					  	
					}
					VendorAvailibility::where('user_id',$request["user_id"])->delete();
					//Add new availability
					foreach ($availability as $key => $value) {

						$availability_data = array(
							'user_id' =>  $request["user_id"],
							'day' =>  $key,
							'from_time' => $value['from'],
							'to_time' =>  $value['to'],
							'full_day' =>  $value['full_day'],
							'is_closed' =>  $value['closed'],
							'updated_at' =>  date('Y-m-d H:i:s')
							);
						VendorAvailibility::insert($availability_data);
					}

					User::whereId($request['user_id'])->update($user_data);

					$data = array(
						'user_id' =>  $request["user_id"],
						'name' => ucfirst($request["name"]),
						'street' => $request["address"],
						'city' => $request["city"],
						'state' => $request["state"],
						'country' => $request["country"],
						'zipcode' => $request["zipcode"],
						'minimum_order_amount' => $request["minimum_order_amount"],
						'latitude' => $request["latitude"],
						'longitude' => $request["longitude"],
						//'delivery_boy_percentage' => $request["delivery_boy_percentage"],
					);

					if(isset($_FILES['bg_img']) && !empty($_FILES['bg_img']) && strlen($_FILES['bg_img']['name']) > 0) {
                		$img_name = 'vendor_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["bg_img"]["tmp_name"], $destination_url, 90)){
					        $data['bg_img'] = $img_name;
					        if($request["old_bg_img"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_bg_img"]);
					        }
					    }
					    $file_upload = true;
                	}

					if(isset($_POST['service_available'])){
							if(count($_POST['service_available']) == 2){
								$service_available = 3;
							}else if($_POST['service_available'][0] == 2){
								$service_available = 2;
							}else{
								$service_available = 1;
							}
						}
					$data['service_available'] = $service_available;
					/*if(isset($_POST['category_id']) && !empty($_POST['category_id'])){
			        	$data['category'] = implode(",", $_POST['category_id']);
			        	unset($_POST['category_id']);
			        }*/

					if(isset($_POST['phone_2']) && !empty($_POST['phone_2'])){
						$data['phone_2'] = $request["phone_2"];
					}

					//_pre($data);

					Vendor::whereId($request['vendor_id'])->update($data);


					/* UsersCategory::where('user_id', $request['user_id'])->delete();
					if(isset($request['category_id'])){
					
						foreach ($request['category_id'] as $key => $value) {
							$insert_category_data[] = array('user_id'=> $id, 'category_id'=> $value);
						}
						UsersCategory::insert($insert_category_data);
					} */
					if($request['email'] != $request['old_email'])
					{
						$template = file_get_contents(base_url('email_templates/account_activation.html'));
						$message  = create_email_template_new($template);
						$link = base_url() .'admin/vendor-active/'.$id;
			            $message = str_replace('##LINK##', $link, $message);
			            $subject = $this->config->item('site_title', 'ion_auth').' User Detail';
			            $to = $request['email'];
			            $sent = send_mail_new($to,$subject, $message);

					}
					

					if ($file_upload){
						$this->session->set_flashdata('success','Profile updated successfully');
						redirect(base_url("vendor/my-profile"));
					}else{
						
						$this->session->set_flashdata('error','Error into updating data');
						redirect(base_url("vendor/my-profile/".$request["id"]));
					}
				} 
			}
		}else{
			$this->session->set_flashdata('error', 'Something went wrong');
			redirect(base_url() . "vendor/my-profile");
		}
	}

	public function my_profile(){
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		$id =  $this->session->userdata['vendor']['user_id'];

		$data['title'] = 'Update Restaurant Profile';
        $this->breadcrumbs->push($this->title , 'vendor/my-profile');
        $this->breadcrumbs->push('Edit', '/', true);
		$data['vendor'] = User::with(['vendor','vendor_availability'])->find($id);
		// $data['category'] = Category::get();

		if(isset($data['vendor'])){
            $data['vendor'] = $data['vendor']->toArray();
        }
        /* if(isset($data['category'])){
            $data['category'] = $data['category']->toArray();
        } */
		//_pre($data);
		$this->vendor_render('my_profile',$data);

	}

	public function forgot_password(){

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('identity', 'email', 'required');
				 
                if ($this->form_validation->run() === true) {
                    $request = $this->input->post();

                    $user = User::select('users.*')->where('email', $request['identity'])->join('users_groups', 'users_groups.user_id', 'users.id')->whereIn('users_groups.group_id', [2])->first();
                    //forgot pass

                    if (isset($user)) {
                        $forgotten = $this->ion_auth_model->forgotten_password_vendor($request['identity'], $user->id);
                        $data = [
							'identity'   => $user->email,
							'id'         => $user->id,
							'email'      => $user->email,
							'forgotten' => $forgotten ,
                    	];
						
						$this->lang->load('ion_auth');
                        $message = $this->load->view('email_templates/reset_password_vendor', $data, true);
						$subject = $this->config->item('site_title', 'ion_auth') . ' -  Reset Password';
						$to_email = $user->email;
						send_mail_new($to_email, $subject, $message);
						$this->session->set_flashdata('success', 'Reset password link sent to your email.');

                        /* $this->load->library(['email']);
                        $this->lang->load('ion_auth');
                    
                        $email_config = $this->config->item('email_config', 'ion_auth');

                        $this->email->initialize($email_config);
                        $message = $this->load->view('email_templates/reset_password_vendor', $data, true);
                    
                        $this->email->set_newline("\r\n");
                        $this->email->from('ruralareadelivery@gmail.com', "RAD");
                        $this->email->to($user->email);
                        $this->email->reply_to('info@radservices.ca', "RAD");
                        $this->email->subject($this->config->item('site_title', 'ion_auth') . ' -  Reset Password');
                        $this->email->message($message);

                        if ($this->email->send() === true) {
                            $this->session->set_flashdata('success', 'Reset password link sent to your email.');
                        } else {
                            $this->session->set_flashdata('error', 'Something went wrong');
                        } */
                        redirect(base_url('vendor/login'));
                    }else{
						$this->session->set_flashdata('error', 'This account does not Exists!');
						redirect(base_url('vendor/forgot-password'));
					}
                }
			}
		}
		$data['user_type'] = "vendor";
		$this->load->view('auth/forgot_password',$data);
	}
}