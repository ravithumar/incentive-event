<?php
if (!function_exists('assets')) {
    function assets($file)
    {
        return base_url('/assets/') . $file;
    }
}
if (!function_exists('assets_css')) {
    function assets_css($file)
    {
        return base_url('assets/css/') . $file . '.css';
    }
}
if (!function_exists('assets_js')) {
    function assets_js($file)
    {
        return base_url('assets/js/') . $file . '.js';
    }
}
if (!function_exists('assets_image')) {
    function assets_image($file)
    {
        return base_url('assets/image/') . $file;
    }
}
if (!function_exists('assets_upload')) {
    function assets_upload($file)
    {
        return base_url('assets/upload/') . $file;
    }
}
if (!function_exists('validation_errors_response_web')) {
    function validation_errors_response_web()
    {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        // $err_array=explode('|',$err_str);
        // $err_array = array_filter($err_array);
        return $err_array;
    }
}
/*
 * Additional :
 */
if (!function_exists('assets_less')) {
    function assets_less($file)
    {
        return base_url('assets/less/') . $file . '.less';
    }
}
if (!function_exists('assets_sass')) {
    function assets_sass($file)
    {
        return base_url('assets/sass/') . $file . '.sass';
    }
}
if (!function_exists('to_json')) {
    function to_json($array)
    {
    }
}
if (!function_exists('config')) {
    function config($key)
    {
        $CI = get_instance();
        $CI->load->model('Config');
        $config = Config::wherePath($key)->first();
        return $config->value;
    }
}
if (!function_exists('config_set')) {
    function config_set($key_array)
    {
        $CI = get_instance();
        $CI->load->model('Config');
        foreach ($key_array as $key => $value) {
            $update['value'] = $value;

            Config::wherePath($key)->update($update);
        }
        return true;
    }
}
if (!function_exists('image_upload')) {
    function image_upload($file, $path = '', $enc = false)
    {
        $CI = get_instance();

        if (!is_dir("./assets/" . $path)) {
            mkdir("./assets/" . $path, 0777, true);
        }

        $config = array(
            'upload_path' => "./assets/" . $path,
            'allowed_types' => "jpg|png|PNG|jpeg|JPG",
            'overwrite' => true,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'remove_spaces' => true,
            'encrypt_name' => $enc,
            'file_ext_tolower' => true,
        );
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        if ($CI->upload->do_upload($file)) {
            $data = $CI->upload->data();

            $source_path = "./assets/" . $path . '/' . $data['file_name'];
            $target_path = './assets/' . $path . '/' . $data['file_name'];
            $check_size = filesize($source_path) / 1024;
            $get_ext = explode('.', $data['file_name']);
            if ($get_ext != 'pdf') {
                if ($check_size > 300) {
                    compress($source_path, $target_path, 30);
                }
            }
            $data['status'] = true;
        } else {
            $data['error'] = $CI->upload->display_errors();
            $data['status'] = false;
        }
        return $data;
    }
}

if (!function_exists('multiple_image_upload')) {
    function multiple_image_upload($file, $folderName = '', $enc = true)
    {
        $CI = & get_instance();
        $return = array();
        if (!is_dir('./assets/'.$folderName)) {
            mkdir('./assets/'.$folderName, 0777, true);
            chmod('./assets/'.$folderName, 0777);
        }
        $config['upload_path']   = './assets/'.$folderName;
        $config['allowed_types'] = '*';
        // $config['max_size']      = 2048000;
        $config['encrypt_name']  = $enc;
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        if (!empty($_FILES[$file]['name'])) {
            $return['status'] = true;
            $return['data']   = array();
            $filesCount = count((array)$_FILES[$file]['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                $_FILES['file']['name']     = $_FILES[$file]['name'][$i];
                $_FILES['file']['type']     = $_FILES[$file]['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES[$file]['tmp_name'][$i];
                $_FILES['file']['error']    = $_FILES[$file]['error'][$i];
                $_FILES['file']['size']     = $_FILES[$file]['size'][$i];
                if (!$CI->upload->do_upload('file')) {
                    $error            		= $CI->upload->display_errors();
                    $return['data'][$i]['status'] 	= false;
                    $return[$i]['error']  	= $error;
                } else {
                    $upload_data      				= $CI->upload->data();
                    $return['data'][$i]['status'] 	= true;
                    $file_name        				= $upload_data['file_name'];
                    $return['data'][$i]['file_name']= $file_name;
                    $return['data'][$i]['path']   	= $file_name;

                    /*Enable This If you want your height width Image Start*/
                    /*$libconfig['image_library'] = 'gd2';
                    $libconfig['source_image'] = $CI->config->item('admin_assets_folder').$return['data'][$i]['path'];
                    $libconfig['create_thumb'] = FALSE;
                    $libconfig['maintain_ratio'] = TRUE;
                    $libconfig['width'] = 200;
                    $libconfig['height'] = 200;
                    $CI->load->library('image_lib', $libconfig);
                    $CI->image_lib->resize();*/
                    /*Enable This If you want your height width Image End*/
                    
                    /*Enable This If you want same resolution Image Start*/
                    $quality = 30;
                    $source_url = $config['upload_path']."/".$return['data'][$i]['path'];
                    $destination_url = $config['upload_path']."/".$return['data'][$i]['path'];
                    $info = getimagesize($source_url);

                    if ($info['mime'] == "image/jpeg" || $info['mime'] == "image/gif" || $info['mime'] == "image/png") {
                        if ($info['mime'] == 'image/jpeg') {
                            $image = imagecreatefromjpeg($source_url);
                        } elseif ($info['mime'] == 'image/gif') {
                            $image = imagecreatefromgif($source_url);
                        } elseif ($info['mime'] == 'image/png') {
                            $image = imagecreatefrompng($source_url);
                        }

                        imagejpeg($image, $destination_url, $quality);
                    }
                    /*Enable This If you want same resolution Image End*/
                }
            }
        } else {
            $return['status'] = false;
            $return['error']  = "No files uploaded";
        }
        return $return;
    }
}

if (!function_exists('fixRotate')) {
    function fixRotate($image)
    {
        $filename = $image;
        $CI =& get_instance();
        $CI->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $filename;
        $config['new_image'] = $filename;
        $exif = exif_read_data($config['source_image']);
        $exif['Orientation'] = 5;
        if ($exif && isset($exif['Orientation'])) {
            $ort = $exif['Orientation'];

            if ($ort == 6 || $ort == 5) {
                $config['rotation_angle'] = '270';
            }

            if ($ort == 3 || $ort == 4) {
                $config['rotation_angle'] = '180';
            }

            if ($ort == 8 || $ort == 7) {
                $config['rotation_angle'] = '90';
            }
        }
        $CI->image_lib->initialize($config);
        if (!$CI->image_lib->rotate()) {
            echo $CI->image_lib->display_errors();
        }
        $CI->image_lib->clear();
    }
}

if (!function_exists('image_upload_api')) {
    function image_upload_api($file, $path = '', $enc = false)
    {
        $CI = get_instance();

        if (!is_dir("./assets/" . $path)) {
            mkdir("./assets/" . $path, 0777, true);
        }

        $config = array(
            'upload_path' => "./assets/" . $path,
            'allowed_types' => "*",
            'overwrite' => true,
            'max_size' => "50000000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'remove_spaces' => true,
            'encrypt_name' => $enc,
            'file_ext_tolower' => true,
        );
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        if ($CI->upload->do_upload($file)) {
            $data = $CI->upload->data();
            $data['status'] = true;
        } else {
            $data['error'] = $CI->upload->display_errors();
            $data['status'] = false;
        }
        return $data;
    }
}
if (!function_exists('upload_image')) {
    function upload_image($image_name, $folder_name, $id)
    {
        $CI = get_instance();
        $config['upload_path'] = 'assets/files/'.$folder_name;
        $config['allowed_types'] = '*';
        $config['max_size'] = 50000;
        $config['encrypt_name'] = true;
        //$config['image_lib'] = 'gd2';
        $CI->load->library('upload', $config);
        if (!$CI->upload->do_upload($image_name)) {
            $error = $CI->upload->display_errors();
            $return['status'] = 0;
            $return['error'] = $error;
        } else {
            $upload_data = $CI->upload->data();
            $file_name = $upload_data['file_name'];
            $return['status'] = 1;
            $return['path'] = 'assets/files/'.$folder_name.'/'.$file_name;
        }
        $quality = 30;
        $source_url = $return['path'];
        $destination_url = $return['path'];
        $info = getimagesize($source_url);

        //print_r($source_url);exit;

         if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source_url);
        } elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source_url);
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source_url);
        }


        imagejpeg($image, $destination_url, $quality); 
        return $file_name;
    }
}
if (!function_exists('CsvUpload')) {
    function CsvUpload($file, $path = '', $enc = false)
    {
        $CI = get_instance();

        if (!is_dir("./assets/" . $path)) {
            mkdir("./assets/" . $path, 0777, true);
        }

        $config = array(
            'upload_path' => "./assets/" . $path,
            'allowed_types' => "csv|xlsx",
            'overwrite' => true,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'remove_spaces' => true,
            'encrypt_name' => $enc,
            'file_ext_tolower' => true,
        );
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        if ($CI->upload->do_upload($file)) {
            $data = $CI->upload->data();
            $data['status'] = true;
        } else {
            $data['error'] = $CI->upload->display_errors();
            $data['status'] = false;
        }
        return $data;
    }
}
if (!function_exists('__')) {
    function __()
    {
        $args = func_get_args();
        if (count($args) >= 1) {
            $CI = get_instance();
            $str = $args[0];
            $nstr = $str;
            $fstr = $CI->lang->line($str);
            if ($fstr != null) {
                $nstr = $fstr;
            }
            if (count($args) >= 2) {
                array_shift($args);
                return vsprintf($nstr, $args);
            }
            return $nstr;
        }
        return null;
    }
}

if (!function_exists('__timeago')) {
    function __timeago($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}

if (!function_exists('__date')) {
    function __date($date, $format = false)
    {
        if ($format) {
            $date = date_create($date);
            return date_format($date, $format);
        } else {
            $date = date_create($date);
            return date_format($date, config('date_format'));
        }
    }
}

if (!function_exists('base64ToImage')) {
    function base64ToImage($image, $dir)
    {
        $img = $image;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $time = time() . '.png';
        $file = 'assets/' . $dir . $time;
        $success = file_put_contents($file, $data);
        if ($success) {
            return $time;
        } else {
            return "";
        }
    }
}

if (!function_exists('_pre')) {
    function _pre($array)
    {
        echo "<pre>";
        print_r($array);
        echo "<pre>";
        exit;
    }
}

if (!function_exists('_prea')) {
    function _prea($array)
    {
        echo "<pre>";
        print_r($array);
        echo "<pre>";
    }
}

if (!function_exists('show_general')) {
    function show_general($message = "")
    {
        $CI = get_instance();
        $data['heading'] = "Auth error";
        $data['message'] = $message;
        $CI->load->view('errors/html/error_general', $data);
    }
}

if (!function_exists('routeLink')) {
    function routeLink($url)
    {
        return base_url($url);
    }
}

if (!function_exists('generateString')) {
    function generateString()
    {
        $strength = 6;
        $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($input);
        $random_string = '';
        for ($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }
}

if (!function_exists('renderFront')) {
    function renderFront($page = null, $params = null, $return = false)
    {
        $CI = get_instance();
        $CI->load->view('front/layout/header', []);
        if ($page != null) {
            $CI->load->view('front/pages/' . $page, $params, $return);
        }
        $CI->load->view('front/layout/footer', $params);
    }
}

if (!function_exists('validation_errors_response')) {
    function validation_errors_response()
    {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('', ''), trim(validation_errors()));

        // $err_str=ltrim($err_str,'|');
        // $err_str=rtrim($err_str,'|');
        // $err_array=explode('|',$err_str);
        // $err_array = array_filter($err_array);
        return $err_str;
    }
}
if (!function_exists('validation_errors_response_arabic')) {
    function validation_errors_response_arabic()
    {
        $CI = &get_instance();
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array;
    }
}

if (!function_exists('getAutoCode')) {
    function getAutoCode()
    {
        $n = 6;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return strtoupper($randomString);
    }
}
if (!function_exists('getExt')) {
    function getExt($file)
    {
        if ($file != "") {
            $array = explode('.', $file);
            return end($array);
        }
    }
}
