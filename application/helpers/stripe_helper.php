<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if (!function_exists('pay_stripe'))
{
    function pay_stripe($card_info)
    {
         $CI =& get_instance();
          $CI->load->library('Stripe');
          $card_array['name'] = $card_info['Fullname'];
          $card_array['card_number'] = $card_info['credit_card_no'];

          $monthyear = explode('/',$card_info['expiry_date']);
          $card_array['month'] = $monthyear[0];
          $card_array['year'] = $monthyear[1];
          $card_array['cvc_number'] = $card_info['security_code'];

         // print_r($card_array);exit;

        //$stripeCardObject=$CI->stripe->createToken($card_array);

        $stripeCardObject = create_token($card_array);
        //print_r($stripeCardObject);exit;
        if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true)
        {            

            $stripe_pay['amount'] = $card_info['cost'];
            $stripe_pay['source'] = $stripeCardObject['token'];
            $booking_id = 0;
            if(isset($card_info['purchase_id']))
            {
              $booking_id = $card_info['purchase_id'];
            }
            $stripe_pay['description'] = "Order ID : ".$booking_id;

            //$paymentResponse = $CI->stripe->addCharge($stripe_pay);
            $paymentResponse = addCharge($stripe_pay);

            if(isset($paymentResponse['status']) && $paymentResponse['status'] == true)
            {
                $response['status'] = $paymentResponse['status'];
                $response['reference_id'] = $paymentResponse['success']->id;
                $response['id'] = $paymentResponse['success']->id;
                return $response;
            }
            else
            {
                $response['status'] = 0;
                $response['reference_id'] = 0;
                //return $response;
                return $paymentResponse;
            }
        }
        else
        {
           
            //print_r($stripeCardObject['error']);
            $stripeCardObject['status'] = $stripeCardObject['status'];
            return $stripeCardObject;
        }
        exit;
    } 


    function refund_stripe($id ='', $amount = NULL){

      $CI =& get_instance();
      $CI->load->library('Stripe');
      $paymentResponse = $CI->stripe->addRefund($id);
      return $paymentResponse;
      // return $paymentResponse->status;
      //print_r($paymentResponse);exit;
    }

    function custom_create_token($card_data = array())
    {
      // This function is use for test the card is real or not.
      $CI =& get_instance();
      $CI->load->library('Stripe');
      $card_array = array();
      $card_array['name'] = $card_data['Fullname'];
      $card_array['card_number'] = $card_data['credit_card_no'];
      $monthyear = explode('/',$card_data['expiry_date']);

      $card_array['month'] = $monthyear[0];
      $card_array['year'] = $monthyear[1];
      $card_array['cvc_number'] = $card_data['security_code'];

      $stripeCardObject = $CI->stripe->createToken($card_array);
      if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true)
      {
        return $stripeCardObject;
      }
      else{
        //return $stripeCardObject;
        $return_array = array("status" => FALSE, "error_title" => ucfirst(str_replace('_', ' ', $stripeCardObject['error']['code'])), "error_message" => $stripeCardObject['error']['message']);
        return $return_array;
      }
    }

    function addCharge($stripe_pay = array()){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->addCharge($stripe_pay);
    }

    function create_token($card_array = array()){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->createToken($card_array);
    }

    function addCustomer($customer = array()){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->addCustomer($customer);
    }

    function getCustomAccount($account_id){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->getCustomAccount($account_id);
    }
    function deleteCustomer($customer_id){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->deleteCustomer($customer_id);
    }

    function createCustomAccount($info = array()){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->createCustomAccount($info);
    }

    function updateCustomAccount($acc_id, $info = array()){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->updateCustomAccount($acc_id, $info);
    }

    function transferToConnectedAccount($amount = 0, $conneced_account_id){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->transferToConnectedAccount($amount, $conneced_account_id);
    }

    function instantPayout($amount = 0, $conneced_account_id){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->instantPayout($amount, $conneced_account_id);
    }

    function getCharge($charge_id){
      $CI =& get_instance();
      $CI->load->library('Stripe');
      return $CI->stripe->getCharge($charge_id);
    }



}