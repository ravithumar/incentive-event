<?php
if(!function_exists('getSession')){
	function getSession()
	{
		$CI = get_instance();
		return $CI->session->userdata();
	}
}
if(!function_exists('isAuthorised')){
	function isAuthorised()
	{
		$CI = get_instance();
		if (!$CI->ion_auth->is_admin()) {
			if (!$CI->ion_auth->is_privileged(PRIVILEGE)) {
				redirect('/');
			}
		}
	}
}
?>