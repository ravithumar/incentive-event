<?php
if(!function_exists('compress')){
	function compress($source, $destination, $quality){
		$info = getimagesize($source);
		// ini_set('memory_limit','3G');
		if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source); 
    
        elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source);

		// return imagejpeg($image, $destination, $quality);
		return imagejpeg($image, $destination, 75);
	}
}
if(!function_exists('compress_new')){
	function compress_new($source, $destination, $quality){
		$info = getimagesize($source);
		$memoryNeeded = round(($info[0] * $info[1] * $info['bits'] * $info['channels'] / 8 + Pow(2,16)) * 1.65); 
		ini_set("memory_limit",$memoryNeeded);
		if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source);
		
        elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source);
		
        elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source);


		imagejpeg($image, $destination,75);

		// Free up memory
		imagedestroy($image);
		
		// header('Content-Type: image/jpg');
		// return imagejpeg($image, $destination, $quality);
		return 1;
		// return $image;
	}
}

if(!function_exists('generate_referral_code')){
	function generate_referral_code($length_of_string = 8){
		$str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'; 
        return substr(str_shuffle($str_result), 0, $length_of_string); 
	}
}

if(!function_exists('generate_OTP')){
	function generate_OTP($length_of_string = 6){

		$generator = "1357902468"; 
  		$result = '';
	    for ($i = 1; $i <= $length_of_string; $i++) { 
	        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
	    }
	    return $result;
	}
}

if(!function_exists('get_delivery_miles')){
	function get_delivery_miles($lat1, $lon1, $lat2, $lon2){
		$result = [];
		$CI = &get_instance();
		$google_key = $CI->config->item('google_key');

		$api = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$lon1."&destinations=".$lat2.",".$lon2."&mode=driving&language=pl-PL&key=".$google_key);

		$data = json_decode($api);
		
		if(isset($data->rows[0]->elements[0]->distance->value)){

			$distance = ((int)$data->rows[0]->elements[0]->distance->value);
			$min = ($data->rows[0]->elements[0]->duration->text);

			if($distance != 0){
				$distance = number_format((float)$distance / 1000, 2, '.', '');
			}

			if($distance <= 7){
				$charge = 7;
			}else{
				$charge = round($distance);
			}
			$charge = number_format((float)$charge, 2, '.', '');

			$result['data'] = $data;
			$result['status'] = true;
			$result['distance'] = $distance;
			$result['min'] = $min;
			$result['charge'] = $charge;
		}else{
			$result['status'] = false;
		}
		return $result;
	}
}

if(!function_exists('get_delivery_charge')){
	function get_delivery_charge($lat1, $lon1, $lat2, $lon2){

		$CI = &get_instance();
	    $google_key = $CI->config->item('google_key');

        $api = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$lon1."&destinations=".$lat2.",".$lon2."&mode=driving&language=pl-PL&key=".$google_key);

        $data = json_decode($api);
        $distance = ((int)$data->rows[0]->elements[0]->distance->value);
        $distance = number_format((float)$distance / 1609, 2, '.', '');

		if($distance <= 7){
			$charge = 7;
		}else{
			$charge = $distance;
		}
		$charge = number_format((float)$charge, 2, '.', '');
		return $charge;
	}
}

if(!function_exists('get_store_time')){
	function get_store_time($current_time,$id){
		$today= date("l");
		$CI =& get_instance();
		$CI->load->database();
		$return_data = array();
		$CI->db->select('*');
		$CI->db->from('vendor_availability');
		$CI->db->where("user_id", $id);
		$CI->db->where('day', $today);
		$sql_query = $CI->db->get();
		if ($sql_query->num_rows() > 0) {
			$return_data = $sql_query->row_array();

			if($return_data['full_day'] == 1){
                // $time = 'Full Day Open';
                $time = true;

            }elseif($return_data['is_closed'] == 1){
                // $time = 'Closed';
                $time = false;
            }else{
                $from_time = DateTime::createFromFormat('h:i A',$return_data['from_time']);
                $from_time = $from_time->format('His');

                $to_time = DateTime::createFromFormat('h:i A',$return_data['to_time']);
                $to_time = $to_time->format('His');

                if ($current_time >= $from_time && $current_time <= $to_time)
                {
                    // $time = 'Open -'." ".$return_data['from_time']." "."To"." ".$return_data['to_time'];
                    $time = true;
                }else{
                    // $time = 'Closed';
					$time = false;
                }
            }
		}
		return $time;
	}
}

if(!function_exists('decode_emoticons')){
	function decode_emoticons($str){
		// return $str;
	    $replaced = preg_replace("/\\\\u([0-9A-F]{1,4})/i", "&#x$1;", $str);
	    $result = mb_convert_encoding($replaced, "UTF-16", "HTML-ENTITIES");
	    $name = mb_convert_encoding($result, 'utf-8', 'utf-16');

	    $name = ucfirst(trim($name, '"'));
        return stripslashes($name);
	}
}

if(!function_exists('get_session_data')){
	function get_session_data($id,$table){
		$CI =& get_instance();
		$CI->load->database();
		$return_data = array();
		$CI->db->select('*');
		$CI->db->from($table);
		$CI->db->where("deleted_at", NULL);
		$CI->db->where('id', $id);
		$sql_query = $CI->db->get();
		if ($sql_query->num_rows() > 0) {
			$return_data = $sql_query->row_array();
		}
		return $return_data;
	}
}
if(!function_exists('validate_customer_card')){
	function validate_customer_card($card_number){
	
	    $cardtype = array(
	        "visa"       => "/^4[0-9]{12}(?:[0-9]{3})?$/",
	        "mastercard" => "/^5[1-5][0-9]{14}$/",
	        "amex"       => "/^3[47][0-9]{13}$/",
	        "jcb"       => "/^(?:2131|1800|35\d{3})\d{11}$/",
	        "dinnerclub"       => "/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/",
	        "discover"   => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
	        "maestro"   => "/^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$/",
	    );
	    if (preg_match($cardtype['visa'],$card_number)){
	        return 'visa';
	    }else if (preg_match($cardtype['mastercard'],$card_number)){
	        return 'mastercard';
	    }else if (preg_match($cardtype['dinnerclub'],$card_number)){
	        return 'dinnerclub';
	    }else if (preg_match($cardtype['jcb'],$card_number)){
	        return 'jcb';
	    }else if (preg_match($cardtype['amex'],$card_number)){
	        return 'amex';
	    }else if (preg_match($cardtype['discover'],$card_number)){
	        return 'discover';
	    }else if (preg_match($cardtype['maestro'],$card_number)){
	        return 'maestro';
	    }else{
	        return '';
	    } 
	}
}
if(!function_exists('encrypt')){
	function encrypt($string){
	    $CI = &get_instance();
	    $key = $CI->config->item('custom_encryption_key');
	    $result = '';
	    for($i=0, $k= strlen($string); $i<$k; $i++) {
	        $char = substr($string, $i, 1);
	        $keychar = substr($key, ($i % strlen($key))-1, 1);
	        $char = chr(ord($char)+ord($keychar));
	        $result .= $char;
	    }
	    return base64_encode($result);
	}
}

if(!function_exists('decrypt')){
	function decrypt($string) {
		$CI = &get_instance();
	    $key = $CI->config->item('custom_encryption_key');
	    $result = '';
	    $string = base64_decode($string);
	    for($i=0,$k=strlen($string); $i< $k ; $i++) {
	        $char = substr($string, $i, 1);
	        $keychar = substr($key, ($i % strlen($key))-1, 1);
	        $char = chr(ord($char)-ord($keychar));
	        $result.=$char;
	    }
	    $result = str_replace('Cardnumber:','',$result);
	    return str_replace(' ','',$result);
	}
}
if(!function_exists('time_elapsed_string')){
    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
              'y' => 'year',
              'm' => 'month',
              'w' => 'week',
              'd' => 'day',
              'h' => 'hour',
              'i' => 'min',
              's' => 'sec',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
if(!function_exists('get_jb_perc')){
    function get_jb_perc() {
        $CI = &get_instance();
	    $key = $CI->config->item('jb_perc');
	    return $key;
    }
}
if (!function_exists('create_email_template'))
{
    function create_email_template($template)
    {
    	$CI = get_instance();
        $template = str_replace('##SITEURL##', BASE_URL(), $template);
        $template = str_replace('##SITELOGO##', $CI->config->item('site_logo'), $template);
        $template = str_replace('##FOOTER##', FOOTER_COPYRIGHT.'<br>'.FOOTER_ADDRESS, $template);
        $template = str_replace('##CONTACTEMAIL##', CONTACT_EMAIL, $template);
        return $template;
    }
}


if(!function_exists('send_SMS')){
	function send_SMS($mobile_number = NULL, $message = '', $prefix = '+1'){

		if(isset($mobile_number) && $mobile_number != ''){
			
			$mobile_number = str_replace(' ', '', trim($mobile_number));
			$mobile_number = preg_replace("/[^0-9]/", "", $mobile_number);
			$mobile_number = $prefix.$mobile_number;

			$CI = &get_instance();
			$CI->load->library('Twilio');
			if(($CI->twilio->validate_phone($mobile_number)) == TRUE){
			    $result = $CI->twilio->send($mobile_number, $message);
				$result = TRUE;
			}else{
				$result = FALSE;
			}

			//$result = $CI->twilio->send($mobile_number, $message);
			$result = TRUE;

			return $result;
		}else{
			return FALSE;
		}
		
	}
}

function send_web_push($player_ids = array(), $message = '' , $title = '',$type = ''){ 
	
	if(isset($player_ids) && !empty($player_ids) && isset($message) && $message != '' && isset($title) && $title != ''){
		$CI = &get_instance();
		$CI->load->library('Onesignal');
		$result = $CI->onesignal->send2($player_ids, $message , $title,$type);

		// $result = json_decode($result);

		// if(isset($result->errors)){
		// 	return array('status' => FALSE, 'error' => $result->errors, 'test_result' => $result);
		// }else{
		// 	return array('status' => TRUE, 'result' => $result->id, 'test_result' => $result);
		// }

		return array('status' => TRUE, 'result' => $result);
	}else{
		return array('status' => FALSE, 'error' =>'Something went wrong');
	}
	
}

if (!function_exists("send_mail_new")) {
    function send_mail_new($to, $subject, $message) {
        $CI = & get_instance();
        $CI->load->library("email");

        $config["protocol"] = $CI->config->item("protocol");
        $config["smtp_host"] = $CI->config->item("smtp_host");
        $config["smtp_port"] = $CI->config->item("smtp_port");
        $config["smtp_user"] = $CI->config->item("smtp_user");
        $config["smtp_pass"] = $CI->config->item("smtp_pass");
        $config["charset"] = $CI->config->item("smtp_charset");
        $config["mailtype"] = $CI->config->item("smtp_mailtype");
        $config["newline"] = "\r\n";

        $CI->email->clear();
        $CI->email->initialize($config);
        $CI->email->from($CI->config->item("from"), $CI->config->item("header"));
        $CI->email->to($to);
        $CI->email->reply_to($CI->config->item("from"), $CI->config->item("header"));
        $CI->email->subject($subject);
        $CI->email->message($message);
        /*$CI->email->attach("http://example.com/filename.pdf");
        $CI->email->attach("/path/to/photo3.jpg");*/
        $CI->email->send();

       	/* echo $CI->email->print_debugger();exit; */
    }
}

if (!function_exists("web_render")) {
	function web_render($page = null, $params = null, $return = false) {
		$CI = &get_instance();
		$params["body_class"] = _generate_body_class();
		$CI->load->view('web/layout/header', $params);
		if ($page != null) {
			$CI->load->view('web/pages/' . $page, $params, $return);
		}
		$CI->load->view('web/layout/footer', $params);
	}
}

if (!function_exists("_generate_body_class")) {
	function _generate_body_class() {
		$CI = &get_instance();
		if ($CI->uri->segment_array() == null) {
			$uri = array("index");
		} else {
			$uri = $CI->uri->segment_array();
			if (end($uri) == "index") {
				array_pop($uri);
			}
		}
		return implode("-", $uri);
	}
}
?>