<?php
if (!function_exists('SendNotification')) {

    function SendNotification($device_type = '',$device_token = '', $push_title = '', $push_data = array(), $push_type = '') {

        $CI = &get_instance();
      $key = $CI->config->item('fcm_key');
      define('API_ACCESS_KEY', $key);

      if($device_type == 'android'){
          // Android

          $fcmFields = array(
              'priority' => 'high',
              'to' => $device_token,
              'sound' => 'default',
              'data' => array( 
                  "title"=> $push_title,
                  "body"=> $push_data,
                  "type"=> $push_type,
                  "message" => $push_data['message']
                  )
              );

          if($push_type == 'silent'){
              $fcmFields['content_available'] = TRUE;
          }
      }else{
          // IOS
          $tokens = array($device_token);
          $fcmFields = array(
              'registration_ids' => $tokens,
              'priority' => 10,
              'notification' => array(
                  'title' => $push_title,
                  'body' => $push_data['message'],
                  'sound' => 'Default',
                  'image' => 'Notification Image',
                  'data' => $push_data,
                  'type' => $push_type
              ),
          );

      }

      $headers = array('Authorization: key=' . API_ACCESS_KEY,'Content-Type: application/json');
       
      $ch = curl_init();
      curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
      $result = curl_exec($ch );
      curl_close( $ch );
      //echo $fcmFields . "\n\n";
      return $result;
    }

	
}

if (!function_exists('SendNotification_old')) {

    function SendNotification_old($device_token = '', $push_title = '', $push_data = array(), $push_type = '') {

        $CI = &get_instance();
        $API_SERVER_KEY = $CI->config->item('fcm_key');

            $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
            $fields = array(
                'to' => $device_token,
                'notification' => array(
                    'title' => $push_title, 'body' => $push_data['message'], "type"=> $push_type, 'sound' => 'Default', 'image' => 'Notification Image',
                    'data' => $push_data
                ),
                'data' => array(
                    'title' => $push_title, 'body' => $push_data['message'], "type"=> $push_type, 'sound' => 'Default', 'image' => 'Notification Image',
                    'data' => $push_data
                ),
            );
            $headers = array(
            'Authorization:key=' . $API_SERVER_KEY,
            'Content-Type:application/json',
            );

            // Open connection

            $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);

            curl_close($ch);

            return $result;

    }
}

if (!function_exists('SendNotificationMultiple')) {

	function SendNotificationMultiple($title, $message, $tokens, $data = []) {

		$API_SERVER_KEY = config('fcm_key');
		$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids' => $tokens,
			'notification' => array('title' => $title, 'body' => $message, 'sound' => 'Default', 'image' => 'Notification Image', 'data' => $data),
			'data' => array('title' => $title, 'body' => $message, 'sound' => 'Default', 'image' => 'Notification Image', 'data' => $data),
		);
		$headers = array(
			'Authorization:key=' . $API_SERVER_KEY,
			'Content-Type:application/json',
		);

		// Open connection

		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);

		curl_close($ch);
		// echo $result;exit;
		return true;

	}
}

if (!function_exists('SendLogoutNotification')) {

	function SendLogoutNotification($title, $message, $token) {

		$API_SERVER_KEY = config('fcm_key');
		$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'to' => $token,
			'notification' => array('title' => $title, 'body' => $message, 'sound' => 'Default', 'image' => 'Notification Image', 'data' => [], 'type' => 'logout'),
			'data' => array('title' => $title, 'body' => $message, 'sound' => 'Default', 'image' => 'Notification Image', 'data' => [], 'type' => 'logout'),
		);
		$headers = array(
			'Authorization:key=' . $API_SERVER_KEY,
			'Content-Type:application/json',
		);

		// Open connection

		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);

		curl_close($ch);
		// echo $result;exit;
		return true;

	}
}

if (!function_exists('sendEmail')) {
	function sendEmail($subject, $to, $template, $data = []) {
		
		$CI = get_instance();
		$CI->load->library('send_email');
		try {
			$CI->send_email->send($subject, $to, $template, $data);
			return true;
		} catch (Exception $e) {
			return true;
		}
	}
}
//Shivani
if (!function_exists('sendEmail3')) {
	function sendEmail3($subject, $to, $template, $data = []) {

		$CI = get_instance();
        $message = $CI->load->view($template, $data, TRUE);
        $CI->load->library('email');
        $config['protocol'] = $CI->config->item("protocol");
        $config['smtp_host'] = $CI->config->item("smtp_host");
        $config['smtp_port'] = $CI->config->item("smtp_port");
        $config['smtp_user'] = $CI->config->item("smtp_user");
        $config['smtp_pass'] = $CI->config->item("smtp_pass");
        $config['charset'] = $CI->config->item("charset");
        $config['mailtype'] = $CI->config->item("mailtype");
        $config['newline'] = "\r\n";

        $CI->email->clear();
        $CI->email->initialize($config);
        //$CI->email->from('eric.a.stout@qwnched.com',"Qwnched");
        $CI->email->from($CI->config->item('from'), "Just Borrowed");
        $CI->email->to($to);
        $CI->email->subject($subject); 
        $CI->email->message($message);
       
        if($CI->email->send()){
            return TRUE;
        }else{
            return FALSE;
        }
        
	}
}
if (!function_exists('notification_add')) {
    function notification_add($user_id = NULL, $message = NULL, $type = NULL,$user_id_2 = 0,$order_id = 0 ,$product_id = 0){
        $CI = &get_instance();
        if(isset($user_id)){
            $data = array(
                        'user_id' => $user_id,
                        'message' => $message,
                        'type' => $type,
                        'user_id_2' => $user_id_2,
                        'order_id' => $order_id,
                        'product_id' => $product_id
                    );

            if($CI->db->insert("notifications", $data)){
                return TRUE;
            }else{
                return FALSE;
            }
            
        }else{
            return FALSE;
        }
        
    }
}

if(!function_exists('send_mail_ci'))
{
	function send_mail_ci()
	{
		$CI = &get_instance();
        $CI->load->library('email');
        $fromemail = "developer.eww@gmail.com";
        $toemail   = "thumarravi77@gmail.com";
        $subject   = "HJM";
        $mesg = "Test";

        $config = [
            'charset'  => 'utf-8',
            'wordwrap' => true,
            'mailtype' => 'html',
            'protocol' => 'sendmail',
            'mailpath' => '/usr/sbin/sendmail',
        ];

        $CI->email->initialize($config);

        $CI->email->to($toemail);
        $CI->email->from($fromemail, "HJM");
        $CI->email->subject($subject);
        $CI->email->message($mesg);
        if ($CI->email->send()) {
        	echo "s";
            // $CI->session->set_flashdata("email_sent", "Email sent successfully.");
        } else {
        	echo "n";
            // $CI->session->set_flashdata("email_sent", "Error in sending Email.");
        }
	}
}
if (!function_exists('sendEmail2')) {

    function sendEmail2($subject = '', $to = '', $message = '') {

        $CI = &get_instance();

        

		$config['protocol']  = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = $CI->config->item('smtp_user');
        $config['smtp_pass'] = $CI->config->item('smtp_pass');
        $config['charset']   = "utf-8";
        $config['mailtype']  = "html";
        $config['newline']   = "\r\n";

        $CI->email->initialize($config);

        $CI->email->from($CI->config->item('from'), SITENAME);

        $CI->email->to($to);

        $CI->email->subject($subject);

        $CI->email->message($message);

        if ($CI->email->send()) {
            return TRUE;
        } else {
//            $error = show_error($CI->email->print_debugger());
            return FALSE;
        }
    }


    if (!function_exists('create_email_template_new'))
{
    function create_email_template_new($template)
    {
        //echo 'dsafsf';exit;
        $ci=& get_instance();
        $base_url = BASE_URL();
        $template = str_replace('##SITEURL##', $base_url, $template);
        $template = str_replace('##SITENAME##', $ci->config->item('site_name'), $template);
        $template = str_replace('##SITEEMAIL##', $ci->config->item('site_email'), $template);
        $template = str_replace('##FOOTER##', $ci->config->item('footer'), $template);
        $template = str_replace('##SITELOGO##', $ci->config->item('site_logo'), $template);
        $template = str_replace('##CURRENCY##', $ci->config->item('currency'), $template);         
        return $template;
    }

}
}