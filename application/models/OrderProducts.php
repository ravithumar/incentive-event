<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class OrderProducts extends Eloquent {

    protected $table = 'order_products';

    protected $appends =
    [
        'product_name',
    ];

    public function product()
    {
        $CI = &get_instance();
        $CI->load->model('Product');
        return $this->hasOne('Product', 'id', 'product_id');
    }

    public function orderproductsaddon() {
        $CI = &get_instance();
        $CI->load->model('OrderProductAddOn');
        return $this->hasMany('OrderProductAddOn', 'order_product_id', 'id');
    }

    public function getProductNameAttribute()
    {
        $CI = &get_instance();
        $product =  $CI->db->get_where('product', ['id' => $this->product_id])->row();
        if(isset($product->title)){
        	return $product->title;
        }else{
        	return '';
        }
    }
}
?>

