<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class TrackOrder extends Eloquent {

	protected $table = 'track_order';

	protected $appends =
    [
        'date_formatted'
    ];

	public function getDateFormattedAttribute()
    {
        if(isset($this->created_at)){

			$created_at = DateTime::createFromFormat('Y-m-d H:i:s',$this->created_at);
			$created_at_date = $created_at->format('d M, Y');
			$created_at_time = $created_at->format('h:i A');
			return $created_at_date.' '.'at'.' '.$created_at_time;
        }else{
            return '';
        }
    }

}
?>

