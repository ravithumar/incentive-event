<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent {
	use SoftDeletes;
	protected $table = 'users';

	protected $hidden = ['password', 'created_at', 'updated_at', 'deleted_at',
		'activation_selector', 'activation_code', 'forgotten_password_selector', 'forgotten_password_code', 'forgotten_password_time', 'remember_selector', 'remember_code', 'created_on', 'last_login', 'username', 'ip_address'];

	protected $appends =
    [
        // 'balance',
  //       'vendor_time',
		// 'rating',
		'group'
	];

	// protected $attribute = ['profile'];

	public function getEmailAttribute($email) {
		return $email == null ? '' : $email;
	}
	public function getRatingAttribute($rating) {

		$CI = &get_instance();
		$rating = 0;
        //$orders =  $CI->db->get_where('orders', ['delivery_boy_id' => $this->id,'status' => 10])->result_array();
        /* if(isset($orders)){
			foreach ($orders as $okey => $ovalue) {
				$rating += $ovalue['delivery_boy_rate'];
			}
			$rating = $rating / count($orders);
			$rating = round($rating);
        } */
		return $rating ;
	}

	public function getBalanceAttribute()
    {
        $CI = &get_instance();
        $wallet =  $CI->db->get_where('wallet', ['user_id' => $this->id])->row();
        if(isset($wallet->balance)){
        	return $wallet->balance;
        }else{
        	return 0;
        }
    }
	public function getGroupAttribute()
    {
        $CI = &get_instance();
        $group =  $CI->db->get_where('users_groups', ['user_id' => $this->id])->row();
        if(isset($group->group_id)){
        	return $group->group_id;
        }else{
        	return 0;
        }
    }
	public function total_orders()
    { 
        $CI = &get_instance();
        $orders =  $CI->db->get_where('orders', ['vendor_id' => $this->id,'status' => 10])->row();
        if(isset($orders)){
        	return array('total_orders' => $orders->num_rows());
        }else{
        	return array('total_orders' => 0);
        }
    }
	public function getProfilePictureAttribute($profile_picture) {
		return $profile_picture == null ? 'assets/images/default.png' : '/assets/files/users/'.$profile_picture;
	}
	public function getDrivingLicenseAttribute($driving_license) {
		return $driving_license == null ? 'assets/images/default.png' : '/assets/files/document/'.$driving_license;
	}
	public function getInsuranceCertiAttribute($insurance_certi) {
		return $insurance_certi == null ? 'assets/images/default.png' : '/assets/files/document/'.$insurance_certi;
	}
	
	public function role() {
		$CI = &get_instance();
		return $CI->db->get_where('users_groups', ['user_id' => $this->id])->row();
	}

	public function style() {
		$CI = &get_instance();
		return $CI->db->get_where('users_style', ['user_id' => $this->id])->result_array();
	}

	public function vendor()
    {
        $CI = &get_instance();
        $CI->load->model('Vendor');
        return $this->hasOne('Vendor', 'user_id', 'id');
    }
	public function device()
    {
        $CI = &get_instance();
        $CI->load->model('Devices');
        return $this->hasOne('Devices', 'user_id', 'id');
    }
	public function product() {
		$CI = &get_instance();
        $CI->load->model('Product');
        return $this->hasMany('Product', 'user_id', 'id');
	}

	public function wallet_history() {
		$CI = &get_instance();
        $CI->load->model('WalletHistory');
        return $this->hasMany('WalletHistory', 'user_id', 'id')->orderBy('id','desc');
	}

	public function delivery_address() {
		$CI = &get_instance();
        $CI->load->model('DeliveryAddress');
        return $this->hasMany('DeliveryAddress', 'user_id', 'id')->orderBy('id','desc');
	}

	public function vendor_availability() {
		$CI = &get_instance();
        $CI->load->model('VendorAvailibility');
        return $this->hasMany('VendorAvailibility', 'user_id', 'id')->orderBy('id');
	}

	public function getVendorTimeAttribute()
    {
        $CI = &get_instance();
        $today= date("l");
        $current_time = date("His");
        $vendor_availability =  $CI->db->get_where('vendor_availability', ['user_id' => $this->id , 'day' => $today])->row();
        if (isset($vendor_availability)){
        	if($vendor_availability->full_day == 1){
                $time = 'Full Day Open';

            }elseif($vendor_availability->is_closed == 1){
                $time = 'Closed';
            }else{
                $from_time = DateTime::createFromFormat('h:i A',$vendor_availability->from_time);
                $from_time = $from_time->format('His');

                $to_time = DateTime::createFromFormat('h:i A',$vendor_availability->to_time);
                $to_time = $to_time->format('His');

                if ($current_time >= $from_time && $current_time <= $to_time)
                {
                    $time = 'Open -'." ".$vendor_availability->from_time." "."To"." ".$vendor_availability->to_time;
                }else{
                    $time = 'Closed';
                }
            }
            // return $current_time.' '.$from_time.' '.$to_time ;
            return $time ;
        }else{
        	return '';
        }
    }

	/*public function vendor_availability_time() {
		$CI = &get_instance();

		$vendor_availability =  $this->morphMany(TraderRatings::class, 'rateable')
        ->select('rateable_id', 'rateable_type', 'rating')
        ->avg('rating');

		$today= date("l");
        $vendor_availability =  $CI->db->get_where('vendor_availability', ['user_id' => $this->id , 'day' => $today])->row();
        if (isset($vendor_availability)){

        	if($vendor_availability['full_day'] == 1){
                $time = 'Full Day Open';

            }elseif($vendor_availability['is_closed'] == 1){
                $time = 'Closed';
            }else{
                $current_time = date("h:i A");
                if ($current_time >= $vendor_availability['from_time'] && $current_time <= $vendor_availability['to_time'])
                {
                    $time = 'Open -'." ".$vendor_availability['from_time']." "."To"." ".$vendor_availability['to_time'];
                }else{
                    $time = 'Closed';
                }
            }
            return $time;
        }else{
        	return '';
        }
	}*/


	public function profileUpdate($update, $id) {
		Self::whereId($id)->update($update);
		return true;
	}

	public function userResponse($id)
	{
		$user = Self::find($id);
		$resp = [
			"id" => $user->id,
			"active" => $user->active,
			"first_name" => $user->first_name,
			"last_name" => $user->last_name,
			"email" => $user->email,
			"phone" => $user->phone,
			"date_of_birth" => $user->date_of_birth,
			"type" => $user->type,
			"image" => $user->image,
			"verify" => $user->verify
		];
		return $resp;
	}

	
	public static function registerResponse() {
		return [
			"id" => $this->id,
			"email" => $this->email,
			"active" => $this->active,
			"first_name" => $this->first_name,
			"last_name" => $this->last_name,
			"phone" => $this->phone,
			"date_of_birth" => $this->date_of_birth,
			"image" => $this->image
		];
	}

}
