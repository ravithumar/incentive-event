<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;

class ProductVariant extends Eloquent {

	protected $table = 'product_variants';

	protected $appends =
    [
        'variant_name',
    ];

    public function getVariantNameAttribute()
    {
        $CI = &get_instance();
        $variant =  $CI->db->get_where('variant', ['id' => $this->variant_id])->row();
        return $variant->name;
    }

}
?>

