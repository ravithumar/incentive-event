<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class PaymentCard extends Eloquent {

	use SoftDeletes;
	protected $table = 'payment_cards';

}
?>

