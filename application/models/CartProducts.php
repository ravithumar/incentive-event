<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class CartProducts extends Eloquent {

	use SoftDeletes; 
    protected $hidden = ['created_at','updated_at', 'deleted_at'];
    protected $table = 'cart_products';

    protected $appends =
    [
        'qty_price'
    ];

    public function cartproductaddon() {
        $CI = &get_instance();
        $CI->load->model('CartProductAddons');
        return $this->hasMany('CartProductAddons', 'cart_product_id', 'id');
    }

    public function productaddon() {
        $CI = &get_instance();
        $CI->load->model('ProductAddOn');
        return $this->hasMany('ProductAddOn', 'product_id', 'product_id');
    }

    public function product()
    {
        $CI = &get_instance();
        $CI->load->model('Product');
        return $this->hasOne('Product', 'id', 'product_id');
    }

    public function cart()
    {
        $CI = &get_instance();
        $CI->load->model('Cart');
        return $this->hasOne('Cart', 'id', 'cart_id');
    } 

    public function getQtyPriceAttribute()
    {
        $CI = &get_instance();
        $product =  $CI->db->get_where('product', ['id' => $this->product_id])->row();
        if(isset($product->price)){
            $price = $product->price * $this->qty;
            $price = number_format((float)$price, 2, '.', '');
        	return $price;
        }else{
        	return 0;
        }
    }
}
?>

