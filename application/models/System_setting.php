<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class System_setting extends Eloquent {

	use SoftDeletes;
	protected $table = 'system_config';

	public function getimageAttribute($image) {
		return $image == null ? '/assets/images/default.png' : '/assets/files/'.$image;
	}
	
}
?>

