<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Product extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['updated_at', 'deleted_at'];

	protected $table = 'product';

    protected $appends =
    [
        'category',
        'created_at_formatted',
    ];

    public function variant() {
        $CI = &get_instance();
        $CI->load->model('ProductVariant');
        return $this->hasMany('ProductVariant', 'product_id', 'id');
    }

     public function addon() {
        $CI = &get_instance();
        $CI->load->model('ProductAddOn');
        return $this->hasMany('ProductAddOn', 'product_id', 'id');
    }

    public function user()
    {
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'user_id');
    }

    public function vendor()
    {
        $CI = &get_instance();
        $CI->load->model('Vendor');
        return $this->hasOne('Vendor', 'user_id', 'user_id');
    }

    public function getCreatedAtFormattedAttribute()
    {
        if(isset($this->created_at)){
            $created_at_obj = DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at);

            return $created_at_obj->format('j M, Y h:i A');
        }else{
            return '';
        }
        
    }

    public function getimageAttribute($image) {
        return $image == null ? '/assets/images/default.png' : '/assets/files/product_pictures/'.$image;
    }

    public function getCategoryAttribute()
    {

        $CI = &get_instance();
        $category =  $CI->db->get_where('category', ['id' => $this->category_id])->row();
        $image = $category->image == null ? '' : '/assets/files/category/'.$category->image;
        return ['name' => $category->name, 'image' => $image];
    }

    
}
?>

