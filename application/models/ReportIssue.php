<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class ReportIssue extends Eloquent {

	use SoftDeletes;
	protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

	protected $table = 'report_issue';
    public function issue()
    {
        $CI = &get_instance();
        $CI->load->model('Issue');
        return $this->hasOne('Issue', 'id', 'issue_id');
    }
}
?>

