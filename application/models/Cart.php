<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Cart extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['created_at','updated_at', 'deleted_at'];
    protected $table = 'cart';

    public function cartproducts()
    {
        $CI = &get_instance();
        $CI->load->model('CartProducts');
        return $this->hasOne('CartProducts', 'cart_id', 'id');
    } 
}
?>

