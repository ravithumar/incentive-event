<?php defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class VendorAvailibility extends Eloquent {
	protected $table = 'vendor_availability';

	protected $hidden = ['updated_at', 'created_at'];
}