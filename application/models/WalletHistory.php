<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class WalletHistory extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['created_at','updated_at', 'deleted_at'];
    protected $table = 'wallet_history';

    protected $appends =
    [
        'created_at_formatted'
    ];

    public function getCreatedAtFormattedAttribute()
    {
        if(isset($this->created_at)){
            $created_at_obj = DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at);
            return $created_at_obj->format('j M, Y H:i A');
        }else{
            return '';
        }
        
    }

}
?>

