<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Promocode extends Eloquent {

	use SoftDeletes;
	protected $table = 'promocode';
	protected $hidden = ['created_at','updated_at', 'deleted_at'];
	
}
?>

