<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Vendor extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['created_at','updated_at', 'deleted_at'];
    protected $table = 'vendor';

    public function getBgImgAttribute($profile_picture) {
		  return $profile_picture == null ? 'assets/images/default.png' : '/assets/files/users/'.$profile_picture;
    }
    public function user()
    {
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'user_id');
    }

	
}
?>

