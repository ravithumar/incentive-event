<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Banner extends Eloquent {

	use SoftDeletes;
	protected $hidden = ['created_at','updated_at', 'deleted_at'];
	protected $table = 'banner';

	public function getimageAttribute($image) {
		return $image == null ? '/assets/images/default.png' : '/assets/files/banner/'.$image;
	}
}
?>

