<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'WebsiteController';
$route['404_override'] = 'WebsiteController';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin/DashboardController/index';
$route['admin/login'] = 'Auth/login';
$route['admin/logout'] = 'Auth/logout';

// $route['cron'] = 'CronController/request_exipre';
$route['email-verify/(:any)'] = 'UserController/email_verify/$1';

$route['admin/change-password'] = 'admin/AdminController/change_password';
$route['admin/dashboard'] = 'admin/DashboardController';

$route['admin/users'] = 'admin/UsersController';
$route['admin/user/edit/(:num)'] = 'admin/UsersController/put/$1';
$route['admin/user/add'] = 'admin/UsersController/put/';
$route['admin/user/view/(:num)'] = 'admin/UsersController/view/$1';

$route['admin/setting'] = 'admin/SystemSettingController/setting';
$route['admin/setting-update'] = 'admin/SystemSettingController/setting_put';

$route['admin/appsetting'] = 'admin/AppSettingController/index';
$route['admin/appsetting-update'] = 'admin/AppSettingController/app_setting_put';

$route['admin/faq'] = 'admin/FaqController';
$route['admin/faq/add'] = 'admin/FaqController/add';	
$route['admin/faq/edit/(:num)'] = 'admin/FaqController/edit/$1';	

$route['admin/issue'] = 'admin/IssueController';
$route['admin/issue/add'] = 'admin/IssueController/add';	
$route['admin/issue/edit/(:num)'] = 'admin/IssueController/edit/$1';	

$route['admin/vendors'] = 'admin/VendorController';
$route['admin/vendor/add'] = 'admin/VendorController/add';
$route['admin/vendor/edit/(:num)'] = 'admin/VendorController/put/$1';
$route['admin/vendor/view/(:num)'] = 'admin/VendorController/view/$1';
$route['admin/vendor-active/(:num)'] = 'vendor/StatusController/vendor_active/$1';

$route['admin/delivery-boy'] = 'admin/DeliveryBoyController';
$route['admin/delivery-person'] = 'admin/DeliveryBoyController';
$route['admin/delivery-boy/edit/(:num)'] = 'admin/DeliveryBoyController/put/$1';
$route['admin/delivery-boy/view/(:num)'] = 'admin/DeliveryBoyController/view/$1';

$route['admin/pending-delivery-boy'] = 'admin/DeliveryBoyController/pending_list';
$route['admin/pending-delivery-person'] = 'admin/DeliveryBoyController/pending_list';
$route['admin/pending-delivery-boy/edit/(:num)'] = 'admin/DeliveryBoyController/pending_list_put/$1';
$route['admin/pending-delivery-boy/view/(:num)'] = 'admin/DeliveryBoyController/pending_list_view/$1';

//style
$route['admin/category'] = 'admin/CategoryController/index';
$route['admin/category/add'] = 'admin/CategoryController/add';	
$route['admin/category/edit/(:num)'] = 'admin/CategoryController/edit/$1';	

//banner 
$route['admin/banner'] = 'admin/BannerController/index';
$route['admin/set-sequence'] = 'admin/BannerController/set_sequence';
$route['admin/banner/add'] = 'admin/BannerController/add';	
$route['admin/banner/edit/(:num)'] = 'admin/BannerController/edit/$1';	


//product
$route['admin/product'] = 'admin/ProductController/index';
$route['admin/product/view/(:num)'] = 'admin/ProductController/view/$1';
$route['admin/featured-products'] = 'admin/ProductController/featured_products';
// $route['admin/set-sequence'] = 'admin/ProductController/set_sequence';

$route['admin/order-arrived'] = 'admin/OrderController/order_arrived_index';
$route['admin/order-arrived/view/(:num)'] = 'admin/OrderController/order_arrived_view/$1';

$route['admin/order-inprocess'] = 'admin/OrderController/order_inprocess_index';

$route['admin/order-inprocess/view/(:num)'] = 'admin/OrderController/order_inprocess_view/$1';

$route['admin/order-assigned'] = 'admin/OrderController/order_assigned_index';
$route['admin/assign_delivery_boy'] = 'admin/OrderController/assign_delivery_boy';
$route['admin/order-assigned/view/(:num)'] = 'admin/OrderController/order_assigned_view/$1';

$route['admin/order-rejected'] = 'admin/OrderController/order_rejected_index';
$route['admin/order-rejected/view/(:num)'] = 'admin/OrderController/order_rejected_view/$1';

$route['admin/order-completed'] = 'admin/OrderController/order_completed_index';
$route['admin/order-completed/view/(:num)'] = 'admin/OrderController/order_completed_view/$1';

$route['admin/all-orders'] = 'admin/OrderController/all_orders_index';
$route['admin/send_push_notification'] = 'admin/OrderController/send_push_notification';
$route['admin/all-orders/view/(:num)'] = 'admin/OrderController/all_orders_view/$1';

$route['admin/earning'] = 'admin/EarningController/index';

$route['admin/promocode'] = 'admin/PromocodeController/index';
$route['admin/promocode/add'] = 'admin/PromocodeController/add';	
$route['admin/promocode/edit/(:num)'] = 'admin/PromocodeController/edit/$1';	

$route['admin/image/add/(:num)'] = 'admin/ProductController/image_add/$1';
$route['admin/img_delete'] = 'admin/ProductController/img_delete';

$route['admin/order'] = 'admin/OrderController/index';
$route['admin/order/view/(:num)'] = 'admin/OrderController/view/$1';
$route['admin/order/track'] = 'admin/OrderController/track';

$route['admin/request'] = 'admin/RequestController/index';

$route['admin/contact-us'] = 'admin/ContactController/index';

$route['admin/report-issue'] = 'admin/ReportIssueController/index';

$route['admin/course'] = 'admin/TestController/index';

$route['admin/testing'] = 'admin/DashboardController/testing';
$route['admin/forgot-password'] = 'admin/AdminController/forgot_password';

// vendor activation 
$route['activate/(:any)'] = 'UserController/activate_account/$1';
$route['something-went-wrong'] = 'UserController/something_went_wrong';
$route['account_activated'] = 'UserController/account_activated';

//vebdor forgot
$route['forgotten/(:any)'] = 'UserController/forgotten_account/$1';

//Event Organizer
$route['organizer/login'] = 'Auth/login';
$route['organizer/logout'] = 'Auth/logout';
$route['organizer/dashboard'] = 'organizer/DashboardController/index';
$route['organizer/change-password'] = 'organizer/OrganizerController/change_password';

$route['organizer/forgot-password'] = 'organizer/OrganizerController/forgot_password';


$route['organizer/eventuser'] = 'organizer/EventUserController/index';
$route['organizer/eventuser/add'] = 'organizer/EventUserController/add';	
$route['organizer/eventuser/edit/(:num)'] = 'organizer/EventUserController/edit/$1';	
$route['organizer/eventuser/view/(:num)'] = 'organizer/EventUserController/view/$1';


$route['organizer/student'] = 'organizer/StudentController/index';
$route['organizer/student/edit/(:num)'] = 'organizer/StudentController/put/$1';
$route['organizer/student/add'] = 'organizer/StudentController/put/';
$route['organizer/student/view/(:num)'] = 'organizer/StudentController/view/$1';
// $route['organizer/my-profile/(:num)'] = 'organizer/OrganizerController/my_profile_update/$1';
$route['organizer/my-profile'] = 'organizer/OrganizerController/my_profile_update/';

$route['organizer/variant'] = 'organizer/VariantController/index';
$route['organizer/variant/add'] = 'organizer/VariantController/add';	
$route['organizer/variant/edit/(:num)'] = 'organizer/VariantController/edit/$1';

$route['organizer/product'] = 'organizer/ProductController/index';
$route['organizer/product/add'] = 'organizer/ProductController/add';	
$route['organizer/product/edit/(:num)'] = 'organizer/ProductController/edit/$1';
$route['organizer/product/view/(:num)'] = 'organizer/ProductController/view/$1';

$route['organizer/featured-products'] = 'organizer/ProductController/featured_products';
$route['organizer/set-sequence'] = 'organizer/ProductController/set_sequence';

$route['organizer/order-arrived'] = 'organizer/OrderController/order_arrived_index';
$route['organizer/order-arrived/view/(:num)'] = 'organizer/OrderController/order_arrived_view/$1';

$route['organizer/order-inprocess'] = 'organizer/OrderController/order_inprocess_index';

$route['organizer/order-inprocess/view/(:num)'] = 'organizer/OrderController/order_inprocess_view/$1';

$route['organizer/order-rejected'] = 'organizer/OrderController/order_rejected_index';
$route['organizer/order-rejected/view/(:num)'] = 'organizer/OrderController/order_rejected_view/$1';

$route['organizer/order-completed'] = 'organizer/OrderController/order_completed_index';
$route['organizer/order-completed/view/(:num)'] = 'organizer/OrderController/order_completed_view/$1';

$route['organizer/all-orders'] = 'organizer/OrderController/all_orders_index';
$route['organizer/send_push_notification'] = 'organizer/OrderController/send_push_notification';
$route['organizer/all-orders/view/(:num)'] = 'organizer/OrderController/all_orders_view/$1';

$route['organizer/earning'] = 'organizer/EarningController/index';
$route['organizer/contact'] = 'organizer/ContactController/index';
$route['organizer/account'] = 'organizer/AccountController/index';

$route['organizer/event'] = 'organizer/EventController/index';
$route['organizer/event/add'] = 'organizer/EventController/add';	
$route['organizer/event/edit/(:num)'] = 'organizer/EventController/edit/$1';	

//dispatcher
$route['dispatcher/login'] = 'Auth/login';
$route['dispatcher/dashboard'] = 'dispatcher/DashboardController/index';
$route['dispatcher/change-password'] = 'dispatcher/DispatcherController/change_password';


$route['dispatcher/user'] = 'dispatcher/DeliveryBoyController';
$route['dispatcher/user/edit/(:num)'] = 'dispatcher/DeliveryBoyController/put/$1';
$route['dispatcher/user/view/(:num)'] = 'dispatcher/DeliveryBoyController/view/$1';


$route['testsocket'] = 'admin/Common/test_socket';

$route['add-channel'] = 'WebsiteController/add_channel';
$route['add-channel-admin'] = 'WebsiteController/add_channel_admin';

$route['login'] = 'WebsiteController';
$route['events'] = 'web/EventController/index';
$route['users/logout'] = 'Auth/logout';

$route['assign-to-admin'] = 'api/customer/Order/web_push';

$route['admin/events-organizer'] = 'admin/EventsOrganizerController';
$route['admin/events-organizer/edit/(:num)'] = 'admin/EventsOrganizerController/put/$1';
$route['admin/events-organizer/add'] = 'admin/EventsOrganizerController/put/';
$route['admin/events-organizer/view/(:num)'] = 'admin/EventsOrganizerController/view/$1';