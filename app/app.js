var config = require("./config")()
var functions = require("./functions");

var https = require("https");
var fs = require("fs");
var unirest = require("unirest");
express = require("express"),
app = express();

/*var ssl_option = {
	key: fs.readFileSync("/etc/nginx/ssl/naqsa.qa/1066239/server.key"),
	cert: fs.readFileSync("/etc/nginx/ssl/naqsa.qa/1066239/server.crt")
};

var server = https.createServer(ssl_option, app).listen(config.port, function () {
	console.log("Server listening on: "+config.port);
});*/

var server = require("http").Server(app);
var io = require("socket.io")(server, {pingInterval: 60000 * 24, pingTimeout: 60000 * 24});
var port = app.get("port");

server.listen(config.port, function () {
	console.log("Server listening on: %s", config.port);
});

var user_ = config.user_prefix;
var deliveryboy_ = config.deliveryboy_prefix;
var base_url = config.base_url;

var unix_time = require("unix-time");

var FCM = require("fcm-node");
var fcm = new FCM(config.fcm_server_key);

var moment = require("moment");
var geodist = require("geodist");
var available_drivers = [];
var drivers_updated = {};
var driver_waiting_time = 150000;

app.get("/", function (req, res) {
  res.json({status:true});
});

app.get("/app.js", function (req, res) {
	functions.get_results_mysql("faq","id <= 4","*",config.con, function(results){  
		res.json({results:results});
	});
});

app.get("/appsetting", function (req, res) {
	functions.get_results_mysql("appsetting","id = 4","*",config.con, function(results){  
		res.json({results:results});
	});
});

io.on("connection", function(socket) {
	socket.on("connect_user", function (data) {
        var customer_id = functions.validate_params(data.customer_id);
		console.log("-------------------------------------");
        console.log("connect_user data : ", data);
		console.log("-------------------------------------");
        if (customer_id !== false) {
            var socket_user_id = user_ + customer_id;
            socket.username = socket_user_id;
            socket.room = socket_user_id;
            socket.join(socket_user_id);
            io.sockets.in(socket_user_id).emit("connect_user", {message: "Customer Connected ..."});
            
        }
    });

  socket.on("connect_deliveryboy", function (data) {
        var deliveryboy_id = functions.validate_params(data.deliveryboy_id);
		console.log("-------------------------------------");
        console.log("connect_deliveryboy data : ", data);
		console.log("-------------------------------------");
        if (deliveryboy_id !== false) {
            var socket_user_id = deliveryboy_ + deliveryboy_id;
            socket.username = socket_user_id;
            socket.room = socket_user_id;
            socket.join(socket_user_id);
            io.sockets.in(socket_user_id).emit("connect_deliveryboy", {message: "Delivery Boy Connected ..."});
            /* console.log("socket.room",socket.room); */
        }
    });

    /* socket.on("customer_interval", function (data) {
		var customer_id = functions.validate_params(data.customer_id);
		if (customer_id !== false) {
			var socket_user_id = user_ + customer_id;
			io.sockets.in(socket_user_id).emit("customer_interval", {message: "Connected ..."});
		}
    });
    socket.on("delivery_interval", function (data) {
		var deliveryboy_id = functions.validate_params(data.deliveryboy_id);
		if (deliveryboy_id !== false) {
			var socket_user_id = deliveryboy_ + deliveryboy_id;
			io.sockets.in(socket_user_id).emit("delivery_interval", {message: "Connected ..."});
		}
    }); */

    socket.on("send_message", function (data) {
		console.log("-------------------------------------");
        console.log("send_message data : ", data);
		console.log("-------------------------------------");
        var sender_id = functions.validate_params(data.sender_id);  
        var receiver_id = functions.validate_params(data.receiver_id);  
        var sender_type = functions.validate_params(data.sender_type);  
        var message = functions.validate_params(data.message); 
        var order_id = functions.validate_params(data.order_id);
        if (sender_id !== false && receiver_id !== false && message !== false && order_id !== false && sender_type !== false) {
			console.log("-------------------------------------");
			console.log("sender_id : ", sender_id);
			console.log("-------------------------------------");
            var UpdatedDate = new Date().toLocaleString("en-US", {
                timeZone: "America/Toronto"
            });

            message = message.replace(/'/g, "\\'");

            var created_at = moment(UpdatedDate).format("YYYY-MM-DD HH:mm:ss");
            var created_time = moment(UpdatedDate).format("hh:mm A");
            /* console.log("date===",created_at); */
            var table = "users";
            var where = "id = " + sender_id;
            var select = "id,first_name,last_name,profile_picture";

            functions.get_results_mysql(table, where, select, config.con, function(results) {
				console.log("-------------------------------------");
				console.log(select+" "+table+" "+where+" "+" results : ", results);
				console.log("-------------------------------------");
                var sql = "insert into chat(order_id,sender_id,receiver_id,message,created_at) values('" + order_id + "','" + sender_id + "','" + receiver_id + "','" + message + "','" + created_at + "')";
                /* console.log('sql: ',sql); */
                /* console.log("@@@msgggggggggggggggggggg", sender_id, sender_id, data.message); */
                functions.insert_record_mysql(sql, config.con, function(last_chat_id) {});

                if(sender_type == "user"){
                    io.sockets.in(deliveryboy_ + receiver_id).emit("receiver_message", {
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        message: data.message,
                        created_at: created_at,
                        created_time: created_time,
                        order_id: order_id,
                        /* sender: results[0], */
                        send_by:"other"
                    });
					console.log("-------------------------------------");
					console.log("user receiver_message : ", deliveryboy_ + sender_id);
					console.log("-------------------------------------");
                    io.sockets.in(user_ + sender_id).emit("receiver_message", {
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        message: data.message,
                        created_at: created_at,
                        created_time: created_time,
                        order_id: order_id,
                        /* sender: results[0], */
                        send_by:"me"
                    });
                } else {
                    io.sockets.in(user_ + receiver_id).emit("receiver_message", {
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        message: data.message,
                        created_at: created_at,
                        created_time: created_time,
                        order_id: order_id,
                        /* sender: results[0], */
                        send_by:"other"
                    });
					console.log("-------------------------------------");
					console.log("receiver_message : ", deliveryboy_ + sender_id);
					console.log("-------------------------------------");
                    io.sockets.in(deliveryboy_ + sender_id).emit("receiver_message", {
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        message: data.message,
                        created_at: created_at,
                        created_time: created_time,
                        order_id: order_id,
                        /* sender: results[0], */
                        send_by:"me"
                    });
                }

                var send_push_data = {};
                var profile_base_url = base_url+"assets/files/profile_pictures/";
                var default_profile_base_url = base_url+"assets/images/default.png";
                if(results[0].profile_picture == "") {
                    results[0].profile_picture = default_profile_base_url;
                } else {
                    results[0].profile_picture = profile_base_url+results[0].profile_picture;
                }
                send_push_data["sender_id"] = sender_id;
                send_push_data["receiver_id"] = receiver_id;
                send_push_data["message"] = data.message;
                send_push_data["user"] = results[0];
                send_push_data["order_id"] = order_id;
                send_push_data["created_at"] = created_at;
                send_push_data["created_time"] = created_time;
                var push_title = results[0].first_name + " " + results[0].last_name;
                send_push(receiver_id, "devices", push_title, data.message, "chat", send_push_data);
            });
        }
    });

    socket.on("update_db_location", function (data) {
        var deliveryboy_id = functions.validate_params(data.deliveryboy_id);  
        var lat = functions.validate_params(data.lat); 
        var lng = functions.validate_params(data.lng);
        var socket_user_id = deliveryboy_ + deliveryboy_id;
        
		/* console.log("-------------------------------------");
		console.log("update_db_location : ", data);
		console.log("-------------------------------------"); */
		
        var timestamp = unix_time(new Date());
        if(available_drivers.indexOf(deliveryboy_id) == -1) {
            available_drivers.push(deliveryboy_id);
            drivers_updated[deliveryboy_id] = timestamp;
        }

        var time_diff = timestamp - drivers_updated[deliveryboy_id];
        if(time_diff > 5 || time_diff == 0){
            var collection = "shift";
            var driver_id_new = deliveryboy_id.toString();
            var where = {driver_id:driver_id_new};
            functions.get_results_mongodb(collection,where,config.client, function(results){
                /* console.log("results update_db_location:",results); */
                var records = functions.validate_results(results);
                if(records !== false) {
                    var sql = "update users set latitude='" + lat + "', longitude ='" + lng + "' where id='" + deliveryboy_id + "'";
                    /* console.log("sql: ",sql); */
                    functions.update_record_mysql(sql, config.con, function(last_chat_id) {});

                    var updated_time = unix_time(new Date());
                    var update_data = {$set: {location: [lng , lat] , updated_at:updated_time}};
                    functions.update_record_mongodb("shift",update_data,where,config.client);

                    /* HH:MM:SS */
                    /* console.log("********************************update_driver_location**********************************************-"+driver_id); */
                    io.sockets.in(socket_user_id).emit("update_db_location", {
						message: "Delivery Boy location updated successfully",
						lat: lat,
						lng: lng
                    });
                } else {
                    /* console.log("Data not found.."); */
                }
            }); 
        }
    });
    socket.on("update_db_location_track", function (data) {
        var deliveryboy_id = functions.validate_params(data.deliveryboy_id);  
        var user_id = functions.validate_params(data.user_id);  
        var lat = functions.validate_params(data.lat); 
        var lng = functions.validate_params(data.lng);
        
        var socket_customer_id = user_ + user_id;
        var socket_user_id = deliveryboy_ + deliveryboy_id;
		
		console.log("-------------------------------------");
		console.log("update_db_location_track : ", data);
		console.log("-------------------------------------");
        
        var timestamp = unix_time(new Date());
        if(available_drivers.indexOf(deliveryboy_id) == -1) {
            available_drivers.push(deliveryboy_id);
            drivers_updated[deliveryboy_id] = timestamp;
        }

        var time_diff = timestamp - drivers_updated[deliveryboy_id];
        if(time_diff > 5 || time_diff == 0) {
            var collection = "shift";
            var driver_id_new = deliveryboy_id.toString();
            var where = {driver_id:driver_id_new};

            functions.get_results_mongodb(collection,where,config.client, function(results){
                /* console.log('results update_db_location:',results); */
                var records = functions.validate_results(results);
                if(records !== false) {
                    var sql = "update users set latitude='" + lat + "', longitude ='" + lng + "' where id='" + deliveryboy_id + "'";
                    /* console.log("sql: ",sql); */
                    functions.update_record_mysql(sql, config.con, function(last_chat_id) {});
                    
                    var updated_time = unix_time(new Date());
                    var update_data = {$set: {location: [lng , lat] , updated_at:updated_time}};
                    functions.update_record_mongodb("shift",update_data,where,config.client);

                    /* HH:MM:SS */
                    /* console.log("********************************update_driver_location**********************************************-"+socket_customer_id); */
                    io.sockets.in(socket_user_id).emit("update_db_location", {
						message: "Delivery Boy location updated successfully",
						lat: lat,
						lng: lng
                    });
                    io.sockets.in(socket_customer_id).emit("db_location", {
						lat: lat,
						lng: lng,
						delivery_boy_id: driver_id_new
                    });
                } else {
                    console.log("Data not found..");
                }
            }); 
        }
    });

    socket.on("forward_booking_request", function (data) {
		console.log("-------------------------------------");
		console.log("forward_booking_request : ", data);
		console.log("-------------------------------------");
        functions.insert_record_mongodb("booking",data,config.client);
        functions.delete_many_record_mongodb("forward_booking_request",{id: data.id},config.client);
        var pickup_lng = parseFloat(data.vendor.longitude);
        var pickup_lat = parseFloat(data.vendor.latitude);
        var coords = [pickup_lng, pickup_lat];
        var client = new config.client(config.mongodb_connection);
        client.connect(function(err, client) {
            const db_name = config.db_name;
            const db = client.db(db_name);
            var mongo_qry = {
                $and: [{
                    location: {
                        $near: {
                            $geometry: {
                              type: "Point" ,
                              coordinates: coords
                           },
                           $maxDistance: config.intial_distance
                        }
                    }
                }, {
                    "duty": 1
                }, {
                    "busy": 0
                }]
            };
			
            /* console.log("mongo_qry",mongo_qry); */
            var cursor = db.collection("shift").find(mongo_qry);
            cursor.toArray(function(err, results) {
				console.log("-------------------------------------");
				console.log("nearby driver : ", results);
				console.log("-------------------------------------");
				var records = functions.validate_results(results);
                /* console.log("records",records); */
                if(records !== false) {
                    function find_nearest_drivers(results, callback) {
						console.log("-------------------------------------");
						console.log("results.length : ", results.length);
						console.log("-------------------------------------");
                        for(var i=0; j=results.length,i<j; i++) {
                            var eta_distance = geodist({lat: pickup_lat, lon: pickup_lng}, {lat: parseFloat(results[i].location[0]), lon: parseFloat(results[i].location[1])}, {exact: true, unit: "feet"});
                            var distance = parseFloat(eta_distance);
                            var insert_driver_id = parseInt(results[i].driver_id);
							console.log("-------------------------------------");
							console.log("available_drivers : ", available_drivers);
							console.log("-------------------------------------");
                            /* if(available_drivers.indexOf(insert_driver_id) >= 0) { */
                                var UpdatedDate = new Date().toLocaleString("en-US", {
                                    timeZone: "Canada/Eastern"
                                });
                                var created_at = moment(UpdatedDate).format("YYYY-MM-DD HH:mm:ss");
                                
                                var forward_booking_request = {id: data.id, driver_id: results[i].driver_id, eta_distance: distance,forward_now: 0,created_at:created_at };
                                functions.insert_record_mongodb("forward_booking_request",forward_booking_request,config.client);
                            /* } */
                        }
                        callback(results.length);
                    }

                    find_nearest_drivers(results,function(count) {
                        setTimeout(wait_for_nearest_drivers, 2000,count);
                        function wait_for_nearest_drivers(count) {
							console.log("-------------------------------------");
							console.log("wait_for_nearest_drivers count : ", count);
							console.log("-------------------------------------");
                            if(count > 0) {
                                var collection = "forward_booking_request";
                                var where = {id:data.id};
                                functions.find_nearest_drivers(collection,where,config.client, function(nearest_drivers){
									console.log("-------------------------------------");
									console.log("nearest_drivers : ", nearest_drivers);
									console.log("-------------------------------------");
                                    var nearest_records = functions.validate_results(nearest_drivers);
                                    if(nearest_records !== false) {
                                        var UpdatedDate = new Date().toLocaleString("en-US", {
                                            timeZone: "Canada/Eastern"
                                        });
            
                                        var created_at = moment(UpdatedDate).format("YYYY-MM-DD HH:mm:ss");
                                        var created_time = moment(UpdatedDate).format("hh:mm A");
                                        var update_data = {$set: {forward_now: 1 ,created_at:created_at}};
                                        var where = {id: data.id,driver_id: nearest_drivers[0].driver_id};
                                        functions.update_record_mongodb("forward_booking_request",update_data,where,config.client);
                                        var send_push_data = {};
                                        send_push_data["sender_id"] = 1;
                                        send_push_data["receiver_id"] = nearest_drivers[0].driver_id;
                                        send_push_data["message"] = "New Delivery Request";
                                        send_push_data["user"] = data;
                                        send_push_data["order_id"] = data.id;
                                        send_push_data["created_at"] = created_at;
                                        send_push_data["created_time"] = created_time;
                                        var push_title = "Request";
            
                                        send_push(nearest_drivers[0].driver_id, "devices", push_title, "New Delivery Request", "new_request", send_push_data);
										console.log("-------------------------------------");
										console.log("forward_booking_request nearest_records !== false : ", config.driver_socket_prefix+nearest_drivers[0].driver_id);
										console.log("-------------------------------------");
                                        io.sockets.in(config.driver_socket_prefix+nearest_drivers[0].driver_id).emit("forward_booking_request", { order_info : data, message: "New request arrived from "+config.name});
                                        setTimeout(check_request_status, driver_waiting_time, data.id, nearest_drivers[0].driver_id);
                                    } else {
										console.log("-------------------------------------");
										console.log("No driver available nearest_records === false");
										console.log("-------------------------------------");
                                        /* cancelled_booking_request_by_system(data.id,data.customer_id); */
                                        /* assign order to admin write here */
                                        assign_to_admin(data.id);
                                        
                                    }
                                });
                            } else {
								console.log("-------------------------------------");
								console.log("No driver available count > 0");
								console.log("-------------------------------------");
                                //cancelled_booking_request_by_system(data.id,data.customer_id);
                                //assign order to admin write here
                                assign_to_admin(data.id)
                            }
                        }
                    });
                } else {
					console.log("-------------------------------------");
					console.log("No driver available records === false");
					console.log("-------------------------------------");
                    /* assign order to admin write here */
                    assign_to_admin(data.id)
                }
                client.close();
            });
        });
    });

    socket.on("forward_booking_request_to_another_driver", function (data) {
        /* PASS order_id and delivery_boy_id */
		console.log("-------------------------------------");
		console.log("forward_booking_request_to_another_driver data : ", data);
		console.log("-------------------------------------");
        var driver_id = functions.validate_params(data.driver_id);
        var booking_id = functions.validate_params(data.booking_id);
     
        if(driver_id !== false && booking_id !== false) {
            driver_id = driver_id.toString();
			console.log("-------------------------------------");
			console.log("forward_booking_request_to_another_driver -> driver_id : ", driver_id);
			console.log("-------------------------------------");
			
            functions.delete_record_mongodb("forward_booking_request",{driver_id: driver_id},config.client);
    
            var collection = "forward_booking_request";
            var where = {id:booking_id,forward_now : 0};
            functions.find_nearest_drivers(collection,where,config.client, function(nearest_drivers){
                var nearest_records = functions.validate_results(nearest_drivers);
				console.log("-------------------------------------");
				console.log("forward_booking_request_to_another_driver -> nearest_records : ", nearest_records);
				console.log("-------------------------------------");
                if(nearest_records !== false) {
                    var UpdatedDate = new Date().toLocaleString("en-US", {
                        timeZone: "Canada/Eastern"
                    });

                    var created_at = moment(UpdatedDate).format("YYYY-MM-DD HH:mm:ss");
                    var created_time = moment(UpdatedDate).format("hh:mm A");

                    var update_data = {$set: {forward_now: 1,created_at:created_at }};
                    var where = {id: booking_id,driver_id: nearest_drivers[0].driver_id};
                    functions.update_record_mongodb("forward_booking_request",update_data,where,config.client);
    
                    functions.get_results_mongodb("booking",{id:booking_id},config.client, function(booking_info){
						console.log("-------------------------------------");
						console.log("forward_booking_request_to_another_driver -> booking_info driver_id : ", config.driver_socket_prefix+nearest_drivers[0].driver_id);
						console.log("-------------------------------------");
						
						var send_push_data = {};
						send_push_data["sender_id"] = 1;
						send_push_data["receiver_id"] = nearest_drivers[0].driver_id;
						send_push_data["message"] = "New Delivery Request";
						send_push_data["user"] = booking_info[0];
						send_push_data["order_id"] = booking_info[0].id;
						send_push_data["created_at"] = created_at;
						send_push_data["created_time"] = created_time;
						var push_title = "Request";

						send_push(nearest_drivers[0].driver_id, "devices", push_title, "New Delivery Request", "new_request", send_push_data);
						var UpdatedDate = new Date().toLocaleString("en-US", {
							timeZone: "Canada/Eastern"
						});
						var created_at = moment(UpdatedDate).format("YYYY-MM-DD HH:mm:ss");
						var created_time = moment(UpdatedDate).format("hh:mm A");
						io.sockets.in(config.driver_socket_prefix+nearest_drivers[0].driver_id).emit("forward_booking_request", { order_info : booking_info[0], message: "New booking request arrived from "+config.name});
                        setTimeout(check_request_status, driver_waiting_time, booking_id,nearest_drivers[0].driver_id);
                    });
                } else {
					console.log("-------------------------------------");
					console.log("forward_booking_request_to_another_driver No driver available nearest_records === false");
					console.log("-------------------------------------");
                    functions.get_results_mongodb("booking",{id:booking_id},config.client, function(booking_info){
                        var booking_records = functions.validate_results(booking_info);
                        if(booking_records !== false) {   
                            assign_to_admin(booking_id);
                        }
                    });
                }
            });
        }
    });

    socket.on("accept_booking_request", function (data) {
		console.log("-------------------------------------");
		console.log("accept_booking_request data : ",data);
		console.log("-------------------------------------");
        var driver_id = functions.validate_params(parseInt(data.delivery_boy_id));
        var booking_id = functions.validate_params(data.id);
        if(driver_id !== false && booking_id !== false) {
            booking_id = booking_id.toString();
            functions.delete_many_record_mongodb("forward_booking_request",{id: parseInt(booking_id)},config.client);
    
            var timestamp = unix_time(new Date());
            /* var where = {id: booking_id};
            var update_booking = {$set: {status:"accepted",accept_time: timestamp,driver_info: data.delivery_boy,driver_id:driver_id}};
            functions.update_record_mongodb("booking",update_booking,where,config.client); */

            var driver_id_string = driver_id.toString();
            var where = {driver_id: driver_id_string};
            var update_shift = {$set: {busy:1}};
            functions.update_record_mongodb("shift",update_shift,where,config.client);

            var delete_id = parseInt(booking_id);
            functions.delete_record_mongodb("booking",{id: delete_id},config.client);
            /* setTimeout(wait_for_results, 1000);
            function wait_for_results() {    
                functions.get_results_mongodb("booking",{id:booking_id},config.client, function(booking_info){
                    console.log("accept_booking_request--------------------------------------------------------booking_info",booking_info);
                    io.sockets.in(config.driver_socket_prefix+driver_id).emit("accept_booking_request", { booking_info : booking_info[0], message: "Thank you for accepting booking request"});
                    io.sockets.in(user_+booking_info[0].user_id).emit("accept_booking_request", { booking_info : booking_info[0], message: "Your Order (ID: "+booking_id+") Is Accepted. Your "+config.name+" Delivery Person Is On The Way!"});
                });
            } */
        }
    });

    socket.on("start_duty", function (data) {
		console.log("-------------------------------------");
		console.log("start_duty data : ",data);
		console.log("-------------------------------------");
        var driver_id = functions.validate_params(data.driver_id);
        setTimeout(start_duty, 1000);
        function start_duty() {
            functions.insert_record_mongodb("shift",data,config.client);
        }
    });

    socket.on("delivery_boy_unset_busy", function (data) {
		console.log("-------------------------------------");
		console.log("delivery_boy_unset_busy data : ",data);
		console.log("-------------------------------------");
        var driver_id = functions.validate_params(data.delivery_boy_id);
        var driver_id_string = driver_id.toString();
        var where = {driver_id: driver_id_string};
        var update_shift = {$set: {busy:0}};
        functions.update_record_mongodb("shift",update_shift,where,config.client);
    });

    socket.on("check_order", function (data) { 
        var UpdatedDate = new Date().toLocaleString("en-US", {
            timeZone: "Canada/Eastern"
        });
        var created_at = moment(UpdatedDate).format("YYYY-MM-DD HH:mm:ss");
		
		/* console.log("-------------------------------------");
		console.log("check_order data : ",data);
		console.log("created_at : ",created_at);
		console.log("-------------------------------------"); */
        var driver_id = functions.validate_params(data.deliveryboy_id);
        var driver_id_string = driver_id.toString();
    
        /* var order_id = functions.validate_params(data.order_id); */
        var where = {driver_id: driver_id_string ,forward_now: 1};
		/* console.log("-------------------------------------");
		console.log("check_order where data : ",where);
		console.log("-------------------------------------"); */
        var collection = "forward_booking_request";
        functions.get_results_mongodb(collection,where,config.client, function(order_details) {
			/* console.log("-------------------------------------");
			console.log("check_order order_details data : ",order_details);
			console.log("-------------------------------------"); */
            if(order_details.length > 0 ){
                var where = {id : order_details[0].id};
                console.log(where);
                var collection = "booking";
                functions.get_results_mongodb(collection,where,config.client, function(booking_details){
					console.log("-------------------------------------");
					/* console.log("check_order booking_details data : ",booking_details); */
					console.log("check_order emit to driver_id : ",config.driver_socket_prefix+driver_id);
					console.log("-------------------------------------");
                    io.sockets.in(config.driver_socket_prefix+driver_id).emit("check_order", {notification_info : order_details, order_info : booking_details,current_time : created_at, message: "New request arrived from "+config.name});
                });
            } 
        });
    });
	
    socket.on("end_duty", function (data) {
		console.log("-------------------------------------");
		console.log("end_duty data : ",data);
		console.log("-------------------------------------");
        var driver_id = functions.validate_params(data.driver_id);
        var collection = "shift";
        if(driver_id !== false) {
            var where = {driver_id:data.driver_id};
            console.log("end_duty where", where);
            functions.delete_record_mongodb(collection,where,config.client);
        }
    });
    
    socket.on("disconnect", function () {
		var username = functions.validate_params(socket.username);
		console.log("-------------------------------------");
		console.log("disconnect username :- ", username);
		console.log("-------------------------------------");
    });
    
});

function check_request_status(booking_id,driver_id) {
	console.log("-------------------------------------");
	console.log("check_request_status function data step-1 : "+booking_id+" "+" driver_id : "+driver_id);
	console.log("-------------------------------------");
	var driver_id = functions.validate_params(driver_id);
	var booking_id = functions.validate_params(booking_id);
	
	if(driver_id !== false && booking_id !== false) {
		functions.get_results_mongodb("forward_booking_request",{id: booking_id,driver_id: driver_id,forward_now : 1},config.client, function(request_status){
			var request_records = functions.validate_results(request_status);
			if(request_records !== false) {
				/* console.log("-------------------------------------");
				console.log("check_request_status function data step-2 : "+booking_id+" "+" driver_id : "+driver_id);
				console.log("-------------------------------------"); */
				functions.delete_record_mongodb("forward_booking_request",{driver_id: driver_id},config.client);

				var collection = "forward_booking_request";
				var where = {id:booking_id,forward_now : 0};
				functions.find_nearest_drivers(collection,where,config.client, function(nearest_drivers){
					/* console.log("-------------------------------------");
					console.log("check_request_status function nearest_drivers : ",nearest_drivers);
					console.log("-------------------------------------"); */
					var nearest_records = functions.validate_results(nearest_drivers);
					if(nearest_records !== false) {
						 var UpdatedDate = new Date().toLocaleString("en-US", {
							timeZone: "Canada/Eastern"
						});
						var created_at = moment(UpdatedDate).format("YYYY-MM-DD HH:mm:ss");
						var created_time = moment(UpdatedDate).format("hh:mm A");
						/* console.log("-------------------------------------");
						console.log("check_request_status function data step-3");
						console.log("-------------------------------------"); */
						var update_data = {$set: {forward_now: 1 ,created_at:created_at}};
						var where = {id: booking_id,driver_id: nearest_drivers[0].driver_id};
						functions.update_record_mongodb("forward_booking_request",update_data,where,config.client);
						functions.get_results_mongodb("booking",{id:booking_id},config.client, function(booking_info){
							console.log("-------------------------------------");
							console.log("Under if forward_booking_request : ",nearest_records);
							console.log("-------------------------------------");
							io.sockets.in(config.driver_socket_prefix+nearest_drivers[0].driver_id).emit("forward_booking_request", { order_info : booking_info[0], message: "New booking request arrived from "+config.name});
							setTimeout(check_request_status, driver_waiting_time, booking_id,nearest_drivers[0].driver_id);
							var send_push_data = {};
							send_push_data["sender_id"] = 1;
							send_push_data["receiver_id"] = nearest_drivers[0].driver_id;
							send_push_data["message"] = "New Delivery Request";
							send_push_data["user"] = booking_info[0];
							send_push_data["order_id"] = booking_info[0].id;
							send_push_data["created_at"] = created_at;
							send_push_data["created_time"] = created_time;
							var push_title = "Request";

							send_push(nearest_drivers[0].driver_id, "devices", push_title, "New Delivery Request", "new_request", send_push_data);
						});
					} else {
						/* console.log("-------------------------------------");
						console.log("check_request_status function No driver available step-4 nearest_records === false");
						console.log("-------------------------------------"); */
						functions.get_results_mongodb("booking",{id:booking_id,status:2},config.client, function(booking_info){
							/* cancelled_booking_request_by_system(booking_id,booking_info[0].customer_id); */
							var booking_records = functions.validate_results(booking_info);
							if(booking_records !== false) {
								console.log("check_request_status ------------------------------------------------------ else part execute");
								/* cancelled_booking_request_by_system(booking_id,booking_info[0].customer_id); */
								/* assign order to admin write here */
								assign_to_admin(booking_id);
							}
						});
					}
				});
			} else {
				/* console.log("-------------------------------------");
				console.log("check_request_status function step-5 request_records === false booking_id : "+booking_id+" "+" driver_id : "+driver_id);
				console.log("-------------------------------------"); */
			}
		});
	}
}

function send_push(user_id, user_type, title, message_body, type, push_data) {
	var table = user_type;
	var where = "user_id = " + user_id;
	var select = "token,type";
	
	functions.get_results_mysql(table, where, select, config.con, function(results) {
		/* console.log("results",results); */
		if (results !== undefined && results !== null && results.length > 0) {
			if (results[0].token !== undefined && results[0].token !== "" && results[0].token !== null) {
				console.log("-------------------------------------");
				console.log("user_id :- " +user_id + " got device token : " + results[0].token + " " + results[0].type);
				console.log("-------------------------------------");
				if(results[0].type == "android") {
					/* console.log("android"); */
					var message = {
						to: results[0].token,
						collapse_key: config.fcm_collapse_key,
						data: {
							body: push_data,
							title: title,
							type: type,
							message: message_body
						}
					};
				} else {
					/* console.log("ios"); */
					var message = {
						to: results[0].token,
						collapse_key: config.fcm_collapse_key,
						notification: {
							body: message_body,
							title: title,
							type: type,
							sound:"default",
							data: push_data
						}
					};
				}

				fcm.send(message, function(err, response) {
					if (err) {
						/* console.log("Something has gone wrong!"+err); */
					} else {
						/* console.log(response); */
					}
				});
			}
		}
	});
}

function assign_to_admin(order_id) {
	console.log("-------------------------------------");
	console.log("assign_to_admin order_id : ", order_id);
	console.log("-------------------------------------");
	var sql = "update orders set status='5' where id='" + order_id + "'";
	functions.update_record_mysql(sql, config.con);

	var url = base_url + "assign-to-admin";
	unirest.post(url).headers({"Content-Type": "multipart/form-data"}).field("order_id", order_id).field("user_id", 1).end(function (response) {
		console.log("assign_to_admin_api response");
		console.log("-------------------------------------");
		console.log("assign_to_admin_api response", order_id);
		console.log("-------------------------------------",response.body);
	});
}