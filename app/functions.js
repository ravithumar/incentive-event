var config = require("./config")();

module.exports = {
	get_results_mongodb: function (collection,where,client, callback){

    	var client = new client(config.mongodb_connection);
	 	client.connect(function(err, client) {
	 	  const db_name = config.db_name;
		  const db = client.db(db_name);
		  db.collection(collection).find(where).toArray(function(err, results) {
		  	callback(results);
		  	client.close();
		  });
		});

	},
	find_nearest_drivers: function (collection,where,client, callback){

		var client = new client(config.mongodb_connection);
	 	client.connect(function(err, client) {
	 	  const db_name = config.db_name;
		  const db = client.db(db_name);
		  db.collection(collection).find(where).limit(1).sort({ eta_distance: 1 }).toArray(function(err, results) {
		  	callback(results);
		  	client.close();
		  });
		});
	},
	insert_record_mongodb: function (collection,data,client){

		var client = new client(config.mongodb_connection);
	 	client.connect(function(err, client) {
	 	  const db_name = config.db_name;
		  const db = client.db(db_name);
		  db.collection(collection).insertOne(data, function(err, results) {
		  	//console.log('err:', err);
		  	//console.log('results insert_record_mongodb: ',results);
		  		  	client.close();
		  });
		});
	},
	update_record_mongodb: function (collection,data,where,client){

		var client = new client(config.mongodb_connection);
	 	client.connect(function(err, client) {
	 	  const db_name = config.db_name;
		  const db = client.db(db_name);
		  db.collection(collection).updateOne(where, data,{upsert:true}, function(err, results) {
		  	client.close();
		  });

		});
	},
	delete_record_mongodb: function (collection,where,client){

		var client = new client(config.mongodb_connection);
	 	client.connect(function(err, client) {
	 	  const db_name = config.db_name;
		  const db = client.db(db_name);
		  db.collection(collection).deleteOne(where, function(err, results) {
		  	client.close();
		  });
		});
	},
	delete_many_record_mongodb: function (collection,where,client){

		var client = new client(config.mongodb_connection);
	 	client.connect(function(err, client) {
	 	  const db_name = config.db_name;
		  const db = client.db(db_name);
		  db.collection(collection).deleteMany(where, function(err, results) {
		  	client.close();
		  });
		});
	},
    get_results_mysql: function(table,where,select = '*',con,callback)
	{
		var query = "Select "+select+" from "+table+" where "+where;

		con.query(query, function (err, results) {
			//console.log(err);
			callback(results);
		});
	},
	delete_record_mysql: function(query,con)
	{
		con.query(query, function (err, results) {});
	},
	insert_record_mysql: function(query,con,callback = [])
	{
		con.query(query, function (err, results) {  if(callback.length > 0) {callback(results.insertId);} });
	},
	update_record_mysql: function(query,con)
	{
		con.query(query, function (err, results) {});
	},
	distance: function(lat1, lon1, lat2, lon2, unit) 
	{
		var radlat1 = Math.PI * lat1/180
		var radlat2 = Math.PI * lat2/180
		var theta = lon1-lon2
		var radtheta = Math.PI * theta/180
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		dist = Math.acos(dist)
		dist = dist * 180/Math.PI
		dist = dist * 60 * 1.1515
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist
	},
	validate_params: function(params)
	{
		if(params !== undefined && params !== null && params !== '')
		{
			return params;
		}
		else
		{
			return false;
		}
	},
	validate_results: function(results)
	{
		if(results !== undefined && results !== null && results.length > 0 )
		{
			return results;
		}
		else
		{
			return false;
		}
	}
};