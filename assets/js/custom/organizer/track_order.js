
    $(document).on('click',"#track-order", function(){
        Notiflix.Loading.Standard();
        var track_order_id = $(this).attr("data-id");
        console.log(track_order_id);
        $('.track #order-id').html(track_order_id);
        $.ajax({
                url: BASE_URL+'admin/Common/get_track_data',
                type: "POST",
                data:{order_id:track_order_id},
                success: function (returnData) {
                    returnData = $.parseJSON(returnData);
                    console.log(returnData);
                    //return false;
                    $(".overlay").css("display", "none");

                    var str = '';
                    if (typeof returnData.data != "undefined" && returnData.data.length > 0){

                        var final_status = 'Pending';
                        var final_status_text = 'Order pending';

                        $.each(returnData.data, function (key, val){

                            if(key != 0){
                                var extra_class = ' not-first mt-3';
                            }else{
                                var extra_class = '';
                            }

                            
                            if(val.status == 1){
                                final_status_text = 'Restaurant approval pending';
                            }else if(val.status == 2){
                                final_status_text = 'Accepted by restaurant';
                            }else if(val.status == 3){
                                final_status = 'Cancelled';
                                final_status_text = 'Rejected by restaurant';
                            }else if(val.status == 4){
                                final_status = 'Cancelled';
                                final_status_text = 'Order has been cancelled';
                            }else if(val.status == 5){
                                final_status_text = 'Delivery Person assigned';
                            }else if(val.status == 6){
                                final_status_text = 'Delivery Person Reached at restaurant';
                            }else if(val.status == 7){
                                final_status_text = 'Delivery Person is on the way';
                            }else if(val.status == 8){
                                final_status = 'Delivered';
                                final_status_text = 'Order has been delivered';
                            }else if(val.status == 9){
                                final_status = 'Completed';
                                final_status_text = 'Order has been picked up';
                            }else{
                                final_status_text = 'Order Pending';
                            }
                            str += '<div class="row align-items-center '+extra_class+'">'
                                        +'<div class="col-md-2 text-left text-md-right status-time">'
                                          +val.created_at+'<br>'
                                        +'</div>'
                                        +'<div class="col-md-2">'
                                          +'<span class="dot"></span>'
                                        +'</div>'
                                        +'<div class="col-md-7 status">'
                                          +'<strong>'
                                            +val.status_title
                                          +'</strong>'
                                          +'<div class="text-muted">'
                                            +val.status_text
                                          +'</div>'
                                        +'</div>'
                                    +'</div>';

                        });

                        $('#trackmodal .modal-body').html(str);


                        $('#trackmodal #order-final-status').html(final_status);
                        $('#trackmodal #order-final-text').html(final_status_text);
                         Notiflix.Loading.Remove();
                    }else{

                        str += '<h4 class="text-center">Tracking Under Development</h4>';
                        $('#trackmodal .modal-body').html(str);

                        $('#trackmodal #order-final-status').html('');
                        $('#trackmodal #order-final-text').html('');
                        Notiflix.Loading.Remove();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });   
    });

