$('.working-time-error td').hide();
$("#phone , #phone_2").inputmask("(999) 999-9999",{"placeholder": ""});
$("#zipcode").inputmask("99999",{"placeholder": ""});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$(".select2").select2();
$(".demo2").TouchSpin({
    initval: 0.00,
    forcestepdivisibility: 'none',
    max: 1000000000,
    decimals: 2,
    step: 0.1,
    prefix: '$',
    buttondown_class: 'btn btn-primary',
    buttonup_class: 'btn btn-primary'
});

$(".demo3").TouchSpin({
    initval: 0.00,
    forcestepdivisibility: 'none',
    max: 100,
    step: 0.1,
    decimals: 2,
    postfix: '%',
    buttondown_class: 'btn btn-primary',
    buttonup_class: 'btn btn-primary'
});

var dt = new Date();
$.each(available_time, function(k, v) {
    $('.from_time').append($("<option>" , { text: v, value: v }));
    $('.to_time').append($("<option>" , { text: v, value: v }));
});

$.each(available_time, function(k, v) {
    $('.hours_from_time').append($("<option>" , { text: v, value: v }));
    $('.hours_to_time').append($("<option>" , { text: v, value: v }));
});


var from_time_option = from_time_selected.toString();
var to_time_option = to_time_selected.toString();

console.log(from_time_option,to_time_option);

$('.from_time option[value="'+from_time_option+'"]').attr('selected', 'selected');
$('.to_time option[value="'+to_time_option+'"]').attr('selected', 'selected');



//select from-to time as DB stored
$.each(vendor_availability, function(k, v) {
     $('tr').each(function(){
        var day = v.day+'[]';
        //console.log(k);
        if($(this).find('#open_'+k).attr('name') == day){
            if(v.is_closed == '0' && v.full_day == '0'){
                $(this).find('.from_td .from_time option[value="'+v.from_time+'"]').attr('selected', 'selected');
                $(this).find('.to_td .to_time option[value="'+v.to_time+'"]').attr('selected', 'selected');
            }
            
        }
     });
});


$(document).on('click','.fullday',function(){

    if ($(this).is(':checked')){
       $(this).closest('tr').find('.from_td').empty();
       $(this).closest('tr').find('.to_td').empty();
       $(this).closest('tr').find('.from_td').html('<div class="text-center"><span class="badge badge-success">24 Hours Open</span></div>');
    }else{

        $(this).closest('tr').find('.from_td').empty();
        $(this).closest('tr').find('.to_td').empty();

        var dataday =  $(this).closest('tr').data("day");

        $(this).closest('tr').find('.from_td').html('<select class="form-control select2 from_time" name="'+dataday+'[]"><option disabled >Select Time</option></select>');
        $(this).closest('tr').find('.to_td').html('<select class="form-control select2 to_time" name="'+dataday+'[]"><option disabled >Select Time</option></select>');

        var this_div = $(this);

        $.each(available_time, function(k, v) {
            this_div.closest('tr').find('.from_time').append($("<option>" , { text: v, value: v }));
            this_div.closest('tr').find('.to_time').append($("<option>" , { text: v, value: v }));
        });
        $('.from_time option[value="'+from_time_option+'"]').attr('selected', 'selected');
        $('.to_time option[value="'+to_time_option+'"]').attr('selected', 'selected');

        $(".select2").select2();
    }
});

$(document).on('click','.open',function(){

    if (!$(this).is(':checked')){
       $(this).closest('tr').find('.from_td').empty();
       $(this).closest('tr').find('.to_td').empty();
       $(this).closest('tr').find('.fullday_td').empty();
                                                       
       $(this).closest('tr').find('.from_td').html(' <div class="text-center"><span class="badge badge-danger">CLOSED</span></div>');
    }else{

        var dataid =  $(this).closest('tr').data("id");
        var dataday =  $(this).closest('tr').data("day");

        $(this).closest('tr').find('.from_td').empty();
        $(this).closest('tr').find('.to_td').empty();

        $(this).closest('tr').find('.from_td').html('<select class="form-control select2 from_time" name="'+dataday+'[]"><option disabled >Select Time</option></select>');
        $(this).closest('tr').find('.to_td').html('<select class="form-control select2 to_time" name="'+dataday+'[]"><option disabled >Select Time</option></select>');

        $(this).closest('tr').find('.fullday_td').html('<input type="checkbox" id="fullday_'+dataid+'" switch="none" class="fullday" value="fullday" name="'+dataday+'[]"><label class="mb-0 mt-1" for="fullday_'+dataid+'" data-on-label="Fullday" data-off-label="Custom"></label>');

        var this_div = $(this);

        $.each(available_time, function(k, v) {
            this_div.closest('tr').find('.from_time').append($("<option>" , { text: v, value: v }));
            this_div.closest('tr').find('.to_time').append($("<option>" , { text: v, value: v }));
        });
        $('.from_time option[value="'+from_time_option+'"]').attr('selected', 'selected');
        $('.to_time option[value="'+to_time_option+'"]').attr('selected', 'selected');

        $(".select2").select2();
    }
});
$(document).on('submit','form',function(){
    var return_val = true;
    var error_msg = '';
    $( ".from_time" ).each(function( index ) {

        if(return_val == true){
            var from = "11/24/2014 "+ $(this).val();
            var to = "11/24/2014 "+ $(this).closest('tr').find('.to_time').val();

            var fromDate = new Date(from).getTime();
            var toDate = new Date(to).getTime();

            console.log(fromDate);
            console.log(toDate);

            
            if(fromDate >= toDate){
                error_msg = $(this).closest('tr').data("day")+' from-to time is invalid. To time should be greater than from time.';
                Notiflix.Notify.Failure( $(this).closest('tr').data("day")+' from-to time is invalid.');
                $('.working-time-error td').html(error_msg);
                $('.working-time-error td').show();
                return_val =  false;
            }
        }
        
    });

    return return_val;
});