function active_deactive(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
    // var tr = $(e).parent().closest('tr');

    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/active_deactive',
              type: "POST",
              data:{
                table:table,
                id:id,
                status:status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  Notiflix.Loading.Remove();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive(this);" data-table="'+table+'" data-id="'+id+'"  class="btn btn-danger btn-sm waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive(this);" data-table="'+table+'" data-id="'+id+'"  class="btn btn-success btn-sm waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}

function change_status(e)
{
    var id = $(e).data('id');
    var status = $(e).data('status');

    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/change_status',
              type: "POST",
              data:{
                id:id,
                status:status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  Notiflix.Loading.Remove();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="change_status(this);" data-status="2" data-id="'+id+'" class="btn btn-warning btn-sm waves-effect waves-light mr-1 w-75">In Process</button>';
                          }else{
                            var replace_str = '<span class="badge badge-success">Resolved</span>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}

function accept_db(e)
{
    
    var id = $(e).data('id');
    var phone = $(e).data('phone');
    var tr = $(e).parent().closest('tr');

    // console.log(phone);
    // return false;

    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to accept this request?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/accept_db',
              type: "POST",
              data:{
                id:id,
                phone:phone,
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  Notiflix.Loading.Remove();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Accepted');
                        tr.remove();
                      } 
                  } 
              }
          });  
    });
}

function reject_db(e)
{
    
    var id = $(e).data('id');
    var phone = $(e).data('phone');
     var td = $(e).parent().closest('td');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to reject this request?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/reject_db',
              type: "POST",
              data:{
                id:id,
                phone:phone,
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  Notiflix.Loading.Remove();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Rejected');
                        var replace_str = '<span class="badge badge-danger">Rejected</span>';
                        td.html(replace_str);
                      } 
                  } 
              }
          });  
    });
}

function delete_confirm(e)
{

var tr = $(e).parent().closest('tr');

  Notiflix.Confirm.Show(
    'Confirm',
    'Are you sure that you want to delete this record?',
    'Yes',
    'No',
    function(){
      Notiflix.Loading.Standard();
      $.ajax({
        url: BASE_URL+'admin/Common/delete',
        type: "POST",
        data:{
          table:table,
          id:id
        },
        success: function (returnData) {
            Notiflix.Loading.Remove();
            Notiflix.Notify.Success('Deleted');
            tr.remove();
        }
      }); 
  });
  

}

function delete_notiflix(e)
{

  var tr = $(e).parent().closest('tr');
  var table = $(e).data('table');
  var id = $(e).data('id');

  Notiflix.Confirm.Show(
    'Confirm',
    'Are you sure that you want to delete this record?',
    'Yes',
    'No',
    function(){
      Notiflix.Loading.Standard();
      $.ajax({
        url: BASE_URL+'admin/Common/delete',
        type: "POST",
        data:{
          table:table,
          id:id
        },
        success: function (returnData) {
            Notiflix.Loading.Remove();
            Notiflix.Notify.Success('Deleted');
            tr.remove();
        }
      }); 
  });

}
function featured(e)
{
    var id = $(e).data('id');
    var status = $(e).data('status');
    var user_id = $(e).data('user_id');

    // var tr = $(e).parent().closest('tr');

    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change featured status of this record?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/featured',
              type: "POST",
              data:{
                id:id,
                user_id:user_id,
                status:status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  //return false;
                  if(returnData['data'] == 0){
                     Notiflix.Loading.Remove();
                     Notiflix.Notify.Failure('Limit Exceeded!');
                  }else{
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Updated');
                    // tr.remove();
                    if(status == 1){
                      var replace_str = '<button onclick="featured(this);" data-id="'+id+'" data-user_id="'+user_id+'"  class="btn btn-danger btn-sm waves-effect waves-light" data-status="0">Off</button>';
                    }else{
                      var replace_str = '<button onclick="featured(this);" data-id="'+id+'" data-user_id="'+user_id+'" class="btn btn-success btn-sm waves-effect waves-light" data-status="1">On</button>';
                    }
                    $(e).replaceWith(replace_str);
                  }
              }
          });  
    }); 
}


function accept_order(e)
{
    var id = $(e).data('id');
    var userid = $(e).data('userid');
    var ordertype = $(e).data('ordertype');
    var tr = $(e).parent().closest('tr');
    var status = 2;
    // console.log(ordertype);
    // return false;
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to accept this order?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/accept_order', 
              type: "POST",
              data:{
                id:id,
                status:status,
                userid:userid,
                ordertype:ordertype
              },
              success: function (returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Order Accepted');
                  tr.remove();
              }
          });  
    });
}
function reject_order(e) 
{
    var id = $(e).data('id');
    var tr = $(e).parent().closest('tr');
    var userid = $(e).data('userid');
    var status = 3;
    var ordertype = $(e).data('ordertype');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to reject this order?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/accept_order', 
              type: "POST",
              data:{
                id:id,
                status:status,
                userid:userid,
                ordertype:ordertype
              },
              success: function (returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Failure('Order Rejected');
                  tr.remove();
              }
          });  
    });
}

function completed(e)
{
    var id = $(e).data('id');
    var userid = $(e).data('userid');
    var alcoholic = $(e).data('alcoholic');
    var tr = $(e).parent().closest('tr');
    if (alcoholic == 1) {var type = "Manual ID Check?";}else{var type = "Are you sure that you want to complete this order?";} 

    Notiflix.Confirm.Show(
      'Confirm',
      ''+type+'',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/completed',
              type: "POST",
              data:{
                id:id,
                userid:userid,
              },
              success: function (returnData) {
                  console.log(returnData);
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Updated');
                  tr.remove();
              }
          });  
    });
}

function admin_cancel_order(e) 
{
    var id = $(e).data('id');
    var tr = $(e).parent().closest('tr');
    var userid = $(e).data('userid');
    var ordertype = $(e).data('ordertype');
    var $this = $(e);

    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to cancel this order?',
      'Yes',
      'No',
      function(){
        Notiflix.Loading.Standard();
        $.ajax({
              url: BASE_URL+'admin/Common/admin_cancel_order', 
              type: "POST",
              data:{
                id:id
              },
              success: function (returnData) {

                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  Notiflix.Loading.Remove();
                  if(returnData['status'] == true){
                      $this.remove();
                      tr.find('.assign-db-btn').remove();
                      Notiflix.Notify.Success(returnData['message']);
                  }else{
                      Notiflix.Notify.Failure(returnData['message']);
                  }
                  
              }
          });  
    });
}

function remove_row(this_element) {
   if (this_element.closest("tr").hasClass("child")){
    //console.log("in");
        this_element.closest("tr").prev("tr").slideUp(300, function () {
            this_element.closest("tr").prev("tr").remove();
            this_element.closest("tr").remove();
        });
    }else{
        //console.log("innn");
        this_element.closest("tr").slideUp(300, function () {
            this_element.closest("tr").remove();
        });
    }
    return true;
}