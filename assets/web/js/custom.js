// on page change add active class to link
$(function(){
  var current_page_URL = location.href;
  $( ".navbar-nav a" ).each(function() {
     if ($(this).attr("href") !== "#") {
       var target_URL = $(this).prop("href");
       if (target_URL == current_page_URL) {
          $('.navbar-nav a').parents('li').removeClass('active');
          $(this).parent('li').addClass('active');
          return false;
       }
     }
  });
});

// var body = document.getElementsByTagName('body')[0];
// var removeLoading = function() {
//     setTimeout(function() {
//         body.className = body.className.replace(/loading/, '');
//     }, 2000);
// };
// removeLoading();

$(document).ready(function() {
  
  setTimeout(function(){
    $('body').addClass('loaded');   
  }, 1000);
  
});

 // VIDEO 
$('.ytvideo[data-video]').one('click', function() {

  var $this = $(this);
  var width = $this.attr("width");
  var height = $this.attr("height");

  $this.html('<iframe src="https://www.youtube.com/embed/' + $this.data("video") + '?autoplay=1"></iframe>');
});

// $(function () {
//     $(".mobile-ticket-allocate .block").slice(0, 2).show();
//     $("#loadMore").on('click', function (e) {
//         e.preventDefault();
//         $(".block:hidden").slice(0, 4).slideDown();
//         if ($(".block:hidden").length == 0) {
//             $("#load").fadeOut('slow');
//         }        
//     });
// });

// product-slider

$(document).ready(function() {
  var $slider = $('.slider');
  var $progressBar = $('.progress');
  var $progressBarLabel = $( '.slider__label' );
  
  $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
    var calc = ( (nextSlide) / (slick.slideCount-1) ) * 100;
    
    $progressBar
      .css('background-size', calc + '% 100%')
      .attr('aria-valuenow', calc );
    
    $progressBarLabel.text( calc + '% completed' );
  });
  
  $slider.slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    speed: 400,
     responsive: [
        {
            breakpoint: 1700, // tablet breakpoint
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 1025, // tablet breakpoint
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
         {
            breakpoint: 768, // tablet breakpoint
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        
    ]
  });  
});

var minVal = 1, maxVal = 20; // Set Max and Min values
// Increase product quantity on cart page
$(".increaseNumb").on('click', function(){
    var $parentElm = $(this).parents(".numbSelector");
    $(this).addClass("clicked");
    setTimeout(function(){
      $(".clicked").removeClass("clicked");
    },100);
    var value = $parentElm.find(".numbValue").val();
    if (value < maxVal) {
      value++;
    }
    $parentElm.find(".numbValue").val(value);
});
// Decrease product quantity on cart page
$(".decreaseNumb").on('click', function(){
    var $parentElm = $(this).parents(".numbSelector");
    $(this).addClass("clicked");
    setTimeout(function(){
      $(".clicked").removeClass("clicked");
    },100);
    var value = $parentElm.find(".numbValue").val();
    if (value > 1) {
      value--;
    }
    $parentElm.find(".numbValue").val(value);
  });