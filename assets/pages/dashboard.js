
/*
 Template Name: Lexa - Responsive Bootstrap 4 Admin Dashboard
 Author: Themesbrand
 File: Dashboard init js
 */

!function($) {
    "use strict";

    var Dashboard = function() {};

    
    //creates area chart
    Dashboard.prototype.createAreaChart = function (element, pointSize, lineWidth, data, xkey, ykeys, labels, lineColors) {
        Morris.Area({
            element: element,
            pointSize: 0,
            lineWidth: 1,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            resize: true,
            gridLineColor: '#eee',
            hideHover: 'auto',
            lineColors: lineColors,
            fillOpacity: .9,
            behaveLikeLine: true
        });
    },
    
    Dashboard.prototype.init = function() {
        
        //creating area chart
        var $areaData = [
            {y: '2011', a: 0},
            {y: '2012', a: 90},
            {y: '2013', a: 30},
            {y: '2014', a: 100},
            {y: '2015', a: 90},
            {y: '2016', a: 175},
            {y: '2017', a: 130}
        ];
        this.createAreaChart('morris-area-example', 0, 0, $areaData, 'y', ['a'], ['Users'], ['#D90C7D']);



    },
    //init
    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Dashboard.init();
}(window.jQuery);