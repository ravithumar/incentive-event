<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Event extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['updated_at', 'deleted_at'];

	protected $table = 'events';

    // protected $appends =
    // [
    //     'category',
    //     'created_at_formatted',
    // ];

    public function user()
    {   
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'user_id');
    }


   
}
?>

