<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class EventPrizes extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['updated_at', 'deleted_at'];

	protected $table = 'event_prizes';

    // protected $appends =
    // [
    //     'category',
    //     'created_at_formatted',
    // ];

    public function event()
    {
        $CI = &get_instance();
        $CI->load->model('Event');
        return $this->hasOne('Event', 'id', 'event_id');
    }


   
}
?>

