<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Wallet extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['created_at','updated_at', 'deleted_at'];
    protected $table = 'wallet';

}
?>

