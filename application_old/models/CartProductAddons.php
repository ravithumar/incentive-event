<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class CartProductAddons extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['created_at','updated_at', 'deleted_at'];
    protected $table = 'cart_product_addons';

    protected $appends =
    [
        'addon_name',
        'addon_price'
    ];
    
    public function productaddon()
    {
        $CI = &get_instance();
        $CI->load->model('ProductAddOn');
        return $this->hasOne('ProductAddOn', 'id', 'addon_id');
    }

    public function product()
    {
        $CI = &get_instance();
        $CI->load->model('Product');
        return $this->hasOne('Product', 'id', 'product_id');
    }

    public function getAddonNameAttribute()
    {
        $CI = &get_instance();
        $product_addon =  $CI->db->get_where('product_addon', ['id' => $this->addon_id])->row();
        if(isset($product_addon->name)){
        	return $product_addon->name;
        }else{
        	return '';
        }
    }
    public function getAddonPriceAttribute()
    {
        $CI = &get_instance();
        $product_addon =  $CI->db->get_where('product_addon', ['id' => $this->addon_id])->row();
        if(isset($product_addon->price)){
        	return $product_addon->price;
        }else{
        	return '';
        }
    }
}
?>

