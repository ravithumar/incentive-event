<?php defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Usergroup extends Eloquent {
	protected $table = 'users_groups';
	public $timestamps = false;

}