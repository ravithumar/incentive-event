<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Orders extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $table = 'orders';

    protected $appends =
    [
        'ordered_on',
        'store_img',
        'order_on_formatted',
        'delivery_time_formatted',
        'user_balance',
        'completed_at_formatted',
        'rating_on_formatted'
    ];

    public function vendor()
    {
        $CI = &get_instance();
        $CI->load->model('Vendor');
        return $this->hasOne('Vendor', 'user_id', 'vendor_id')->withTrashed();
    }
    public function user_vendor()
    {
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'vendor_id');
    }
    public function user_device()
    {
        $CI = &get_instance();
        $CI->load->model('Devices');
        return $this->hasOne('Devices', 'user_id', 'user_id');
    }
    public function promocode()
    {
        $CI = &get_instance();
        $CI->load->model('Promocode');
        return $this->hasOne('Promocode', 'id', 'promo_id');
    }
    public function user()
    {
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'user_id')->withTrashed();
    } 
    public function delivery_boy()
    {
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'delivery_boy_id');
    }
    public function deliveryboy_device()
    {
        $CI = &get_instance();
        $CI->load->model('Devices');
        return $this->hasOne('Devices', 'user_id', 'delivery_boy_id');
    }

    public function orderproducts() {
        $CI = &get_instance();
        $CI->load->model('OrderProducts');
        return $this->hasMany('OrderProducts', 'order_id', 'id');
    }

    public function getOrderedOnAttribute() 
    {
        if(isset($this->created_at)){
            $created_at_obj = DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at);

            return $created_at_obj->format('jS M Y');
        }else{
            return '';
        }
        
    }

    public function getOrderOnFormattedAttribute()
    {
        if(isset($this->created_at)){
            $created_at_obj = DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at);
            return $created_at_obj->format('j M, Y h:i A');
        }else{
            return '';
        }
    }
    public function getRatingOnFormattedAttribute()
    {
        if(isset($this->rate_time)){
            $created_at_obj = DateTime::createFromFormat('Y-m-d H:i:s', $this->rate_time);
            return $created_at_obj->format('j M, Y h:i A');
        }else{
            return '';
        }
    }
    public function getCompletedAtFormattedAttribute()
    {
        if(isset($this->completed_at) && $this->completed_at != null){
            $created_at_obj = DateTime::createFromFormat('Y-m-d H:i:s', $this->completed_at);
            return $created_at_obj->format('j M, Y h:i A');
        }else{
            return '';
        }
    }
    public function getDeliveryTimeFormattedAttribute()
    {
        if(isset($this->prep_time)){
            $created_at_obj = DateTime::createFromFormat('Y-m-d H:i:s', $this->prep_time);
            return $created_at_obj->format('h:i A');
        }else{
            return '';
        }
    }

    public function getStoreImgAttribute()
    {
        $CI = &get_instance();
        $img =  $CI->db->get_where('users', ['id' => $this->vendor_id])->row();
        if(isset($img->profile_picture)){
        	return $img->profile_picture == null ? 'assets/images/default.png' : '/assets/files/users/'.$img->profile_picture;
        }else{
        	return 0;
        }
    }

    public function getIdProofAttribute($id_proof) {
		return $id_proof == null ? '' : '/assets/files/document/'.$id_proof;
	}

    public function getUserBalanceAttribute()
    {
        $CI = &get_instance();
        $balance =  $CI->db->get_where('wallet', ['user_id' => $this->user_id])->row();
        if(isset($balance->balance)){
        	return $balance->balance ;
        }else{
        	return 0;
        }
    }

}
?>

