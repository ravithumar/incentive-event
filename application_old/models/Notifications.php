<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Notifications extends Eloquent {
	protected $hidden = ['updated_at'];
	protected $table = 'notifications';  
	
	protected $appends =
    [
        'order_status'
	];

	public function getOrderStatusAttribute()
    {
        $CI = &get_instance();
        $order =  $CI->db->get_where('orders', ['id' => $this->order_id])->row();
        if(isset($order->status)){
        	return $order->status;
        }else{
        	return 0;
        }
    }
}
?>

