<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class VendorChannels extends Eloquent {

    protected $table = 'vendor_channels';
}
?>

