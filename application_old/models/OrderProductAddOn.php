<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class OrderProductAddOn extends Eloquent {

    protected $table = 'order_product_addons';

    protected $appends =
    [
        'addon_name',
        'addon_price'
    ];

    public function product()
    {
        $CI = &get_instance();
        $CI->load->model('Product');
        return $this->hasOne('Product', 'id', 'product_id');
    }

    public function getAddonNameAttribute()
    {
        $CI = &get_instance();
        $product_addon =  $CI->db->get_where('product_addon', ['id' => $this->addon_id])->row();
        if(isset($product_addon->name)){
        	return $product_addon->name;
        }else{
        	return '';
        }
    }
    public function getAddonPriceAttribute()
    {
        $CI = &get_instance();
        $product_addon =  $CI->db->get_where('product_addon', ['id' => $this->addon_id])->row();
        if(isset($product_addon->price)){
        	return $product_addon->price;
        }else{
        	return '';
        }
    }

    public function add_on()
    {
        $CI = &get_instance();
        $CI->load->model('ProductAddOn');
        return $this->hasOne('ProductAddOn', 'id', 'addon_id');
    }
}
?>

