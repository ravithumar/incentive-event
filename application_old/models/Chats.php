<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Chats extends Eloquent {

	use SoftDeletes;
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $table = 'chat';

    protected $appends =
    [
        'order_status'
    ];

    public function getOrderStatusAttribute()
    {
        $CI = &get_instance();
        $order =  $CI->db->get_where('orders', ['id' => $this->order_id])->row();
        if(isset($order->status)){
        	return $order->status;
        }else{
        	return 0;
        }
    }

    public function sender_profile()
    {
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'sender_id');
    }

    public function order()
    {
        $CI = &get_instance();
        $CI->load->model('Orders');
        return $this->hasOne('Orders', 'id', 'order_id');
    }

    public function receiver_profile()
    {
        $CI = &get_instance();
        $CI->load->model('User');
        return $this->hasOne('User', 'id', 'receiver_id');
    }

}
?>

