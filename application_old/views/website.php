<!DOCTYPE html>
<html lang="en">
<head>
    <title>Just Borrowed | A wardrobe full of dreams</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="<?php echo assets('website/images/logo.ico'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo assets('website/css/bootstrap.min.css'); ?>">
 	<link rel="stylesheet" type="text/css" href="<?php echo assets('website/css/style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo assets('website/css/responsive.css'); ?>">  

    
</head>
<body>	
	<div class="main-section index-page">
		<div class="bg-color container-fluide">
			<div class="container  mb-5">
				<div class="row mx-0 align-items-center py-4">
					<!-- <div class="col-md-2 d-none d-lg-block"></div> -->
					<div class="col-md-6">
						<img src="<?php echo assets('website/images/logo.png'); ?>" class="mb-5" style="width: 150px;max-width: 100%;">
						<h1 class="main-title">A <span class="color-brown">WARDROBE</span> <br/>FULL OF DREAMS</h1>
						<p class="mt-4">Look good, do good and get paid by sharing your wardrobe</p>
						<div class="contact-form p-4" >
						    <?php
						    if(isset($msg) && $msg != ''){
						        echo $msg;
						    }
						    ?>
							<h4 class="mb-4">Get early access & special perks!</h4>
							<form method="post" class="row" action="<?php echo base_url('/'); ?>">
							  <div class="form-group col-md-6">
							 	<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" required>
							  </div>
							  <div class="form-group col-md-6">
							   	<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" required>
							  </div>
							  <div class="form-group col-md-12">
							   	<input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email Address" required>
							  </div>
							  <div class="form-group col-md-12 text-center mb-0">
							   	 <button type="submit" class="btn btn-primary brown-btn" name="submit">Sign Up</button>
							  </div>
							  
							 
							</form>
						</div>
					</div>
					<div class="col-md-6 mt-4 mt-md-0">
						<img src="<?php echo assets('website/images/first-section-image.png'); ?>" class="w-100" />
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluide second-section-bg">
			<div class="container">
				<div class="row py-5 flex-column-reverse flex-md-row">
					<div class="col-md-6">
						<div class="our-key-section">
							<div class="img-section">
								<img src="<?php echo assets('website/images/bg-clumn-1.png'); ?>" class="w-100">
								<div class="text-section">
									<img src="<?php echo assets('website/images/Group-860.svg'); ?>" class="mb-3 img-icon">
									<h3 class="mb-3 rotate">Sustainability at heart</h3>
									<P class="rotate">Do good for the environment<br/> by extending the lifespan of<br/> your fashion items</P>
								</div>
							</div>
							
						</div>
						<div class="our-key-section mt-0 mt-md-5">
							<div class="img-section">
								<img src="<?php echo assets('website/images/bg-clumn-1.png'); ?>" class="w-100 ">
								<div class="text-section">
									<img src="<?php echo assets('website/images/Group-862.svg'); ?>" class="mb-3 img-icon">
									<h3 class="mb-3 rotate">Entrepreneurship <br/>for everyone</h3>
									<P class="rotate">Be your own boss and <br/>earn extra $$ each month</P>
								</div>
							</div>
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="section-title mb-0 pb-0  mb-md-5 pb-md-5 text-center"><h2>OUR KEY PILLARS</h2></div>
						<div class="our-key-section mt-5">
							<div class="img-section">
								<img src="<?php echo assets('website/images/bg-clumn-1.png'); ?>" class="w-100 img-icon">
								<div class="text-section">
									<img src="<?php echo assets('website/images/Group-861.svg'); ?>" class="mb-3 img-icon">
									<h3 class="mb-3 rotate">A community that shares</h3>
									<P class="rotate">Be a part of our community<br/> where what’s mine is yours</P>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container mb-5 pb-5">
			<div class="row mt-5 mx-0 mb-5">
				<div class="col-md-12"><div class="section-title text-center"><h2>How It Works</h2></div></div>
			</div>
			<div class="row mx-0 align-items-center">
				
				<div class="col-md-6 text-center">
					<img src="<?php echo assets('website/images/Group-866.png'); ?>" style="width: 400px;max-width: 100%;"> 
				</div>
				<div class="col-md-6 mt-5 mt-md-0">
					<div class="how-it d-flex align-items-center justify-content-start mb-5 pb-5">
						<img src="<?php echo assets('website/images/Group-863.svg'); ?>" class="">
						<h3 class="pl-4">List: Upload and list your<br/> beautiful items via our app</h3>
					</div>
					<div class="how-it d-flex align-items-center justify-content-start mb-5 pb-5 pl-0 pl-md-5 py-4">
						<img src="<?php echo assets('website/images/Group-864.svg'); ?>" class="">
						<h3 class="pl-4">Share: Connect to the community<br/> and start sharing your wardrobe</h3>
					</div>
					<div class="how-it d-flex align-items-center justify-content-start">
						<img src="<?php echo assets('website/images/Group-865.svg'); ?>" class="">
						<h3 class="pl-4">Earn: Get paid every time<br/> someone borrows your item</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluide our-story-section mt-5 d-inline-block pt-5">
			
			<div class="row mx-0 align-items-center mb-5 pb-4">
				<div class="col-md-1"></div>
				<div class="col-md-5 text-center">
					<img src="<?php echo assets('website/images/Group-867.png'); ?>" style="width: 400px;max-width: 100%;">
				</div>
				<div class="col-md-5">
					<div class="col-md-12 mt-5 mt-md-0"><div class="section-title"><h2>Our Story</h2></div></div>
					<p class="col-lg-10 col-md-12 mt-4">Just Borrowed was an idea that sparked from a 
						passion for the environment and style. We 
						believe that being environmentally conscious 
						should not limit our expression of style and vice 
						versa. Each of us can make an impact on our 
						environment by sharing our quality wardrobes 
						with each other. Just Borrowed provides a 
						platform for like-minded individuals to maximize 
						their wardrobe and extend its lifespan by renting 
						it out to others. We aim to close the loop in the 
						fashion industry and make fashion circular! Why 
						buy when you can borrow? </p>
				</div>
			</div>
			<div class="row mx-0">
				<div class="container mt-5 mb-5">
					<h2 class="text-center launching-text pt-5">Launching <span class="color-brown">Soon</span></h2>
				</div>
			</div>
		</div>
	</div>	

	<script type="text/javascript" src="<?php echo assets('website/js/jquery-1.11.0.min.js'); ?>"></script>
   	<script type="text/javascript" src="<?php echo assets('website/js/bootstrap.min.js'); ?>"></script> 
    <script type="text/javascript" src="<?php echo assets('website/js/popper.min.js'); ?>"></script>
   
</body>
</html>

