<?php
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo config('site_meta'); ?>">
        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/rad.ico') ?>">
        <title><?php echo config('site_meta').' | Vendor Panel'; ?></title>
        
        <link rel="stylesheet" href="<?php echo base_url('plugins/morris/morris.css'); ?>">

        <!-- DataTables -->
        <link href="<?php echo base_url('plugins/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('plugins/datatables/buttons.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="<?php echo base_url('plugins/datatables/responsive.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- autoclose date picker -->
        <link href="<?php echo base_url('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo assets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/metismenu.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/icons.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/style.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/custom.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('plugins/notiflix/notiflix-2.1.2.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" type="text/css">

        <link href="<?php echo base_url('plugins/magnific-popup/magnific-popup.css') ;?>" rel="stylesheet" type="text/css">
        
        <!-- js -->
        <!-- jQuery  -->
        <script src="<?php echo assets('js/jquery.min.js'); ?>"></script>

        <!-- Js form validations -->
        <script src="<?php echo assets('js/validation/jquery.validate.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.min.js'); ?>"></script>

        <script src="<?php echo assets('js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo assets('js/metisMenu.min.js'); ?>"></script>
        <script src="<?php echo assets('js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo assets('js/waves.min.js'); ?>"></script>

        <script src="<?php echo base_url('plugins/jquery-sparkline/jquery.sparkline.min.js'); ?>"></script>
        <script src="<?php echo base_url('plugins/select2/js/select2.min.js'); ?>"></script>

        <script src="<?php echo base_url('plugins/magnific-popup/jquery.magnific-popup.min.js');?>" ></script>
        


        <!--Morris Chart-->
        <script src="<?php echo base_url('plugins/morris/morris.min.js'); ?>"></script>
        <script src="<?php echo base_url('plugins/raphael/raphael-min.js'); ?>"></script>
        <script src="<?php echo assets('js/custom/admin/custom.js'); ?>"></script>

        <!-- notiflix -->
        <script src="<?php echo base_url('plugins/notiflix/notiflix-2.1.2.js'); ?>"></script>

        <!-- parsleyjs -->
        <script src="<?php echo base_url('plugins/parsleyjs/parsley.min.js'); ?>"></script>

        <!-- Auto Close datepicker -->
        <script src="<?php echo base_url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
        <script src="<?php echo base_url('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js'); ?>"></script>

        <script type="text/javascript">
            var BASE_URL = '<?php echo base_url(); ?>';
            var prof_default = '<?php echo assets("images")."/avtar.png"; ?>';
        </script>
    </head>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <div class="page-title-box">
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
            <div class="col-lg-3">
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6">
                <?php
            $this->load->view('admin/includes/message');
            ?>
            </div>
            <div class="col-lg-1">
            </div>
        </div>

        <form class="form-validate"  method="post" action="<?php echo $change_pw_link; ?>">

            <div class="row">

                <div class="col-lg-1">
                </div>

                <div class="col-lg-10">

                    <div class="card m-b-20">
                        <div class="card-body">
<!-- 
                                <div class="form-group" style="display:none;">
                                    <label>Current Password</label>
                                    <div>
                                    <?php
$field_value = NULL;
$temp_value = set_value('old');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                        <input type="password" name="old" class="form-control" id="old" placeholder="Enter current password" value="<?php echo $field_value; ?>" required> 
                                        <div class="validation-error-label">
                                            <?php echo form_error('old'); ?>
                                        </div>
                                    </div>
                                </div> -->
                           
                                <div class="form-group">
                                    <label>New Password</label>
                                    <div>
                                    <?php
$field_value = NULL;
$temp_value = set_value('new');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                        <input type="password" name="new" class="form-control" id="new" placeholder="Enter new password" value="<?php echo $field_value; ?>" required data-parsley-minlength="8">
                                        <div class="validation-error-label">
                                            <?php echo form_error('new'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Confirm New Password</label>
                                    <div>
                                        <?php
$field_value = NULL;
$temp_value = set_value('new_confirm');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                        <input type="password" name="new_confirm" class="form-control" id="new_confirm" placeholder="Enter confirm new password" value="<?php echo $field_value; ?>" required data-parsley-equalto="#new">
                                        <div class="validation-error-label">
                                            <?php echo form_error('new_confirm'); ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group m-b-0">
                                    <div>
                                        <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                            Reset
                                        </button>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div> 

                <div class="col-lg-1">
                </div>

            </div> <!-- end row -->
        </form>

    </div>
</div>

        <footer class="footer">
        Copyright &copy; <?php echo config('site_title')." ".date('Y'); ?>. <!-- <span class="d-none d-sm-inline-block"> Crafted with <i class="mdi mdi-heart text-danger"></i> by Excellent WebWorld</span>. -->
        </footer>
    
        </div>
</div>
        <!-- END wrapper -->

</body>

</html>

<?php
if(isset($datatable) && $datatable)
{
    $this->load->view('vendor/includes/datatable');
}
?>


<!-- App js -->
<script src="<?php echo assets('js/app.js'); ?>"></script>
<script type="text/javascript">
    $('form').parsley();
    var path = window.location.pathname;
    $(".metismenu li a[href*='"+path.split('/')[1]+'/'+path.split('/')[2] +"']").addClass("active").closest('li').addClass("active").closest('ul').addClass('in');
    
</script>
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
    var add_channel_url = "<?php echo base_url().'add-channel'; ?>";    
    console.log('add_channel_url:'+add_channel_url);
    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
        OneSignal.init({
            appId: "fe939676-d326-4f0a-9cbb-15b6f5bc2f77",
        });
        // OneSignal.showNativePrompt();
  });

  OneSignal.push(function() {
        // alert('here');
        OneSignal.on('subscriptionChange', function(isSubscribed) {
            // alert('here123');
            console.log('isSubscribed:'+isSubscribed);            
            if (isSubscribed === true) {

                console.log('The user subscription state is now:', isSubscribed);

                OneSignal.getUserId( function(userId) {
                    // Make a POST call to your server with the user ID
                    console.log('The player id is :', userId);                    

                    $.ajax({
                        url: add_channel_url,
                        type: "POST",
                        data:{
                          // user_id:user_id,
                          channel_id:userId,
                          // user:user_type
                        },
                        success: function (returnData) {
                            returnData = $.parseJSON(returnData);
                            if (typeof returnData != "undefined")
                            {
                                console.log(returnData);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log('error in saving channel');
                        }
                    });

                  });

            }
        });

        OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            console.log('isEnabled:'+isEnabled);
          if (isEnabled) {

              // user has subscribed
              OneSignal.getUserId( function(userId) {
                  console.log('player_id of the subscribed user is : ' + userId);
                  // Make a POST call to your server with the user ID   
                    $.ajax({
                        url: add_channel_url,
                        type: "POST",
                        data:{
                          // user_id:user_id,
                          channel_id:userId,    
                          // user:user_type
                        },
                        success: function (returnData) {
                            returnData = $.parseJSON(returnData);
                            if (typeof returnData != "undefined")
                            {
                                console.log(returnData);
                            } 
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log('error in saving channel');
                        }
                    });


              });
          }
        });
    });

</script>