<!-- <style type="text/css" media="screen">
.side-menu {
    background: #ffffff;
}
#sidebar-menu > ul > li > a:hover, #sidebar-menu > ul > li > a:focus, #sidebar-menu > ul > li > a:active, #sidebar-menu .submenu > li > a:hover, #sidebar-menu .submenu > li > a:focus, sidebar-menu .submenu > li > a:active {
    color: #87c242;
    background-color: #8bc34a42;
}

#sidebar-menu > ul > li > a{
     font-weight: bold;
}
#sidebar-menu > ul > li > a.active, .submenu li.active > a.active {
    background-color: #8bc34a42;
}

.enlarged #wrapper #sidebar-menu ul ul {
    background-color: #ffffff;
}
</style>
 --><!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Main Menu</li>
                <li>
                    <a href="<?php echo base_url('vendor/dashboard'); ?>" class="waves-effect">
                        <i class="mdi mdi-view-dashboard"></i><span> Dashboard </span>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo base_url('vendor/my-profile'); ?>" class="waves-effect"><i class="mdi mdi-store"></i><span> My Profile </span></a>
                </li> 

                <li class="">
                    <a href="<?php echo base_url('vendor/promocode'); ?>" class="waves-effect"><i class="mdi mdi-tag-multiple"></i><span> Promocode </span></a>
                </li>

                <li class="">
                    <a href="<?php echo base_url('vendor/variant'); ?>" class="waves-effect"><i class="mdi mdi-google-circles"></i><span> Variant </span></a>
                </li>

                
                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-food"></i><span>Manage Products<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="submenu">
                        <li><a href="<?php echo base_url('vendor/product'); ?>">All Products</a></li>
                        <li><a href="<?php echo base_url('vendor/featured-products'); ?>">Featured Products</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-content-paste"></i><span>Orders<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="submenu">
                        <li><a href="<?php echo base_url('vendor/order-arrived'); ?>">Arrived</a></li>
                        <li><a href="<?php echo base_url('vendor/order-inprocess'); ?>">In process</a></li>
                        <li><a href="<?php echo base_url('vendor/order-rejected'); ?>"> Rejected</a></li>
                        <li><a href="<?php echo base_url('vendor/order-completed'); ?>">Completed</a></li>
                        <li><a href="<?php echo base_url('vendor/all-orders'); ?>">All Orders</a></li>
                    </ul>
                </li>

                <li class="">
                    <a href="<?php echo base_url('vendor/earning'); ?>" class="waves-effect"><i class="mdi mdi-chart-bar"></i><span> Earnings </span></a>
                </li>

                <li class="">
                    <a href="<?php echo base_url('vendor/contact'); ?>" class="waves-effect"><i class="mdi mdi-message-text"></i><span> Admin Contact </span></a>
                </li>

                <li class="">
                    <a href="<?php echo base_url(); ?>vendor_contract.pdf" target="_blank" class="waves-effect"><i class="mdi mdi-alert-circle-outline"></i><span> Contract </span></a>
                </li>

               <!--  <li class="">
                    <a href="<?php //echo base_url('vendor/account'); ?>" class="waves-effect"><i class="mdi mdi-bank"></i><span> Bank Details </span></a>
                </li> -->

            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->