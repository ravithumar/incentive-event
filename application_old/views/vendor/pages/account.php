<?php
$post_link = base_url('vendor/account/add');
$back = base_url('vendor/account');
?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                       <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('vendor/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                       <form class="form-validate"  method="post" action="<?php echo $post_link; ?>" enctype="multipart/form-data">
                            
                            <div class="row" >
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required">Email</label>
                                        <div>
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required" for="dob">Date of Birth</label>
                                        <div>
                                            <div class="input-group">
                                                <input required type="text" class="form-control datepicker-autoclose" placeholder="Ex. 1989-05-12" name="dob" id="dob" data-parsley-errors-container="#error_from" autocomplete="off" readonly> 
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('dob'); ?>
                                            </div>
                                            <div id="error_from"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required">City</label>
                                        <div> 
                                            <input type="text" name="city" class="form-control" id="city" placeholder="Enter City" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required">Line1 Address</label>
                                        <div> 
                                            <input type="text" name="line1" class="form-control" id="line1" placeholder="Enter Line1 Address" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required">Line2 Address</label>
                                        <div> 
                                            <input type="text" name="line2" class="form-control" id="line2" placeholder="Enter Line2 Address" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required">Postal Code</label>
                                        <div> 
                                            <input type="text" name="postal_code" class="form-control" id="postal_code" placeholder="Enter Postal Code" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required">Routing Number</label>
                                        <div> 
                                            <input type="text" name="routing_number" class="form-control" id="routing_number" placeholder="Enter Routing Number" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="required">Account Number</label>
                                        <div> 
                                            <input type="text" name="account_number" class="form-control" id="account_number" placeholder="Enter Account Number" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Submit
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
</div>  
<script>
    $("#dob").datepicker({
        todayBtn:  1,
        autoclose: true,
        format: 'yyyy-mm-dd',
        endDate: '-0d',   
    })
</script>   