<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo $add_url; ?>" class="btn btn-dark waves-effect waves-light float-right">
                            <i class="ion-plus mr-1"></i> Add 
                        </a>
                        <h4 class="page-title"><?php echo $title; ?></h4>
                        <?php
                        echo $this->breadcrumbs->show();
                     ?>
                </div>
            </div>
        </div>
         <?php
        $this->load->view('vendor/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                         <?php echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
</div>