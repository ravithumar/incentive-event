<style type="text/css">
    .dot {
      height: 25px;
      width: 25px;
      background-color: #f05b4f;
      border-radius: 50%;
      display: inline-block;
    }
    .dot2{
        background-color: #d70206;
    }
    .dot3{
        background-color: #f4c63d;
    }
    
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Welcome to Vendor Dashbaord</a></li>
                    </ol>
                </div>
            </div>
        </div>

        <?php
        $this->load->view('vendor/includes/message');
        ?>

        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" onclick="window.location='<?php echo base_url('vendor/product'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-account-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Products</h6>
                            <h4 class="mb-4"><?php echo $total_products; ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" onclick="window.location='<?php echo base_url('vendor/order-completed'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-food float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Completed Orders</h6>
                            <h4 class="mb-4"><?php echo $total_orders; ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" onclick="window.location='<?php echo base_url('vendor/earning'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-chart-bar float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Revenue</h6>
                            <h4 class="mb-4"><?php echo number_format((float)$revenue, 2, '.', ''); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" >
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-currency-usd float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Net profit</h6>
                            <h4 class="mb-4"><?php echo number_format((float)$net_profit, 2, '.', ''); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>