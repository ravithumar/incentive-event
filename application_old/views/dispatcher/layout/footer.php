
        <footer class="footer">
            &copy; <?php echo date('Y').' '.config('site_title'); ?>. <span class="d-none d-sm-inline-block"> Crafted with <i class="mdi mdi-heart text-danger"></i> by Excellent WebWorld</span>.
        </footer>
    
        </div>
</div>
        <!-- END wrapper -->

</body>

</html>

<?php
if(isset($datatable) && $datatable)
{
    $this->load->view('dispatcher/includes/datatable');
}
?>


<!-- App js -->
<script src="<?php echo assets('js/app.js'); ?>"></script>
<script type="text/javascript">
    $('form').parsley();
    var path = window.location.pathname;
    //console.log(path);
    $(".metismenu li a[href*='"+path.split('/')[1]+'/'+path.split('/')[2] +"']").addClass("active").closest('li').addClass("active").closest('ul').addClass('in');
</script>