<style type="text/css">
    .dot {
      height: 25px;
      width: 25px;
      background-color: #f05b4f;
      border-radius: 50%;
      display: inline-block;
    }
    .dot2{
        background-color: #d70206;
    }
    .dot3{
        background-color: #f4c63d;
    }
    
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Welcome to Dispatcher Dashbaord</a></li>
                    </ol>
                </div>
            </div>
        </div>

        <?php
        $this->load->view('dispatcher/includes/message');
        ?>

        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" onclick="window.location='<?php //echo base_url('admin/users'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-account-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Delivery Boy</h6>
                            <h4 class="mb-4"><?php echo $total_products; ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" onclick="window.location='<?php //echo base_url('admin/products'); ?>'">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-food float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Order Delivered</h6>
                            <h4 class="mb-4"><?php echo $total_products; ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-primary" >
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-chart-bar float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Growth amount</h6>
                            <h4 class="mb-4"><?php echo number_format((float)$growth_amount, 2, '.', ''); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat bg-dark" >
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-currency-usd float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Net amount</h6>
                            <h4 class="mb-4"><?php echo number_format((float)$net_amount, 2, '.', ''); ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xl-12">
                <div class="card m-b-20">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-30 header-title">Recent Orders</h4>

                        <div class="table-responsive">
                            <table class="table table-vertical">

                                <tbody>
                                    <tr>
                                        <th>
                                            Profile
                                        </th>
                                        <th>
                                            First Name
                                        </th>
                                        <th>
                                            Last Name
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Phone Number
                                        </th>
                                        <th class="text-center">
                                            Action
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        </div>

    </div>
</div>