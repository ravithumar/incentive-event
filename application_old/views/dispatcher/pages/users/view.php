<style type="text/css" media="screen">
    #delivery_address {
        display: block;
        height: 450px;
        overflow-y: auto;
    }
    #cards img {
    height: 430px;
    width: auto;
    object-fit: contain;
}
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo base_url('dispatcher/user'); ?>" class="ml-2 btn btn-dark waves-effect waves-light float-right">
                        <i class="far fa-arrow-alt-circle-left m-r-10 "></i> Back 
                    </a>
                    <a href="<?php echo base_url('dispatcher/user/edit/').$db['id']; ?>" class="btn btn-dark waves-effect waves-light float-right">
                        <i class="ion-edit mr-1"></i> Edit 
                    </a>
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                    echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div> 
        </div>

        <?php
        $this->load->view('dispatcher/includes/message');
        ?>
        <div class="row d-flex">
            <div class="col-lg-4">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="card-title font-16 mt-0">
                            <?php echo stripslashes($db['first_name']).' '.stripslashes($db['last_name']);
                            if($db['duty'] == 1){
                                echo '<span class="ml-2 badge badge-pill badge-success">Online</span>'; 
                            }else{
                                echo '<span class="ml-2 badge badge-pill badge-danger">Offline</span>';
                            } ?>
                        </h4>
                        <h6 class="card-subtitle font-12 text-muted">
                            <p class="mb-1">
                                <?php  
                                for ($x = 0; $x < 5; $x++) {
                                    //if($x < $rating){
                                    if($x < 4){
                                        echo "<i class='mdi mdi-star mr-2 font-18'> </i>";
                                    }else{
                                        echo "<i class='mdi mdi-star-outline mr-2 font-18'> </i>";
                                    }
                                }
                                ?> 
                            </p>
                            <p class="mb-1"><i class="fas fa-phone mr-2"></i><?php echo $db['phone']; ?></p>
                            <p class="mb-1"><a href="mailto:<?php echo $db['email']; ?>"><i class="fas fa-envelope mr-2"></i><?php if($db['email'] != ''){ echo $db['email'];}else{echo '-';} ?></a></p>
                        </h6>
                    </div>
                    <img class="img-fluid border" src="<?php echo BASE_URL().$db['profile_picture']; ?>" alt="Profile Picture" style="height: 378px;">
                </div>
            </div>

            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card m-b-30" id="cards">
                            <h4 class="card-header font-16 mt-0">Driving License</h4>
                            <div class="card-body text-center"> 
                                <a class="image-popup-no-margins" href="<?php echo BASE_URL().$db['driving_license'];?>">
                                    <img class="img-fluid border" src="<?php echo BASE_URL().$db['driving_license']; ?>" alt="Profile Picture">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card m-b-30" id="cards">
                            <h4 class="card-header font-16 mt-0">Insurance Certificate</h4>
                            <div class="card-body text-center"> 
                                <a class="image-popup-no-margins" href="<?php echo BASE_URL().$db['insurance_certi'];?>">
                                    <img class="img-fluid border" src="<?php echo BASE_URL().$db['insurance_certi']; ?>" alt="Profile Picture">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- <div class="col-lg-5">

                <div class="card m-b-30">
                    <h4 class="card-header font-16 mt-0">Wallet History</h4>
                    <div id="delivery_address">
                        <table class="table table-hover" >
                        <tbody>
                            <?php if (isset($db['wallet_history'])&& !empty($db['wallet_history']) ){ 
                            foreach ($db['wallet_history'] as $key => $value) { 
                                
                            ?>
                            <tr>
                                <?php if($value['type'] == 'plus') {?>
                                <td>
                                    <b><p class="mb-0"><i class="mdi mdi-plus mr-2 text-success"></i><?php echo '$'.$value['amount'].' '.$value['message']; ?></p></b>
                                </td>
                                <?php }else{?>
                                <td>
                                    <b><p class="mb-0"><i class="mdi mdi-minus mr-2 text-danger"></i><?php echo '$'.$value['amount'].' '.$value['message']; ?></p></b>
                                </td>
                                <?php }?>
                            </tr>
                            <?php } }else{?>
                                    <tr>
                                        <td>No any wallet history found!</td>
                                    </tr>
                                <?php }?>
                        </tbody>
                    </table>
                    </div>
                    
                </div>

            </div> -->
           
        </div> <!-- end row -->

        <div class="row">
            <div class="col-md-3">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-food float-right" ></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Completed Orders</h6> 
                            <h4 class="mb-4"><?php if(isset($amount['total_orders'])){echo $amount['total_orders']; }else{ echo '0';} ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stat bg-dark">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-square-inc-cash float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Pending Orders</h6> 
                            <h4 class="mb-4"><?php if(isset($amount['admin_amount'])){ echo number_format((float)$amount['admin_amount'], 2, '.', ''); }else{ echo '0';} ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stat bg-primary ">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-cash-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Failed Orders</h6>
                            <h4 class="mb-4"><?php if(isset($amount['store_amount'])){ echo number_format((float)$amount['store_amount'], 2, '.', ''); }else{ echo '0';} ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stat bg-dark ">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-cash-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Earning Balance</h6>
                            <h4 class="mb-4"><?php if(isset($db['balance'])){ echo number_format((float)$db['balance'], 2, '.', ''); }else{ echo '0';} ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <h4 class="card-header font-16 mt-0">Orders</h4>
                    <div class="card-body">
                         <?php //echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

        <div class="modal fade bs-example-modal-lg" id="trackmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content track">
              <div class="modal-header bg-blue">
                <h6 class="modal-title text-center m-0 font-weight-bold text-white">Track Order #<span id="order-id"></span></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               <h4 class="text-center">Tracking data fetching..</h4>
              </div>
              <div class="text-center text-white bg-blue py-2">
                <h6 class="text-uppercase font-weight-bold"><span id="order-final-status"></span></h6>
                <div class="pb-2"><span id="order-final-text"></span></div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
<script src="<?php echo assets('pages/lightbox.js');?>"></script>
<script src="<?php echo base_url('/assets/js/custom/admin/track_order.js');?>" type="text/javascript"></script>       