<html lang="en">
    <head>
    <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo config('site_meta'); ?>">
        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
        <title><?php echo config('site_meta')."- Reset password"; ?> </title>

        <link href="<?php echo assets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/metismenu.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/icons.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/style.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/custom.css'); ?>" rel="stylesheet" type="text/css">
    </head>

    <body>

        <!-- Begin page -->
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0 pt-2">
                        <a href="javascript:void(0)" class="logo logo-admin"><img src="<?php echo assets('images/logo_vector.png'); ?>" height="90" alt="RAD"></a>
                        <!-- <a href="#" class="logo logo-admin">logo</a> -->
                    </h3>

                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Welcome !</h4>
                        <p class="text-muted text-center">Set password to continue to RAD.</p>

                        <form class="form-validate form-horizontal m-t-30" method="post" action="<?php echo base_url('forgotten/').$forgotten_password_selector; ?>">

                            <?php// echo get_msg(); ?>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <?php
$field_value = NULL;
$temp_value = set_value('password');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Enter  password" data-parsley-minlength="8" autocomplete="off" required value="<?php echo $field_value; ?>">
                                <div class="validation-error-label">
                                    <?php echo form_error('password'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cpassword">Confirm Password</label>
                                <?php
$field_value = NULL;
$temp_value = set_value('cpassword');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                <input type="password" name="cpassword" class="form-control" id="cpassword" placeholder="Enter confirm password" data-parsley-minlength="8" autocomplete="off" required data-parsley-equalto="#password" value="<?php echo $field_value; ?>">
                                <div class="validation-error-label">
                                    <?php echo form_error('cpassword'); ?>
                                </div>
                            </div>

                            <input type="hidden" name="forgotten_password_selector" value="<?php echo $forgotten_password_selector; ?>">

                            <div class="form-group row m-t-20">
                                <div class="col-12 text-center">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit" name="submit">Set Password</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
            <p> Copyright &copy; <?php echo config('site_title')." ".date('Y'); ?></p>
                <!-- <p>&copy; 2020 <?php //echo config('site_title'); ?>. Crafted with <i class="mdi mdi-heart text-danger"></i> by Excellent WebWorld</p> -->
            </div>

        </div>


        
        <script src="<?php echo assets('js/jquery.min.js'); ?>"></script>
        <script src="<?php echo assets('js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo assets('js/metisMenu.min.js'); ?>"></script>
        <script src="<?php echo assets('js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo assets('js/waves.min.js'); ?>"></script>

        <!-- Js form validations -->
        <script src="<?php echo assets('js/validation/jquery.validate.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.min.js'); ?>"></script>

        

        <script src="<?php echo base_url('plugins/jquery-sparkline/jquery.sparkline.min.js'); ?>"></script>
        <script src="<?php echo base_url('plugins/parsleyjs/parsley.min.js'); ?>"></script>
        <script src="<?php echo assets('js/custom/reset_password.js');?>"></script>
        <script type="text/javascript">
        $('form').parsley();
        </script>

    </body>
</html>