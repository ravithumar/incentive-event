<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo config('site_meta'); ?>">
        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
        <title><?php echo config('site_meta')."- Reset password"; ?> </title>

        <link href="<?php echo assets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/metismenu.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/icons.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/style.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/custom.css'); ?>" rel="stylesheet" type="text/css">
    </head>

    <body>

        <!-- Begin page -->
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0 pt-2">
                        <a href="javascript:void(0)" class="logo logo-admin"><img src="<?php echo assets('images/logo_vector.png'); ?>" height="90" alt="logo"></a>
                        <!-- <a href="#" class="logo logo-admin">logo</a> -->
                    </h3>

                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Reset Password</h4>
                        
                        <?php
                        //$this->load->view('admin/includes/message');
                        ?>
                        <form class="form-validate form-horizontal m-t-30" method="post" action="<?php echo base_url('/auth/reset_password/'.$code); ?>">



                            <div class="form-group">
                                    New Password(at least 8 characters long)
                                 <?php
$field_value = NULL;
$temp_value = set_value('new');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                <input type="password" name="new" class="form-control" id="new" pattern="^.{8}.*$" placeholder="Enter password" autocomplete="off" value="<?php echo $field_value; ?>" required>
                                <div class="validation-error-label">
                                    <?php echo form_error('identity'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?>
                                <?php
$field_value = NULL;
$temp_value = set_value('new_confirm');
if (isset($temp_value) && !empty($temp_value)) {
    $field_value = $temp_value;
} 
?>
                                <input type="password" name="new_confirm" class="form-control" id="new_confirm" pattern="^.{8}.*$" placeholder="Enter confirm password" autocomplete="off" value="<?php echo $field_value; ?>" required data-parsley-equalto="#new">
                                <div class="validation-error-label">
                                    <?php echo form_error('new_confirm'); ?>
                                </div>
                            </div>

                            <?php echo form_hidden($csrf); ?>
                            <?php echo form_input($user_id);?>

                            <div class="form-group row m-t-20">
                                <!-- <div class="col-6 m-t-5">
                                    <a href="forgot_password" class="text-muted"><i class="mdi mdi-lock"></i><?php //echo lang('login_forgot_password');?></a>
                                </div> -->
                                <div class="col-12 text-center">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit" name="submit">
                                        <?php echo lang('reset_password_submit_btn');?>
                                    </button>
                                </div>
                            </div>
                        
                            
                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
            <p> Copyright &copy; <?php echo config('site_title')." ".date('Y'); ?></p>
                <!-- <p>&copy; <?php //echo date('Y').' '. config('site_title'); ?>. Crafted with <i class="mdi mdi-heart text-danger"></i> by Excellent WebWorld</p> -->
            </div>

        </div>


        
        <script src="<?php echo assets('js/jquery.min.js'); ?>"></script>
        <script src="<?php echo assets('js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo assets('js/metisMenu.min.js'); ?>"></script>
        <script src="<?php echo assets('js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo assets('js/waves.min.js'); ?>"></script>

        <!-- Js form validations -->
        <script src="<?php echo assets('js/validation/jquery.validate.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.min.js'); ?>"></script>



        <script src="<?php echo base_url('plugins/jquery-sparkline/jquery.sparkline.min.js'); ?>"></script>

        <!-- parsleyjs -->
        <script src="<?php echo base_url('plugins/parsleyjs/parsley.min.js'); ?>"></script>

    </body>
</html>
<script type="text/javascript">
    $('form').parsley();
</script>