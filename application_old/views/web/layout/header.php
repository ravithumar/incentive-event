<!DOCTYPE html>
<html>
<head>




	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- <title>Incentive Event</title> -->
    <meta name="description" content="<?php echo config('site_meta'); ?>">
    <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
    <title><?php echo config('site_meta'); ?></title>
	<link rel="icon" href="<?= assets('web/images/favicon.png'); ?>" type="png" sizes="16x16">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/owl.carousel.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css'); ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/slick.css'); ?>">

</head>
<body class="relative">
	<div id="loader-wrapper" class="d-flex align-items-center justify-content-center">
		<div id="loader">
			<img src="<?php echo assets('web/images/preload.png'); ?>" alt="" class="img-fluid">
			<div class="text-wrap d-flex align-items-center mt-2">
				<span class="text font-35 text-uppercase font-futura-bq text-white">Now Loading</span>
				<div class="loader-inner pl-3"> <span></span> <span></span> </div>
			</div>
		</div>
		<div class="loader-section section-left"></div>
	    <div class="loader-section section-right"></div>
	</div>
<?php 
	if (isset($this->session->userdata['users']['user_id'])) {
		$this->load->view('web/layout/menu');
	}  ?>