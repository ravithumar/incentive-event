
    <div class="schedule-scroll pt150">
		<div class="container-fluid">
			<div class="row align-items-stretch flex-nowrap scrollbar">
				<div class="col-sm-6 col-lg-4 mb-3">
					<a href="event_step01.php" class="block br-white p-3 mx-auto mb-3 border-r6 d-block">
						<div class="info-block br-yellow border-r4 p-2 d-flex align-items-center justify-content-between mb-3 position-relative">
							<div class="date text-center">
								<span class="numb t-yellow d-block font-24 text-left text-uppercase">6</span>
                            	<span class="line d-block"></span>
                            	<span class="month t-yellow d-block font-24 text-uppercase text-right">Jul</span>
                            </div>
							<div class="time font-22">
								<p class="text-white mb-0 text-uppercase">Time: 18:00 - 21:00</p>
								<p class="text-white mb-0 text-uppercase">Venue: Kitec</p>
							</div>
                            
						</div>
						<div class="name-block t-yellow text-center text-uppercase d-flex justify-content-center align-items-center p-2 font-40 font-futura-bq border-r5">
							Sunlife Acm
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-lg-4  mb-3">
					<a href="javascript:void(0);" class="block br-white p-3 mx-auto mb-3 border-r6 d-block">
						<div class="info-block br-yellow border-r4 p-2 d-flex align-items-center justify-content-between mb-3 position-relative">
							<div class="date text-center">
								<span class="numb t-yellow d-block font-24 text-left text-uppercase">15</span>
                            	<span class="line d-block"></span>
                            	<span class="month t-yellow d-block font-24 text-uppercase text-right">Sep</span>
                            </div>
							<div class="time font-22">
								<p class="text-white mb-0 text-uppercase">Time: 18:00 - 21:00</p>
								<p class="text-white mb-0 text-uppercase">Venue: Tbc</p>
							</div>
                            
						</div>
						<div class="name-block t-yellow text-center text-uppercase d-flex justify-content-center align-items-center font-40 font-futura-bq p-2 border-r5">
							Annual Dinner
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-lg-4 mb-3">
					<a href="javascript:void(0);" class="block br-white p-3 mx-auto mb-3 border-r6 d-block">
						<div class="info-block br-yellow border-r4 p-2 d-flex align-items-center justify-content-between mb-3 position-relative">
							<div class="date text-center">
								<span class="numb t-yellow d-block font-24 text-left text-uppercase">2</span>
                            	<span class="line d-block"></span>
                            	<span class="month t-yellow d-block font-24 text-uppercase text-right">Oct</span>
                            </div>
							<div class="time font-22">
								<p class="text-white mb-0 text-uppercase">Time: 18:00 - 21:00</p>
								<p class="text-white mb-0 text-uppercase">Venue: Kitec</p>
							</div>
                            
						</div>
						<div class="name-block t-yellow text-center text-uppercase d-flex justify-content-center align-items-center font-40 font-futura-bq p-2 border-r5">
							Award Presentation
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-lg-4  mb-3">
					<a href="javascript:void(0);" class="block br-white p-3 mx-auto mb-3 border-r6 d-block">
						<div class="info-block br-yellow border-r4 p-2 d-flex align-items-center justify-content-between mb-3 position-relative">
							<div class="date text-center">
								<span class="numb t-yellow d-block font-24 text-left text-uppercase">6</span>
                            	<span class="line d-block"></span>
                            	<span class="month t-yellow d-block font-24 text-uppercase text-right">Jul</span>
                            </div>
							<div class="time font-22">
								<p class="text-white mb-0 text-uppercase">Time: 18:00 - 21:00</p>
								<p class="text-white mb-0 text-uppercase">Venue: Kitec</p>
							</div>
                            
						</div>
						<div class="name-block t-yellow text-center text-uppercase d-flex justify-content-center align-items-center font-40 font-futura-bq p-2 border-r5">
							Sunlife Acm
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-lg-4  mb-3">
					<a href="javascript:void(0);" class="block br-white p-3 mx-auto mb-3 border-r6 d-block">
						<div class="info-block br-yellow border-r4 p-2 d-flex align-items-center justify-content-between  mb-3 position-relative">
							<div class="date text-center">
								<span class="numb t-yellow d-block font-24 text-left text-uppercase">6</span>
                            	<span class="line d-block"></span>
                            	<span class="month t-yellow d-block font-24 text-uppercase text-right">Jul</span>
                            </div>
							<div class="time font-22">
								<p class="text-white mb-0 text-uppercase">Time: 18:00 - 21:00</p>
								<p class="text-white mb-0 text-uppercase">Venue: Kitec</p>
							</div>
                            
						</div>
						<div class="name-block t-yellow text-center text-uppercase d-flex justify-content-center align-items-center font-40 font-futura-bq p-2 border-r5">
							Sunlife Acm
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="mobile-schedule d-none">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<a href="event_step01.php" class="block br-white border-r10 p-3 mx-auto mb-3 position-relative border-r6 d-block">
						<div class="info-block d-flex align-items-center justify-content-between position-relative">
							<div class="date text-center br-yellow p-2 border-r5">
								<span class="numb t-yellow d-block font-50 text-left text-uppercase">6</span>
                            	<span class="line d-block"></span>
                            	<span class="month t-yellow d-block font-50 text-right text-uppercase">Jul</span>
                            </div>
							<div class="event-title font-22">
								<p class="text-uppercase mb-0 t-yellow">Sunlife ACM</p>
							</div>
                            
						</div>
						
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<a href="#" class="block br-white border-r10 p-3 mx-auto mb-3 position-relative border-r6 d-block">
						<div class="info-block d-flex align-items-center justify-content-between position-relative">
							<div class="date text-center br-yellow p-2 border-r5">
								<span class="numb t-yellow d-block font-50 text-left text-uppercase">15</span>
                            	<span class="line d-block"></span>
                            	<span class="month t-yellow d-block font-50 text-right text-uppercase">May</span>
                            </div>
							<div class="event-title font-22">
								<p class="text-uppercase mb-0 t-yellow">Sales Award</p>
							</div>
                            
						</div>
						
					</a>
				</div>
			</div>

		</div>
	</div>	
</section>