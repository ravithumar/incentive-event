<?php
		// echo "Sdf"; exit;
$msg = '';
if (isset($_POST) && !empty($_POST)){
	if (isset($_POST['submit'])){
		// echo "<pre>";
		// print_r($_POST);

		$mailchimp_apikey = '137abdbc3e988b5346c873a9543b9459-us1';
		$mailchimp_audience_id = '9a4b4bc091';
	    
	    $json_data = array(
    		"email_address" => strtolower($_POST['email']),
    		"status" => "subscribed",
    		"merge_fields" => array(
    			"FNAME" => $_POST['firstname'],
    			"LNAME" => $_POST['lastname'],
    			"USERTYPE" => $_POST['user_type'],
    		),
    	);
    	
    	$curl = curl_init();
        
        $dataCenter = substr($mailchimp_apikey,strpos($mailchimp_apikey,'-')+1);
        $mailchimp_apiurl	= "https://" . $dataCenter . ".api.mailchimp.com/3.0/";
        
    	$url = $mailchimp_apiurl."lists/".$mailchimp_audience_id."/members";
    	$authorization = base64_encode("user:".$mailchimp_apikey);
    
    	curl_setopt_array($curl, array(
    		CURLOPT_URL => $url,
    		CURLOPT_RETURNTRANSFER => true,
    		CURLOPT_ENCODING => "",
    		CURLOPT_MAXREDIRS => 10,
    		CURLOPT_TIMEOUT => 30,
    		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    		CURLOPT_CUSTOMREQUEST => "POST",
    		CURLOPT_POSTFIELDS => json_encode($json_data),
    		CURLOPT_HTTPHEADER => array(
    			"authorization: Basic ".$authorization,
    			"cache-control: no-cache",
    			"content-type: application/json",
    		),
    	));
    
    	$response = curl_exec($curl);
    	$err = curl_error($curl);
    
    	curl_close($curl);

    	if ($err){
    	    $msg = '<div class="alert alert-danger" role="alert"><strong>Oh snap!</strong> Something went wrong.</div>';
    	}else{
    	    if(isset($response)){
    	        $response = json_decode($response);
    	        if(isset($response->status) && $response->status == "400"){
    	            $msg = '<div class="alert alert-danger" role="alert"><strong>Oh snap!</strong> '.$response->detail.'</div>';
    	        }else{
    	               $msg = '<div class="alert alert-success" role="alert"><strong>Thanks!</strong> You successfully subscribed to RAD.</div>';
    	        }
    	    }else{
    	        $msg = '<div class="alert alert-danger" role="alert"><strong>Oh snap!</strong> Something went wrong.</div>';
    	    }
    	}

	}
}
?>
<section class="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div id="rad-logo" class="rad-logo">
					<img id="okkkkk" src="<?php echo assets("web/images/rad-logo.png"); ?>">
				</div>
				<div id="banner-title">
					<h1 class="banner-title font-weight-bold text-white">Rural Area <span class="green vapour">Delivery</span></h1>
					<p class="banner-subtitle pb-5">Eat your way through the best that your local area has to offer without having to leave the comfort of your couch or lakeside lounge chair. We’re RAD - a food delivery service with a difference.</p>
				</div>
				<div id="banner-subscribe" class="banner-subscribe border-r10 p-4 p-sm-5 mt-sm-5 mx-sm-5">
					<form method="post" action="<?php echo base_url(); ?>website/">
						<div class="row px-lg-5">
							<div class="col-md-12">
								<?php
							    if(isset($msg) && $msg != ''){
							        echo $msg;
							    }
							    ?>
							</div>
							<div class="col-md-6">
								<div id="f-name" class="form-group my-4">
									<input type="text" name="firstname" value="" placeholder="First Name" class="form-control" required>
								</div>
							</div>
							<div class="col-md-6">
								<div id="l-name" class="form-group my-4">
									<input type="text" name="lastname" value="" placeholder="Last Name" class="form-control" required>
								</div>
							</div>
							<div class="col-md-12">
								<div  class="form-group my-4">
									<input id="email" type="text" name="email" value="" placeholder="Email Address" class="form-control" required>
								</div>
							</div>
							<div class="col-md-12 mt-4 ">
								<h6 class="text-left mb-3 ml-2">I am...</h6>
								<div class="d-flex flex-wrap justify-content-between">
									<div class="form-group mb-0 d-flex flex-wrap">
										<div class="form-check mr-5 d-flex align-items-center mb-3">
		  									<input class="form-check-input" type="radio" name="user_type" id="Consumer" checked value="Consumer">
		  									<label class="form-check-label" for="Consumer">Consumer</label>
										</div>
										<div class="form-check mr-5 d-flex align-items-center mb-3">
	  										<input class="form-check-input" type="radio" name="user_type" id="Delivery" value="Delivery Boy">
	  										<label class="form-check-label" for="Delivery">Delivery Driver</label>
										</div>
										<div class="form-check mr-5 d-flex align-items-center mb-3">
	  										<input class="form-check-input" type="radio" name="user_type" id="Restaurant" value="Restaurant">
	  										<label class="form-check-label" for="Restaurant">Restaurant</label>
										</div>
									</div>
									<div class="btn-green">
										<button type="submit" class="" name="submit">Subscribe</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="delivery-takeaway pb-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center delivery-takeaway-title">
				<h5 class="green d-inline-block title-img">Food Delivery Near Me</h5>
				<h2 class="font-weight-bold">A <span class="green"> Simpler </span> Way to <br> Order Your Food</h2>
			</div>
			<div class="col-md-4">
				<div class="delivery-takeaway-inner text-center pb-5">
					<div class="delivery-takeaway-inner-img">
						<img src="<?php echo assets("web/images/find-restaurant.png"); ?>" class="img-fluid">
					</div>
					<h4 class="font-weight-normal mt-5">Find Restaurant</h4>
					<p class="pb-5 font-14 px-3 px-md-4">Check out the restaurants we’re partnered with and find the food you’re craving.</p>
				</div>
			</div>
			<div class="col-md-4 order-food">
				<div class="delivery-takeaway-inner text-center pb-5">
					<div class="delivery-takeaway-inner-img">
						<img src="<?php echo assets("web/images/order-food.png"); ?>" class="img-fluid">
					</div>
					<h4 class="font-weight-normal mt-5">Place Your Order</h4>
					<p class="pb-5 font-14 px-3 px-md-4">Order food online from our easy-to-use site or app.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="delivery-takeaway-inner text-center pb-5">
					<div class="delivery-takeaway-inner-img">
						<img src="<?php echo assets("web/images/get-delivered.png"); ?>" class="img-fluid">
					</div>
					<h4 class="font-weight-normal mt-5">RAD Delivers</h4>
					<p class="pb-5 font-14 px-3 px-md-4">We’ll bring your order right to your door, no matter how far you are from town.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-grey py-5 we-provide">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-6">
				<h5 class="green d-inline-block title-img">Food That Delivers Near Me</h5>
				<h2 class="font-weight-bold">We Have the  <span class="green">Most Delicious </span> Food in Town</h2>
				<p class="">All the best restaurants in your local area are listed with RAD, including restaurants that wouldn’t normally deliver. Tight on time or just don’t feel like leaving the house? We’ve got you covered with an affordable and efficient way to get the food you’re craving.</p>
				<div class="btn-green pt-4">
					<a href="#" title="">Explore now</a>
				</div>
			</div>
			<div class="col-md-6 mt-5 mt-md-0 d-flex justify-content-center align-items-center">
				<img src="<?php echo assets("web/images/we-provide-img.png"); ?>" class="img-fluid">
			</div>
		</div>
	</div>
</section>

<section class="what-our-mission py-5 my-sm-5">
	<div class="container">
		<div class="row align-items-center flex-column-reverse flex-md-row">
			<div class="col-md-7 mt-5 mt-md-0">
				<img src="<?php echo assets("web/images/what-ur-mission-1.png"); ?>" class="img-fluid">
			</div>
			<div class="col-md-5 ">
				<h2 class="font-weight-bold title-img">Our <span class="green"> Mission </span></h2>
				<p class="font-16">We aim to bring you all the conveniences of the big city without sacrificing our awesome rural lifestyle. We’re taking farm-to-table to the next level!</p>
				<div class="bg-grey p-4 pl-sm-5 border-r10 mt-4 border-tr0">
					<h5 class="font-weight-normal save">Ultimate Convenience</h5>
					<p class="font-12 col-xl-10 pl-0 mb-0">RAD makes it easy to dig in even when dining options aren’t close by.</p>
				</div>
				<div class="bg-grey p-4 pl-sm-5 border-r10 mt-4 border-tr0">
					<h5 class="font-weight-normal help">Old Favourites, New Experiences</h5>
					<p class="font-12 col-xl-10 pl-0 mb-0">Order food online from the restaurants you love and the ones you’d love to try.</p>
				</div>
				<div class="bg-grey p-4 pl-sm-5 border-r10 mt-4 border-tr0">
					<h5 class="font-weight-normal eat">Eat Your Heart Out</h5>
					<p class="font-12 col-xl-10 pl-0 mb-0">Enjoy the best that local restaurants have to offer, anytime, anywhere.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="launching-soon">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3 class="font-weight-normal mb-0">Photo Credits</h3>
				<p class="font-50 text-white m-0 font-weight100">|</p>
				<div class="photo-credits col-md-10 col-lg-7 mx-auto bg-grey px-5 d-sm-flex flex-wrap justify-content-between">
					<h6 class="d-inline-block mb-0 mx-3 my-3">@TheWaringHouse</h6>
					<h6 class="d-inline-block mb-0 mx-3 my-3">@PrinceEddys</h6>
					<h6 class="d-inline-block mb-0 mx-3 my-3">@HuffEstates</h6>
				</div>
				<h1 class="font-weight-bold text-white mt-5"><span class="green">LAUNCHING </span>SOON</h1>
				<!-- <p class="mb-4">Copyright © 2021 Emoto All Rights Reserved.</p> -->
			</div>
		</div>
	</div>
</section>