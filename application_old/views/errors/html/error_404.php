<?php  
$base_url = isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) !== "off" ? "https" : "http";
$base_url .= "://". $_SERVER["HTTP_HOST"];
$base_url .= str_replace(basename($_SERVER["SCRIPT_NAME"]), "", $_SERVER["SCRIPT_NAME"]);
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>RAD - Page not found</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <link rel="shortcut icon" href="<?php echo $base_url; ?>assets/images/favicon.ico">

        <link href="<?php echo $base_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $base_url; ?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $base_url; ?>assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $base_url; ?>assets/css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <!-- Begin page -->
        <div class="wrapper-page">
            <div class="card">
                <div class="card-block">

                    <div class="ex-page-content text-center">
                        <h1 class="text-dark">404!</h1>
                        <h4 class="">Sorry, page not found</h4><br><br>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                &copy; 2020 RAD. <span class="d-none d-sm-inline-block"> Crafted with <i class="mdi mdi-heart text-danger"></i> by Excellent WebWorld</span>
            </div>

        </div> 

    </body>

</html>