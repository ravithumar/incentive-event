<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
          <tbody>
            <tr>
              <td align="center" style="background-color: #f1f2f2;padding-left: 8px;padding-right: 8px;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 632px;margin: 0 auto;">
                  <tbody>
                    <tr>
                      <td align="center" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 19px;line-height: 28px;background-color: #ffffff;color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 64px;padding-bottom: 64px;">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                            <tr>
                              <td height="40" width="32" style="border-bottom: 2px solid #894C48;">&nbsp; </td>
                              <td rowspan="2" align="center" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: #424651;padding-left: 16px;padding-right: 16px;padding-top: 16px;padding-bottom: 16px;">
                                <img src="https://justborrowed.co/dashboard/assets/images/email_template/tick.png" width="40" height="40" alt="" style="max-width: 48px;-ms-interpolation-mode: bicubic;vertical-align: middle;border: 0;line-height: 100%;height: auto;outline: none;text-decoration: none;">
                              </td>
                              <td height="40" width="32" style="border-bottom: 2px solid #894C48;">&nbsp; </td>
                            </tr>
                            <tr>
                              <td height="40">&nbsp; </td>
                              <td height="40">&nbsp; </td>
                            </tr>
                            <tr>
                              <td style="font-size: 8px; line-height: 8px; height: 8px;">&nbsp; </td>
                              <td style="font-size: 8px; line-height: 8px; height: 8px;">&nbsp; </td>
                            </tr>
                          </tbody>
                        </table>
                        <h2 style="font-family: Helvetica, Arial, sans-serif;font-weight: bold;margin-top: 0px;margin-bottom: 4px;color: #242b3d;font-size: 30px;line-height: 39px;">Forgot Your Password ?</h2>
                        <p style="margin-top: 0px;margin-bottom: 42px;color: #82899a;">We heard you need a password reset. Click the button below and you'll be redirected to a secure site from which you can set a new password.</p>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 632px;margin: 0 auto;">
                          <tbody>
                            <tr>
                              <td align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 8px;padding-bottom: 8px;">
                                <table align="center" cellspacing="0" cellpadding="0" border="0" role="presentation">
                                  <tbody>
                                    <tr>
                                      <td width="300" align="center" style="font-family: Helvetica, Arial, sans-serif;font-weight: bold;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;mso-padding-alt: 12px 24px;border-radius: 4px; background-color:  #894C48; ">
                                        <a href="<?php echo  $activation_link;?>" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;">Reset Password</a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
      </table>

         <table data-module="footer-white-2cols" data-thumb="footer-white-2cols.png" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
          <tbody>
            <tr>
              <td  align="center" data-bgcolor="Bg Light" style="padding-left: 8px;padding-right: 8px;padding-bottom: 32px;">
                <table  width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 632px;margin: 0 auto;">
                  <tbody>
                    <tr>
                      <td align="center" data-bgcolor="Bg White" data-border-top-color="Border Light" style="font-size: 0;vertical-align: top;    background-color: #6BBD45;border-top: 1px solid #d3dce0;border-radius: 0px 0px 4px 4px;padding-bottom: 32px;">
                        
                        <div style="display: inline-block;vertical-align: top;width: 100%;max-width: 400px;">
                          <div style="font-size: 32px; line-height: 32px; height: 32px;">&nbsp; </div>
                          <div data-color="Light" data-size="Text XS" data-min="10" data-max="18" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;color: #ffffff;padding-left: 8px;padding-right: 8px;">
                            <p style="margin-top: 0px;margin-bottom: 8px;">Copyright &copy; RADSERVICES 2021</p>
                            <!-- <p style="margin-top: 0px;margin-bottom: 8px;">2603 Woodridge Lane, Memphis, TN 38104, USA</p> -->
                            <p style="margin-top: 0px;margin-bottom: 0px;">
                              <a href="<?php echo base_url(); ?>terms-of-services.pdf" data-color="Light" style="text-decoration: underline;outline: none;color: #ffffff;">Terms Of Services</a> <span >&nbsp; | &nbsp;</span><br  style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                              <a href="<?php echo base_url(); ?>privacy-policy.pdf" data-color="Light" style="text-decoration: underline;outline: none;color: #ffffff;">Privacy policy</a> <span >&nbsp; | &nbsp;</span><br  style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                              <a href="mailto:info@radservices.ca" data-color="Light" style="text-decoration: underline;outline: none;color: #ffffff;">Contact us</a>
                            </p>
                          </div>
                        </div>
                        
                      </td>
                    </tr>
                  </tbody>
                </table>
               
                <div style="font-size: 64px; line-height: 64px; height: 64px;">&nbsp; </div>
              </td>
            </tr>
          </tbody>
        </table>

    </body>
</html>
