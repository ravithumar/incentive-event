<?php
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo config('site_meta'); ?>">
        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png'); ?>">
        <title><?php echo config('site_meta').' | Admin Panel'; ?></title>
        
        <link rel="stylesheet" href="<?php echo base_url('plugins/morris/morris.css'); ?>">

        <!-- DataTables -->
        <link href="<?php echo base_url('plugins/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('plugins/datatables/buttons.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="<?php echo base_url('plugins/datatables/responsive.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- autoclose date picker -->
        <link href="<?php echo base_url('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo assets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/metismenu.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/icons.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/style.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('css/custom.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('plugins/notiflix/notiflix-2.1.2.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" type="text/css">

        <link href="<?php echo base_url('plugins/magnific-popup/magnific-popup.css') ;?>" rel="stylesheet" type="text/css">
        
        <!-- js -->
        <!-- jQuery  -->
        <script src="<?php echo assets('js/jquery.min.js'); ?>"></script>

        <!-- Js form validations -->
        <script src="<?php echo assets('js/validation/jquery.validate.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.js'); ?>"></script>
        <script src="<?php echo assets('js/validation/additional-methods.min.js'); ?>"></script>

        <script src="<?php echo assets('js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo assets('js/metisMenu.min.js'); ?>"></script>
        <script src="<?php echo assets('js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo assets('js/waves.min.js'); ?>"></script>

        <script src="<?php echo base_url('plugins/jquery-sparkline/jquery.sparkline.min.js'); ?>"></script>
        <script src="<?php echo base_url('plugins/select2/js/select2.min.js'); ?>"></script>

        <script src="<?php echo base_url('plugins/magnific-popup/jquery.magnific-popup.min.js');?>" ></script>
        <!--Morris Chart-->
        <script src="<?php echo base_url('plugins/morris/morris.min.js'); ?>"></script>
        <script src="<?php echo base_url('plugins/raphael/raphael-min.js'); ?>"></script>
        <script src="<?php echo assets('js/custom/admin/custom.js'); ?>"></script>

        <!-- notiflix -->
        <script src="<?php echo base_url('plugins/notiflix/notiflix-2.1.2.js'); ?>"></script>

        <!-- parsleyjs -->
        <script src="<?php echo base_url('plugins/parsleyjs/parsley.min.js'); ?>"></script>

        <!-- Auto Close datepicker -->
        <script src="<?php echo base_url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
        <script src="<?php echo base_url('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js'); ?>"></script>

        <script type="text/javascript">
            var BASE_URL = '<?php echo base_url(); ?>';
            var prof_default = '<?php echo assets("images")."/avtar.png"; ?>';
        </script>
    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left lu-bg text-center">
                    <a href="<?php echo base_url('admin/dashboard'); ?>" class="logo">
                        <span>
                            <img src="<?php echo assets('images/logo-yellow.png'); ?>" alt="" style="height: -webkit-fill-available;">
                            <!-- <span class="text-white" style="font-size: 22px;"><?php //echo config('site_title'); ?></span> -->
                        </span>
                        <i>
                            <img src="<?php echo assets('images/logo-yellow.png'); ?>" alt="" height="50">
                        </i>
                    </a>
                </div>
                <?php 
                $id = $this->session->userdata['admin']['user_id'];
                $profile =get_session_data($id,'users');
                 if(!empty($profile['profile_picture']) && isset($profile['profile_picture'])){
                    $prof_default = base_url().$this->config->item("user_profile_path"). $profile['profile_picture'];
                }else{
                    $prof_default = assets("images")."/default.png";
                } ?>
                <nav class="navbar-custom">

                    <ul class="navbar-right d-flex list-inline float-right mb-0">
                        
                        <li class="dropdown notification-list">
                            <div class="dropdown notification-list nav-pro-img">
                                <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false" id="header-user">
                                    <!-- <img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt="user" class="border rounded-circle"> -->
                                    <img src="<?php echo $prof_default ;?>" alt="user" class="border rounded-circle" style="object-fit: cover;">
                                    <span class="ml-1"><?php echo ($profile['first_name']);?>
                                    <i class="mdi mdi-menu-down mdi-16"></i></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <!-- <a class="dropdown-item" href="<?php echo base_url('admin/setting'); ?>"><i class="mdi mdi-settings m-r-5"></i> Settings</a> -->
                                    <a class="dropdown-item" href="<?php echo base_url('admin/change-password'); ?>"><i class="mdi mdi-key-variant m-r-5"></i> Change Password</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item text-danger" href="<?php echo base_url('admin/logout'); ?>"><i class="mdi mdi-power text-danger"></i> Logout</a>
                                </div>                                                                    
                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-effect lu-color">
                                <i class="mdi mdi-menu"></i>
                            </button>
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->
            <?php $this->load->view('admin/layout/sidebar'); ?>

            <div class="content-page">

                