<?php

if($this->session->flashdata('error')!="") {

?>

<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">

        <span aria-hidden="true">&times;</span>

    </button>

    <?php echo $this->session->flashdata('error');
    unset($_SESSION['error']); ?>

</div>

<?php

}

?>

<?php

if($this->session->flashdata('success')!="") {

?>

<div class="alert alert-success alert-dismissible fade show" role="alert">

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">

        <span aria-hidden="true">&times;</span>

    </button>

    <?php echo $this->session->flashdata('success');
    unset($_SESSION['success']);

    ?>

</div>



<?php

}

?>

<?php

if($this->session->flashdata('info')!="") {

?>

<div class="alert alert-info alert-dismissible fade show" role="alert">

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">

        <span aria-hidden="true">&times;</span>

    </button>

    <?php echo $this->session->flashdata('info'); 
        unset($_SESSION['info']); 
    ?>

</div>

<?php

}

?>

<?php

if($this->session->flashdata('warning')!="") {

?>

<div class="alert alert-warning alert-dismissible fade show" role="alert">

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">

        <span aria-hidden="true">&times;</span>

    </button>

    <?php echo $this->session->flashdata('warning'); 
        unset($_SESSION['warning']);
    ?>

</div>

<?php

}

if($this->session->flashdata('message')!="") {

?>

<div class="alert alert-success alert-dismissible fade show" role="alert">

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">

        <span aria-hidden="true">&times;</span>

    </button>

    <?php echo $this->session->flashdata('message');
    unset($_SESSION['message']);
    ?>

</div>

<?php

}

?>