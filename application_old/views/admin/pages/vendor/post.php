 <!-- Start content -->
 <style type="text/css" media="screen">
input[switch] + label {
    width: 78px !important;
}
input[switch]:checked + label:after {
        left: 55px !important;
}

input[switch]:checked + label {
    background-color: #2a3142 !important;
    }
.working-time-error{
    font-size: 16px;
    list-style: none;
    color: #ec536c;
    margin-top: 5px;
}
.custom-control {
    display: inline-block;
    padding-right: 2rem;
}
.custom-control-input{
    position: absolute !important;
} 
.validation-error-label{
    margin-top: 0;
 }

.custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
    background-color: #2a3142;
}

 </style>
<?php
$post_link = base_url('admin/vendor/add');
$back_url = base_url('admin/vendors');
$prof_url =  BASE_URL().'assets/images/default.png';

// get from to time
$from_time = DateTime::createFromFormat('H:i',$this->config->item("start_time"));
$to_time = DateTime::createFromFormat('H:i',$this->config->item("end_time"));

$from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("start_time_defualt"));
$to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("end_time_defualt"));

$from_time_selected = $from_time_selected->format('h:i A');
$to_time_selected = $to_time_selected->format('h:i A');

for($j = $from_time; $j <= $to_time;){
    $available_time[] = $j->format('h:i A');
    $j->add(new DateInterval('PT30M'));
}
 //_pre($vendor['category']);
 ?>
 
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-box">
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                    echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-12">
                <?php
                $this->load->view('admin/includes/message');
                ?>

            </div>
        </div>

        <form class="form-validate" method="post" action="<?php echo $post_link; ?>" enctype="multipart/form-data" onsubmit="return check_lat_lon()">
            <div class="row">

                <div class="col-lg-12">

                    <div class="card m-b-20">
                        <div class="card-body">
                        <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Basic Information</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="name">Store Title</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Enter store title" value="<?php echo $field_value; ?>"required>
                                            <div class="validation-error-label" >
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Email</label>
                                        <div>
                                            <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('email');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="email" name="email" value="<?php echo $field_value; ?>" class="form-control" id="email" placeholder="Enter email" required>
                                        </div>
                                        <div class="validation-error-label">
                                            <?php echo form_error('email'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="first_name">Contact Person First Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('first_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter first name" data-parsley-pattern="^[a-zA-Z ]*$" value="<?php echo $field_value; ?>" data-parsley-pattern-message="Only alphanumeric characters & spaces allowed." required>
                                            <div class="validation-error-label" >
                                                <?php echo form_error('first_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="last_name">Contact Person Last Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('last_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last name" data-parsley-pattern="^[a-zA-Z ]*$" data-parsley-pattern-message="Only alphanumeric characters & spaces allowed." value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('last_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Contact Information</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="phone">Phone No</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('phone');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone no" value="<?php echo $field_value; ?>" data-parsley-minlength="14"  data-parsley-minlength-message="This value is invalid." required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('phone'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="" for="phone_2">Alternative Phone No</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('phone_2');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="phone_2" class="form-control" id="phone_2" placeholder="Enter alternative phone no" value="<?php echo $field_value; ?>" data-parsley-minlength="14"  data-parsley-minlength-message="This value is invalid." >
                                            <div class="validation-error-label">
                                                <?php echo form_error('phone_2'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                            </div>

                            <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Location</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="autocomplete" class="required">Street</label>
                                        <div>
                                            <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('street');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                                ?>
                                            <input id="autocomplete" placeholder="Start typing and find your place in google map" onFocus="geolocate()" class="form-control" type="text" value="<?php echo $field_value; ?>" name="address" required>
                                            <input type="hidden" id="administrative_area_level_2" name="city">
                                            <input type="hidden" id="administrative_area_level_1" name="state">

                                            <input type="hidden" name="latitude" id="latitude" value="">
                                            <input type="hidden" name="longitude" id="longitude" value="">

                                            <div class="validation-error-label">
                                                <?php echo form_error('address'); ?>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('latitude'); ?>
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('longitude'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="required" for="street">Street</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('street');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="street" class="form-control" id="street" placeholder="Enter street" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('street'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="city">County</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('city');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="city" readonly class="form-control city" id="city" placeholder="Enter country" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('city'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="state">Province</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('state');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="state" readonly class="form-control state" id="state" placeholder="Enter province" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('state'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="country">Country</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('country');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="country" readonly class="form-control country" id="country" placeholder="Enter country" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('country'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="zipcode">Zipcode</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('zipcode');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="zipcode" readonly class="form-control zipcode" id="zipcode" placeholder="Enter zipcode" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('zipcode'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group ">
                                        <label  class="required">HD Logo</label>
                                          <input type="file" required data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL1(this);" id="profile_picture" name="profile_picture" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                            <img class="border rounded p-0 >"  src="" onerror="this.src='<?php echo $prof_url; ?>'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" id="blah1"/>
                                        
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group ">
                                        <label  class="required">HD Backgroud Image</label>
                                          <input type="file" required  data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png"  accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL2(this);" id="bg_img" name="bg_img" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        
                                        <img class="border rounded p-0 >"  src="" onerror="this.src='<?php echo $prof_url; ?>'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" id="blah2"/>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Working Hours</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                <div class="col-lg-12">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table class="table table-hover mb-0">
                                            <tbody>
                                                <?php
                                                /* foreach ($this->config->item("days") as $key => $value) { 
                                                    $open = '';
                                                    $full_day = '';
                                                    if(!empty($vendor_availability)){
                                                        foreach ($vendor_availability as $key_data => $value_data) {
                                                            if($value == $value_data['day']){
                                                                if($value_data['is_closed'] == '0'){
                                                                    $open = 'checked';
                                                                }
                                                                 if($value_data['full_day'] == '1'){
                                                                    $full_day = 'checked';
                                                                }
                                                                break;
                                                            }
                                                        }
                                                    } */
                                                    ?>
                                                  <tr data-id='<?php //echo $key; ?>' data-day='<?php //echo $value; ?>'>
                                                    <td><?php //echo $value; ?></td>
                                                    <?php 
                                                     /* if($open == 'checked'){ 
                                                        if($full_day == 'checked'){ */?>

                                                        <td class="from_td">
                                                            <div class="text-center"><span class="badge badge-success">24 Hours Open</span></div>
                                                        </td>
                                                        <td class="to_td">
                                                        </td>
                                                    
                                                    <?php //}else{ ?>
                                                    <td class="from_td">
                                                        <select class="form-control select2 from_time" name="<?php //echo $value; ?>[]">
                                                            <option disabled >Select Time</option>
                                                        </select>
                                                    </td>
                                                    <td class="to_td">
                                                        <select class="form-control select2 to_time" name="<?php //echo $value; ?>[]">
                                                            <option disabled >Select Time</option>
                                                        </select>
                                                    </td>
                                                <?php/*  }
                                                     }else{ */ ?>
                                                        <td class="from_td">
                                                            <div class="text-center"><span class="badge badge-danger">CLOSED</span></div>
                                                        </td>
                                                        <td class="to_td">
                                                        </td>
                                                    <?php// }
                                                    //if($open == 'checked'){
                                                    ?>

                                                    <td class="fullday_td text-center">
                                                        <input type="checkbox" id="fullday_<?php //echo $key; ?>" switch="none" class="fullday" value="fullday" name="<?php// echo $value; ?>[]" <?php //echo $full_day; ?> >
                                                        <label class="mb-0 mt-1" for="fullday_<?php //echo $key; ?>" data-on-label="Fullday" data-off-label="Custom"></label>
                                                    </td>
                                                    <?php// } else{ ?>
                                                        <td class="fullday_td text-center" >
                                                        </td>
                                                    <?php// } ?>
                                                    <td class="text-center">
                                                        <input type="checkbox" id="open_<?php //echo $key; ?>" switch="none"  class="open" value='open' name="<?php //echo $value; ?>[]" <?php// echo $open; ?>>
                                                        <label class="mb-0 mt-1" for="open_<?php //echo $key; ?>" data-on-label="Open" data-off-label="Closed"></label>
                                                    </td>
                                                  </tr>  
                                                <?php// }
                                                ?> 
                                                <tr class="working-time-error">
                                                    <td colspan="5"></td>
                                                </tr>   
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="validation-error-label">
                                        <?php// echo form_error('availibality'); ?>
                                    </div>
                                    <div class="validation-availibality">
                                    </div>
                                </div>
                            </div> -->

                            <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Store Information</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                <!-- <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="required control-label">Category</label>
                                        <select id="category_id" name="category_id[]" class="form-control select2" required multiple data-parsley-errors-container="#category_id_error" data-placeholder="Select Category">
                                            <option value="" disabled > </option>
                                           <?php
                                            /* $field_value = NULL;
                                            $temp_value = set_value('category_id');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $categorys = array_column($vendor['category'], 'category_id');
                                                $field_value = $categorys;
                                                //_pre($vendor);

                                            }
                                            if(isset($category) && !empty($category)){
                                                foreach ($category as $key => $value) 
                                                {
                                                    if(in_array($value['id'], $field_value))
                                                    {
                                                        $selected= 'selected';
                                                    }
                                                    else
                                                    {
                                                         $selected= '';
                                                    } */
                                                ?>
                                                <option  value="<?php //echo $value['id'];?>" <?php //echo $selected; ?> > <?php //echo ($value['name']);?> </option>
                                                <?php
                                                /* }
                                                } */
                                                ?>
                                             
                                             <div class="validation-error-label">
                                                <?php //echo form_error('category_id'); ?>
                                            </div>
                                        </select>
                                        <div id="category_id_error"></div>
                                    </div> 
                                </div> -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="percentage">Vendor Percentage</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('percentage');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="percentage" class="form-control demo3" id="percentage" data-parsley-errors-container="#error_msg" placeholder="Enter qwnched percentage" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('percentage'); ?>
                                            </div>
                                            <div id="error_msg">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="delivery_boy_percentage">Delivery Boy Percentage</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('delivery_boy_percentage');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="delivery_boy_percentage" class="form-control demo3" id="delivery_boy_percentage" data-parsley-errors-container="#delivery_boy_percentage_error_msg" placeholder="Enter delivery boy percentage" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('delivery_boy_percentage'); ?>
                                            </div>
                                            <div id="delivery_boy_percentage_error_msg">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="delivery_charge">Delivery Charge</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('delivery_charge');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="delivery_charge" class="form-control demo3" id="delivery_charge" data-parsley-errors-container="#error"  placeholder="Enter delivery charge" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('delivery_charge'); ?>
                                            </div>
                                            <div id="error">
                                                
                                            </div>

                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="minimum_order_amount">Minimum Order Amount</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('minimum_order_amount');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="minimum_order_amount" class="form-control demo2" id="minimum_order_amount" data-parsley-errors-container="#error_m" placeholder="Enter minimum order amount" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('minimum_order_amount'); ?>
                                            </div>
                                            <div id="error_m"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Available Services</label>
                                        <div class="d-block mt-2">
                                            <?php
                                                $field_value = '';
                                                $temp_value = set_value('service_available');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                   if (in_array("1", $temp_value)){
                                                        $field_value = 'checked';
                                                   }
                                                }
                                                ?>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" required class="custom-control-input" data-parsley-errors-container="#error_service" id="customControlInline3" value="1" name="service_available[]" <?php echo $field_value; ?> >
                                                <label class="custom-control-label" for="customControlInline3">Delivery</label>
                                            </div>
                                            <?php
                                                $field_value = '';
                                                $temp_value = set_value('service_available');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                   if (in_array("2", $temp_value)){
                                                        $field_value = 'checked';
                                                   }
                                                }
                                                ?>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customControlInline4" value="2" name="service_available[]" <?php echo $field_value; ?> >
                                                <label class="custom-control-label" for="customControlInline4">Takeaway</label>
                                            </div>
                                        </div>
                                        <div id="error_service">
                                                
                                        </div>
                                        <div class="validation-error-label">
                                            <?php echo form_error('service_available[]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <!--  <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Payment Mode</label>
                                        <div class="d-block mt-2">
                                            <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('payment_type');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                                ?>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" required class="custom-control-input" data-parsley-errors-container="#error_payment" id="customControlInlinecod" value="1" name="payment_type[]" <?php echo $field_value; ?> >
                                                <label class="custom-control-label" for="customControlInlinecod">COD</label>
                                            </div>
                                            <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('payment_type');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                                ?>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customControlInlinedc" value="2" name="payment_type[]" <?php echo $field_value; ?> >
                                                <label class="custom-control-label" for="customControlInlinedc">Debit/Credit Card</label>
                                            </div>
                                            <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('payment_type');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                                ?>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customControlInlinepg" value="3" name="payment_type[]" <?php echo $field_value; ?> >
                                                <label class="custom-control-label" for="customControlInlinepg">Payment Gateway</label>
                                            </div>
                                        </div>
                                        <div id="error_payment">
                                                
                                        </div>
                                        <div class="validation-error-label">
                                            <?php echo form_error('payment_type[]'); ?>
                                        </div>
                                    </div>
                                </div> -->

                            </div>
                            <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Bank Account Information</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="dob">Date of Birth</label>
                                        <div>
                                            <div class="input-group">
                                                <?php
                                                    $field_value = NULL;
                                                    $temp_value = set_value('dob');
                                                    if (isset($temp_value) && !empty($temp_value)) {
                                                        $field_value = $temp_value;
                                                    } 
                                                ?>
                                                <input required type="text" class="form-control datepicker-autoclose" placeholder="Ex. 10 September , 2020" name="dob" id="dob" data-parsley-errors-container="#error_from" autocomplete="off" value="<?php echo $field_value; ?>" readonly> 
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>  
                                            </div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('dob'); ?>
                                            </div>
                                            <div id="error_from"></div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="bank_city">Country</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('bank_city');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="bank_city" class="form-control" id="bank_city" placeholder="Enter city" value="Canada" required readonly>
                                            <div class="validation-error-label">
                                                <?php echo form_error('bank_city'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="line1" class="required">Address 1</label>
                                        <div>
                                            <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('line1');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                            ?>
                                            <input id="line1" placeholder="Address 1" class="form-control" type="text" value="<?php echo $field_value; ?>" name="line1" required>

                                            <div class="validation-error-label">
                                                <?php echo form_error('line1'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="line2" class="required">Address 2</label>
                                        <div>
                                            <?php
                                                $field_value = NULL;
                                                $temp_value = set_value('line2');
                                                if (isset($temp_value) && !empty($temp_value)) {
                                                    $field_value = $temp_value;
                                                }
                                            ?>
                                            <input id="line2" placeholder="Address 1" class="form-control" type="text" value="<?php echo $field_value; ?>" name="line2" required>

                                            <div class="validation-error-label">
                                                <?php echo form_error('line2'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="bank_state">Province</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('bank_state');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                        ?>
                                        <select name="bank_state" class="form-control state select2" data-parsley-errors-container="#state_id_error" required d id="bank_state">
                                            <option disabled="" selected="">Select province</option>
                                            <?php foreach($state as $kry => $value){ ?>
                                                <option value="<?php echo $value['abbreviation']; ?>"><?php echo $value['abbreviation'].' ('.$value['name'].')'; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div id="state_id_error"></div>
                                            <div class="validation-error-label">
                                                <?php echo form_error('bank_state'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="postal_code">Postal Code</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('postal_code');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="postal_code" class="form-control" id="postal_code" placeholder="Enter postal code" value="<?php echo $field_value; ?>" required data-parsley-minlength="7">
                                            <div class="validation-error-label">
                                                <?php echo form_error('postal_code'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="routing_number">Routing Number</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('routing_number');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="routing_number" class="form-control state" id="routing_number" placeholder="Enter routing number" value="<?php echo $field_value; ?>" required data-parsley-minlength="9">
                                            <div class="validation-error-label">
                                                <?php echo form_error('routing_number'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="account_number">Account Number</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('account_number');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                        ?>
                                            <input type="text" name="account_number" class="form-control state" id="account_number" placeholder="Enter account number" value="<?php echo $field_value; ?>" required >
                                            <div class="validation-error-label">
                                                <?php echo form_error('account_number'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-b-0">
                                        <div>
                                            <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <a href="<?php echo $back_url; ?>" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> 

            </div> <!-- end row -->
        </form>

    </div>
</div>

<script src="<?php echo base_url().'assets/js/mask/jquery.inputmask.bundle.js'; ?>"></script>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script src="<?php echo assets('pages/lightbox.js');?>"></script>

<?php
$google_key = $this->config->item("google_key");
?>
<script type="text/javascript">
    // var available_time = jQuery.parseJSON('<?php //echo json_encode($available_time); ?>');
    // var vendor_availability = jQuery.parseJSON('<?php //echo json_encode($vendor_availability); ?>');

    // var from_time_selected = '<?php //echo $from_time_selected; ?>';
    // var to_time_selected = '<?php// echo $to_time_selected; ?>';

    $(document).on('change','#autocomplete',function(){
        $('#latitude').val('');
        $('#longitude').val('');
        $('#city').val('');
        $('#state').val('');
        $('#country').val('');
        $('#zip_code').val('');
    });

     
    var placeSearch, autocomplete;

    var componentForm = {
        administrative_area_level_2: 'long_name',
        administrative_area_level_1: 'long_name'
      };



    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
           (document.getElementById('autocomplete')),
            // {types: ['geocode'] , componentRestrictions: {country: "us"} });
            {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        if (typeof place.address_components != "undefined" || place.address_components != null){

            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());

            console.log(place.address_components);

            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++){
                    if (place.address_components[i].types[j] == "postal_code") {
                        $('.zipcode').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[j] == "country") {
                        $('.country').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[j] == "administrative_area_level_1") {
                        $('.state').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[j] == "administrative_area_level_2") {
                        $('.city').val(place.address_components[i].long_name);
                    }
                }
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
    }

    function geolocate() {
        $(".overlay").css("display", "block");
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            // console.log(geolocation);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
        $(".overlay").css("display", "none");
    }

    function check_lat_lon() {
        if($('#latitude').val() == '' || $('#longitude').val() == ''){
            Notiflix.Notify.Warning('Please select address from google suggestion!');
            return false;
        }
    }

    $("#phone , #phone_2").inputmask("(999) 999-9999",{"placeholder": ""});
    $("#postal_code").inputmask("A9A 9A9",{"placeholder": ""});
    $("#routing_number").inputmask("99999-999",{"placeholder": ""});
    // $("#account_number").inputmask("9999999",{"placeholder": ""});

   function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1').attr('src', e.target.result);
            $('.blah1').attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
   }

   function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2').attr('src', e.target.result);
            $('.blah2').attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
   }

   $(".select2").select2();
    $(".demo2").TouchSpin({
        initval: 0.00,
        forcestepdivisibility: 'none',
        max: 1000000000,
        decimals: 2,
        step: 0.1,
        prefix: '$',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $(".demo3").TouchSpin({
        initval: 0.00,
        forcestepdivisibility: 'none',
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $("#dob").datepicker({
        todayBtn:  1,
        autoclose: true,
        format: 'dd MM, yyyy',
        endDate: '-0d',   
    });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_key; ?>&libraries=places&callback=initAutocomplete" async defer></script>
<script src="<?php //echo assets('js/custom/admin/vendor_post_put.js'); ?>"></script>