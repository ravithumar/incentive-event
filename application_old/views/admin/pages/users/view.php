<style type="text/css" media="screen">
    #delivery_address {
        display: block;
        height: 450px;
        overflow-y: auto;
    }
    .track .dot {
    height: 25px;
    width: 25px;
    background-color: #8bc34a;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
}
.track .not-first .dot:before {
    content: '';
    width: 0;
    height: 174%;
    position: absolute;
    border: 1px solid #8bc34a;
    bottom: 25px;
}
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <a href="<?php echo base_url('admin/users'); ?>" class="ml-2 btn btn-dark waves-effect waves-light float-right">
                        <i class="far fa-arrow-alt-circle-left m-r-10 "></i> Back 
                    </a>
                    <a href="<?php echo base_url('admin/user/edit/').$customer['id']; ?>" class="btn btn-dark waves-effect waves-light float-right">
                        <i class="ion-edit mr-1"></i> Edit 
                    </a>
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                    echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div> 
        </div>

        <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row d-flex">
            <div class="col-lg-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="card-title font-16 mt-0">
                            <?php echo ($customer['first_name']).' '.($customer['last_name']);?>
                        </h4>
                        <h6 class="card-subtitle font-12 text-muted">
                            <p class="mb-1"><i class="fas fa-phone mr-2"></i><?php echo $customer['phone']; ?></p>
                            <p class="mb-1"><a href="mailto:<?php echo $customer['email']; ?>"><i class="fas fa-envelope mr-2"></i><?php  if ($customer['email'] != ''){ echo $customer['email']; } else{ echo "-";}?></a></p>
                        </h6>
                        <!-- <div><i class="mdi mdi-link-variant font-16 mr-1"></i>Referral Code : <b><?php //echo $customer['referral_code']; ?></b></div> -->
                    </div>
                    <img class="img-fluid border" src="<?php echo BASE_URL().$customer['profile_picture']; ?>" alt="Profile Picture" style="height: 378px;">
                </div>
            </div>

            <div class="col-lg-5">

                <div class="card m-b-30">
                    <h4 class="card-header font-16 mt-0">Events</h4>
                    <div id="delivery_address">
                        <table class="table table-hover">
                            <tbody>
                                <?php if (isset($customer['delivery_address']) && !empty($customer['delivery_address'])) { 
                                foreach ($customer['delivery_address'] as $key => $value) { 
                                    $delivery_address_str = $value['address'].' '.$value['location'];
                                ?>
                                <tr>
                                    <td><b><p class="mb-0"><i class="fas fa-map-pin mr-2"></i><?php echo $delivery_address_str; if($value['default'] == 1){ echo "<span class='badge badge-success ml-2'>Default</span>"; } 
                                        if($value['type'] == 1){
                                            echo "<span class='badge badge-info ml-2'>Home</span>";
                                        }elseif($value['type'] == 2){
                                            echo "<span class='badge badge-info ml-2'>Work</span>";
                                        }elseif ($value['type'] == 3) {
                                            echo "<span class='badge badge-info ml-2'>Hotel</span>";
                                        }elseif ($value['type'] == 4) {
                                            echo "<span class='badge badge-info ml-2'>Other</span>";
                                        }else{
                                            echo "";
                                        }
                                        ?> 
                                        </p></b>
                                </td>
                                </tr>
                                <?php } }else{?>
                                    <tr>
                                        <td>No any events found!</td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">

                <div class="card m-b-30">
                    <h4 class="card-header font-16 mt-0">Event Tickets</h4>
                    <div id="delivery_address">
                        <table class="table table-hover" >
                        <tbody>
                            <?php if (isset($customer['wallet_history']) && !empty($customer['wallet_history'])){ 
                            foreach ($customer['wallet_history'] as $key => $value) { 
                                
                            ?>
                            <tr>
                                <?php if($value['type'] == 'plus') {?>
                                <td>
                                    <b><p class="mb-0"><i class="mdi mdi-plus mr-2 text-success"></i><?php echo '$'.$value['amount'].' '.$value['message']; ?></p></b>
                                </td>
                                <?php }else{?>
                                <td>
                                    <b><p class="mb-0"><i class="mdi mdi-minus mr-2 text-danger"></i><?php echo '$'.$value['amount'].' '.$value['message']; ?></p></b>
                                </td>
                                <?php }?>
                            </tr>
                            <?php } }else{?>
                                <tr>
                                    <td>No any event tickets found!</td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                    </div>
                    
                </div>

            </div>
           
        </div> <!-- end row -->

        <div class="row">
            <div class="col-md-3">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-food float-right" ></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Total Tickets</h6> 
                            <h4 class="mb-4"><?php echo (isset($total_tickets)) ? $total_tickets : '0';  ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stat bg-dark">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-square-inc-cash float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Total Win</h6> 
                            <h4 class="mb-4"><?php echo (isset($total_win)) ? $total_win : '0';  ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stat bg-primary ">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-cash-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Events</h6>
                            <h4 class="mb-4"><?php echo (isset($total_events)) ? $total_events : '0'; ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stat bg-dark ">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-cash-multiple float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Used Tickets</h6>
                            <h4 class="mb-4"><?php if(isset($used_ticket)){ echo $used_ticket; }else{ echo '0';} ?></h4>
                            <span class="ml-2">Total added in system</span>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

       <!--  <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <h4 class="card-header font-16 mt-0">Orders</h4>
                    <div class="card-body">
                         <?php //echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> 
        </div>  -->

        <div class="modal fade bs-example-modal-lg" id="trackmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content track">
              <div class="modal-header bg-primary">
                <h6 class="modal-title text-center m-0 font-weight-bold text-white">Track Order #<span id="order-id"></span></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               <h4 class="text-center">Tracking data fetching..</h4>
              </div>
              <div class="text-center text-white bg-primary py-2">
                <h6 class="text-uppercase font-weight-bold"><span id="order-final-status"></span></h6>
                <div class="pb-2"><span id="order-final-text"></span></div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('/assets/js/custom/admin/track_order.js');?>" type="text/javascript"></script>     