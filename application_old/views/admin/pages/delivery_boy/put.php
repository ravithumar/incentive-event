 <!-- Start content -->
<?php
$put_link = base_url('admin/delivery-boy/edit/').$user->id;
$back_url = base_url('admin/delivery-boy');
$prof_url = assets('images/default_user.png');
// _pre($user_style);
?>
 
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <div class="page-title-box">
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <?php
                    $this->load->view('admin/includes/message');
                ?>
            </div>
            <div class="col-lg-1">
            </div>
        </div>

        <form class="form-validate" method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

            <div class="row">

                <div class="col-lg-1">
                </div>

                <div class="col-lg-10">
                    <div class="card m-b-20">
                        <div class="card-body row">
                            <div class="col-12 text-center">

                                <img src="<?php echo base_url($user->profile_picture); ?>" class="img-circle profile-avatar pointer" alt="Shop Image" id="blah" onerror="this.src='<?php echo $prof_url; ?>'">
                                <input type='file' name="profile_picture" id="imgInp" accept="image/*" style="visibility:hidden; position: absolute;"   class="input-file upload-img" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png"/>
                                <input type="hidden" name="old_profile_picture" value="<?php echo $user->profile_picture; ?>">
                                <input type="hidden" name="old_driving_license" value="<?php echo $user->driving_license; ?>">
                                <input type="hidden" name="old_insurance_certi" value="<?php echo $user->insurance_certi; ?>">
                                <h4 class="mt-3"><span class="pointer upload-txt">Upload Photo</span></h4>
                            </div>
                        </div>
                    </div>

                    <div class="card m-b-20">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Personal Info</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                <input type="hidden" name="id" value="<?php echo $user->id; ?>">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="first_name">First Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('first_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = stripslashes($user->first_name);
                                            }
                                            ?>
                                            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter first name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('first_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="last_name">Last Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('last_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = stripslashes($user->last_name);
                                            }
                                            ?>
                                            <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter last name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('last_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="" for="phone">Phone</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('phone');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = $user->phone;
                                            }
                                            ?>
                                            <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone number" value="<?php echo $field_value; ?>" disabled>
                                            <div class="validation-error-label">
                                                <?php echo form_error('phone'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <div>
                                            <input type="text" name="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo $user->email; ?>" disabled>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <div class="form-group ">
                                        <label class="">Driving License</label>
                                          <input type="file" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png"  accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL2(this);" id="driving_license" name="driving_license" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <a class="image-popup-no-margins blah2" href="<?php echo BASE_URL().$user->driving_license;?>">
                                            <img class="border rounded p-0 >"  src="<?php echo BASE_URL().$user->driving_license;?>" onerror="this.src='<?php echo $prof_url; ?>'" alt="your image" style="height: 130px;width: 130px" id="blah2"/>
                                        </a>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                
                                <div class="col-lg-6">
                                    <div class="form-group ">
                                        <label class="">Insurance Certificate</label>
                                          <input type="file"  data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL3(this);" id="insurance_certi" name="insurance_certi" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <a class="image-popup-no-margins blah3" href="<?php echo BASE_URL().$user->insurance_certi;?>">
                                            <img class="border rounded p-0 >"  src="<?php echo BASE_URL().$user->insurance_certi;?>" onerror="this.src='<?php echo $prof_url; ?>'" alt="your image" style="height: 130px;width: 130px" id="blah3"/>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-b-0">
                                        <div>
                                            <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <a href="<?php echo $back_url; ?>" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> 

                <div class="col-lg-1">

                </div>

            </div> <!-- end row -->
        </form>

    </div>
</div>
<script src="<?php echo base_url().'assets/js/mask/jquery.inputmask.bundle.js'; ?>"></script>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script src="<?php echo assets('pages/lightbox.js');?>"></script>
<script type="text/javascript">
   var prof_url = '<?php echo assets('images/default.png'); ?>' ;
     $("#phone").inputmask("(999) 999-9999",{"placeholder": ""});
     $(document).on('click', '.upload-txt, #blah', function(){
        $('#imgInp').click();
        $('#blah').attr('src', prof_url);
        return false;
    });
     function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change', '#imgInp', function() {
        readURL(this);
    });
    function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2').attr('src', e.target.result);
            $('.blah2').attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
   }

   function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah3').attr('src', e.target.result);
            $('.blah3').attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
   }
   
</script>