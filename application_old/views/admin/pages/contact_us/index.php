<script src="<?php echo assets('js/ckeditor/ckeditor.js'); ?>"></script>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                  <!--   <a href="<?php //echo $add_url; ?>" class="btn btn-primary waves-effect waves-light float-right">
                            <i class="ion-plus mr-1"></i> Add 
                        </a> -->
                        <h4 class="page-title"><?php echo $title; ?></h4>
                        <?php
                        echo $this->breadcrumbs->show();
                     ?>
                </div>
            </div>
        </div>
         <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                         <?php echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->


        <!-- Modal HTML -->
           <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">From : <label id="name"></label></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <table class="table table-hover"> 
                      <tbody>
                          <tr>
                              <td>Email</td>
                              <td id="modal-email">dhrumi.m@gmail.com</td>
                          </tr>
                          <tr>
                              <td>Message</td>
                              <td id="modal-msg"></td>
                          </tr>
                      </tbody>
                  </table>
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>


        <div class="modal fade" id="modalreply" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <form enctype="multipart/form-data" action="" method="post" class="form-horizontal">
              <div class="modal-body">
                <form method="post">
                  <div class="form-group">
                      <label for="title">To:</label>
                      <div>
                          <input type="text" id="email-to" class="form-control" placeholder="Enter Email" value="" readonly disabled>
                      </div>
                      
                  </div>
                  <div class="form-group">
                      <label class="required" for="title">Subject:</label>
                      <div>
                          <input type="text" id="subject" name="subject" class="form-control" placeholder="Enter Subject" value="">
                          <div class="validation-error-label" style="display: none;" id="subject-error">
                            The subject field is required.
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div>
                          <textarea name="editor1" id="editor1" rows="10" cols="80" class="form-control">
                          </textarea>
                      </div>
                  </div>
              </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="send" type="submit">Send<i class="fab fa-telegram-plane m-l-10"></i></button>
              </div>
            </form>
          </div>
        </div>
      </div>


    </div>
</div>
<script type="text/javascript">
function nl2br (str, is_xhtml) {   
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
$(document).on('click',"#view", function(){
    $('#name').html($(this).data("name"));
    $('#modal-email').html($(this).data("email"));
    $('#modal-msg').html(nl2br($(this).data("message")));
});
$(document).on('click', '#btnReply', function(){
    $('#modalreply').modal('show');
    $('#modalreply #subject-error').hide();
    $('#email-to').val($(this).data("email"));
    CKEDITOR.replace('editor1');
});
$('#modalreply').on('hidden.bs.modal', function () {
    $('#subject').val('');
    CKEDITOR.instances.editor1.setData(""); 
})
var reply_url = "<?php echo base_url().'contact-us-reply'; ?>";
  $(document).on('click',"#modalreply #send", function(){
            var email = $('#email-to').val();
            var email_subject = $('#modalreply #subject').val();
            email_subject = email_subject.trim();
            if(email_subject == ''){
                $('#modalreply #subject-error').show();
                return false;
            }else{

                $(".overlay").css("display", "block");

                $('#modalreply #subject-error').hide();

                var email_msg = CKEDITOR.instances.editor1.getData();

                console.log(email);
                console.log(email_subject);
                console.log(email_msg);

                /*$.ajax({
                        url: reply_url,
                        type: "POST",
                        data:{
                            email_to:email,
                            email_subject:email_subject,
                            email_msg:email_msg
                        },
                        success: function (returnData) {
                            $(".overlay").css("display", "none");
                            returnData = $.parseJSON(returnData);
                            $('#modalreply').modal('hide');
                            CKEDITOR.instances.editor1.setData("");
                            if(returnData.is_success == true){
                                swal(
                                    'Mail Sent!',
                                    '',
                                    'success'
                                )
                            }else{
                                swal(
                                    'Something went wrong!',
                                    'Please try again later',
                                    'warning'
                                )
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $(".overlay").css("display", "none");
                            console.log('error');
                        }
                    }); */   

            }
        });

</script>