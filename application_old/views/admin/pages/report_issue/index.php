<script src="<?php echo assets('js/ckeditor/ckeditor.js'); ?>"></script>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                  <!--   <a href="<?php //echo $add_url; ?>" class="btn btn-primary waves-effect waves-light float-right">
                            <i class="ion-plus mr-1"></i> Add 
                        </a> -->
                        <h4 class="page-title"><?php echo $title; ?></h4>
                        <?php
                        echo $this->breadcrumbs->show();
                     ?>
                </div>
            </div>
        </div>
         <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">
                         <?php echo $this->datatables->generate(); ?>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->


        <!-- Modal HTML -->
           <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Issue for Order Id : <label id="name"></label></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <table class="table table-hover"> 
                      <tbody>
                          <!-- <tr>
                              <td>Order Id</td>
                              <td id="modal-order"></td>
                          </tr> -->
                          <tr>
                              <td>Issue</td>
                              <td id="modal-issue"></td>
                          </tr>
                          <tr>
                              <td>Description</td>
                              <td id="modal-description"></td>
                          </tr>
                      </tbody>
                  </table>
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

    </div>
</div>
<script type="text/javascript">
function nl2br (str, is_xhtml) {   
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
$(document).on('click',"#view", function(){
    $('#name').html($(this).data("order_id"));
    // $('#modal-order').html($(this).data("order_id"));
    $('#modal-issue').html($(this).data("issue"));
    $('#modal-description').html(nl2br($(this).data("description")));
});
</script>