<?php
$put_link = base_url('admin/faq/edit/').$faq['id'];
$back = base_url('admin/faq');
?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                     <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('admin/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <form class="form-validate"  method="post" action="<?php echo $put_link; ?>" >
                            <div class="form-group">
                                <label class="required">Question</label>
                                <div>
                                <?php
                                    $field_value = NULL;
                                    $temp_value = set_value('question');
                                    if (isset($temp_value) && !empty($temp_value)) {
                                    $field_value = $temp_value;
                                    } else{
                                        $field_value  = $faq['question'];
                                    }
                                ?>
                                    
                                    <textarea class="form-control" placeholder="Enter question" name="question" id="question" required><?php echo $field_value; ?></textarea>
                                    <div class="validation-error-label">
                                        <?php echo form_error('question'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="required">Answer</label>
                                <div>
                                <?php
                                    $field_value = NULL;
                                    $temp_value = set_value('answer');
                                    if (isset($temp_value) && !empty($temp_value)) {
                                    $field_value = $temp_value;
                                    } else{
                                        $field_value  = $faq['answer'];
                                    }
                                ?>
                                    
                                    <textarea class="form-control" placeholder="Enter answer" name="answer" id="answer" required><?php echo $field_value; ?></textarea>
                                    <div class="validation-error-label">
                                        <?php echo form_error('answer'); ?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $faq['id'] ?>">

                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> 
        </div>

   </div>
</div>


    

        