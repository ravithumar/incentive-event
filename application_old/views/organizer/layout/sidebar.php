<!-- <style type="text/css" media="screen">
.side-menu {
    background: #ffffff;
}
#sidebar-menu > ul > li > a:hover, #sidebar-menu > ul > li > a:focus, #sidebar-menu > ul > li > a:active, #sidebar-menu .submenu > li > a:hover, #sidebar-menu .submenu > li > a:focus, sidebar-menu .submenu > li > a:active {
    color: #87c242;
    background-color: #8bc34a42;
}

#sidebar-menu > ul > li > a{
     font-weight: bold;
}
#sidebar-menu > ul > li > a.active, .submenu li.active > a.active {
    background-color: #8bc34a42;
}

.enlarged #wrapper #sidebar-menu ul ul {
    background-color: #ffffff;
}
</style>
 --><!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                
                <li class="mt-2">
                    <a href="<?php echo base_url('organizer/dashboard'); ?>" class="waves-effect">
                        <i class="mdi mdi-view-dashboard"></i><span> Dashboard </span>
                    </a>
                </li>

                <li class="mt-2">
                    <a href="<?php echo base_url('organizer/my-profile'); ?>" class="waves-effect"><i class="mdi mdi-store"></i><span> My Profile </span></a>
                </li> 

                 <li class="mt-2">
                    <a href="<?php echo base_url('organizer/event'); ?>" class="waves-effect"><i class="mdi mdi-store"></i><span> Event </span></a>
                </li> 

                 <li class="mt-2">
                    <a href="<?php echo base_url('organizer/student'); ?>" class="waves-effect"><i class="fa fa-users"></i><span> Student </span></a>
                </li> 


               <!--  <li class="">
                    <a href="<?php //echo base_url('organizer/account'); ?>" class="waves-effect"><i class="mdi mdi-bank"></i><span> Bank Details </span></a>
                </li> -->

            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->