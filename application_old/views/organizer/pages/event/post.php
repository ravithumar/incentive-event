<?php
$put_link = base_url('organizer/event/add');
$back = base_url('organizer/event');
$image  = BASE_URL().'assets/images/default.png';
//$product_data = $product_data->toArray();
//$product_style = array_column($product_data['style'], 'style_id');
// echo "<pre>";
// print_r($product_data);
// print_r($product_style);
// print_r($variants);
// print_r($category);
// exit();
?>
<style type="text/css">
.custom-control {
    display: inline-block;
    padding-right: 2rem;
}
.custom-control-input{
    position: absolute !important;
} 
.validation-error-label{
    margin-top: 0;
 }

.custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
    background-color: #2a3142;
}
input[switch] + label {
    width: 85px !important;
}
input[switch]:checked + label:after {
        left: 63px !important;
}

input[switch]:checked + label {
    background-color: #2a3142 !important;
    }
</style>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><?php echo $title; ?></h4>
                   <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('organizer/includes/message');
        ?>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <form class="form-validate"  method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }
                                            ?>
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Event Date</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('event_date');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="date" name="event_date" class="form-control" id="event_date" placeholder="Event Date" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('event_date'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Start Time</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('start_time');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="time" name="start_time" class="form-control" id="start_time" placeholder="Start Time" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('start_time'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">End Time</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('end_time');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="time" name="end_time" class="form-control" id="end_time" placeholder="End Time" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('end_time'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Dress Code</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('dresscode');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="text" name="dresscode" class="form-control" id="end_time" placeholder="Dress Code" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('dresscode'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Address</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('address');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="text" name="address" class="form-control" id="address" placeholder="Address" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('address'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Latitude</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('lat');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="text" name="lat" class="form-control" id="address" placeholder="Latitude" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('lat'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Longitude</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('lng');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="text" name="lng" class="form-control" id="lng" placeholder="Longitude" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('lng'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Max Ticket Count</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('max_ticket_count');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            } 
                                            ?>
                                            <input type="text" name="max_ticket_count" class="form-control" id="max_ticket_count" placeholder="Max Ticket Count" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('max_ticket_count'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div id="variants">
                                <input type="file" name="image">
                                 
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="button" name="add_variant" class="btn btn-primary waves-effect waves-light" id="add_variant" title="Add More Attribute">
                                            Add More Attribute
                                        </button>
                                    </div>
                                </div>
                            </div> 

                            <hr>
                            <div class="form-group m-b-0">
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                        Submit
                                    </button>
                                    <a href="<?php echo $back; ?>" class="btn btn-secondary waves-effect m-l-5">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div> 
        </div>

   </div>
</div>
<style type="text/css">
    .select2-selection__rendered{
        margin-left: 10px!important;
    }
</style>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script type="text/javascript">

    $(".demo1").TouchSpin({
        initval: 0.00,
        min: 0,
        max: 1000000,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

    $(".demo3").TouchSpin({
        initval: 00,
        min: 0,
        max: 1000000,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });
     $(".demo2").TouchSpin({
        forcestepdivisibility: 'none',
        max: 1000000000,
        decimals: 2,
        prefix: '$',
        step: 0.1,
        buttondown_class: 'btn btn-dark',
        buttonup_class: 'btn btn-dark'
    });

     $("#picture").on("select2:selecting", function(e) {
        console.log($(this).val(), e.params.args.data);
        if($(this).val() && $(this).val().length >= 3) {
          e.preventDefault();
        }
      });


    $(".select2").select2({
        maximumSelectionLength: 3,
    });
    $(document).on('click', '.upload', function(){
        $('#blah').attr('src', '<?php echo BASE_URL()."assets/images/default.png"; ?>');
    });
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
       }

     $(document).on('click','.remove-btn',function(){
        $(this).closest(".varient-row").remove();
    });

    var count = 1;
    $(document).on('click','#add_variant',function(){

        count = count + 1;

        var variant_row = 
        '<div class="varient-row">'+
        '<hr>'+
            '<div class="row">'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Name</label>'+
                        '<div>'+
                            '<input required="" type="text" name="event_prize_name[]" class="form-control name" placeholder="Ex: Cheese">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Image</label>'+
                        '<div>'+
                            '<input type="file" name="image[]" class="form-control"  value="" data-parsley-errors-container="#image'+count+'"   required>'+
                            '<div id="image'+count+'"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                
                '<div class="col-md-2">'+
                    '<div class="form-group">'+
                    '<label >Remove</label>'+
                    '<div class="d-flex h-100">'+
                        '<div class="justify-content-center align-self-center ">'+
                            '<label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="ion-close-round"></i></label>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';

        $('#variants').append(variant_row);
        $(".select2").select2();
        $(".demo2").TouchSpin({
            forcestepdivisibility: 'none',
            max: 1000000000,
            decimals: 2,
            prefix: '$',
            step: 0.1,
            buttondown_class: 'btn btn-dark',
            buttonup_class: 'btn btn-dark'
        });

    });

    if ($('#inventory_status').is(':checked')){
        $('#quantity-div').removeClass("d-none");
    }else{
        $('#quantity-div').addClass("d-none");
    }

    $(document).on('change','#inventory_status',function(){
        if ($(this).is(':checked')){
            $('#quantity-div').removeClass("d-none");
        }else{
            $('#quantity-div').addClass("d-none");
        }
    });

</script>

    

        