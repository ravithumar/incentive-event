 <!-- Start content -->
<style type="text/css" media="screen">
input[switch] + label {
    width: 78px !important;
}
input[switch]:checked + label:after {
        left: 55px !important;
}

input[switch]:checked + label {
    background-color: #2a3142 !important;
    }
.working-time-error{
    font-size: 16px;
    list-style: none;
    color: #ec536c;
    margin-top: 5px;
}
.custom-control {
    display: inline-block;
    padding-right: 2rem;
}
.custom-control-input{
    position: absolute !important;
} 
.validation-error-label{
    margin-top: 0;
 }

.custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
    background-color: #2a3142;
}

 </style>
<?php
$put_link = base_url('organizer/my-profile/');

$back_url = base_url('organizer/dashboard');
$prof_url =  BASE_URL().'assets/images/default.png';

// get from to time
$from_time = DateTime::createFromFormat('H:i',$this->config->item("start_time"));
$to_time = DateTime::createFromFormat('H:i',$this->config->item("end_time"));

$from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("start_time_defualt"));
$to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("end_time_defualt"));

$from_time_selected = $from_time_selected->format('h:i A');
$to_time_selected = $to_time_selected->format('h:i A');

for($j = $from_time; $j <= $to_time;){
    $available_time[] = $j->format('h:i A');
    $j->add(new DateInterval('PT30M'));
}
 //_pre($vendor['category']);
 ?>
 
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-box">
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                    echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-12">
                <?php
                $this->load->view('organizer/includes/message');
                ?>

            </div>
        </div>

        <form class="form-validate" method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

            <div class="row">

                <div class="col-lg-12">

                    <div class="card m-b-20">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Basic Information</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                              
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="first_name">First Name</label>
                                         <input type="hidden" name="user_id" value="<?php echo $organizer['id']; ?>">
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('first_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = stripslashes($organizer['first_name']);
                                            }
                                            ?>
                                            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter first name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('first_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="last_name">Last Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('last_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = stripslashes($organizer['last_name']);
                                            }
                                            ?>
                                            <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter last name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('last_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Email</label>
                                        <div>
                                            <input type="text" name="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo $organizer['email']; ?>" required  readonly>
                                            <input type="hidden" name="old_email" value="<?php echo $organizer['email']; ?>">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="phone">Phone No</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('phone');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = $organizer['phone'];
                                            }
                                            ?>
                                            <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone no" value="<?php echo $field_value; ?>" data-parsley-minlength="14"  data-parsley-minlength-message="This value is invalid." readonly>
                                            <div class="validation-error-label">
                                                <?php echo form_error('phone'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                               
                            </div>

                        
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group ">
                                        <label class="">Photo</label>
                                          <input type="file" accept="image/*" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL1(this);" id="profile_picture" name="profile_picture" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <a class="image-popup-no-margins blah1" href="<?php echo BASE_URL().$organizer['profile_picture'];?>">
                                            <img class="border rounded p-0 >"  src="<?php echo BASE_URL().$organizer['profile_picture'];?>" onerror="this.src='<?php echo $prof_url; ?>'" alt="your image" style="height: 130px;width: 130px" id="blah1"/>
                                        </a>
                                    </div>
                                </div>

                                
                            </div>
                       

                       

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-b-0">
                                        <div>
                                            <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                Update
                                            </button>
                                            <a href="<?php echo $back_url; ?>" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> 

            </div> <!-- end row -->
        </form>

    </div>
</div>

<script src="<?php echo base_url().'assets/js/mask/jquery.inputmask.bundle.js'; ?>"></script>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script src="<?php echo assets('pages/lightbox.js');?>"></script>
<?php
$google_key = $this->config->item("google_key");
?>
<script type="text/javascript">


    var available_time = jQuery.parseJSON('<?php echo json_encode($available_time); ?>');
    var vendor_availability = jQuery.parseJSON('<?php echo json_encode($vendor['vendor_availability']); ?>');

    var from_time_selected = '<?php echo $from_time_selected; ?>';
    var to_time_selected = '<?php echo $to_time_selected; ?>';

     $(document).on('change','#autocomplete',function(){
        $('#latitude').val('');
        $('#longitude').val('');
        $('#city').val('');
        $('#state').val('');
        $('#country').val('');
        $('#zip_code').val('');
    });

     
    var placeSearch, autocomplete;

    var componentForm = {
        administrative_area_level_2: 'long_name',
        administrative_area_level_1: 'long_name'
      };



    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
           (document.getElementById('autocomplete')),
            // {types: ['geocode'] , componentRestrictions: {country: "us"} });
             {types: ['geocode'] });
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        if (typeof place.address_components != "undefined" || place.address_components != null){

            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());

            console.log(place.address_components);

            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++){
                    if (place.address_components[i].types[j] == "postal_code") {
                        $('.zipcode').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[j] == "country") {
                        $('.country').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[j] == "administrative_area_level_1") {
                        $('.state').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[j] == "administrative_area_level_2") {
                        $('.city').val(place.address_components[i].long_name);
                    }
                }
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
    }

    function geolocate() {
        $(".overlay").css("display", "block");
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            // console.log(geolocation);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
        $(".overlay").css("display", "none");
    }

    function check_lat_lon() {
        if($('#latitude').val() == '' || $('#longitude').val() == ''){
            Notiflix.Notify.Warning('Please select address from google suggestion!');
            return false;
        }
    }


</script>
<!-- <script src="<?php echo assets('js/custom/admin/vendor_profile.js'); ?>"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_key; ?>&libraries=places&callback=initAutocomplete" async defer></script>