 <!-- Start content -->
<?php
$put_link = base_url('admin/user/edit/').$user->id;
$back_url = base_url('admin/users');
$prof_url = assets('images/default_user.png');
// _pre($user_style);
?>
 
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <div class="page-title-box">
                   <h4 class="page-title"><?php echo $title; ?></h4>
                    <?php
                        echo $this->breadcrumbs->show();
                    ?>
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <?php
                    $this->load->view('admin/includes/message');
                ?>
            </div>
            <div class="col-lg-1">
            </div>
        </div>

        <form class="form-validate" method="post" action="<?php echo $put_link; ?>" enctype="multipart/form-data">

            <div class="row">

                <div class="col-lg-1">
                </div>

                <div class="col-lg-10">
                    <div class="card m-b-20">
                        <div class="card-body row">
                            <div class="col-12 text-center">

                                <img src="<?php echo base_url($user->profile_picture); ?>" class="img-circle profile-avatar pointer" alt="Shop Image" id="blah" onerror="this.src='<?php echo $prof_url; ?>'">
                                <input type='file' name="profile_picture" id="imgInp" accept="image/*" style="visibility:hidden; position: absolute;"   class="input-file upload-img" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png"/>
                                <input type="hidden" name="old_profile_picture" value="<?php echo $user->profile_picture; ?>">
                                <h4 class="mt-3"><span class="pointer upload-txt">Upload Photo</span></h4>
                            </div>
                        </div>
                    </div>

                    <div class="card m-b-20">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 mt-2 mb-1">
                                    <h4 class="mt-0 mb-0 header-title">Personal Info</h4>
                                    <hr class="mt-1 mb-3">
                                </div>
                                <input type="hidden" name="id" value="<?php echo $user->id; ?>">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="first_name">First Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('first_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = stripslashes($user->first_name);
                                            }
                                            ?>
                                            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter first name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('first_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="last_name">Last Name</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('last_name');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = stripslashes($user->last_name);
                                            }
                                            ?>
                                            <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter last name" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('last_name'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required" for="phone">Phone</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('phone');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = $user->phone;
                                            }
                                            ?>
                                            <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone number" value="<?php echo $field_value; ?>" required>
                                            <div class="validation-error-label">
                                                <?php echo form_error('phone'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <div>
                                            <input type="text" name="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo $user->email; ?>" disabled>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <div>
                                        <?php
                                            $field_value = NULL;
                                            $temp_value = set_value('description');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                $field_value = $user->description;
                                            }
                                            ?>
                                            <textarea name="description" id="description" class="form-control" ><?php echo $field_value; ?></textarea>
                                            <div class="validation-error-label">
                                                <?php echo form_error('description'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Style</label>
                                        <select id="style_id" name="style_id[]" class="form-control select2" multiple data-parsley-errors-container="#style_id_error" data-placeholder="Select Styles">
                                            <option value="" disabled > </option>
                                           <?php
                                            $field_value = NULL;

                                            $temp_value = set_value('style_id');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                if(!empty($user_style)){
                                                    $field_value = array_column($user_style, 'style_id');
                                                }
                                            }

                                            if(isset($styles) && !empty($styles)){
                                                foreach ($styles as $key => $value) {
                                                    if(in_array($value['id'], $field_value)){
                                                        $selected = 'selected';
                                                    }else{
                                                         $selected= '';
                                                    }
                                                ?>
                                                <option  value="<?php echo $value['id'];?>" <?php echo $selected; ?> > <?php echo ($value['name']);?> </option>
                                                <?php
                                                }
                                                }
                                                ?>
                                             
                                             <div class="validation-error-label">
                                                <?php echo form_error('style_id'); ?>
                                            </div>
                                        </select>
                                        <div id="style_id_error"></div>
                                    </div> 
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Category</label>
                                        <select id="category_id" name="category_id[]" class="form-control select2" multiple data-parsley-errors-container="#category_id_error" data-placeholder="Select Category">
                                            <option value="" disabled > </option>
                                           <?php
                                            $field_value = NULL;

                                            $temp_value = set_value('category_id');
                                            if (isset($temp_value) && !empty($temp_value)) {
                                                $field_value = $temp_value;
                                            }else{
                                                if(!empty($user_category)){
                                                    $field_value = array_column($user_category, 'category_id');
                                                }
                                            }

                                            if(isset($categorys) && !empty($categorys)){
                                                foreach ($categorys as $key => $value) {
                                                    if(in_array($value['id'], $field_value)){
                                                        $selected = 'selected';
                                                    }else{
                                                         $selected= '';
                                                    }
                                                ?>
                                                <option  value="<?php echo $value['id'];?>" <?php echo $selected; ?> > <?php echo ($value['name']);?> </option>
                                                <?php
                                                }
                                                }
                                                ?>
                                             
                                             <div class="validation-error-label">
                                                <?php echo form_error('category_id'); ?>
                                            </div>
                                        </select>
                                        <div id="category_id_error"></div>
                                    </div> 
                                </div>
                            </div>

                           

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-b-0">
                                        <div>
                                            <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <a href="<?php echo $back_url; ?>" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> 

                <div class="col-lg-1">

                </div>

            </div> <!-- end row -->
        </form>

    </div>
</div>
<script src="<?php echo base_url().'assets/js/mask/jquery.inputmask.bundle.js'; ?>"></script>
<script src="<?php echo assets('js/custom/admin/parsley_img_validate.js'); ?>"></script>
<script type="text/javascript">
   var prof_url = '<?php echo assets('images/default.png'); ?>' ;
    $(".select2").select2();
     $("#phone").inputmask("(999) 999-999",{"placeholder": ""});
     $(document).on('click', '.upload-txt, #blah', function(){
        $('#imgInp').click();
        $('#blah').attr('src', prof_url);
        return false;
    });
     function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change', '#imgInp', function() {
        readURL(this);
    });
</script>