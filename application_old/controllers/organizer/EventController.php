<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EventController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_organizer()) {
			redirect('/organizer/login');
		}
		
		$this->load->model('Event');
		$this->load->model('EventPrizes');
		$this->title = 'Event';
		
	}

	public function index()
	{
		$id = $this->session->userdata['organizer']['user_id'];
		// _pre($id);
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		// _pre($dt_data);
		$dt_data->select('events.*', false)->from('events')->where('events.user_id', $id)->where('events.deleted_at', NULL);
		$action['edit'] = base_url('organizer/event/edit/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Name', 'name')
			->column('Event Date', 'event_date')
			->column('Start Time', 'start_time')
			->column('End Time', 'end_time')
			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="events" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="events" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">Edit</a>';
				$option .= " <button type='button' data-table='events' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('id,question');
		$dt_data->datatable('Event');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'organizer/event');
		$data['add_url'] = base_url('organizer/event/add');
        
		$this->organizer_render('event/index',$data);	
	}
	public function add(){
		
		// _pre('helo');
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				
				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					array('field' => 'name', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'event_date', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'start_time', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'end_time', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'dresscode', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'address', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'lat', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'lng', 'label' => 'name', 'rules' => 'required'),
					array('field' => 'max_ticket_count', 'label' => 'name', 'rules' => 'required'),
					

				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){
					$id = $this->session->userdata['organizer']['user_id'];

					$request = $this->input->post();
                	_pre($_FILES['image']);
					// print_r(count($request['testimage']));
     //            	die;
                	$user_data['user_id'] = $id;
                	$user_data['name'] = $request['name'];
                	$user_data['event_date'] = $request['event_date'];
                	$user_data['start_time'] = $request['start_time'];
                	$user_data['end_time'] = $request['end_time'];
                	$user_data['dresscode'] = $request['dresscode'];
                	$user_data['address'] = $request['address'];
                	$user_data['lat'] = $request['lat'];
                	$user_data['lng'] = $request['lng'];
                	$user_data['max_ticket_count'] = $request['max_ticket_count'];
                	$user_data['status'] = 1;
                	$insert_event=Event::insertGetId($user_data);
                	// echo $insert_event;die;
              
                	
                	
                	if(isset($_POST['event_prize_name']) && isset($_POST['image']) ){
						$group_array = array();

						foreach ($this->input->post("event_prize_name") as $key => $value) {
							$group_array[$key]['name'] = $value;
							$group_array[$key]['event_id'] = $insert_event;
						}

						
						foreach ($this->input->post("image") as $key => $value) {
							$image_is_uploaded = image_upload('image', 'images/event/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');
							$img_array = [];

                            if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])
                            {
                                $all_images[] = $image_is_uploaded['file_name']; 
                            }
							$group_array[$key]['image'] = $value;
							
						}


						EventPrizes::insert($group_array);
						
					}


                	// $event_prizes = EventPrizes::where('event_id',$id)->get();
                	// _pre($event_prizes);

              
					$this->session->set_flashdata('success','Event added successfully');
					redirect(base_url("organizer/event"));

				}
			}
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'organizer/event');
        $this->breadcrumbs->push('Add', '/', true);
		$this->organizer_render('event/post', $output_data);
	}
	public function edit($id = ''){

		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){

				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					
					array('field' => 'question', 'label' => 'question', 'rules' => 'trim|required|min_length[2]'),
					array('field' => 'answer', 'label' => 'answer', 'rules' => 'trim|required|min_length[2]'),
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {

                    	$request = $this->input->post();

                    	$user_data['question'] = $request['question'];
                		$user_data['answer'] = $request['answer'];
                    	Event::whereId($id)->update($user_data);

                    	$this->session->set_flashdata('success', 'Faq updated successfully');
						redirect(base_url('organizer/faq'));

				} 
			}	
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'organizer/faq');
        $this->breadcrumbs->push('Edit', '/', true);

		$output_data['faq'] = Event::find($id);
		$this->organizer_render('event/put', $output_data);
	}

}
?>
