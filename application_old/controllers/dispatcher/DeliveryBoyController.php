<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DeliveryBoyController extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if (!$this->ion_auth->is_dispatcher()) {
			redirect('/dispatcher/login');
		}
		$this->load->model('User');
		
		$this->title = 'Delivery Boy';
	}
	public function index(){

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('users.id, users.first_name, users.last_name, users.email, users.phone, users.active', false)
		->from('users')
		->join('users_groups', 'users_groups.user_id = users.id')
		->where('users_groups.group_id',4)
		->where('users.deleted_at', NULL);
		$action['edit'] = base_url('dispatcher/user/edit/');
		$action['view'] = base_url('dispatcher/user/view/');
		$dt_data
			->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Name', 'first_name',function($first_name,$row){
				if($first_name != ''){
					return ucfirst(stripslashes($first_name.' '.$row['last_name']));
				}else{
					return '-';
				}
			})
			
			->column('Phone', 'phone',function($phone,$row){
				if($phone != '' && $phone != '0'){
					return $phone;
				}else{
					return '-';
				}
			})
			//->column('Email', 'email')
			->column('Status', 'active', function ($active, $row){
				$option = "";
				if ($active == 1) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Active</button>';
				} elseif ($active == 2) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Inactive</button>';
				}else{
					return '<button class="btn btn-warning btn-sm waves-effect waves-light">Pending</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light" >Edit</a>';
				$option .= '<a href="' . $action['view'] . $id . '" class="ml-1 btn btn-dark btn-sm waves-effect waves-light" >View</a>';
				$option .= " <button type='button' data-table='users' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});
			
		$dt_data->searchable('users.id,first_name,last_name,email,phone');
		$dt_data->datatable('users');
		$dt_data->init();
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'dispatcher/user');
		$data['datatable'] = true;
		$this->dispatcher_render('users/index', $data);	
	}
	public function customAlpha($str) {
		if (!preg_match('/^[a-z \-]+$/i', $str)) {
			$this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
			return false;
		}
		return TRUE;
	}
	public function put($id = ''){
		$this->load->library(['ion_auth', 'form_validation']);
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'first_name', 'label' => 'first name', 'rules' => 'trim|required|min_length[2]'),
					array('field' => 'last_name', 'label' => 'last name', 'rules' => 'trim|required|min_length[2]'),
				);
				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();
					
					$user_data = [
							'first_name' => $request['first_name'],
							'last_name' => $request['last_name'],
						];
					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
                		$img_name = 'user_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
					        $user_data['profile_picture'] = $img_name;
					        if($request["old_profile_picture"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_profile_picture"]);
					        }
					        
					    }
					    $file_upload = true;
                	}

                	if(isset($_FILES['driving_license']) && !empty($_FILES['driving_license']) && strlen($_FILES['driving_license']['name']) > 0) {
                		$img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                		$destination_url = FCPATH . $this->config->item("document").$img_name;
					    if(compress($_FILES["driving_license"]["tmp_name"], $destination_url, 90)){
					        $user_data['driving_license'] = $img_name;
					        if($request["old_driving_license"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_driving_license"]);
					        }
					        
					    }
					    $file_upload = true;
                	}

                	if(isset($_FILES['insurance_certi']) && !empty($_FILES['insurance_certi']) && strlen($_FILES['insurance_certi']['name']) > 0) {
                		$img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                		$destination_url = FCPATH . $this->config->item("document").$img_name;
					    if(compress($_FILES["insurance_certi"]["tmp_name"], $destination_url, 90)){
					        $user_data['insurance_certi'] = $img_name;
					        if($request["old_insurance_certi"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_insurance_certi"]);
					        }
					        
					    }
					    $file_upload = true;
                	}
                	// _pre($user_data);
  
					if ($file_upload){
						User::whereId($id)->update($user_data);
						$this->session->set_flashdata('success', 'Delivery Boy updated successfully');
						redirect(base_url('dispatcher/user'));
					}
				} 
			}
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'dispatcher/user');
        $this->breadcrumbs->push('Edit', '/', true);
		$output_data['user'] = User::find($id);
		if(!isset($output_data['user']) && count($output_data['user']) == 0){
			$this->session->set_flashdata('error', 'No any delivery boy found!');
			redirect(base_url('dispatcher/user'));
		}
		$this->dispatcher_render('users/put', $output_data);
	}
	public function view($id = ''){
		$output_data['db'] = User::with(['delivery_address','wallet_history'])->find($id);
		if(isset($output_data['db'])){
            $output_data['db'] = $output_data['db']->toArray();
        }
		//_pre($output_data);
		if(!isset($output_data['db']) && count($output_data['db']) == 0){
			$this->session->set_flashdata('error', 'No any delivery boy found!');
			redirect(base_url('dispatcher/user'));
		}
		
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'dispatcher/user');
        $this->breadcrumbs->push('View', '/', true);
		$this->dispatcher_render('users/view', $output_data);
	}
}