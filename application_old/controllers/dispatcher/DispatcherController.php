<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DispatcherController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this->ion_auth->is_dispatcher()) {
			redirect('/dispatcher/login');
		}
		$this->load->model('User');
		$this->title = 'Dispatcher Dashboard';
	}

	public function change_password(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
				$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]');
				$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

				 
				if ($this->form_validation->run() === true){
					$request = $this->input->post();


					$identity =  $this->session->userdata['dispatcher']['identity'];
					$id =  $this->session->userdata['dispatcher']['user_id'];

					// _pre($this->session->userdata());
					$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'),$id);

					if($change){
						$this->session->set_flashdata('success', $this->ion_auth->messages());
					}else{
						$this->session->set_flashdata('error', $this->ion_auth->errors());
					}
					redirect(base_url('dispatcher/change-password'));
				}

			}
		}

		$output_data['title'] = 'Change Password';
		$output_data['change_pw_link'] = base_url('dispatcher/change-password');
		$this->breadcrumbs->push('Change Password' , 'dispatcher/change-password');
		$this->dispatcher_render('change_password',$output_data);
	}
}