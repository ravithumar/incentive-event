<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //echo '<pre>'; print_r($this->session->all_userdata());exit;
        if (!$this->ion_auth->is_dispatcher()) {
            redirect('/');
        }
        $this->title = 'Dashboard';
        $this->load->model('User');
        $this->load->model('Product');
        $this->load->model('Orders');
    }

    public function index(){
        //echo '<pre>'; print_r($this->session->all_userdata());exit;
        //echo "dispatcher Dashbord"; exit;

        // $chat = Chat::find(6);
        // print_r($chat);
        // echo "df"; exit;
        // $push_data['message'] = 'this is new 2';

        // $SendNotification = SendNotification('eDu-Jus3q0H0syIw1HS1GJ:APA91bERMYetcSP5NTqJZSU_WyUdpdtQEIOiRQyaUQ-o9OzbCdwXm3OKu71cnHwpo5yALsRVpizGbpbh_gd-yNzg19YHi7M5TzSSse0MWGCuRbLtNHfbc3uMzsVNadSxZ-zGhf8VtrhM', 'testing ioss NEW', $push_data, 'request' );

        // echo "<pre>";
        // print_r($SendNotification);
        // exit;


        /* $this->load->library('email');
        $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'ruralareadelivery@gmail.com', // change it to yours
                'smtp_pass' => 'Picton2525', // change it to yours
                'smtp_timeout'=>20,
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
               );

        $data["email_template"] = 'reset_password';
        $message = $this->load->view('email_templates/template', $data, TRUE);
        $this->email->initialize($config);// add this line

        $this->email->set_newline("\r\n");
        $this->email->from('ruralareadelivery@gmail.com',"RAD");
        $this->email->to('developer.eww2@gmail.com');
        $this->email->subject('Rad Testing'); 
        $this->email->message('Testing mail');

        if($this->email->send()){
            echo 'send success';
        }else{
            print_r($this->email->print_debugger());
        }
        exit();  */

        // $activation_token = bin2hex(random_bytes(20));
        // $email_var_data["activation_link"] = base_url() . 'user-activate/'. $activation_token;
        // //$email_var_data["name"] = $user_data['first_name'].' '.$user_data['last_name'];
        // $email_var_data["name"] = 'XYZ';
        // $email_var_data["email_template"] = 'account_activation';
        // $mail = sendEmail('Email Verification', 'developer.eww2@gmail.com', 'email_templates/template', $email_var_data);

        $this->load->library('breadcrumbs');
        $data['title'] = $this->title;
        $data['total_products'] = 0;
        $data['products'] = 0;

      
        $data['net_amount'] = 0;
        $data['growth_amount'] = 0;



        $this->dispatcher_render('dashboard', $data);    
    }
}