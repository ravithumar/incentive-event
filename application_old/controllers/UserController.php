<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class UserController extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('User');
		$this->load->model('ion_auth_model');
		$this->load->library(['ion_auth', 'form_validation']);
	}
	
	public function activate_account($activation_token = NULL){
		
		$code = explode('.',$activation_token);
		$user = User::where('activation_selector',$code[0])->first();
	/*	_pre($user);*/
		if (isset($_POST) && !empty($_POST)){
			/*echo "123";*/
			if (isset($_POST['submit'])){

				// $this->auth->clear_messages();
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

				$validation_rules = array(
					array('field' => 'password', 'label' => 'password', 'rules' => 'trim|required|min_length[8]'),
					array('field' => 'cpassword', 'label' => 'confirm password', 'rules' => 'trim|required|matches[password]')
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true){
					
					$change = $this->ion_auth->reset_password($user->email, $this->input->post('password'),$user->id);
					$this->ion_auth_model->activate($user->id,$activation_token);
					

					if($change){
						// $this->session->set_flashdata($this->auth->get_messages_array());
						redirect(base_url('account_activated'));
						//echo 'account activated';
						//exit;
					}else{
						// $this->auth->set_error_message("Something went wrong");
						// $this->session->set_flashdata($this->auth->get_messages_array());
						redirect(base_url('something-went-wrong'));
					}
				}
			}
		}
		if(isset($activation_token) && $activation_token != '' && isset($user)){
			//_pre($activation_token);
			$output_data["activation_token"] = $activation_token;
			$output_data["id"] = $user->id;
			$this->load->view('set_password',$output_data);	
		}else{
			// $this->auth->set_error_message("Something went wrong");
			// $this->session->set_flashdata($this->auth->get_messages_array());
			redirect(base_url('something-went-wrong'));
		}
	}
	public function forgotten_account($forgotten_password_selector = NULL){
		
		$code = explode('.',$forgotten_password_selector);
		$user = User::where('forgotten_password_selector',$code[0])->first();
		// _pre($user);
		$user_type = User::select('users.*','users_groups.group_id')->where('users.id', $user->id)->join('users_groups', 'users_groups.user_id', 'users.id')->whereIn('users_groups.group_id', [1])->first();

		if(isset($user_type)){

			$type = "admin";
		}else{

			$type = "vendor";
		}

		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){

				// $this->auth->clear_messages();
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

				$validation_rules = array(
					
					array('field' => 'password', 'label' => 'password', 'rules' => 'trim|required|min_length[8]'),
					array('field' => 'cpassword', 'label' => 'confirm password', 'rules' => 'trim|required|matches[password]')
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true){
					
					$change = $this->ion_auth->reset_password($user->email, $this->input->post('password'),$user->id);
					
					if($change){
						$this->session->set_flashdata('success', 'Password changed succesfully.');
						redirect(base_url().$type.'/login');
						//echo 'account activated';
						//exit;
					}else{
						// $this->auth->set_error_message("Something went wrong");
						// $this->session->set_flashdata($this->auth->get_messages_array());
						redirect(base_url('something-went-wrong'));
					}
				}
			}
		}
		if(isset($forgotten_password_selector) && $forgotten_password_selector != '' && isset($user)){
			$output_data["forgotten_password_selector"] = $forgotten_password_selector;
			$output_data["id"] = $user->id;
			$this->load->view('reset_password',$output_data);	
		}else{
			// $this->auth->set_error_message("Something went wrong");
			// $this->session->set_flashdata($this->auth->get_messages_array());
			redirect(base_url('something-went-wrong'));
		}
	}
	public function email_verify($email_activation_code = NULL){

		$user = User::where('email_activation_code', $email_activation_code)->first();

		if($user){
			if($user->email_verify == 0){
				$user->email_verify = 1;
				$user->email_activation_code = '';
				$user->save();

				$data['title'] = 'Email verify successfully';
				$this->load->view('auth/email_verified', $data);	

			}else if($user->email_verify == 1){
				$data['title'] = 'Email already verified';
				$this->load->view('auth/email_verified', $data);	
			}else{
				$this->load->view('auth/something_went_wrong');	
			}
		}else{
			$this->load->view('auth/something_went_wrong');	
		}

	}

	
	public function something_went_wrong(){
		$this->load->view('something_went_wrong');	
	}
	public function account_activated(){
		$this->load->view('account_activated');	
	}


	public function vendor_reset($id='')
	{

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
				$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]');
				$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

				 
				if ($this->form_validation->run() === true){
					$request = $this->input->post();


					$identity =  $this->session->userdata['vendor']['identity'];
					//$id =  $this->session->userdata['vendor']['user_id'];

					// _pre($this->session->userdata());
					$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'),$id);

					if($change){
						$this->session->set_flashdata('success', $this->ion_auth->messages());
					}else{
						$this->session->set_flashdata('error', $this->ion_auth->errors());
					}
					redirect(base_url('vendor/change-password'));
				}

			}
		}

		$output_data['title'] = 'Change Password';
		$output_data['change_pw_link'] = base_url('vendor/change-password');
	//	$this->breadcrumbs->push('Change Password' , 'vendor/change-password');
		$this->load->view('vendor/change_password',$output_data);
	}
}