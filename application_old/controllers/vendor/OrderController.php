<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
        
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		
        $this->load->model('User');

        $this->load->model('Orders');
        $this->load->model('OrderProductAddOn');
        $this->load->model('Devices');
        $this->load->model('OrderProducts');
		$this->title = 'Order';
		
	}

	public function order_arrived_index()
	{
		$id = $this->session->userdata['vendor']['user_id'];
	
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name', false)->from('orders')->join('users','orders.user_id = users.id')->where('orders.deleted_at', NULL)->where('orders.status',1)->where('vendor_id',$id);
		$action['view'] = base_url('vendor/order-arrived/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<button onclick="accept_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-success btn-sm waves-effect waves-light mr-1">Accept</button>';
				
				$option .= '<button onclick="reject_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-danger btn-sm waves-effect waves-light mr-1" >Reject</button>';

				// $option .= '<button type="button" id="'.$id.'" data-id ="'.$id.'" data-userid ="'.$row['user_id'].'"  data-deliveryid ="'.$row['delivery_boy_id'].'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1" onclick="send_push_notification('.$id.')" data-toggle="modal" data-target="#sendpush" >Push</button>';

				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$data['user'] = User::all();
        $this->breadcrumbs->push($this->title , 'vendor/order-arrived');
        $this->breadcrumbs->push('Arrived', '/', true);

		$this->vendor_render('order/index',$data);	
	}

	public function order_arrived_view($id = ''){
		
		$output_data['title'] = $this->title;

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$output_data['order'] = $orders;
			$output_data['order_products'] = $order_products;

		}

		if(!isset($output_data['order_products']) && count($output_data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('vendor/order-arrived'));
		}
		// _pre($output_data);
		$output_data['back_url'] = base_url('vendor/order-arrived');		
		$this->breadcrumbs->push($this->title , 'vendor/order-arrived');
        $this->breadcrumbs->push('View', '/', true);
		$this->vendor_render('order/view', $output_data);
	}

	public function order_inprocess_index(){
		$login_id = $this->session->userdata['vendor']['user_id'];
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$status = [2,5,6,7,8,9];
		$dt_data->select('orders.*,users.first_name,users.last_name', false)->from('orders')->join('users','orders.user_id = users.id')->where('orders.deleted_at', NULL)->where_in('orders.status',$status)->where('vendor_id',$login_id);
		$action['view'] = base_url('vendor/order-inprocess/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Assigned Delivery Person', 'delivery_boy_id',function($delivery_boy_id,$row){

				if($delivery_boy_id != 0){
					$this->db->select('first_name,last_name');
					$this->db->from('users');
					$this->db->where('id', $delivery_boy_id);
					$sql_query = $this->db->get();
					if ($sql_query->num_rows() > 0) {
						$delivery_boy = $sql_query->row_array();

						return $delivery_boy['first_name'].' '.$delivery_boy['last_name'];
					}
				}else{
					return '';
				}

			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";

				if($row['order_type'] == 1 || $row['order_type'] == 3){

					/* if($row['status'] == 2 || $row['status'] == 5 || $row['status'] == 7){
						if($row['delivery_boy_id'] != 0){
							$option .= '<button type="button" id="1'.$id.'" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-2 reassign" onclick="assign_delivery_boy('.'1'.$id.','.$row['delivery_boy_id'].')" data-toggle="modal" data-target="#exampleModal" >Reassign</button>';
						}else{
							$option .= '<button type="button" id="1'.$id.'" data-id ="'.$id.'"  class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-2" onclick="assign_delivery_boy('.'1'.$id.')" data-toggle="modal" data-target="#exampleModal" >Assign</button>';
						}
					} */
				}else{
					$option .= '<button onclick="completed(this);" data-alcoholic="'.$row['alcoholic'].'" data-id="' . $id . '"  data-userid="' . $row['user_id'] . '" class="btn btn-success btn-sm waves-effect waves-light mr-1 reassign">Complete</button>';
				}
				// $option .= '<button type="button" id="'.$id.'" data-id ="'.$id.'" data-userid ="'.$row['user_id'].'"  data-deliveryid ="'.$row['delivery_boy_id'].'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1" onclick="send_push_notification('.$id.')" data-toggle="modal" data-target="#sendpush" >Push</button>';

				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				return $option;
				

			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		// $data['delivery_boy'] = $this->Order_model->list_delivery_boy();
		$data['user'] = User::all();

        $this->breadcrumbs->push($this->title , 'vendor/order-inprocess');
        $this->breadcrumbs->push('In Process', '/', true);

		$this->vendor_render('order/index',$data);	
	}

	public function order_inprocess_view($id = ''){
		$data['title'] = $this->title;
		$data['back_url'] = base_url('vendor/order-inprocess');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$data['order'] = $orders;
			$data['order_products'] = $order_products;

		}

		if(!isset($data['order_products']) && count($data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('vendor/order-arrived'));
		}

        $this->breadcrumbs->push($this->title , 'vendor/order-inprocess');
        $this->breadcrumbs->push('In Process', 'vendor/order-inprocess');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->vendor_render('order/view',$data);
	}

	public function assign_delivery_boy(){
		$request = $_POST;

		// $result = $this->Order_model->assign_delivery_boy($request);

		// echo "<pre>";
		// print_r($result);
		// exit;
		if($result['status'] == TRUE){
			$this->session->set_flashdata('success',$result['message']);
		}else{
			$this->session->set_flashdata('error',$result['message']);
		}
		
		redirect(base_url('vendor/order-inprocess'),'refresh'); 
	}

	public function order_rejected_index() {
		$login_id = $this->session->userdata['vendor']['user_id'];
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name', false)->from('orders')->join('users','orders.user_id = users.id')->where('orders.deleted_at', NULL)->where('orders.status',3)->where('vendor_id',$login_id);
		$action['view'] = base_url('vendor/order-rejected/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Reason', 'status', function ($status, $row){
	
				return '<span class="badge badge-danger">Store Rejected</span>';
				
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'vendor/order-rejected');
        $this->breadcrumbs->push('Rejected', '/', true);
		$this->vendor_render('order/index',$data);	
	}

	public function order_rejected_view($id = ''){
		$data['title'] = $this->title;
		$data['back_url'] = base_url('vendor/order-rejected');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$data['order'] = $orders;
			$data['order_products'] = $order_products;

		}

		if(!isset($data['order_products']) && count($data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('vendor/order-arrived'));
		}

        $this->breadcrumbs->push($this->title , 'vendor/order-rejected');
        $this->breadcrumbs->push('Rejected', 'vendor/order-rejected');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->vendor_render('order/view',$data);
	}

	public function order_completed_index() {
		$login_id = $this->session->userdata['vendor']['user_id'];
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name', false)->from('orders')->join('users','orders.user_id = users.id')->where('orders.deleted_at', NULL)->where('orders.status',10)->where('vendor_id',$login_id);
		$action['view'] = base_url('vendor/order-completed/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'vendor/order-completed');
        $this->breadcrumbs->push('Completed', '/', true);
		$this->vendor_render('order/index',$data);	
	}

	public function order_completed_view($id = ''){
		$data['title'] = $this->title;
		$data['back_url'] = base_url('vendor/order-completed');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$data['order'] = $orders;
			$data['order_products'] = $order_products;

		}

		if(!isset($data['order_products']) && count($data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('vendor/order-arrived'));
		}

        $this->breadcrumbs->push($this->title , 'vendor/order-completed');
        $this->breadcrumbs->push('Completed', 'vendor/order-completed');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->vendor_render('order/view',$data);
	}

	public function all_orders_index() {
		$login_id = $this->session->userdata['vendor']['user_id'];
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name', false)->from('orders')->join('users','orders.user_id = users.id')->where('orders.deleted_at', NULL)->where('vendor_id',$login_id);
		$action['view'] = base_url('vendor/all-orders/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			})  
			 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Status', 'status',function($status,$row){

				if($status == 1){
                    return '<span class="badge badge-primary">Pending</span>';
                }else if($status == 2){
                    return '<span class="badge badge-info">Accepted by store</span>';
                }else if($status == 3){
                    return '<span class="badge badge-danger">Rejected by store</span>';
                }else if($status == 4){
                    return '<span class="badge badge-danger">Cancelled</span>';
                }else if($status == 5){
                    return '<span class="badge badge-info">Assign Delivery Person</span>';
                }else if($status == 6){
                    return '<span class="badge badge-info">Accepted by Delivery Person</span>';
                }else if($status == 7){
                    return '<span class="badge badge-danger">Rejected by Delivery Person</span>';
                }else if($status == 8){
                    return '<span class="badge badge-info">Reached at restaurant</span>';
                }else if($status == 9){
                    return '<span class="badge badge-info">On the Way</span>';
                }else if($status == 10){
                    return '<span class="badge badge-success">Delivered</span>';
                }else{
                    return ' ';
                }

			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				if($row['status'] == 1 || $row['status'] == 2 || $row['status'] == 5 || $row['status'] == 6 || $row['status'] == 7 || $row['status'] == 8 || $row['status'] == 9  ){
					//$option .= '<button type="button" id="'.$id.'" data-id ="'.$id.'" data-userid ="'.$row['user_id'].'"  data-deliveryid ="'.$row['delivery_boy_id'].'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1" onclick="send_push_notification('.$id.')" data-toggle="modal" data-target="#exampleModal" >Push</button>';
				}
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$data['user'] = User::all();
        $this->breadcrumbs->push($this->title , 'vendor/all-orders');
        $this->breadcrumbs->push('All Orders', '/', true);
		$this->vendor_render('order/index',$data);	
	}
	public function send_push_notification(){  
		$request = $_POST;

		$result =[];
		$user = User::where("id",$request['user'])->first();
        if (isset($user)){
            $user = $user->toArray();
            $user['order_id'] = $request['order_id_push'];
			
			$device = Devices::where('user_id',$request['user'])->first();

			$push_title = 'Order #'.$request['order_id_push'].' : '. $request['title'];
			$push_data_msg = 'Hey '.$user['first_name'].'! ';
			$push_data_msg .= $request['description'];
			
			$push_type = 'order_detail';

			$push_data = array(
				'order_id' => $request['order_id_push'],
				'message' => $push_data_msg
			);

			if($user['notification_status'] == 1 && $device['token'] != ''){
				$SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
			}
			// notification_add($request['user'],$push_data_msg,$push_type,0,$request['order_id'],0);


            
            $result['status'] = TRUE;
            $result['message'] = 'Notification send successfully.';
        }else{
            $result['status'] = false;
            $result['message'] = 'User not found.';
        }

		// $result = $this->Order_model->send_push_notification($request);
		if($result['status'] == TRUE){
			$this->session->set_flashdata('success',$result['message']);
		}else{
			$this->session->set_flashdata('error',$result['message']);
		}
		 
		redirect($request['url'],'refresh'); 
	}

	public function all_orders_view($id = ''){
		$data['title'] = $this->title;
		$data['back_url'] = base_url('vendor/all-orders');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$data['order'] = $orders;
			$data['order_products'] = $order_products;

		}

		if(!isset($data['order_products']) && count($data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('vendor/order-arrived'));
		}

        $this->breadcrumbs->push($this->title , 'vendor/all-orders');
        $this->breadcrumbs->push('All Orders', 'vendor/all-orders');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->vendor_render('order/view',$data);
	}

}
?>
