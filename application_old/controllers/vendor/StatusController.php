<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class StatusController extends MY_Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->helper('stripe_helper');
		$this->load->model('User');
		$this->load->model('Category');
		$this->load->model('VendorAvailibility');
		$this->load->model('Vendor');
		$this->load->model('ion_auth_model');
		$this->load->model('Orders');
		$this->load->library(['ion_auth', 'form_validation']);
		
		$this->title = 'Restaurants';
	}
	public function vendor_active($id){
		$details = (array)$this->db->get_where('users',array('id'=>$id))->row();
      	$data['active'] = '1';
        $this->db->update('users',$data,array('id' => $id));
        redirect('vendor/login');
	}
}
?>