<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VariantController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		$this->load->model('Variant');

		$this->title = 'Variant';
	}

	public function index()
	{
		
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('variant.*', false)->from('variant')->where('deleted_at', NULL);
		$action['edit'] = base_url('vendor/variant/edit/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Variant Name', 'name',function($name,$row){

				return ucfirst(htmlentities($name));

			})
			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="variant" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="variant" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">Edit</a>';
				$option .= " <button type='button' data-table='variant' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('id,name');
		$dt_data->datatable('variant');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'vendor/variant');
		$this->vendor_render('variant/index',$data);	
	}
	public function isexists_variant($str = NULL, $id = NULL) {

			$this->db->select('*');
			if ($id != '') {
				$this->db->where('id !=', $id);
			}
			$this->db->where('name', $str);
			$this->db->where('deleted_at', NULL);
			$this->db->from('variant');
			$sql_query = $this->db->get();
			if ($sql_query->num_rows() > 0) {
				$this->form_validation->set_message('isexists_variant', "This name field is already exists.");
				return FALSE;
			} else {
				return TRUE;
			}
	}
	public function add(){

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				
				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					array('field' => 'name', 'label' => 'name', 'rules' => 'trim|required|min_length[2]|max_length[50]|callback_isexists_variant')
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){

					$request = $this->input->post();
                	$user_data['name'] = $request['name'];
                	Variant::insert($user_data);

					$this->session->set_flashdata('success','Variant added successfully');
					redirect(base_url("vendor/variant"));

				}
			}
		}
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'vendor/variant');
        $this->breadcrumbs->push('Add', '/', true);

		$this->vendor_render('variant/post', $data);
	}
	public function edit($id = ''){
		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){

				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					
					array('field' => 'name', 'label' => 'name', 'rules' => 'trim|required|min_length[2]|max_length[50]|callback_isexists_variant[' . $this->input->post("id") . ']')
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {

                    	$request = $this->input->post();
                    	 //_pre(FCPATH.$request["old_img"]);

                    	$user_data['name'] = $request['name'];
                    	Variant::whereId($id)->update($user_data);

                    	$this->session->set_flashdata('success', 'Variant updated successfully');
						redirect(base_url('vendor/variant'));

				} 
			}
		}
		$output_data['title'] = $this->title;
		$output_data['variant_data'] = Variant::find($id);
		$this->breadcrumbs->push($this->title , 'vendor/variant');
        $this->breadcrumbs->push('Edit', '/', true);
         if(!isset($output_data['variant_data']) && count($output_data['variant_data']) == 0){
			$this->session->set_flashdata('error', 'No any variant found!');
			redirect(base_url('vendor/variant'));
		}
		// _pre($output_data['variant_data']);
		$this->vendor_render('variant/put', $output_data);
	}

}
?>
