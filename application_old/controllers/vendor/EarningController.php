<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EarningController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
        
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		
        $this->load->model('User');

        $this->load->model('Orders');
        $this->load->model('OrderProductAddOn');
        $this->load->model('Devices');
        $this->load->model('OrderProducts');
		$this->title = 'Earning';
		
	}

	public function index() {
		$login_id = $this->session->userdata['vendor']['user_id'];
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name', false)->from('orders')->join('users','orders.user_id = users.id')->where('orders.deleted_at', NULL)->where('orders.status',10)->where('vendor_id',$login_id);
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Order Id', 'id')
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Revenue', 'store_amount',function($store_amount,$row){

				return "$ ".$store_amount;

			})
			->column('Tip', 'vendor_tip',function($vendor_tip,$row){
				if($vendor_tip != 0){
					return "$ ".$vendor_tip;
				}else{
					return "-";
				}

			})
			
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Status', 'store_withdraw_id',function($store_withdraw_id,$row){
				if($store_withdraw_id != 0){
					return '<span class="badge badge-success">Paid</span>';
				}else{
					return '<span class="badge badge-warning">Pending</span>';
				}

			})
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'vendor/earning');
		$this->vendor_render('earning/index',$data);	
	}


}
?>
