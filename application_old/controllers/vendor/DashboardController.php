<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		//echo "ee";echo '<pre>'; print_r($this->session->all_userdata());exit;
		if (!$this->ion_auth->is_vendor()) {
			redirect('/vendor/login');
		}
		$this->load->model('Product');
		$this->load->model('Orders');
		$this->title = 'Dashboard';
	}

	public function index(){
		// echo '<pre>'; print_r($this->session->all_userdata());exit;
		$id = $this->session->userdata['vendor']['user_id'];
        $this->load->library('breadcrumbs');
        $data['title'] = $this->title;
        $data['total_products'] = Product::where('user_id',$id)->count();
		
		
		$net_profit = $revenue = 0;
        $orders = Orders::where('status',10)->where('vendor_id',$id)->get();
        if(isset($orders)){
            $orders = $orders->toArray();
           
            foreach ($orders as $key => $value) {
                $net_profit += $value['admin_amount'];
                $revenue += $value['total_amount'];
            }
        }
        // $vendor_channels_data = $this->db->get_where('vendor_channels')->result_array();
        // $description = "This is a testing notification for web push";
        // $title = "Test Notification";
        // $player_ids = array_column($vendor_channels_data, 'channel_id');
        // $send_web_push = send_web_push($player_ids, $description, $title);


        $data['net_profit'] = $net_profit;
        $data['revenue'] = $revenue;
        $data['total_orders'] = count($orders);

        $this->vendor_render('dashboard', $data);    
    }

}