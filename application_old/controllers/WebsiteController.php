<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebsiteController extends CI_Controller {

public $data = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language']);

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        // $this->load->model('VendorChannels');

    }

	// function __construct() {
	// 	header('Access-Control-Allow-Origin: *');
	// 	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	// 	parent::__construct();
	// 	$this->load->model('PaymentCard');
	// 	$this->load->library(['ion_auth', 'form_validation']);

	// }

	public function index()
	{
        $data = array();
		// echo "<pre>";
        // print_r($_SESSION);
        // exit;
        $this->data['title'] = $this->lang->line('login_heading');

        // validate form input
        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');
        if ($this->form_validation->run() === TRUE)
        {
            // check to see if the user is logging in
            // check for "remember me"form_checkbox

            $role = $this->uri->segment(1);

            $remember = (bool)$this->input->post('remember');

            $role_id = 3;

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $role_id, $remember = 0))
            {
  
                // _pre($_SESSION);
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('events');
            }
            else
            {
                // if the login was un-successful
                // redirect them back to the login page
               // echo "string"; exit();
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                web_render("login", $data);

                // redirect($role.'/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        }
        else
        {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['user_type'] = "Users";

            web_render("login", $data);

            //$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'login', $this->data);
        }
        
		// web_render("index", $data);
	}

	public function add_channel(){
        if(isset($_POST['channel_id']) && $_POST['channel_id'] != ''){
            $user_id = $this->session->userdata()['vendor']['user_id'];
            $this->db->select('id, channel_id');
            $this->db->where('vendor_id',$user_id);
            $this->db->where('channel_id',$_POST['channel_id']);
            $this->db->from('vendor_channels');
            $sql_query = $this->db->get();
            if ($sql_query->num_rows() > 0){
                $data = $sql_query->row_array();
                if($data['channel_id'] != $_POST['channel_id']){                        
                    $data = array(
                        'vendor_id' => $user_id,
                        'channel_id' => $_POST['channel_id'],                       
                    );
                    $this->db->insert('vendor_channels', $data);
                }
                // else
                // {
                //  $data = array('channel_id' => $_POST['channel_id']);
                //  $this->db->where('vendor_id', $user_id);
                //  $this->db->update('vendor_channels', $data);
                // }
                
            }else{
                $data = array(
                    'vendor_id' => $user_id,
                    'channel_id' => $_POST['channel_id'],
                );
                $this->db->insert('vendor_channels', $data);
            }

            echo json_encode(array("is_success" => true, "post" => $_POST, 'restore' => TRUE));
            // }

        }else{
            echo json_encode(array("is_success" => false, "post" => $_POST));
        }
    }
	public function add_channel_admin(){
        if(isset($_POST['channel_id']) && $_POST['channel_id'] != ''){
            $user_id = $this->session->userdata()['admin']['user_id'];
            $this->db->select('id, channel_id');
            $this->db->where('vendor_id',$user_id);
            $this->db->where('channel_id',$_POST['channel_id']);
            $this->db->from('vendor_channels');
            $sql_query = $this->db->get();
            if ($sql_query->num_rows() > 0){
                $data = $sql_query->row_array();
                if($data['channel_id'] != $_POST['channel_id']){                        
                    $data = array(
                        'vendor_id' => $user_id,
                        'channel_id' => $_POST['channel_id'],                       
                    );
                    $this->db->insert('vendor_channels', $data);
                }
                // else
                // {
                //  $data = array('channel_id' => $_POST['channel_id']);
                //  $this->db->where('vendor_id', $user_id);
                //  $this->db->update('vendor_channels', $data);
                // }
                
            }else{
                $data = array(
                    'vendor_id' => $user_id,
                    'channel_id' => $_POST['channel_id'],
                );
                $this->db->insert('vendor_channels', $data);
            }

            echo json_encode(array("is_success" => true, "post" => $_POST, 'restore' => TRUE));
            // }

        }else{
            echo json_encode(array("is_success" => false, "post" => $_POST));
        }
    }

}
