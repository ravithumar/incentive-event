<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
error_reporting(0);

class StripeAccount extends REST_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->helper('stripe_helper');
        $this->load->model('User');
        $this->load->model('ion_auth_model');
        $this->load->library(['ion_auth', 'form_validation']);

    }
    public function customAlpha($str) {
        if($str != ''){
            if (!preg_match('/^[a-z \-]+$/i', $str)) {
                $this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
                return false;
            }
        }
        
        return TRUE;
    }
    public function customRouting($str) {
        if($str != ''){
            if (!preg_match('/^\d{4}-\d{3}$/', $str)) {
                $this->form_validation->set_message('customRouting', 'The {field} field contain invalid format.');
                return false;
            }
        }
        
        return TRUE;
    }
     public function validateDateFormat($str) {
        if($str != ''){
            if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$str)) {
                $this->form_validation->set_message('validateDateFormat', 'The {field} field contain invalid date format.');
                return false;
            }
            $now =  date("Y-m-d");
            if($str >= $now ){
                $this->form_validation->set_message('validateDateFormat', 'The {field} field contain invalid date.');
                return false;
            }
        }
        return TRUE;
    }
    public function add_account_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('dob', 'date of birth', 'trim|required|callback_validateDateFormat');
        $this->form_validation->set_rules('city', 'city', 'trim|required');
        $this->form_validation->set_rules('state', 'state', 'trim|required');
        $this->form_validation->set_rules('line1', 'address 1', 'trim|required');
        $this->form_validation->set_rules('line2', 'address 2', 'trim|required');
        $this->form_validation->set_rules('postal_code', 'zip code', 'trim|required');
        $this->form_validation->set_rules('routing_number', 'routing number', 'trim|required');
        $this->form_validation->set_rules('account_number', 'account number', 'trim|required');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $request = array_map('trim', $request);
            $user = User::where('id',$request['user_id'])->where('account_id','')->first();
            
            if(count($user) == 1){
                
                $time=strtotime($request['dob']);
                $month=date("m",$time);
                $year=date("Y",$time);
                $day=date("d",$time);

                $info = [
                    'type' => 'custom',
                    'country' => 'CA',
                    'email' => $request['email'],
                    'business_type' => 'individual',
                    'individual' => [
                        'dob' => [
                            'day' => $day,
                            'month' => $month,
                            'year' => $year
                        ],
                       'first_name' => $user['first_name'],
                       'last_name' => $user['last_name'],
                       'address' => [
                            'city' => $request['city'],
                            'country' =>  'CA',
                            'line1' =>  $request['line1'],
                            'line2' =>  $request['line2'],
                            'state' => $request['state'],
                            'postal_code' =>  $request['postal_code']
                       ]
                    ],
                    'capabilities' => [
                        'card_payments' => ['requested' => true],
                        'transfers' => ['requested' => true]
                    ],
                    'external_account' => [
                        'object' => 'bank_account',
                        'country' => 'CA',
                        'currency' => 'CAD',
                        'routing_number' =>  $request['routing_number'],
                        'account_number' =>  $request['account_number']
                    ],
                    'tos_acceptance' => [
                        'date' => time(),
                        'ip' => $_SERVER['REMOTE_ADDR']
                    ]
                ];
                
                $result = createCustomAccount($info);

                if($result['status'] == TRUE){

                    $update_data = array('account_id' => $result['success']['id']);
                    User::where('id', $request['user_id'])->update($update_data);
                    $response['account_id'] = $result['success']['id'];
                    $response['status'] = TRUE;
                    $response['message'] = 'Bank account added successfully.';
                }else{
                    $response['status'] = false;
                    $response['message'] = $result['error']['message'];
                }

            }else{
                $response['status'] = false;
                $response['message'] = 'Bank account detail already added.';
            }

            
            
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function get_account_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $accounts = [];

            $user = User::where('id',$request['user_id'])->where('account_id','!=','')->first();
            
            // $result = deleteCustomer($user->account_id);
            $result = getCustomAccount($user->account_id);
            // echo json_encode($result, JSON_UNESCAPED_UNICODE);
            // exit;

            if($result['status'] == TRUE){
                $connected_account = array(
                    'account_id' => $result['success']['id'],
                    'email' => $result['success']['email'],
                    'dob' => $result['success']['individual']['dob']['year'].'-'.$result['success']['individual']['dob']['month'].'-'.$result['success']['individual']['dob']['day'],

                    'city' => $result['success']['company']['address']['city'],
                    'country' => $result['success']['company']['address']['country'],
                    'line1' => $result['success']['company']['address']['line1'],
                    'line2' => $result['success']['company']['address']['line2'],
                    'postal_code' => $result['success']['company']['address']['postal_code'],
                    'state' => $result['success']['company']['address']['state'],
                    'routing_number' => $result['success']['external_accounts']['data'][0]['routing_number'],
                    'account_number' =>  $result['success']['external_accounts']['data'][0]['last4'],
                    'bank_name' =>  $result['success']['external_accounts']['data'][0]['bank_name']
                );
                $response['data'] = $connected_account;
                $d_o_b = $connected_account['dob'];
                $dob = date('Y-m-d', strtotime(str_replace('-', '-', $d_o_b)));
                $response['data']['dob'] = $dob;


                $response['status'] = TRUE;
            }else{
                $response['status'] = false;
                $response['message'] = 'No any account found';
            }
            
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    /*edit bank account start*/
     public function edit_account_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('dob', 'date of birth', 'trim|required|callback_validateDateFormat');
        $this->form_validation->set_rules('city', 'city', 'trim|required');
        $this->form_validation->set_rules('state', 'state', 'trim|required');
        $this->form_validation->set_rules('line1', 'address 1', 'trim|required');
        $this->form_validation->set_rules('line2', 'address 2', 'trim|required');
        $this->form_validation->set_rules('postal_code', 'zip code', 'trim|required');
        $this->form_validation->set_rules('routing_number', 'routing number', 'trim|required');
        $this->form_validation->set_rules('account_number', 'account number', 'trim|required');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $request = array_map('trim', $request);
            $user = User::where('id',$request['user_id'])->first();
            
            if(count($user) == 1){
                
                $time=strtotime($request['dob']);
                $month=date("m",$time);
                $year=date("Y",$time);
                $day=date("d",$time);

                $info = [
                    'type' => 'custom',
                    'country' => 'CA',
                    'email' => $request['email'],
                    'business_type' => 'individual',
                    'individual' => [
                        'dob' => [
                            'day' => $day,
                            'month' => $month,
                            'year' => $year
                        ],
                       'first_name' => $user['first_name'],
                       'last_name' => $user['last_name'],
                       'address' => [
                            'city' => $request['city'],
                            'country' =>  'CA',
                            'line1' =>  $request['line1'],
                            'line2' =>  $request['line2'],
                            'state' => $request['state'],
                            'postal_code' =>  $request['postal_code']
                       ]
                    ],
                    'capabilities' => [
                        'card_payments' => ['requested' => true],
                        'transfers' => ['requested' => true]
                    ],
                    'external_account' => [
                        'object' => 'bank_account',
                        'country' => 'CA',
                        'currency' => 'CAD',
                        'routing_number' =>  $request['routing_number'],
                        'account_number' =>  $request['account_number']
                    ],
                    'tos_acceptance' => [
                        'date' => time(),
                        'ip' => $_SERVER['REMOTE_ADDR']
                    ]
                ];
                
                $result = createCustomAccount($info);

                if($result['status'] == TRUE){

                    $update_data = array('account_id' => $result['success']['id']);
                    User::where('id', $request['user_id'])->update($update_data);
                    $response['account_id'] = $result['success']['id'];
                    $response['status'] = TRUE;
                    $response['message'] = 'Bank account edited successfully.';
                }else{
                    $response['status'] = false;
                    $response['message'] = $result['error']['message'];
                }

            }else{
                $response['status'] = false;
                $response['message'] = 'Bank account edited successfully.';
            }

            
            
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    /*edit bank account end*/


    public function state_get()
    {
        $state = $this->db->get('state')->result_array();
        if(!empty($state))
        {
            $response['data'] = $state;
            $response['status'] = TRUE;
        }
        else
        {
            $response['status'] = false;
            $response['message'] = 'No any state found';
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        $err_array[0] = str_replace("\n","",$err_array[0]);
        return $err_array[0];
    }
}
?>