<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class Auth extends REST_Controller {

	function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Devices');
        $this->load->model('Wallet');
		$this->load->model('ion_auth_model');
        $this->load->model('Token');
        $this->load->model('System_setting');

		$this->load->library(['ion_auth', 'form_validation']);

	}

    public function init_get($app_version = '',$type = '', $user_id = ''){

        $postFields['app_version'] = $app_version;        
        $type = ($type == 'ios')?'delivery_boy_ios':'delivery_boy_android';               
        $postFields['type'] = $type;                  
        $errorPost = $this->ValidatePostFields($postFields);
        if(empty($errorPost)){

            $MaintenanceMode = (array)$this->db->get_where('appsetting',array('app_name' => 'maintenance_mode'))->row();
            $AppVersion = (array)$this->db->get_where('appsetting',array('app_name' => $type))->row();
            $current_version = (Int)str_replace('.', '',$AppVersion['app_version']);
            $app_version = (Int)str_replace('.', '', $app_version);
            $delivery_person_response_time = System_setting::select('value')->where('path','delivery_person_response_time')->first();
            $response['delivery_person_response_time'] = $delivery_person_response_time['value'];
            if($MaintenanceMode['updates'] == 1){
                $response['status'] = false;
                $response['state'] = 2;
                // $response['update'] = false;
                // $response['maintenance'] = true;
                $response['message'] = 'Server under maintenance, please try again after some time';
            }

            else if($app_version < $current_version && $AppVersion['updates'] == 0){
                $response['status'] = true;
                $response['state'] = 0;

                // $response['update'] = true; 
                // $response['maintenance'] = false;
                $response['message'] = 'RAD delivery person app new version available';
            }

            else if($app_version < $current_version && $AppVersion['updates'] == 1){
                $response['status'] = false;
                $response['state'] = 1;

                // $response['update'] = true;
                // $response['maintenance'] = false;
                $response['message'] = 'RAD delivery person app new version available, please upgrade your application';
            }
            else{
                $response['status'] = true;
                $response['state'] = 3;

                // $response['update'] = false;
                // $response['maintenance'] = false;
                $response['message'] = '';
            }

            if($response['status'] == TRUE && $user_id != ''){

                $user = User::whereId($user_id)->where("active",1)->first();

                if (isset($user) && count($user) > 0){

                    // update last activity time in users table
                    /* $current_datetime = new DateTime("now");
                    $update_data = array('last_active_at' => $current_datetime->format('Y-m-d H:i:s'));
                    User::whereId($user_id)->update($update_data); */

                    $response['user'] = $user;
                    $response['status'] = true;
                }else{
                    $response['status'] = true;
                    $response['blocked'] = true;
                    $response['message'] = 'RAD delivery person app new version available';
                }
            }

        }else{
            $response['status'] = false;
            $response['message'] = $errorPost;
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function login_post(){

        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
        $this->form_validation->set_rules('device_name', 'device name', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim|required');
        $this->form_validation->set_rules('lng', 'longitude', 'trim|required');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{

            $request = $this->post();
            $user = User::select('users.*')->where(array('users.phone'=> $request['phone'], "users_groups.group_id" => "4"))->join('users_groups', 'users_groups.user_id', 'users.id')
            ->join('groups', 'users_groups.group_id', 'groups.id')->first();

            // $this->db->select('users.*');
            // $this->db->from('users');
            // $this->db->join('users_groups', 'users_groups.user_id = users.id');
            // $this->db->join('groups', 'users_groups.group_id = groups.id');
            // $this->db->where('users.phone', $request['phone']);
            // $user = $this->db->get()->row_array();
                // _pre($request);
            if(isset($user) && !empty($user)){
                $user = $user->toArray();
                if($user['active'] == '1'){
                    $check = $this->ion_auth->api_login($request['phone'], $request['password'], $request['device_token'], $request['device_type'], $request['lat'], $request['lng'], 4, FALSE);
                    
                    if ($check){
                        $check = (array)$check;
                        $messages = $this->ion_auth->messages();

                        $messages = str_ireplace('<p>', '', $messages);
                        $messages = str_ireplace('</p>', '', $messages); 

                        $response['message'] = $messages;

                        $user['api_key'] = $this->_generate_key($check['id']);
                        $d_id = $user['id'];

                        $delivery_average = $this->db->query('SELECT AVG(delivery_boy_rate) as rating FROM orders WHERE delivery_boy_id = '.$d_id.' AND delivery_boy_rate > "0" ')->row_array();
                        $delivery_boy_rating  =  $delivery_average['rating'];
                       
                        if($delivery_boy_rating != null){
                            $user['avg_rating'] = number_format((float)$delivery_boy_rating, 1, '.', '');
                        } else{
                            $user['avg_rating'] = "0";
                        }

                        /*$user['duty'] = 0;*/
                        $user_data = array(
                            'latitude' => $request['lat'],
                            'longitude' => $request['lng'],
                           /* 'duty' => 0,*/
                        );

                        User::where('id',$check['id'])->update($user_data);
                        $check_device = Devices::where('user_id', $check['id'])->first();
						/* _pre($check_device);  */
                        if($check_device->token != '' && $check_device->token != $request['device_token']){
                            $push_title = 'logout';
                            $push_data = array(
                                'user_id' => $check['id'],
                                'message' => 'logout'
                            );
                            $push_type = 'silent';

                            $SendNotification = SendNotification($check_device->type,$check_device->token, $push_title, $push_data, $push_type);
                        }
                        if (isset($check_device)) {
                            $check_device->token = $request['device_token'];
                            $check_device->type = $request['device_type'];
                            $check_device->name = $request['device_name'];
                            $check_device->lat = $request['lat'];
                            $check_device->lng = $request['lng'];
                            $check_device->update();
                        } else {
                            $device = new Devices;
                            $device->user_id = $check['id'];
                            $device->token = $request['device_token'];
                            $device->type = $request['device_type'];
                            $device->name = $request['device_name'];
                            $device->lat = $request['lat'];
                            $device->lng = $request['lng'];
                            $device->save();
                        }
                        

                        $response['status'] = TRUE;
                        $response['check_device'] = $check_device;
                        $response['request'] = $request['device_token'];
                        $response['SendNotification'] = $SendNotification;
                        $response['user'] = $user;
                    }else{
                        $errors = $this->ion_auth->errors();
                        // $this->response($user);
                        $errors = str_ireplace('<p>', '', $errors);
                        $errors = str_ireplace('</p>', '', $errors);    

                        $response['message'] = $errors;
                        $response['status'] = FALSE;
                    }
                }else if($user['active'] == '0'){
                    $response['message'] = 'Your account is not activated yet!.';
                    $response['status'] = FALSE;
                }else if($user['active'] == '2'){
                    $response['message'] = 'Your account is deactivated by admin!.';
                    $response['status'] = FALSE;
                }else if($user['active'] == '3'){
                    $response['message'] = 'Your account activation request has been rejected by admin.';
                    $response['status'] = FALSE;
                }else{
                    $response['message'] = 'Something went wrong';
                    $response['status'] = FALSE;
                }
                
            }else{
                $response['message'] = 'The phone number or password you have entered is incorrect.';
                $response['status'] = FALSE;
            }
            
            

        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function email_exist_check($str){
        if($str != ''){
            $user = User::select('users.*')->where('users.email', $str)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[4])
            ->first();
            if (count($user) == 0) {
                return TRUE;
            }
            $this->form_validation->set_message('email_exist_check', "Email is already exist.");
            return FALSE;
        }else{
            return TRUE; 
        }
       
    }

    function phone_exist_check($str) {
        if (!preg_match('/^\((\d)\d{2}\) \d{3}-\d{4}$/', $str)) {
            $this->form_validation->set_message('phone_exist_check', 'The {field} field is invalid.');
            return false;
        }else{
            $user = User::select('users.*')->where('users.phone', $str)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[4])
            ->first();
            if (count($user) == 0) {
                return TRUE;
            }
            $this->form_validation->set_message('phone_exist_check', "Phone number is already exist.");
            return FALSE;
        }
    }

    function can_use_phone_check_post(){
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|callback_email_exist_check');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            
            $user = User::select('users.*')->where('users.phone', $request['phone'])
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[4])
                ->first();
            if (count($user) == 0) {
                $otp = generate_OTP();
                $msg = 'Your RAD OTP is '.$otp.'.';
                send_SMS($request['phone'],$msg);
                $response['otp'] = $otp;
				$this->db->insert("sended_otp", array("type" => "driver", "phone" => $_POST['phone'], "otp" => $otp));
                $response['status'] = true;
                $response['message'] = 'User can use this phone number';
            }else{
                $response['status'] = false;
                $response['message'] = 'Phone number is already exist.';
            }

            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function register_post()
    {

        /*$req_dump = print_r($_FILES, TRUE);
        $fp = fopen('data.php', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);

        $response['status'] = TRUE;
        $response['message'] = 'Printed in file';

        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;*/
        $this->form_validation->set_rules('first_name', 'first name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'last name', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|callback_email_exist_check');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required|callback_phone_exist_check');
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
        $this->form_validation->set_rules('device_name', 'device name', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim|required');
        $this->form_validation->set_rules('lng', 'longitude', 'trim|required');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{


            $request = $this->post();

            $req_dump = print_r($_FILES, TRUE);
            $fp = fopen('data.php', 'a');
            fwrite($fp, $req_dump);
            fclose($fp);

            
            $phone = $request['phone'];
            $password = $request['password'];

            $user_data = array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],

                'phone' => $request['phone'],
                'latitude' => $request['lat'],
                'longitude' => $request['lng'],
            );
            if(isset($request['email'] )){
                $email = $request['email'];
            }else{
                $email = '';
            }

            $file_upload = FALSE;
            if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0){
                
                $img_name = 'user_' . time().rand(1000, 9999).'.jpg';
                $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
                if(compress($_FILES['profile_picture']['tmp_name'], $destination_url, 30)){
                    $user_data['profile_picture'] = $img_name;
                    $file_upload = TRUE;
                }else{
                    $file_upload = FALSE;
                    $response['status'] = false;
                    $response['message'] = 'Unable to upload profile picture';
                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                    return TRUE;
                }

            }else{
                $response['status'] = false;
                $response['message'] = 'Profile picture is required1';
                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                return TRUE;
            }

            if (isset($_FILES['driving_license']) && !empty($_FILES['driving_license']) && strlen($_FILES['driving_license']['name']) > 0){
                
                $img_name = 'driving_license_' . time().rand(1000, 9999).'.jpg';
                $destination_url = FCPATH . $this->config->item("document").$img_name;
                if(compress($_FILES['driving_license']['tmp_name'], $destination_url, 30)){
                    $user_data['driving_license'] = $img_name;
                    $file_upload = TRUE;
                }else{
                    $file_upload = FALSE;
                    $response['status'] = false;
                    $response['message'] = 'Unable to upload driving license';
                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                    return TRUE;
                }

            }else{
                $response['status'] = false;
                $response['message'] = 'Driving license is required';
                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                return TRUE;
            }

            if (isset($_FILES['insurance_certi']) && !empty($_FILES['insurance_certi']) && strlen($_FILES['insurance_certi']['name']) > 0){
                
                $img_name = 'insurance_certi_' . time().rand(1000, 9999).'.jpg';
                $destination_url = FCPATH . $this->config->item("document").$img_name;
                if(compress($_FILES['insurance_certi']['tmp_name'], $destination_url, 30)){
                    $user_data['insurance_certi'] = $img_name;
                    $file_upload = TRUE;
                }else{
                    $file_upload = FALSE;
                    $response['status'] = false;
                    $response['message'] = 'Unable to upload insurance certificate';
                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                    return TRUE;
                }

            }else{
                $response['status'] = false;
                $response['message'] = 'Insurance certificate is required';
                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                return TRUE;
            }

           
            $id = $this->ion_auth->register($phone, $password, $email, $user_data, [4]);

            //$this->response($id);
            if(isset($id) && !is_null($id) && !$id == false){

                //Wallet Balance
                $wallet['user_id'] = $id;
                $wallet['balance'] = '0';
                Wallet::insert($wallet);

                // OTP Send

                Devices::insert(['user_id' => $id, 'name' => $request['device_name'], 'token' => $request['device_token'], 'type' => $request['device_type'], 'lat' => $request['lat'],'lng' => $request['lng'] ]);

                $user = User::find($id)->toArray();
                $user['api_key'] = $this->_generate_key($id);
                unset($user['password']);
                $response['status'] = TRUE;
                $response['user'] = $user;
                $response['message'] = 'Registered successfully';
            }else{
                $response['status'] = false;
                $errors = $this->ion_auth->errors();

                $errors = str_ireplace('<p>', '', $errors);
                $errors = str_ireplace('</p>', '. ', $errors);    

                $response['message'] = $errors;

                //$response['message'] = 'something went wrong';
            }
            
                                                     
        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function upload_docs_post()
    {
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{


            $request = $this->post();

            $user = User::select('users.*')->where('users.id', $request['user_id'])
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[4])
                ->get();

                // $this->response($user);
            if (count($user) == 1){


                if (isset($_FILES['driving_license']) && !empty($_FILES['driving_license']) && strlen($_FILES['driving_license']['name']) > 0){
                   
                    $img_name = 'driving_license_' . time().rand(1000, 9999).'.jpg';
                    $destination_url = FCPATH . $this->config->item("document").$img_name;
                    if(compress($_FILES['driving_license']['tmp_name'], $destination_url, 30)){
                        $driving_license = $img_name;

                        if (isset($_FILES['insurance_certi']) && !empty($_FILES['insurance_certi']) && strlen($_FILES['insurance_certi']['name']) > 0){

                            $img_name = 'insurance_certi_' . time().rand(1000, 9999).'.jpg';
                            $destination_url = FCPATH . $this->config->item("document").$img_name;
                            if(compress($_FILES['insurance_certi']['tmp_name'], $destination_url, 30)){
                                $insurance_certi = $img_name;

                                $user = User::find($request['user_id']);
                                $user->driving_license = $driving_license;
                                $user->insurance_certi = $insurance_certi;
                                $user->save();

                                $api_key = Token::where('user_id',$request['user_id'])->first();
                                $user->api_key = $api_key['token'];

                                $response['user'] = $user;
                                $response['status'] = TRUE;
                                $response['message'] = 'Documents uploaded successfully';

                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Unable to upload insurance certificate.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }
                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Insurance certificate is required.';
                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                            return TRUE;
                        }
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Unable to upload driving license.';
                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                        return TRUE;

                    }
                   
                }else{
                    $response['status'] = FALSE;
                    $response['message'] = 'Driving license is required';
                }
                

            }else{
                $response['status'] = FALSE;
                $response['message'] = 'User not exist.';
            }
            
            

                                                     
        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function send_otp_post(){
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $otp = generate_OTP();
            $msg = 'Your RAD OTP is '.$otp.'.';
           /* send_SMS($request['phone'],$otp);*/
            send_SMS($request['phone'],$msg);
            $response['otp'] = $otp;
			$this->db->insert("sended_otp", array("type" => "driver", "phone" => $_POST['phone'], "otp" => $otp));
            $response['status'] = true;
            $response['message'] = 'OTP has been sent to the registered phone number.';
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }


    public function logout_post(){
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $data['token'] = '';
            Devices::where('token',$request['device_token'])->update($data);

            $response['status'] = true;
            $response['message'] = 'Logged out successfully.';
            
        }
        
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }   

    public function change_password_post(){


        $this->form_validation->set_rules('old', 'current password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']');
        $this->form_validation->set_rules('new', 'new password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']');
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $user = User::select('users.*')->where('users.id', $request['user_id'])
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[4])
            ->first();

            if (count($user) > 0) {
                $change = $this->ion_auth->change_password($user->phone, $this->input->post('old'), $this->input->post('new'), $user->id);
            
                if($change){
                    $response['status'] = true;
                    $response['message'] = 'Password changed successfully';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Current password is incorrect';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'This account does not exists';
            }
        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }
    public function phone_check($str){
        $user = User::select('users.*')->where('users.phone', $str)
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[4])
                ->first();

        if (count($user) > 0) {
            return TRUE;
        }
        $this->form_validation->set_message('phone_check', "This account does not exists");
        return FALSE; 
    }
    
    public function forgot_password_post() {
        $this->form_validation->set_rules('phone', 'phone', 'trim|required|callback_phone_check');

        if ($this->form_validation->run() == FALSE) {
            $response['status'] = false;
            $response['message'] = validation_errors_response();
        } else {
            $phone = $this->post('phone');
            $user = User::select('users.*')->where('users.phone', $phone)
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[4])
                ->first();

            if (count($user) > 0) {

                if($user['active'] == 1){
                    //send otp
                    $result = true;

                    //$result = $this->ion_auth->forgotten_password($phone);

                    $otp = generate_OTP();
                    

                    if ($result == TRUE) {
                        $response['status'] = true;
                        $response['message'] = "OTP has been sent to the registered phone number";
                        $response['otp'] = $otp;
						$this->db->insert("sended_otp", array("type" => "driver", "phone" => $_POST['phone'], "otp" => $otp));
                    } else {
                        $response['status'] = false;
                        $response['message'] = strip_tags($this->ion_auth->errors());
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'The account with this phone number is not activated yet.';
                }
               
            }else{
                $response['status'] = false;
                $response['message'] = 'This account does not exists';
            }
        }
        //$this->response($data);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function reset_password_post(){
        $this->form_validation->set_rules('password', 'password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required|callback_phone_check');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $user = User::select('users.*')->where('users.phone', $request['phone'])
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[4])
                ->first();

            if (count($user) > 0) {
                $change = $this->ion_auth->reset_password($user->phone, $this->input->post('password'),$user->id);
             
                if($change){
                    $response['status'] = true;
                    $response['message'] = 'New password has been set successfully!';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Something went wrong.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'This account does not exists';
            }

        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function ValidatePostFields($postFields){
        $error = array();        
        foreach ($postFields as $field => $value){            
            if(!isset($field) || $value == '' || is_null($value)){                
                $error[]= ucfirst(str_replace('_', ' ',$field)) ." field is required";             
            }        
        }    
        return $error;   
    }
    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }

    public function _generate_key($uid) {
		do {
			// Generate a random salt
			$salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);
			// If an error occurred, then fall back to the previous method
			if ($salt === FALSE) {
				$salt = hash('sha256', time() . mt_rand());
			}
			$new_key = substr($salt, 0, config_item('rest_key_length'));
		} while ($this->_key_exists($new_key));
		$this->_insert_key($new_key, ['level' => 1, 'ignore_limits' => 1, 'user_id' => $uid]);
		return $new_key;
	}

	private function _key_exists($key) {
		return $this->rest->db
			->where(config_item('rest_key_column'), $key)
			->count_all_results(config_item('rest_keys_table')) > 0;
	}
	private function _insert_key($key, $data) {
		$data[config_item('rest_key_column')] = $key;
		$data['date_created'] = function_exists('now') ? now() : time();
		$check_key = $this->db->get_where('tokens', array('user_id' => $data['user_id']))->row();
		if (empty($check_key)) {
			return $this->rest->db
				->set($data)
				->insert(config_item('rest_keys_table'));
		} else {
			$this->_update_key($key, $data['user_id']);
		}
	}

	private function _update_key($key, $uid) {
		return $this->rest->db
			->where('user_id', $uid)
			->update(config_item('rest_keys_table'), array('date_created' => time(), 'token' => $key));
	}
	private function _delete_key($key) {
		return $this->rest->db
			->where(config_item('rest_key_column'), $key)
			->delete(config_item('rest_keys_table'));
	}


}