<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

require '/var/www/html/vendor/autoload.php';
use ElephantIO\Client;
// use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Engine\SocketIO\Version2X;

class Profile extends REST_Controller {

	function __construct() {
        header('Content-Type: text/html; charset=utf-8');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Token');
		$this->load->model('ContactUs');
		$this->load->model('Orders');
		$this->load->model('ion_auth_model');
		$this->load->model('Notifications');
		$this->load->library(['ion_auth', 'form_validation']);

	}

    public function email_exist_check($str, $user_id){
        if($str != ''){
            $user = User::where('users.email', $str)
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->where('users.id','!=',$user_id)
                ->first();
            if (empty($user)) {
                return TRUE;
            }
            $this->form_validation->set_message('email_exist_check', "Email is already exist.");
            return FALSE;
        }else{
            return TRUE; 
        }
    }

    function phone_exist_check($str, $user_id) {
        $user = User::where('users.phone', $str)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[5])
            ->where('users.id','!=',$user_id)
            ->first();
        if (empty($user)) {
            return TRUE;
        }
        $this->form_validation->set_message('phone_exist_check', "Phone number is already exist.");
        return FALSE;
    }

	public function profile_post(){

        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('first_name', 'first name', 'trim');
        $this->form_validation->set_rules('last_name', 'last name', 'trim');

        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|callback_email_exist_check['.$this->input->post('user_id').']');
        $this->form_validation->set_rules('phone', 'phone', 'trim|callback_phone_exist_check['.$this->input->post('user_id').']');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{

            $request = $this->post();
            // $this->response($_FILES);

            $user = User::find($request['user_id']);
            if(isset($user)){

            	if($user->active == '1'){


                    if(isset($request['first_name'])){
                        $user->first_name = $request['first_name'];
                    }

                    if(isset($request['last_name'])){
                        $user->last_name = $request['last_name'];
                    }

                    
                    if(isset($request['email'])){
                        $user->email = $request['email'];
                    }

                    if(isset($request['phone'])){
                        $user->phone = $request['phone'];
                    }

                    if (isset($_FILES['profile_picture'])) {
                        // if ($_FILES['profile_picture']['name'] != "") {
                        if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0){
                            
                            $file_upload = TRUE;

                            if($file_upload == TRUE){

                                $img_name = 'user_' . time().rand(1000, 9999).'.jpg';
                                $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
                                if(compress($_FILES['profile_picture']['tmp_name'], $destination_url, 30)){

                                    $user->profile_picture = $img_name;
                                }
                                
                                // $data = upload_image('profile_picture','users',$user->id);
                                // $user->profile_picture = $data;
                                // $this->response($data);

                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Error on adding profile picture.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }
                           
                        }
                    }

                    if (isset($_FILES['driving_license'])) {
                        // if ($_FILES['driving_license']['name'] != "") {
                        if (isset($_FILES['driving_license']) && !empty($_FILES['driving_license']) && strlen($_FILES['driving_license']['name']) > 0){

                            $file_upload = TRUE;
                           

                            if($file_upload == TRUE){
                                $img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                                $destination_url = FCPATH . $this->config->item("document").$img_name;
                                if(compress($_FILES['driving_license']['tmp_name'], $destination_url, 30)){
                                    $user_data['driving_license'] = $img_name;
                                }


                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Error on adding driving license.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }
                           
                        }
                    }
                    if (isset($_FILES['insurance_certi'])) {
                        // if ($_FILES['insurance_certi']['name'] != "") {
                        if (isset($_FILES['insurance_certi']) && !empty($_FILES['insurance_certi']) && strlen($_FILES['insurance_certi']['name']) > 0){

                            $file_upload = TRUE;
                           

                            if($file_upload == TRUE){
                                $img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                                $destination_url = FCPATH . $this->config->item("document").$img_name;
                                if(compress($_FILES['insurance_certi']['tmp_name'], $destination_url, 30)){
                                    $user_data['insurance_certi'] = $img_name;
                                }


                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Error on adding insurance certificate.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }
                           
                        }
                    }
                    
                    $user->save();
                    $api_key = Token::where('user_id',$request['user_id'])->first();
                    $user->api_key = $api_key['token'];
                    $d_id = $user['id'];
                    $delivery_average = $this->db->query('SELECT AVG(delivery_boy_rate) as rating FROM orders WHERE delivery_boy_id = '.$d_id.' AND delivery_boy_rate > "0" ')->row_array();
                    $delivery_boy_rating  =  $delivery_average['rating'];
                    if($delivery_boy_rating != null){
                        $user['avg_rating'] = number_format((float)$delivery_boy_rating, 1, '.', '');
                    } else{
                        $user['avg_rating'] = "0";
                    }

            		$response['user'] = $user;
                    $response['message'] = 'Profile details have been updated successfully';
                    $response['status'] = TRUE;
            		
            	}else if($user->active == '0'){
            		$response['message'] = 'Sorry! Your account is not verified, please activate it from your registered email';
            		$response['status'] = FALSE;
            	}else if($user->active == '2'){
            		$response['message'] = 'Account deactivated.';
            		$response['status'] = FALSE;
            	}else{
            		$response['message'] = 'Something went wrong';
            		$response['status'] = FALSE;
            	}
            	
            }else{
            	$response['message'] = 'Account not found.';
            	$response['status'] = FALSE;
            }

        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

     public function duty_change_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim');
        $this->form_validation->set_rules('lng', 'longitude', 'trim');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $request['lat'] = 21.36655655;
            $request['lng'] = 72.36655655;

            if($request['status'] == 0 ) {

                $orders = Orders::select('id','user_id','delivery_boy_id','status','created_at')->where('delivery_boy_id',$request['user_id'])->where('status','!=', 10)->where('status','!=', 4)->get();
                if(count($orders) == 0 ){
                    $data['duty'] = 0;
                    User::whereId($request['user_id'])->update($data);
                    $response['status'] = true;
                    $response['message'] = 'Delivery duty off successfully.';
                    
                    $driver_id = $request['user_id'];
                    $shift_mongo['driver_id'] = $request['user_id'];
                    $shift_mongo['location'] = array($request['lng'],$request['lat']);
                    $shift_mongo['duty'] = 0;
                    $shift_mongo['status'] = 0;
                    $shift_mongo['created_at'] = date('Y-m-d H:i:s'); 
                    
                    $client = new Client(new Version2X($this->config->item('socket_url')));
                    $client->initialize();
                    
                    $client->emit('end_duty', ['driver_id' => $driver_id]);
                    $client->close();
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Complete your order first.After that you can off your duty.';
                }

            }elseif($request['status'] == 1 ){
                $data['duty'] = 1;
                User::whereId($request['user_id'])->update($data);
                $response['status'] = true;
                $response['message'] = 'Delivery duty on successfully.';

                $shift_mongo['driver_id'] = $request['user_id'];
                $shift_mongo['location'] = array($request['lng'],$request['lat']);
                $shift_mongo['duty'] = 1;
                $shift_mongo['busy'] = 0;
                $shift_mongo['created_at'] = date('Y-m-d H:i:s');      

                $client = new Client(new Version2X($this->config->item('socket_url')));
                $client->initialize();                       

                $client->emit('start_duty', $shift_mongo);
                $client->close();       

            }else{
                $response['status'] = false;
                $response['message'] = 'Something went wrong.';
            }
        }
        
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function contact_us_post(){
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('comment', 'comment', 'trim|required');


        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $conatct_us_data = array(
                'name' => $request['name'],
                'email' => $request['email'],
                'message' => $request['comment'],
            );

            $insert_id = ContactUs::insertGetId($conatct_us_data);

            if($insert_id){
                // $response['data'] = ContactUs::find($insert_id);
                $response['status'] = true;
                $response['message'] = "Thank you for contacting us! We'll get back to you shortly";
            }else{
                $response['status'] = false;
                $response['message'] = 'Server encountered an error. please try again';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function rating_review_list_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $orders = Orders::select('id','user_id','delivery_boy_id','status','delivery_boy_rate','delivery_boy_review','created_at','rate_time')->where('delivery_boy_id',$request['user_id'])->where('status', 10)->with(['user'])->where('delivery_boy_rate','!=',0)->orderBy('rate_time','DESC')->get();

            if(count($orders) > 0){
                $orders = $orders->toArray();
                $response['data'] = $orders;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No reviews found!';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }
    public function notification_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $data = [];
            $data = Notifications::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();   
            if(count($data) > 0){
                $data = $data->toArray();
                foreach ($data as $key => $value) {
                    $data[$key]['created_at'] = time_elapsed_string($value['created_at']);
                }
                Notifications::where('user_id',$request['user_id'])->where('seen',0)->update(array('seen' => 1));
                $response['data'] = $data;
                $response['status'] = TRUE;
            }else{
                $response['message'] = 'No notification found.';
                $response['status'] = false;
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function notification_on_off_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('status', 'status', 'trim|required'); //0-off 1-on
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $data = [];
            $data = User::where('id',$request['user_id'])->first();   
            if(count($data) > 0){
                $data = $data->toArray();

                if($request['status'] == 1){
                    User::where('id',$request['user_id'])->update(array('notification_status' => $request['status']));
                    $response['message'] = 'Notification turned on successfully.';
                }else{
                    User::where('id',$request['user_id'])->update(array('notification_status' => $request['status']));
                    $response['message'] = 'Notifications turned off successfully.';
                }
                $response['status'] = TRUE;
            }else{
                $response['message'] = 'No user found.';
                $response['status'] = false;
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
}