<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class Users extends REST_Controller {

	function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
        $this->load->model('Notifications');
		$this->load->model('ion_auth_model');
		$this->load->library(['ion_auth', 'form_validation']);

	}


    public function notification_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $data = [];
            $data = Notifications::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();   
            if(count($data) > 0){
                $data = $data->toArray();
                foreach ($data as $key => $value) {
                    $data[$key]['created_at'] = time_elapsed_string($value['created_at']);
                }
                Notifications::where('user_id',$request['user_id'])->where('seen',0)->update(array('seen' => 1));
                $response['data'] = $data;
                $response['status'] = TRUE;
            }else{
                $response['message'] = 'No notification found.';
                $response['status'] = false;
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function notification_on_off_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('status', 'status', 'trim|required'); //0-off 1-on
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $data = [];
            $data = User::where('id',$request['user_id'])->first();   
            if(count($data) > 0){
                $data = $data->toArray();

                if($request['status'] == 1){
                    User::where('id',$request['user_id'])->update(array('notification_status' => $request['status']));
                    $response['message'] = 'Notification turned on successfully.';
                }else{
                    User::where('id',$request['user_id'])->update(array('notification_status' => $request['status']));
                    $response['message'] = 'Notification turned off successfully.';
                }
                $response['status'] = TRUE;
            }else{
                $response['message'] = 'No user found.';
                $response['status'] = false;
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

	public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }

    public function upload_image_post(){
        echo "Hii";
        if(isset($_FILES["profile_image"])) {
            if($_FILES["profile_image"]["name"] != "") {
                $image = upload_image("profile_image", "users", 1);
                _prea($image);
            }
        }

        echo $img_name = 'user_custom_' . time().rand(1000, 9999).'.jpg';
        $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
        if(compress($_FILES['profile_image']['tmp_name'], $destination_url, 30)){
            $user_data['profile_image'] = $img_name;
            $file_upload = TRUE;
        }else{
            echo 'Unable to upload profile picture';
        }
    }
}
?>