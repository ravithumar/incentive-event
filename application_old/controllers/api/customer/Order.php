<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);
class Order extends REST_Controller {
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Devices');
        $this->load->model('DeliveryAddress');
        $this->load->model('Product');
        $this->load->model('ProductAddOn');
        $this->load->model('Promocode');
        $this->load->model('Orders');
        $this->load->model('OrderProductAddOn');
        $this->load->model('OrderProducts');
        $this->load->model('TrackOrder');
        $this->load->model('Category');
        $this->load->model('Wallet');
        $this->load->model('WalletHistory');
        $this->load->model('PaymentCard');
        $this->load->model('Vendor');
        $this->load->model('Favourite');
        $this->load->model('ion_auth_model');
        $this->load->model('System_setting');
        $this->load->model('Cart');
        $this->load->model('CartProducts');
        $this->load->model('CartProductAddons');
        $this->load->model('Issue');
        $this->load->model('ReportIssue');
        $this->load->model('VendorChannels');
        // $this->load->library(['ion_auth', 'form_validation']);
    }
    public function customAlpha($str) {
        if($str != ''){
            if (!preg_match('/^[0-9a-z .,\-]+$/i', $str)) {
                $this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
                return false;
            }
        }
        
        return TRUE;
    }
    public function validateDateFormat($str) {
        if($str != ''){
            if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$str)) {
                $this->form_validation->set_message('validateDateFormat', 'The {field} field contain invalid date format.');
                return false;
            }
        }
        return TRUE;
    }

    public function validate_latlong($str){
        if (!preg_match('/([0-9.-]+).+?([0-9.-]+)/', $str)) {
                $this->form_validation->set_message('validate_latlong', 'The {field} field is invalid.');
                return false;
        }
        
        return TRUE;
    }

    public function issue_get(){
        $issue = Issue::where('status',1)->orderBy('id', 'desc')->get();
        if (count($issue) > 0){
            $response['data'] = $issue;
            $response['status'] = true;
        }else{
            $response['data'] = array();
            $response['message'] = 'No any Issue found';
            $response['status'] = false;
        }
        // $this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function report_issue_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('issue_id', 'issue id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('description', 'description', 'trim|required');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $orders = Orders::where('id', $request['order_id'])->first();
            if (count($orders) > 0) {
                $orders = $orders->toArray();
                $data['order_id'] = $request['order_id'];
                $data['issue_id'] = $request['issue_id'];
                $data['description'] = $request['description'];
                ReportIssue::insert($data);

                $vendor_channels_data = VendorChannels::where('vendor_id',$orders['vendor_id'])->get();
                $description = 'New Report Issue arrived for the order #'.$request['order_id'].'.';
                $title = "Report Issue";
                if(count($vendor_channels_data) > 0){
                    $vendor_channels_data = $vendor_channels_data->toArray();
                    $player_ids = array_column($vendor_channels_data, 'channel_id');
                    $send_web_push = send_web_push($player_ids, $description, $title,$type = 3);
                }

                $response['status'] = true;
                $response['message'] = 'Issue reported successfully'; 
            }else{
                $response['status'] = false;
                $response['message'] = 'No any order found.';
            }
              
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function add_to_cart_old_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('vendor_id', 'vendor id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('product_id', 'product id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('qty', 'qty', 'trim|required');
        $this->form_validation->set_rules('addon_id', 'addon id', 'trim');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();

            $restaurant = User::with(['vendor'])->where('active','1')->find($request['vendor_id']);
                
            if(isset($restaurant)){

                $product_data = Product::with(['addon'])->where('status', 1)->where('user_id',$request['vendor_id'])->where('id',$request['product_id'])->first();
                
                if (isset($product_data)){
                    $product_data = $product_data->toArray();

                    if($product_data['inventory_status'] != 0){
                        if($product_data['stock'] < $request['qty']){
                            $response['status'] = false;
                            $response['message'] = 'Product is out of stock!';  
                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                            return true;
                        }
                    }

                    if(isset($request['addon_id']) && $request['addon_id'] != ''){
                        $add_on_ids = explode(',',$request['addon_id']);
                        rsort($add_on_ids);
                    }

                    $same_restaurant = Cart::where('user_id',$request['user_id'])->where('order_id',0)->get();
                    
                    if(count($same_restaurant) > 0){
                        $same_restaurant = $same_restaurant->toArray();
                        $vendor_ids = array_column($same_restaurant,'vendor_id');
                        $cart_product_ids = array_column($same_restaurant,'id');
                        $vendor_ids = array_unique($vendor_ids);

                        if(!in_array($request['vendor_id'],$vendor_ids)){
                            //delete all carts

                            //find cart_add_on_ids
                            if(!empty($cart_product_ids)){
                                $cart_products_exists = CartProducts::whereIn('cart_id',$cart_product_ids)->get();
                                if(count($cart_products_exists) > 0){
                                    $cart_products_exists = $cart_products_exists->toArray();
                                    $cart_add_on_ids = array_column($cart_products_exists,'id'); 

                                    //delete cart_add_on
                                    CartProductAddons::whereIn('cart_product_id',$cart_add_on_ids)->delete();
                                    CartProducts::whereIn('id',$cart_add_on_ids)->delete();
                                    Cart::where('user_id',$request['user_id'])->where('order_id',0)->delete();

                                }
                            }
                            
                        }
                    }
                    
                    $existing_cart = CartProducts::select("cart.user_id","cart.vendor_id","cart_products.*")
                                        ->join('cart', 'cart.id','=','cart_products.cart_id')
                                        ->where('cart.user_id',$request['user_id'])
                                        ->where('cart.vendor_id',$request['vendor_id'])
                                        ->where('cart.order_id',0)
                                        ->where('cart_products.product_id',$request['product_id'])
                                        ->with(['cartproductaddon'])
                                        ->when(isset($request['addon_id']), function ($query) {
                                                $query->join('cart_product_addons', 'cart_product_addons.cart_product_id','=','cart_products.id');
                                        })
                                        ->when(isset($request['addon_id']), function ($query) use ($add_on_ids){
                                            $query->whereIn('cart_product_addons.addon_id',$add_on_ids);
                                        })
                                        ->groupBy('cart.id')
                                        ->get();

                    $quantity = false;
                    if(isset($existing_cart) && count($existing_cart) > 0){
                        $existing_cart = $existing_cart->toArray();

                        if(isset($request['addon_id']) && $request['addon_id'] != ''){

                            foreach ($existing_cart as $ekey => $evalue) {
                                if(!empty($evalue['cartproductaddon']) && $evalue['cartproductaddon'] != ''){
                                    foreach ($evalue['cartproductaddon'] as $akey => $avalue) {
                                        $cart_product_add_on[] = $avalue;
                                    }
                                }
                                if(!empty($cart_product_add_on)){
                                    $cart_ids = array_column($cart_product_add_on,'cart_product_id');
                                    $cart_ids = array_unique($cart_ids);
                                    $cart_ids = array_values($cart_ids);

                                    foreach ($cart_product_add_on as $ckey => $cvalue) {
                                        foreach ($cart_ids as $cart_key => $cart_value) {
                                            if($cart_value == $cvalue['cart_product_id']){
                                                $cart[$cart_key]['id'] = $cart_value;
                                                $cart[$cart_key]['add_on'][] = $cvalue['addon_id'];
                                                
                                            }
                                        }
                                    } 

                                    foreach ($cart as $key => $value) {
                                    $cart[$key]['add_on'] = array_unique($value['add_on']);
                                    $cart[$key]['total_add_on'] = count($cart[$key]['add_on']);
                                    }
                                    $quantity = false;
                                    foreach ($cart as $key => $value) {
                                        if(count($add_on_ids) == $value['total_add_on']){
                                            if($value['add_on'] == $add_on_ids){
                                                $quantity = true;
                                                $quantity_id = $value['id'];
                                            }else{
                                                $quantity = false;
                                            }
                                        }
                                    }

                                }

                            }
                        }else{
                            $quantity = false;
                            foreach ($existing_cart as $ekey => $evalue) {
                                if(count($evalue['cartproductaddon']) == 0){
                                    $quantity = true;
                                    $quantity_id = $evalue['id'];
                                }
                            }
                        }
                    }
                    // $this->response($quantity);

                    if($quantity == false){


                        $add_on = true;
                        if(isset($request['addon_id']) && $request['addon_id'] != ''){
                            if(!empty($product_data['addon']) && $product_data['addon'] != ''){
                                $product_data_add_on_ids = array_column($product_data['addon'],'id');
                                
                                foreach ($add_on_ids as $add_key => $add_value) {
                                    if(in_array($add_value,$product_data_add_on_ids)){
                                        $add_on = true;
                                    }else{
                                        $add_on = false;
                                        break;
                                    } 
                                }
                                
                            }else{
                                $add_on = false;
                            }
                        }

                        if($add_on == true){

                            $cart_data['user_id'] = $request['user_id'];
                            $cart_data['vendor_id'] = $request['vendor_id'];
                            $cart_data['order_id'] = 0;

                            $cart_insert_id = Cart::insertGetId($cart_data);

                            $cart_products['cart_id'] = $cart_insert_id;
                            $cart_products['product_id'] = $request['product_id'];
                            $cart_products['qty'] = $request['qty'];

                            $cart_products_insert_id = CartProducts::insertGetId($cart_products);

                            if(isset($request['addon_id']) && $request['addon_id'] != ''){
                                foreach ($add_on_ids as $key => $value) {

                                    $temp_add_on = array(
                                        'cart_product_id' => $cart_products_insert_id,
                                        'addon_id' => $value,
                                    );   
                                    $cart_product_addons[] = $temp_add_on;
                                }
                                $cart_product_addons_insert_id = CartProductAddons::insert($cart_product_addons);
                            }
                            

                            $response['status'] = true;
                            $response['message'] = 'Added to cart successfully';  


                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Product add on is not available!'; 
                        }
                    }else{

                        //Quantity+1
                        if(isset($quantity_id) && $quantity_id != ''){
                            $cart_products_details = CartProducts::whereId($quantity_id)->first();
                            if(isset($cart_products_details)){
                                $updated_data['qty'] = $cart_products_details['qty'] + $request['qty'];
                                CartProducts::whereId($quantity_id)->update($updated_data);

                                $response['status'] = true;
                                $response['message'] = 'Added to cart successfully'; 
                            }
                        }
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Product not found.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Restaurant not found.';
            }

        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function add_to_cart_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('vendor_id', 'vendor id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('product_id', 'product id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('qty', 'qty', 'trim|required');
        $this->form_validation->set_rules('addon_id', 'addon id', 'trim');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();

            $restaurant = User::with(['vendor'])->where('active','1')->find($request['vendor_id']);
                
            if(isset($restaurant)){

                $product_data = Product::with(['addon'])->where('status', 1)->where('user_id',$request['vendor_id'])->where('id',$request['product_id'])->first();
                
                if (isset($product_data)){
                    $product_data = $product_data->toArray();

                    if($product_data['inventory_status'] != 0){
                        if($product_data['stock'] < $request['qty']){
                            $response['status'] = false;
                            $response['message'] = 'Product is out of stock!';  
                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                            return true;
                        }
                    }

                    if(isset($request['addon_id']) && $request['addon_id'] != ''){
                        $add_on_ids = explode(',',$request['addon_id']);
                        rsort($add_on_ids);
                        $available_addons = ProductAddOn::whereIn('id',$add_on_ids)->where('product_id',$request['product_id'])->get();
                        if(count($add_on_ids) != count($available_addons)){
                            $response['status'] = false;
                            $response['message'] = 'Product addons are not exists';  
                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                            return true;
                        }
                    }

                    $same_restaurant = Cart::where('user_id',$request['user_id'])->where('order_id',0)->first();
                    
                    if(isset($same_restaurant)){
                        
                        if($request['vendor_id'] != $same_restaurant['vendor_id']){
                            //delete all carts
                            
                            //find cart_add_on_ids
                            
                            $cart_products_exists = CartProducts::where('cart_id',$same_restaurant['id'])->get();
                            if(count($cart_products_exists) > 0){
                                $cart_products_exists = $cart_products_exists->toArray();
                                $cart_add_on_ids = array_column($cart_products_exists,'id'); 
                                
                                // $this->response($cart_add_on_ids);
                                //delete cart_add_on
                                CartProductAddons::whereIn('cart_product_id',$cart_add_on_ids)->delete();
                                CartProducts::whereIn('id',$cart_add_on_ids)->delete();
                                Cart::where('user_id',$request['user_id'])->where('order_id',0)->delete();

                            }
                            
                            
                        }
                    }
                    
                    $existing_cart = CartProducts::select("cart.user_id","cart.vendor_id","cart_products.*")
                                        ->join('cart', 'cart.id','=','cart_products.cart_id')
                                        ->where('cart.user_id',$request['user_id'])
                                        ->where('cart.vendor_id',$request['vendor_id'])
                                        ->where('cart.order_id',0)
                                        ->where('cart_products.product_id',$request['product_id'])
                                        ->with(['cartproductaddon'])
                                        ->when(!empty($request['addon_id']), function ($query) {
                                                $query->join('cart_product_addons', 'cart_product_addons.cart_product_id','=','cart_products.id');
                                        })
                                        ->when(!empty($request['addon_id']), function ($query) use ($add_on_ids){
                                            $query->whereIn('cart_product_addons.addon_id',$add_on_ids);
                                        })
                                        //->groupBy('cart.id')
                                        ->get();

                    $quantity = false;
                    // $this->response($existing_cart);

                    if(isset($existing_cart) && count($existing_cart) > 0){
                        $existing_cart = $existing_cart->toArray();

                        if(isset($request['addon_id']) && $request['addon_id'] != ''){

                            foreach ($existing_cart as $ekey => $evalue) {
                                if(!empty($evalue['cartproductaddon']) && $evalue['cartproductaddon'] != ''){
                                    foreach ($evalue['cartproductaddon'] as $akey => $avalue) {
                                        $cart_product_add_on[] = $avalue;
                                    }
                                }
                                if(!empty($cart_product_add_on)){
                                    $cart_ids = array_column($cart_product_add_on,'cart_product_id');
                                    $cart_ids = array_unique($cart_ids);
                                    $cart_ids = array_values($cart_ids);

                                    foreach ($cart_product_add_on as $ckey => $cvalue) {
                                        foreach ($cart_ids as $cart_key => $cart_value) {
                                            if($cart_value == $cvalue['cart_product_id']){
                                                $cart[$cart_key]['id'] = $cart_value;
                                                $cart[$cart_key]['add_on'][] = $cvalue['addon_id'];
                                                
                                            }
                                        }
                                    } 

                                    foreach ($cart as $key => $value) {
                                        $cart[$key]['add_on'] = array_unique($value['add_on']);
                                        $cart[$key]['total_add_on'] = count($cart[$key]['add_on']);
                                    }
                                    $quantity = false;
                                    foreach ($cart as $key => $value) {
                                        if(count($add_on_ids) == $value['total_add_on']){
                                            if($value['add_on'] == $add_on_ids){
                                                $quantity = true;
                                                $quantity_id = $value['id'];
                                            }else{
                                                $quantity = false;
                                            }
                                        }
                                    }

                                }

                            }
                        }else{
                            $quantity = false;
                            foreach ($existing_cart as $ekey => $evalue) {
                                if(count($evalue['cartproductaddon']) == 0){
                                    $quantity = true;
                                    $quantity_id = $evalue['id'];
                                }
                            }
                        }
                    }
                    // $this->response($quantity);

                    if($quantity == false){


                        $add_on = true;
                        if(isset($request['addon_id']) && $request['addon_id'] != ''){
                            if(!empty($product_data['addon']) && $product_data['addon'] != ''){
                                $product_data_add_on_ids = array_column($product_data['addon'],'id');
                                
                                foreach ($add_on_ids as $add_key => $add_value) {
                                    if(in_array($add_value,$product_data_add_on_ids)){
                                        $add_on = true;
                                    }else{
                                        $add_on = false;
                                        break;
                                    } 
                                }
                                
                            }else{
                                $add_on = false;
                            }
                        }

                        if($add_on == true){
                            // $this->response($same_restaurant);
                            if(isset($same_restaurant) && $request['vendor_id'] == $same_restaurant['vendor_id'] ){
    
                                $cart_products['cart_id'] = $same_restaurant['id'];
                                $cart_products['product_id'] = $request['product_id'];
                                $cart_products['qty'] = $request['qty'];
    
                                $cart_products_insert_id = CartProducts::insertGetId($cart_products);
    
                                if(isset($request['addon_id']) && $request['addon_id'] != ''){
                                    foreach ($add_on_ids as $key => $value) {
    
                                        $temp_add_on = array(
                                            'cart_product_id' => $cart_products_insert_id,
                                            'addon_id' => $value,
                                        );   
                                        $cart_product_addons[] = $temp_add_on;
                                    }
                                    $cart_product_addons_insert_id = CartProductAddons::insert($cart_product_addons);
                                }
                            }else{
                                $cart_data['user_id'] = $request['user_id'];
                                $cart_data['vendor_id'] = $request['vendor_id'];
                                $cart_data['order_id'] = 0;
    
                                $cart_insert_id = Cart::insertGetId($cart_data);
    
                                $cart_products['cart_id'] = $cart_insert_id;
                                $cart_products['product_id'] = $request['product_id'];
                                $cart_products['qty'] = $request['qty'];
    
                                $cart_products_insert_id = CartProducts::insertGetId($cart_products);
    
                                if(isset($request['addon_id']) && $request['addon_id'] != ''){
                                    foreach ($add_on_ids as $key => $value) {
    
                                        $temp_add_on = array(
                                            'cart_product_id' => $cart_products_insert_id,
                                            'addon_id' => $value,
                                        );   
                                        $cart_product_addons[] = $temp_add_on;
                                    }
                                    $cart_product_addons_insert_id = CartProductAddons::insert($cart_product_addons);
                                }
                            }

                            $response['status'] = true;
                            $response['message'] = 'Added to cart successfully';  


                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Product add on is not available!'; 
                        }
                    }else{

                        //Quantity+1
                        if(isset($quantity_id) && $quantity_id != ''){
                            $cart_products_details = CartProducts::whereId($quantity_id)->first();
                            if(isset($cart_products_details)){
                                $updated_data['qty'] = $cart_products_details['qty'] + $request['qty'];
                                CartProducts::whereId($quantity_id)->update($updated_data);

                                $response['status'] = true;
                                $response['message'] = 'Added to cart successfully'; 
                            }
                        }
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Product not found.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Restaurant not found.';
            }

        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function get_cart_data_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();
            $cart_data = Cart::where('user_id',$request['user_id'])->where('order_id',0)->first();
                
            if(isset($cart_data)){
                $cart_data = $cart_data->toArray();
                // $cart_product_ids = array_column($cart_data,'id');
                // $cart_product_ids = array_unique($cart_product_ids);

                // $vendor_ids = array_column($cart_data,'vendor_id');
                // $vendor_ids = array_unique($vendor_ids);

                $restaurant = User::with(['vendor'])->where('id',$cart_data['vendor_id'])->first();
                
                $cart_products = CartProducts::where('cart_id',$cart_data['id'])->with(['product'])->get();
                if(count($cart_products) > 0){
                    $cart_products = $cart_products->toArray();
                    
                    $cart_product_addon_ids = array_column($cart_products,'id');
                    $cart_product_addon_ids = array_unique($cart_product_addon_ids);
                    if(!empty($cart_product_addon_ids)){
                        $cart_product_addons = CartProductAddons::whereIn('cart_product_id',$cart_product_addon_ids)->with(['productaddon'])->get();
                        
                        if(count($cart_product_addons) > 0){
                            // $this->response($cart_product_addons);
                            $cart_product_addons = $cart_product_addons->toArray();
                            foreach ($cart_products as $key => $value) {
                                $total = 0;
                                $single_total_price = 0;
                                foreach ($cart_product_addons as $pkey => $pvalue) {
                                    if($pvalue['cart_product_id'] == $value['id']){
                                        $cart_products[$key]['cart_product_addons'][] = $pvalue;
                                        $total += $pvalue['productaddon']['price'];
                                        $cart_products[$key]['total_price'] = $total;
                                    }
                                }
                                
                            }
                        }
                    }

                    foreach ($cart_products as $key => $value) {
                        $single_total_price = $value['total_price'] + $value['product']['price'];
                        $cart_products[$key]['single_total_price'] =  number_format((float)$single_total_price, 2, '.', '');

                        $addon = $value['qty'] * $value['total_price'];
                        $cart_products[$key]['total_price'] =  number_format((float)$addon, 2, '.', '');
                        
                    }

                    foreach ($cart_products as $key => $value) {

                        $qty_price = $value['qty_price'] + $value['total_price'];
                        $cart_products[$key]['qty_price'] =  number_format((float)$qty_price, 2, '.', '');
                    }
                    $subtotal = 0;
                    foreach ($cart_products as $key => $value) {
                        $subtotal += $value['qty_price'] ;
                    }
                    $service_charge = System_setting::select('value')->where('path','service_charge')->first();

                    $service_charge_amount = ($subtotal * $service_charge['value'] ) /100;
                    
                    $cart_data['subtotal'] = number_format((float)$subtotal, 2, '.', '');
                    $cart_data['service_charge'] = number_format((float)$service_charge['value'], 2, '.', '');
                    $cart_data['service_charge_amount'] = number_format((float)$service_charge_amount, 2, '.', '');
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Cart is empty.';
                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                    return true;
                }
                
                $data = [];

                $data['cart_data'] = $cart_data;
                $data['restaurant'] = $restaurant;
                $data['cart_products'] = $cart_products;

                $response['data'] = $data;
                $response['status'] = true;


            }else{
                $response['status'] = false;
                $response['message'] = 'Cart is empty.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function delete_cart_product_post(){ 
        $this->form_validation->set_rules('cart_product_id', 'cart product id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();
            $cart_products_exists = CartProducts::where('id',$request['cart_product_id'])->get();
            if(count($cart_products_exists) > 0){

                $cart_products_exists = $cart_products_exists->toArray();
               
                // $this->response($cart_id[0]);
                //delete cart_add_on
                CartProductAddons::where('cart_product_id',$request['cart_product_id'])->delete();
                CartProducts::where('id',$request['cart_product_id'])->delete();

                $cart_id = array_column($cart_products_exists,'cart_id');
                $cart_id = array_unique($cart_id);
 
                $cart_products = CartProducts::where('cart_id',$cart_id[0])->get();
                if(count($cart_products) == 0){
                    Cart::where('id',$cart_id[0])->where('order_id',0)->delete();
                }

                $response['status'] = true;
                $response['message'] = 'Product removed successfully';
            }else{
                $response['status'] = false;
                $response['message'] = 'Cart is empty.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function get_delivery_charge_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('vendor_id', 'vendor id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('delivery_address_id', 'delivery address id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();
            $restaurant = User::select("users.*")
                                    ->join('users_groups', 'users_groups.user_id','=','users.id')
                                    ->where('users_groups.group_id','2')
                                    ->with(['vendor'])
                                    ->where('users.id',$request['vendor_id'])
                                    ->where('users.active','1')
                                    ->first();
            if (isset($restaurant)){
                $delivery_address = DeliveryAddress::whereId($request['delivery_address_id'])->where('user_id',$request['user_id'])->first();
                if (isset($delivery_address)){

                    $delivery_miles = get_delivery_miles($restaurant['vendor']['latitude'],$restaurant['vendor']['longitude'], $delivery_address['latitude'], $delivery_address['longitude']);

                    $delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();

                    if ($delivery_miles['status'] == true) {
                        if ($delivery_miles['distance'] <= $delivery_miles_available['value']) {
                            $delivery_fee = $delivery_miles['charge'];
                            $response['delivery_fee'] = $delivery_fee;
                           /* $response['delivery_fee'] = '7.00';*/
                            $response['status'] = true;
                        } else {
                            $response['status'] = false;
                            $response['message'] = 'This restaurant does not deliver to this address';
                        }
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'This restaurant does not deliver to this address';
                    }
 
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Delivery address not found.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'No restaurant found.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function update_cart_quantity_post(){
        $this->form_validation->set_rules('cart_product_id', 'cart product id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('qty', 'qty', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required'); // 1- increment 0 - decrement
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();
            $cart_products_exists = CartProducts::where('id',$request['cart_product_id'])->first();
            if(isset($cart_products_exists)){
                $cart_products_exists = $cart_products_exists->toArray();

                if($request['status'] == 1){
                    $updated_data['qty'] = $cart_products_exists['qty'] + $request['qty'];
                    CartProducts::where('id',$request['cart_product_id'])->update($updated_data);
                }else{
                    if($cart_products_exists['qty'] <= 1 || $cart_products_exists['qty'] == $request['qty']){
                        CartProductAddons::where('cart_product_id',$request['cart_product_id'])->delete();
                        CartProducts::where('id',$request['cart_product_id'])->delete();
                    }else{
                        $updated_data['qty'] = $cart_products_exists['qty'] - $request['qty'];
                        CartProducts::where('id',$request['cart_product_id'])->update($updated_data);
                    }
                    
                }

                $cart_data = Cart::where('id',$cart_products_exists['cart_id'])->where('order_id',0)->first();
                
                if(isset($cart_data)){
                    $cart_data = $cart_data->toArray();
                    $cart_products = CartProducts::where('cart_id',$cart_data['id'])->with(['product'])->get();
                    if(count($cart_products) > 0){
                        $cart_products = $cart_products->toArray();
                        
                        $cart_product_addon_ids = array_column($cart_products,'id');
                        $cart_product_addon_ids = array_unique($cart_product_addon_ids);
                        if(!empty($cart_product_addon_ids)){
                            $cart_product_addons = CartProductAddons::whereIn('cart_product_id',$cart_product_addon_ids)->with(['productaddon'])->get();
                            
                            if(count($cart_product_addons) > 0){
                                // $this->response($cart_product_addons);
                                $cart_product_addons = $cart_product_addons->toArray();
                                foreach ($cart_products as $key => $value) {
                                    $total = 0;
                                    foreach ($cart_product_addons as $pkey => $pvalue) {
                                        if($pvalue['cart_product_id'] == $value['id']){
                                            $cart_products[$key]['cart_product_addons'][] = $pvalue;
                                            $total += $pvalue['productaddon']['price'];
                                            $cart_products[$key]['total_price'] = $total;

                                        }
                                    }
                                }
                            }
                        }
                        foreach ($cart_products as $key => $value) {
                            $addon = $value['qty'] * $value['total_price'];
                            $cart_products[$key]['total_price'] =  number_format((float)$addon, 2, '.', '');
                        }

                        
                        foreach ($cart_products as $key => $value) {
                            $qty_price = $value['qty_price'] + $value['total_price'];
                            $cart_products[$key]['qty_price'] =  number_format((float)$qty_price, 2, '.', '');
                        }
                        $subtotal = 0;
                        foreach ($cart_products as $key => $value) {
                            $subtotal += $value['qty_price'] ;
                        }
                        $service_charge = System_setting::select('value')->where('path','service_charge')->first();

                        $service_charge_amount = ($subtotal * $service_charge['value'] ) /100;
                        
                        $cart_data['subtotal'] = number_format((float)$subtotal, 2, '.', '');
                        $cart_data['service_charge'] = number_format((float)$service_charge['value'], 2, '.', '');
                        $cart_data['service_charge_amount'] = number_format((float)$service_charge_amount, 2, '.', '');
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Cart is empty.';
                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                        return true;
                    }
                    
                    $data = [];

                    // $data['cart_data'] = $cart_data;
                    $response['data'] = $cart_data;
                    $response['status'] = true;
                    $response['message'] = 'Cart updated successfully';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Cart is empty.'; 
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Cart is empty.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    
    public function place_order_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('vendor_id', 'vendor id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('order_type', 'order type', 'trim|required|is_natural_no_zero'); //   1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
        $this->form_validation->set_rules('later_time', 'later time', 'trim');
        $this->form_validation->set_rules('cart_id', 'cart id', 'trim|required|is_natural_no_zero');

        // $this->form_validation->set_rules('delivery_charge', 'delivery charge', 'trim');
        $this->form_validation->set_rules('promocode_id', 'promocode id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('promo_amount', 'promo amount', 'trim');
        $this->form_validation->set_rules('delivery_address_id', 'delivery address id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('pay_type', 'pay type', 'trim|required|is_natural_no_zero'); // 1- card, 2 -wallet
        $this->form_validation->set_rules('special_instruction', 'special instruction', 'trim');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $order_data = $order_products_array =[];
            

            $service_charge = System_setting::select('value')->where('path','service_charge')->first();
           
            $total_price = 0;
            $cart_error = 'Product price is invalid.';
            
            $cart_products = CartProducts::select('cart_products.*','product.title','product.inventory_status','product.stock','product.price','product.type')
            ->where('cart_id',$request['cart_id'])
            ->join('cart', 'cart.id','=','cart_products.cart_id')
            ->join('product', 'product.id','=','cart_products.product_id')
            ->where('cart.user_id',$request['user_id'])
            ->where('cart.order_id',0)
            ->where('product.status',1)
            ->where('cart.vendor_id',$request['vendor_id'])
            ->with(['cartproductaddon','productaddon'])
            ->get();
            //ordeerid 0 n join product n display array

            

            // $this->response($date); 

            if(count($cart_products) > 0){
                $cart_products = $cart_products->toArray();
                $product_ids = array_column($cart_products,'product_id');
                $subtotal = 0;
                foreach ($cart_products as $key => $value) {
                    $subtotal = 0;

                    if(count($value['cartproductaddon']) > 0){
                        foreach ($value['cartproductaddon'] as $ckey => $cvalue) {
                            $subtotal += $cvalue['addon_price'];
                        }

                    }
                    $cart_products[$key]['addon_total'] = $subtotal * $value['qty'];
                }
                $subtotal_all =0;
                $alcoholic = 0;
                foreach ($cart_products as $key => $value) {
                    if($value['qty_price'] != 0){
                        $subtotal_all = $value['qty_price'] + $value['addon_total'];
                        $total_price  += $subtotal_all;  
                        $cart_products[$key]['subtotal_all'] =  number_format((float)$subtotal_all, 2, '.', '');         
                    }else{
                        $response['status'] = false;
                        $response['message'] =  $value['title'].' price is invalid!'; 
                        break; 
                    }

                    if($value['inventory_status'] != 0){
                        if($value['stock'] < $value['qty']){
                            $response['status'] = false;
                            $response['message'] = $value['title'].' is out of stock!';  
                            break;
                        }
                    }
                    if($value['type'] == 1){
                        $alcoholic = 1;
                    }
                    $add_on_ids =[];
                    if(isset($value['cartproductaddon']) && $value['cartproductaddon'] != ''){
                        $add_on_ids = array_column($value['cartproductaddon'],'addon_id');
                    } 
                    array_push($order_products_array, $value);
                    $order_products_array[$key]['quantity'] = $value['qty'];
                    $order_products_array[$key]['order_addons'] = $add_on_ids;
                    
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Cart is empty';  
            }
            // $this->response($alcoholic); 
            // $this->response($order_products_array); 
           
            if($response['message'] != '' && !empty($response['message'])){
                //$this->response($response);
                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                return TRUE;
            }else{
                $total_price = number_format((float)$total_price, 2, '.', '');
                $today= date("l");
                // for get store data & check status

                $restaurant = User::select("users.*")
                                    ->join('users_groups', 'users_groups.user_id','=','users.id')
                                    ->where('users_groups.group_id','2')
                                    ->with(['vendor'])
                                    ->where('users.id',$request['vendor_id'])
                                    ->where('users.active','1')
                                    ->first();
                if (isset($restaurant)){
                    // $this->response($restaurant);
                    $restaurant_name = $restaurant['vendor']['name'];
                    $restaurant_email = $restaurant['email'];
                    //delivery takeout 
                    if($request['order_type'] == 3 || $request['order_type'] == 4){
                        $later_time = DateTime::createFromFormat('H:i:s',$request['later_time']);
                        $current_time = $later_time->format('His');
                        $order_data['later_time'] = $current_time;
                    }else{
                        $current_time = date("His");
                    }
                    $service_available = true;
                    if($request['order_type'] == 1 || $request['order_type'] == 3){
                        //delivery
                        if($restaurant['vendor']['service_available'] == 1 || $restaurant['vendor']['service_available'] == 3){
                            $service_available = true;
                        }else{
                            $service_available = false;
                        }
                        
                    }else{
                        $prep_time = date('Y-m-d H:i:s', strtotime('+30 minutes'));
                        $order_data['prep_time'] = $prep_time;
                        //takeout
                        if($restaurant['vendor']['service_available'] == 2 || $restaurant['vendor']['service_available'] == 3){
                            $service_available = true;
                        }else{
                            $service_available = false;
                        }     
                    }
                    // $this->response($service_available);
                    if($service_available == true){
                        $store_time = get_store_time($current_time,$request['vendor_id']);
                        
                        if($store_time == true){
                            
                            // $this->response($restaurant);
                        
                            if($restaurant['vendor']['minimum_order_amount'] <= $total_price){

                                if(isset($request['promocode_id']) && $request['promocode_id'] != ''){

                                    $user_ids = [$request['vendor_id'], 0];

                                    $promocode = Promocode::where('status','=',1)->whereIn('user_id',$user_ids)->where('id', $request['promocode_id'])->first();

                                    if (isset($promocode)){

                                        $promocode = $promocode->toArray();

                                        if($promocode['promo_min_order_amount'] >= $total_price){
                                            $response['status'] = false;
                                            $response['message'] = 'Add more products to avail this promocode.';
                                            //$this->response($response);
                                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                            return TRUE;
                                        }
                                        //order status = 1-Placed , 2-Store accepted , 3-Store rejected , 4-Expired , 5-Delivery boy request sent , 6-Delivery boy accepted , 7-Delivery boy rejected , 8-Delivery boy Picked , 9-On the way , 10-Delivered , 11-Delivery Fail , 12-Order Cancel
                                        $status = ['1,2,5,6,8,9,10'];
                                        $usage_limit_order = Orders::where('user_id', $request['user_id'])->where('promo_id', $request['promocode_id'])->whereIn('status',$status)->get();
                                        //status 

                                        $usage_limit = count($usage_limit_order);
                                        if($promocode['usage_limit'] <= $usage_limit){
                                            $response['status'] = false;
                                            $response['message'] = 'Promocode usage limit exceeded.';
                                            //$this->response($response);
                                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                            return TRUE;
                                        }

                                    }else{
                                        $response['status'] = false;
                                        $response['message'] = 'Promocode is invalid.';
                                        //$this->response($response);
                                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                        return TRUE;
                                    }
                                }

                                if($request['order_type'] == '1' || $request['order_type'] == '3' ){

                                    $delivery_address = DeliveryAddress::whereId($request['delivery_address_id'])->where('user_id',$request['user_id'])->first();

                                    if (isset($delivery_address)){
                                        $delivery_address_string = '';
                                        if($delivery_address['address'] != ''){
                                            $delivery_address_string = $delivery_address['address'].", ";
                                        }
                                        if($delivery_address['location'] != ''){
                                            $delivery_address_string .= $delivery_address['location'].", ";
                                        }
                                        $order_data['delivery_address'] = $delivery_address_string;
                                        $order_data['delivery_address_lat'] = $delivery_address['latitude'];
                                        $order_data['delivery_address_long'] = $delivery_address['longitude'];

                                        $delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();
                                        $delivery_miles = get_delivery_miles($restaurant['vendor']['latitude'],$restaurant['vendor']['longitude'], $delivery_address['latitude'], $delivery_address['longitude']);
                                        if ($delivery_miles['status'] == true) {
                                            if ($delivery_miles['distance'] <= $delivery_miles_available['value']) {
                                                $delivery_fee = $delivery_miles['charge'];
                                            }else{
                                                $response['status'] = false;
                                                $response['message'] = 'This restaurant does not deliver to this address';
                                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                                return TRUE;
                                            }
                                        }else{
                                            $response['status'] = false;
                                            $response['message'] = 'This restaurant does not deliver to this address';
                                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                            return TRUE;
                                        }
                                        $order_data['delivery_fee'] =  $delivery_fee;
                                        /*$order_data['delivery_fee'] =  '0.00';*/
                                        $time = intval($delivery_miles['min']) + 20;
                                        // $this->response($time);
                                        $prep_time = date('Y-m-d H:i:s', strtotime('+'.$time.' minutes'));
                                        $order_data['prep_time'] = $prep_time;
                                    }else{
                                        $response['status'] = false;
                                        $response['message'] = 'Delivery address not found.';
                                        //$this->response($response);
                                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                        return TRUE;
                                    }

                                }
                                // $this->response($order_data);

                                if($request['pay_type'] == '1'){
                                    // Pay by Card

                                    $card = PaymentCard::where('user_id',$request['user_id'])->where('id', $request['payment_card_id'])->first();

                                    if (isset($card)){

                                        // $this->load->library('Stripe');
                                        $this->load->helper("stripe_helper");

                                        $card_array['name'] = $card['card_holder_name'];
                                        $card_array['card_number'] = decrypt($card['card_number']);
                                        $monthyear = explode('/',decrypt($card['expiry_date']));
                                        $card_array['month'] = $monthyear[0];
                                        $card_array['year'] = $monthyear[1];
                                        $card_array['cvc_number'] = decrypt($card['cvv']);

                                        $stripeCardObject = create_token($card_array);
                                        // $this->response($stripeCardObject);
                                        if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true){
                                            $stripe_token = $stripeCardObject['token'];
                                        }else{
                                            $error = $stripeCardObject['error'];
                                            $response['message'] = $error['message'];
                                            $response['status'] = false;
                                            // $response['card_array'] = $card_array;
                                            //$this->response($response);
                                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                            return TRUE;
                                            
                                        }

                                    }else{
                                        $response['status'] = false;
                                        $response['message'] = 'Payment card is invalid.';
                                        //$this->response($response);
                                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                        return TRUE;
                                    }

                                }else{
                                    $user_balance = 0;
                                    $wallet = Wallet::where('user_id',$request['user_id'])->first();
                                    $user_balance = $wallet['balance'];
                                }

                                                        
                                if($response['message'] != '' && !empty($response['message'])){
                                    //$this->response($response);
                                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                    return TRUE;
                                }else{
                                    // $this->response($cart_products);
                                    
                                    // $this->response($order_products_array);
                                    if(isset($request['special_instruction']) && $request['special_instruction'] != ''){
                                        $order_data['special_instruction'] = $request['special_instruction'];
                                    }
                                    $order_data['user_id'] = $request['user_id'];
                                    $order_data['vendor_id'] = $restaurant['id'];
                                    $order_data['order_type'] = $request['order_type'];
                                    $order_data['promo_amount'] = 0;

                                    if(isset($request['promocode_id']) && $request['promocode_id'] != ''){
                                        $order_data['promo_id'] = $promocode['id'];

                                        if($promocode['discount_type']  == '1'){

                                            $promo_amount = ($total_price / 100) * $promocode['amount'];
                                            if($promocode['max_disc'] > $promo_amount){
                                                $promo_amount = number_format((float)$promo_amount, 2, '.', '');
                                            }else{
                                                $promo_amount =  number_format((float)$promocode['max_disc'], 2, '.', ''); 
                                            }

                                            $order_data['promo_amount'] = $promo_amount;
                                                
                                        }else{
                                            // discount_type = 0
                                            $promo_amount = number_format((float)$promocode['amount'], 2, '.', ''); 
                                            $order_data['promo_amount'] = $promo_amount;
                                        }
                                    }
                                    
                                    $order_data['service_charge'] = $service_charge['value'];
                                    $order_data['reason'] = '';
                                    $service_charge_amount = ($total_price * $order_data['service_charge'] ) /100;
                                    $order_data['service_charge_amount'] = number_format((float)$service_charge_amount, 2, '.', ''); 

                                    if($request['order_type'] == '1' || $request['order_type'] == '3' ){
                                        $total_amount = ($total_price - $order_data['promo_amount']) + $order_data['delivery_fee'] + $service_charge_amount;
                                    }else{
                                        $total_amount = ($total_price - $order_data['promo_amount']) + $service_charge_amount;    
                                    }

                                    $order_data['total_amount'] = number_format((float)$total_amount, 2, '.', ''); 
                                    if($request['pay_type'] == '1'){
                                        $order_data['payment_card_id'] = $request['payment_card_id'];
                                    }

                                    if($request['order_type'] == '1' || $request['order_type'] == '3' ){

                                        $delivery_boy_percentage = System_setting::select('value')->where('path','delivery_person_percentage')->first();

                                        $delivery_boy_amount = ($delivery_fee * $delivery_boy_percentage['value'] ) /100;
                                        $order_data['delivery_boy_amount'] = number_format((float)$delivery_boy_amount, 2, '.', ''); 

                                    }

                                    $store_amount = $order_data['total_amount'] - $order_data['delivery_fee'] - $order_data['service_charge_amount'];
                                    $store_amount = ($store_amount * $restaurant['vendor']['percentage'] ) /100;
                                    $store_amount = $store_amount + $order_data['service_charge_amount'];
                                    
                                    $order_data['store_amount'] = number_format((float)$store_amount, 2, '.', '');


                                    $admin_amount = ($order_data['total_amount'] -  $order_data['store_amount'] - $order_data['delivery_boy_amount']);
                                    $order_data['admin_amount'] = number_format((float)$admin_amount, 2, '.', ''); 

                                    $order_data['alcoholic'] = $alcoholic;

                                    // $this->response($order_data);

                                    if($request['pay_type'] == '2' && $user_balance < $total_amount){
                                        $response['status'] = false;
                                        $response['message'] = 'Insufficient wallet balance.';
                                        //$this->response($response);
                                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                        return TRUE;
                                    }

                                    $order_data['payment_mode'] = $request['pay_type'];
                                    $order_data['created_at'] = date("Y-m-d H:i:s");

                                    // $this->response($order_products_array);
                                    $insert_id = Orders::insertGetId($order_data);
                                    // $insert_id = '1';
                                    if(isset($insert_id)){

                                        foreach ($order_products_array as $key => $value) {
                                            // $order_products = [];
                                            $order_addons_data = [];
                                            $order_addons = [];
                                            $order_products_data = [];

                                            $order_products_data['order_id'] = $insert_id;
                                            $order_products_data['product_id'] = $value['product_id'];
                                            $order_total_amount = $value['quantity'] * $value['price'];
                                            $order_products_data['total_amount'] = number_format((float)$order_total_amount, 2, '.', ''); 
                                            $order_products_data['amount'] = $value['price'];
                                            $order_products_data['quantity'] = $value['quantity'];
                                            // array_push($order_products, $order_products_data);
                                            // $this->response($order_products_data);
                                            
                                            $order_product_insert_id = OrderProducts::insertGetId($order_products_data);

                                            if(!empty($value['order_addons']) && count($value['order_addons']) > 0){
                                                foreach ($value['order_addons'] as $okey => $ovalue) {
                                                    $order_addons['order_id'] = $insert_id;
                                                    $order_addons['order_product_id'] = $order_product_insert_id;
                                                    $order_addons['addon_id'] = $ovalue;
                                                    foreach ($value['productaddon'] as $akey => $avalue) {
                                                        if($avalue['id'] == $ovalue){
                                                            $order_addons['price'] = $avalue['price'];
                                                        }
                                                    }
                                                    array_push($order_addons_data, $order_addons);
                                                } 
                                                OrderProductAddOn::insert($order_addons_data);
                                            }
                                        }
                                    
                                        // $this->response($order_addons_data);

                                        if($request['pay_type'] == '1'){

                                            // Add Charge
                                            $transact = array();
                                            $transact['description'] = "Order ID : ".$insert_id;
                                            $transact['source'] = $stripe_token;
                                            $transact['amount'] = $order_data['total_amount'] * 100;
                                          /*  $transact['amount'] = 100;*/
                                            $paymentResponse = addCharge($transact);
                                            // $this->response($paymentResponse);
                                            if(isset($paymentResponse['status']) && $paymentResponse['status'] == true){
                                                $res['status'] = $paymentResponse['status'];
                                                $res['reference_id'] = $paymentResponse['success']->id;
                                                $res['id'] = $paymentResponse['success']->id;

                                                if($res['status'] == 1){
                                                    $update_order_data = array('transaction_id' => $res['reference_id']);
                                                    Orders::where('id', $insert_id)->update($update_order_data);
                                                    
                                                }else{

                                                    if(isset($paymentResponse['error']['message'])){
                                                        $error = $paymentResponse['error']['message'].' '.$paymentResponse['error']['code'];
                                                       
                                                        // $error = $paymentResponse['error']['message'];
                                                        // $error = 'Please use testing payment card to make payment.';
                                                    }else{
                                                        $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                                    }
                                                    Orders::where('id', $insert_id)->delete();
                                                    OrderProducts::where('order_id', $insert_id)->delete();
                                                    OrderProductAddOn::where('order_id', $insert_id)->delete();


                                                    $response['message'] = $error;
                                                    $response['status'] = false;
                                                    //$this->response($response);
                                                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                                    return TRUE;

                                                }

                                            }else{
                                                if(isset($paymentResponse['error']['message'])){
                                                    $error = $paymentResponse['error']['message'];
                                                    // $error = 'Please use testing payment card to make payment.';
                                                }else{
                                                    $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                                }

                                                Orders::where('id', $insert_id)->delete();
                                                OrderProducts::where('order_id', $insert_id)->delete();
                                                OrderProductAddOn::where('order_id', $insert_id)->delete();

                                                $response['message'] = $error;
                                                $response['status'] = false;
                                                //$this->response($response);
                                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                                return TRUE;
                                            }

                                            

                                        }else{
                                            $insert_data = array(
                                                'user_id' => $request['user_id'],
                                                'order_id' => $insert_id,
                                                'amount' => number_format((float)$total_amount, 2, '.', ''),
                                                'type' => 'minus',
                                                'message' => 'Paid to '.$restaurant_name.' for Order #'.$insert_id
                                            );
                                            WalletHistory::insert($insert_data);

                                            $balance = $user_balance - number_format((float)$total_amount, 2, '.', '');
                                            $update_wallet_data['balance'] = number_format((float)$balance, 2, '.', '');

                                            Wallet::where('user_id', $request['user_id'])->update($update_wallet_data);
                                        }
                                        

                                        // for decrease stock

                                        foreach ($order_products_array as $dkey => $dvalue) {
                                        if($dvalue['inventory_status'] != 0){
                                            $stock = $dvalue['stock'] - $dvalue['quantity']; 
                                            $update_data['stock'] = intval($stock);
                                            Product::where('id', $value['product_id'])->update($update_data);
                                        }
                                        }
                                        //add order id
                                        $update_cart['order_id'] = $insert_id;
                                        Cart::where('id',$request['cart_id'])->where('order_id',0)->update($update_cart);
                                        

                                        //for order tracking
                                        $current_datetime = date("Y-m-d H:i:s");
                                        $track_data['order_id'] = $insert_id;
                                        $track_data['status'] = 1;
                                        $track_data['created_at'] = $current_datetime;
                                        $track_data['status_title'] = 'Order Placed';
                                        $track_data['status_text'] = 'Order received. Awaiting store confirmation.';

                                        TrackOrder::insert($track_data);

                                        $device = Devices::where('user_id',$request['user_id'])->first();
                                        $user = User::find($request['user_id']);

                                        $push_title = 'Order Placed #'.$insert_id ;
                                        $push_data = array(
                                            'order_id' => $insert_id,
                                            'message' => 'Please wait for confirmation from '.$restaurant_name.'.'
                                            /*Please wait for confirmation from Robert store.*/
                                        );
                                        $push_type = 'order_placed';
                                        $message = 'Order placed successfully';

                                        if($user['notification_status'] == 1 && $device['token'] != ''){
                                            $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
                                        }
                                        
                                        notification_add($request['user_id'],$message,$push_type,0,$insert_id,0);

                                        

                                        $phone_no = $user['phone'];
                                        if($request['order_type'] == '1' || $request['order_type'] == '3' ){
                                            // $sms_text = 'Please wait for the '.$restaurant_name.' confirmation on order #'.$insert_id.'. After the confirmation, your order will deliver.';
                                            $sms_text = 'Please wait for confirmation from '.$restaurant_name.' for order #'.$insert_id.'. After the confirmation, your order will delivered.';
                                        }else{
                                            $sms_text = 'Please wait for the '.$restaurant_name.' confirmation on order #'.$insert_id.'.';

                                        }
                                       

                                        send_SMS($phone_no,$sms_text);
                                        
                                        //send push to vendor

                                        $vendor_channels_data = VendorChannels::where('vendor_id',$request['vendor_id'])->get();
                                        if($request['order_type'] == '1' || $request['order_type'] == '3' ){
                                            $description = 'You received a new order #'.$insert_id.' for delivery.';
                                        }else{
                                            $description = 'You received a new order #'.$insert_id.' for pickup.';
                                        }
                                        $title = "New Order Arrived";


                                        if(count($vendor_channels_data) > 0){
                                            $vendor_channels_data = $vendor_channels_data->toArray();
                                            $player_ids = array_column($vendor_channels_data, 'channel_id');
                                            $send_web_push = send_web_push($player_ids, $description, $title,$type = 2);
                                        }

                                        //sms store
                                        $store = User::find($request['vendor_id']);
                                        $store_phone_no = $store['phone'];
                                        if($request['order_type'] == '1' || $request['order_type'] == '3' ){
                                            $store_sms_text = 'You received a new order #'.$insert_id.' for delivery.';
                                        }else{
                                            $store_sms_text = 'You received a new order #'.$insert_id.' for pickup.';

                                        }
                                        send_SMS($store_phone_no,$store_sms_text);
                                        /*send mail new order*/
                                        $template = file_get_contents(base_url('email_templates/new_order_request.html'));
                                        $message  = create_email_template_new($template);
                                        /*$link = base_url() .'admin/vendor-active/'.$id;*/
                                        $insertid = $insert_id;
                                        $message = str_replace('##INSERTID##', $insertid, $message);
                                        $subject = $this->config->item('site_title', 'ion_auth').' - New Order Request';
                                        $to = $restaurant_email;
                                        $sent = send_mail_new($to,$subject, $message);
                                        /*send mail end*/

                                        $response['status'] = TRUE;
                                        $response['message'] = 'Order placed successfully.';
                                        $response['order_id'] = $insert_id;
                                        $response['SendNotification'] = $SendNotification;

                                    }else{
                                        $response['status'] = false;
                                        $response['message'] = 'Server encountered an error. please try again.';
                                    }
                                    
                                }

                                
                            }else{
                                $response['status'] = false;
                                $response['message'] = 'You have not reached the minimum order amount. Please add some more items.';
                            }

                        
                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Restaurant is currently closed.';
                        }
                    }else{
                        $response['status'] = false;
                        if($request['order_type'] == 1 || $request['order_type'] == 3){
                           $text = 'Delivery';
                        }else{
                            $text = 'Takeout';   
                        }
                        $response['message'] = $text.' service is not available. ';
                    }

                }else{
                    $response['status'] = false;
                    $response['message'] = 'No restaurant found.';
                }
            
            }   

        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function get_all_orders_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();
            $orders = Orders::with(['orderproducts','vendor','delivery_boy'])->where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();
            if(count($orders) > 0){

                $orders = $orders->toArray();

                $response['data'] = $orders;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No order found.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function get_order_detail_post(){ 
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{ 
            $request = $this->post();

            $orders = $order_products = []; 
            $orders = Orders::with(['vendor','promocode','delivery_boy','user_vendor'])->where('id',$request['order_id'])->first();
            if(isset($orders)){
                $orders = $orders->toArray();

                $order_products = OrderProducts::where('order_id',$request['order_id'])->with(['orderproductsaddon'])->get();
                $subtotal = 0;
                if(count($order_products) > 0){
                    $order_products = $order_products->toArray();
                    foreach ($order_products as $key => $value) {
                        $addon_total = 0;
                        if(count($value['orderproductsaddon']) > 0){
                            foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
                                $addon_total += $cvalue['price'];
                            }
                        }
                        $addon_totals = $addon_total * $value['quantity'];
                        $order_products[$key]['addon_total'] = number_format((float)$addon_totals, 2, '.', '');
                        $total_amount = $addon_totals + $value['total_amount'];
                        $order_products[$key]['total_amount'] = number_format((float)$total_amount, 2, '.', '');
                    }
                    foreach ($order_products as $key => $value) {
                        $total_price  += $value['total_amount'];  
                        
                    }

                }
            // _pre($orders);

                if($orders['payment_mode'] == 1){
                    $orders['payment_type'] = 'Paid: Using card';
                }else{
                    $orders['payment_type'] = 'Paid: Using wallet';
                }

                if($orders['status'] == 1){
                    $order_status = 'Order received. Awaiting store confirmation.';
                }else if($orders['status'] == 2){
                    $order_status = 'Food is being prepared.';
                }else if($orders['status'] == 5){
                    $order_status = 'Food is being prepared.';
                }else if($orders['status'] == 6){
                    $order_status = 'Delivery Person is on his way to pickup your order';
                }else if($orders['status'] == 8){
                    $order_status = 'Delivery Person is reached at restaurant';
                }else if($orders['status'] == 9){
                    $order_status = 'Your order is on the way';
                }else if($orders['status'] == 10){
                    $order_status = 'Order delivered';
                }else{
                    echo $order_status ='';
                }
            // _pre($orders);

                $orders['order_status'] = $order_status;


                $orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
                $data = [];
                $data['order'] = $orders;
                $d_id = $data['order']['delivery_boy']['id'];
            // _pre($data);
                if($d_id != NULL) {
    
                 $delivery_average = $this->db->query('SELECT AVG(`delivery_boy_rate`)  as rating FROM `orders` WHERE `delivery_boy_id` = '.$d_id.' AND `delivery_boy_rate` > 0')->row_array();
            
                    $delivery_boy_rating  =  $delivery_average['rating'];
                    $data['order']['delivery_boy']['average_rating'] = number_format((float)$delivery_boy_rating, 1, '.', '');
                  
                }  
              
/*                  
                }*/

                $data['order_products'] = $order_products;
                
                $response['data'] = $data;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No order found.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function cancel_order_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $order_data =[];
            $order_data = Orders::where('id',$request['order_id'])->first();
            if (isset($order_data)){
                $order_data = $order_data->toArray();
                $order_products = OrderProducts::where('order_id',$request['order_id'])->with(['product'])->get();
                if (count($order_products) > 0){
                    $order_products = $order_products->toArray();
                    // $this->response($order_products);
                    if($order_data['status'] == 1){
                        if($order_data['payment_mode'] == 1){
                            $this->load->helper("stripe_helper");

                            $refund_stripe = refund_stripe($order_data['transaction_id'],$order_data['total_amount']);

                            if($refund_stripe['status']){
                            
                                $push_data = array(
                                    'order_id' => $request['order_id'],
                                    'message' => 'As per your request, your order #'.$request['order_id'].' has been canceled.You will get your refund in 3-4 working days.'
                                );
                                
                                $message = 'As per your request, your order #'.$request['order_id'].' has been canceled.You will get your refund in 3-4 working days.';

                                $sms_text = 'Hello '.$order_data['first_name'].'! As per your request, your order #'.$request['order_id'].' has been canceled.You will get your refund in 3-4 working days.';
                                
                            }else{
                                $response['status'] = false;
                                // $response['message'] = 'You cannot cancel the order, Payment has already been refunded';
                                $response['message'] = $refund_stripe['error']['message'];
                                //$this->response($response);
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }

                        }else{
                            $wallet = Wallet::where('user_id',$order_data['user_id'])->first();

                            $update_wallet = [];
                            $balance = $order_data['total_amount'] + $wallet['balance'] ;
                            $update_wallet['balance'] = number_format((float)$balance, 2, '.', '');

                            if( Wallet::where('user_id', $order_data['user_id'])->update($update_wallet)){
                                
                                
                                $transaction['user_id'] = $order_data['user_id'];
                                $transaction['order_id'] = $request['order_id'];
                                $transaction['amount'] = number_format((float)$order_data['total_amount'], 2, '.', '');
                                $transaction['type'] = 'plus';
                                $transaction['message'] = 'Money refunded to wallet for order #'.$request['order_id'];

                                WalletHistory::insert($insert_data);

                                $push_data = array(
                                    'order_id' => $request['order_id'],
                                    'message' => 'As per your request, your order #'.$request['order_id'].' has been canceled. Your refund will be credited to your wallet.'
                                );
                                
                                $message = 'As per your request, your order #'.$request['order_id'].' has been canceled.Your refund will be credited to your wallet.';

                            
                                $sms_text = 'Hello '.$order_data['first_name'].'! As per your request, your order #'.$request['order_id'].' has been canceled.Your refund will be credited to your wallet.';
                                
                            }else{
                                $response['status'] = false;
                                $response['message'] = 'Server encountered an error. please try again';
                                //$this->response($response);
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }
                            
                        }

                        $order_update_data['status'] = 4;
                        $order_update_data['reason'] = 'Cancel by customer';
                        Orders::where('id', $request['order_id'])->update($order_update_data);

                        //for order tracking
                        $current_datetime = date("Y-m-d H:i:s");
                        $track_data['order_id'] = $request['order_id'];
                        $track_data['status'] = 4;
                        $track_data['created_at'] = $current_datetime;
                        $track_data['status_title'] = 'Order Canceled';
                        $track_data['status_text'] = 'As per your request, your order has been canceled.';

                        TrackOrder::insert($track_data);

                        $push_title = 'Order Canceled #'.$request['order_id'] ;
                        $push_type = 'order_cancel';

                        $device = Devices::where('user_id',$order_data['user_id'])->first();
                        $user = User::find($order_data['user_id']);
                        if($user['notification_status'] == 1 && $device['token'] != ''){
                            $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
                        }
                        notification_add($order_data['user_id'],$message,$push_type,0,$order_data['id'],0);

                        
                        $phone_no = $user['phone'];
                        $phone_no = $order_data['phone'];
                        send_SMS($phone_no,$sms_text);

                        //add stock
                        foreach ($order_products as $okey => $ovalue) {
                            if($ovalue['product']['inventory_status'] != 0){
                                $stock = $ovalue['stock'] + $ovalue['quantity']; 
                                $update_data['stock'] = intval($stock);
                                Product::where('id', $value['product_id'])->update($update_data);
                            }
                        }

                        $response['status'] = true;
                        $response['message'] = 'Order cancelled successfully.';
                        
                    }elseif ($order_data['status'] == 2) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because store had already accepted the order.';
                    }elseif ($order_data['status'] == 3) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because store had already rejected the order.';
                    }elseif ($order_data['status'] == 4) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because order expired.';
                    }elseif ($order_data['status'] == 5) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because the delivery person is assigned.';
                    }elseif ($order_data['status'] == 6) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because the delivery person is assigned.';
                    }elseif ($order_data['status'] == 7) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because the delivery person is assigned.';
                    }elseif ($order_data['status'] == 8) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because the delivery person picked up your order.';
                    }elseif ($order_data['status'] == 9) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because the delivery person is on the way.';
                    }elseif ($order_data['status'] == 10) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because order has been delivered.';
                    }elseif ($order_data['status'] == 11) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because delivery failed.';
                    }elseif ($order_data['status'] == 12) {
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order because your order is already cancelled.';
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'You cannot cancel the order.';
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'No any order found.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'No any order found.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function add_rating_review_tip_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('rate', 'rate', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('review', 'review', 'trim');
        $this->form_validation->set_rules('tip', 'tip', 'trim');
        $this->form_validation->set_rules('tip_type', 'tip type', 'trim|is_natural_no_zero'); // 1-amount 2-percentage
        $this->form_validation->set_rules('payment_type', 'payment card id', 'trim|is_natural_no_zero'); // 1- card 2- wallet
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $request = array_map('trim', $request);
            if($request['rate'] > 1){
                $stars = 'stars';
            }else{
                $stars = 'star';
            }
            $orders = Orders::with(['user'])->where('user_id',$request['user_id'])->where('status', 10)->where('id', $request['order_id'])->first();
            if (count($orders) > 0) {
                $orders = $orders->toArray();
                $device = Devices::where('user_id',$orders['delivery_boy_id'])->first();
                $user = User::where('id',$orders['delivery_boy_id'])->first();
                $push_type = 'order_detail';

                if($request['tip'] != '' && $request['tip_type'] != '' && $request['payment_type'] != ''){
                    if($request['tip_type'] == 1){
                        $order_data['tip'] = round((double)$request['tip'], 2);
                    }else{
                        $total_amount =  $orders['delivery_fee'];
                        $tip = ($request['tip'] * $total_amount ) /100;
                        $order_data['tip'] = round((double)$tip, 2); 
                    }
                    // Pay by Card

                    

                    if ($request['payment_type'] == 1) {
                        $card = PaymentCard::where('user_id', $request['user_id'])->where('id', $request['payment_card_id'])->first();

                        if (isset($card)) {
                            $this->load->helper("stripe_helper");
                            $card_array['name'] = $card['card_holder_name'];
                            $card_array['card_number'] = decrypt($card['card_number']);
                            $monthyear = explode('/',decrypt($card['expiry_date']));
                            $card_array['month'] = $monthyear[0];
                            $card_array['year'] = $monthyear[1];
                            $card_array['cvc_number'] = decrypt($card['cvv']);
                            $stripeCardObject = create_token($card_array);
                            if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true){
                                $stripe_token = $stripeCardObject['token'];

                                $transact = array();
                                $transact['description'] = "Add tip for order id ".$request['order_id'];
                                $transact['source'] = $stripe_token;
                                $transact['amount'] = (double)$order_data['tip'] * 100;
                                $paymentResponse = addCharge($transact);
                                if (isset($paymentResponse['status']) && $paymentResponse['status'] == true) {
                                    $res['status'] = $paymentResponse['status'];
                                    $res['reference_id'] = $paymentResponse['success']->id;
                                    $res['id'] = $paymentResponse['success']->id;

                                    if ($res['status'] == 1) {

                                        $insert_data = array(
                                            'user_id' => $request['user_id'],
                                            'order_id' => $request['order_id'],
                                            'amount' => number_format((float)$order_data['tip'], 2, '.', ''),
                                            'type' => 'minus',
                                            'message' => 'Paid tip for Order #'.$request['order_id']
                                        );
                                        WalletHistory::insert($insert_data);
                                        
                                        $push_title = 'Tip added for Order #'.$request['order_id'] ;
                                        $message = $orders['user']['first_name'].' '.$orders['user']['last_name'].' gave $'.$order_data['tip'].' tip for Order #'.$request['order_id'].'.';
                                        $push_data = array(
                                            'order_id' => $request['order_id'],
                                            'message' => $message
                                        );
                                        
                                        if ($user['notification_status'] == 1 &&  $device['token'] != '') {
                                            $SendNotification = SendNotification($device['type'], $device['token'], $push_title, $push_data, $push_type);
                                        }
                                        notification_add($orders['delivery_boy_id'], $message, $push_type, 0, $request['order_id'], 0);
                                    } else {
                                        if (isset($paymentResponse['error']['message'])) {
                                            $error = $paymentResponse['error']['message'].' '.$paymentResponse['error']['code'];
                                        // $error = $paymentResponse['error']['message'];
                                            // $error = 'Please use testing payment card to make payment.';
                                        } else {
                                            $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                        }
    
                                        $response['message'] = $error;
                                        $response['status'] = false;
                                        //$this->response($response);
                                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                        return true;
                                    }
                                }else{
                                    if(isset($paymentResponse['error']['message'])){
                                        $error = $paymentResponse['error']['message'];
                                        // $error = 'Please use testing payment card to make payment.';
                                    }else{
                                        $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                    }
    
                                    $response['message'] = $error;
                                    $response['status'] = false;
                                    //$this->response($response);
                                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                    return TRUE;
                                }
                            }else{
                                $error = $stripeCardObject['error'];
                                $response['message'] = $error['message'];
                                $response['status'] = false;
                                // $response['card_array'] = $card_array;
                                //$this->response($response);
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return true;
                            }
                        } else {
                            $response['status'] = false;
                            $response['message'] = 'Payment card is invalid.';
                            //$this->response($response);
                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                            return true;
                        }
                    }else{
                        $user_balance = 0;
                        $wallet = Wallet::where('user_id',$request['user_id'])->first();
                        $user_balance = $wallet['balance'];
                        if($request['payment_type'] == '2' && $user_balance < $order_data['tip']){
                            $response['status'] = false;
                            $response['message'] = 'Insufficient wallet balance.';
                            //$this->response($response);
                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                            return TRUE;
                        }
                        $insert_data = array(
                            'user_id' => $request['user_id'],
                            'order_id' => $request['order_id'],
                            'amount' => number_format((float)$order_data['tip'], 2, '.', ''),
                            'type' => 'minus',
                            'message' => 'Paid tip for Order #'.$request['order_id']
                        );
                        WalletHistory::insert($insert_data);

                        $balance = $user_balance - number_format((float)$order_data['tip'], 2, '.', '');
                        $update_wallet_data['balance'] = number_format((float)$balance, 2, '.', '');

                        Wallet::where('user_id', $request['user_id'])->update($update_wallet_data);
                        
                    } 

                }

                if ($orders['delivery_boy_rate'] == 0) {
                    $rating_time = date('Y-m-d H:i:s');
                    $order_data['delivery_boy_rate'] = $request['rate'];
                    $order_data['delivery_boy_review'] = $request['review'];
                    $order_data['rate_time'] = $rating_time;
                    Orders::whereId($request['order_id'])->update($order_data);

                    $push_title = 'Rate and Review for Order #'.$request['order_id'] ;
                    $message = $orders['user']['first_name'].' '.$orders['user']['last_name'].' gave '.$request['rate'].' '.$stars.' for Order #'.$request['order_id'].'.';
                    $push_data = array(
                        'order_id' => $request['order_id'],
                        'message' => $message
                    );
                    
                    if($user['notification_status'] == 1 && $device['token'] != ''){
                        $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
                    }
                    notification_add($orders['delivery_boy_id'],$message,$push_type,0,$request['order_id'],0);

                    $response['balance'] = $balance;
                    $response['device'] = $device;
                    $response['SendNotification'] = $SendNotification;
                    $response['message'] = 'Rate & Review successfully submitted.';
                    $response['status'] = true;    

                }else{
                    $response['message'] = 'Rating already submitted.';
                    $response['status'] = false;
                }

            }else{
                $response['status'] = false;
                $response['message'] = 'No any order found.';
            }
              
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }
    public function add_tip_restaurant_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('tip', 'tip', 'trim|required');
        $this->form_validation->set_rules('tip_type', 'tip type', 'trim|required|is_natural_no_zero'); // 1-amount 2-percentage
        $this->form_validation->set_rules('payment_type', 'payment card id', 'trim|required|is_natural_no_zero'); // 1- card 2- wallet
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $request = array_map('trim', $request);
            $orders = Orders::with(['user','vendor'])->where('user_id',$request['user_id'])->where('status', 10)->where('id', $request['order_id'])->first();
            if (count($orders) > 0) {
                $orders = $orders->toArray();

                // $device = Devices::where('user_id',$orders['delivery_boy_id'])->first();
                // $user = User::where('id',$orders['delivery_boy_id'])->first();
                // $push_type = 'order_detail';
                if($orders['vendor_tip'] == 0){
                    if($request['tip'] != '' && $request['tip_type'] != '' && $request['payment_type'] != ''){
                        // _prea($request);
                        if($request['tip_type'] == 1){
                            $order_data['vendor_tip'] = number_format((float)$request['tip'], 2, '.', '');
                        }else{
                            $total_amount = $orders['total_amount'] - $orders['service_charge_amount'] - $orders['delivery_fee'];
                            $tip = ($request['tip'] * $total_amount ) /100;
                            /*_prea($tip);*/
                              $order_data['vendor_tip'] = number_format((float)$tip, 2, '.', ''); 
                                /* _prea($tip);*/
                        }
    
                        // Pay by Card
    
                        if ($request['payment_type'] == 1) {
                            $card = PaymentCard::where('user_id', $request['user_id'])->where('id', $request['payment_card_id'])->first();
    
                            if (isset($card)) {
    
                                $this->load->helper("stripe_helper");
                                $card_array['name'] = $card['card_holder_name'];
                                $card_array['card_number'] = decrypt($card['card_number']);
                                $monthyear = explode('/',decrypt($card['expiry_date']));
                                $card_array['month'] = $monthyear[0];
                                $card_array['year'] = $monthyear[1];
                                $card_array['cvc_number'] = decrypt($card['cvv']);
    
                                $stripeCardObject = create_token($card_array);
                                if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true){
                                    $stripe_token = $stripeCardObject['token'];
    
                                    $transact = array();
                                    $transact['description'] = "Add tip for order id ".$request['order_id'];
                                    $transact['source'] = $stripe_token;
                                    $transact['amount'] = $order_data['vendor_tip'] * 100;
                                    $paymentResponse = addCharge($transact);
    
                                    if (isset($paymentResponse['status']) && $paymentResponse['status'] == true) {
                                        $res['status'] = $paymentResponse['status'];
                                        $res['reference_id'] = $paymentResponse['success']->id;
                                        $res['id'] = $paymentResponse['success']->id;
    
                                        if ($res['status'] == 1) {

                                            $insert_data = array(
                                                'user_id' => $request['user_id'],
                                                'order_id' => $request['order_id'],
                                                'amount' => number_format((float)$order_data['vendor_tip'], 2, '.', ''),
                                                'type' => 'minus',
                                                'message' => 'Paid tip to '.$orders['vendor']['name'].' for Order #'.$request['order_id']
                                            );
                                            WalletHistory::insert($insert_data);
    
                                            /* $push_title = 'Tip added for Order #'.$request['order_id'] ;
                                            $message = $orders['user']['first_name'].' '.$orders['user']['last_name'].' gave $'.$order_data['vendor_tip'].' as tip for Order #'.$request['order_id'].'.';
                                            $push_data = array(
                                                'order_id' => $request['order_id'],
                                                'message' => $message
                                            );
                                            
                                            if ($user['notification_status'] == 1 &&  $device['token'] != '') {
                                                $SendNotification = SendNotification($device['type'], $device['token'], $push_title, $push_data, $push_type);
                                            }
                                            notification_add($orders['delivery_boy_id'], $message, $push_type, 0, $request['order_id'], 0); */
                                            
                                            Orders::whereId($request['order_id'])->update($order_data);
                                            $response['message'] = 'Tip successfully submitted.';
                                            $response['status'] = true;  
                                        } else {
                                            if (isset($paymentResponse['error']['message'])) {
                                                $error = $paymentResponse['error']['message'].' '.$paymentResponse['error']['code'];
                                            } else {
                                                $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                            }
        
                                            $response['message'] = $error;
                                            $response['status'] = false;
                                            //$this->response($response);
                                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                            return true;
                                        }
                                    }else{
                                        if(isset($paymentResponse['error']['message'])){
                                            $error = $paymentResponse['error']['message'];
                                            if($error == 'This value must be greater than or equal to 1.'){
                                                $error = 'Amount must be at least $0.50 cad';
                                            }
                                            // $error = 'Please use testing payment card to make payment.';
                                        }else{
                                            $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                        }
        
                                        $response['message'] = $error;
                                        $response['status'] = false;
                                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                        return TRUE;
                                    }
                                }else{
                                    $error = $stripeCardObject['error'];
                                    $response['message'] = $error['message'];
                                    $response['status'] = false;
                                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                    return true;
                                }
                            } else {
                                $response['status'] = false;
                                $response['message'] = 'Payment card is invalid.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return true;
                            }
                        }else{
                            $user_balance = 0;
                            $wallet = Wallet::where('user_id',$request['user_id'])->first();
                            $user_balance = $wallet['balance'];
                            if($request['payment_type'] == '2' && $user_balance < $order_data['vendor_tip']){
                                $response['status'] = false;
                                $response['message'] = 'Insufficient wallet balance.';
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }
                            $insert_data = array(
                                'user_id' => $request['user_id'],
                                'order_id' => $request['order_id'],
                                'amount' => number_format((float)$order_data['vendor_tip'], 2, '.', ''),
                                'type' => 'minus',
                                'message' => 'Paid tip to '.$orders['vendor']['name'].' for Order #'.$request['order_id']
                            );
                            WalletHistory::insert($insert_data);
    
                            $balance = $user_balance - number_format((float)$order_data['vendor_tip'], 2, '.', '');
                            $update_wallet_data['balance'] = number_format((float)$balance, 2, '.', '');
    
                            Wallet::where('user_id', $request['user_id'])->update($update_wallet_data);
                            
                            Orders::whereId($request['order_id'])->update($order_data);
    
                            $response['message'] = 'Tip successfully submitted.';
                            $response['status'] = true;  
                        } 
    
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Tip is already given.';
                }
                

            }else{
                $response['status'] = false;
                $response['message'] = 'No any order found.';
            }
              
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function re_order_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('vendor_id', 'vendor id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            


            $request = $this->post();
            $cart_data =[];
            $product_data = $this->db->query("SELECT * FROM order_products WHERE order_id =  '".$request['order_id']."' ")->row_array(); 
            $od_data = OrderProducts::where('order_id',$request['order_id'])->get();
            foreach($od_data as $key => $value){
            $product_id = $value['product_id'];
            $p_data = $this->db->from('product')->where('id',$product_id)->get()->row_array();
            $stock =$p_data['stock'];
            $inventory_status =$p_data['inventory_status'];
            if($inventory_status == 0 OR $stock == 0) {
                $response['status'] = FALSE;
                $response['message'] = 'Product is out of stock!';
                $this->response($response);
                die(); 
                                
            }
            
            }

            $cart_data = CartProducts::select('cart_products.*','product.title','product.inventory_status','product.stock','product.price','product.type')
            ->join('cart', 'cart.id','=','cart_products.cart_id')
            ->join('product', 'product.id','=','cart_products.product_id')
            ->where('cart.order_id',$request['order_id'])
            ->where('cart.user_id',$request['user_id'])
            ->with(['cartproductaddon'])
            ->get();

            if (count($cart_data) > 0){ 
                $cart_data = $cart_data->toArray();
                
                $existing_cart_data = Cart::where('user_id',$request['user_id'])->where('order_id',0)->first();
                
                if (isset($existing_cart_data)) {
                    $existing_cart_data = $existing_cart_data->toArray();

                    $cart_id = $existing_cart_data['id'];

                    $cart_products_exists = CartProducts::where('cart_id',$cart_id)->get();
                    if(count($cart_products_exists) > 0){
                        $cart_products_exists = $cart_products_exists->toArray();
                        $cart_add_on_ids = array_column($cart_products_exists,'id'); 
                            
                        // $this->response($cart_add_on_ids);
                        //delete cart_add_on
                        CartProductAddons::whereIn('cart_product_id',$cart_add_on_ids)->delete();
                        CartProducts::where('cart_id',$cart_id)->delete();
                        Cart::where('user_id',$request['user_id'])->where('order_id',0)->delete();
                        $response['status'] = true;
                    }
                }

                // $response['cart_data'] =$cart_data;
                foreach ($cart_data as $key => $value) {
                    //user_id vendor_id product_id qty addon_id
                    $add_on_ids = '';
                    if(!empty($value['cartproductaddon'])){
                        $add_on_ids = array_column($value['cartproductaddon'],'addon_id');
                        $add_on_ids = implode(',',$add_on_ids); 
                    }
                    $response_data = $this->add_to_cart_reorder($request['user_id'],$request['vendor_id'],$value['product_id'],$value['qty'],$add_on_ids);
                    if($response_data['status'] == false){
                        $response['status'] = false;
                        $response['message'] = $response_data['message'];
                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                        return TRUE;
                    }
                }
                // add to cart
                $response['message'] = "Added to cart successfully";
                $response['status'] = true;
                
            }else{
                $response['status'] = false;
                $response['message'] = 'Products are not available.';
            }
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function add_to_cart_reorder($user_id,$vendor_id,$product_id,$qty ,$addon_id){

        $restaurant = User::with(['vendor'])->where('active','1')->find($vendor_id);
                
            if(isset($restaurant)){

                $product_data = Product::with(['addon'])->where('status', 1)->where('user_id',$vendor_id)->where('id',$product_id)->first();
                
                if (isset($product_data)){
                    $product_data = $product_data->toArray();

                    if($product_data['inventory_status'] != 0){
                        if($product_data['stock'] < $qty){
                            $response['status'] = false;
                            $response['message'] = 'Product is out of stock!';  
                            return $response;
                        }
                    }

                    if(isset($addon_id) && $addon_id != ''){
                        $add_on_ids = explode(',',$addon_id);
                        rsort($add_on_ids);
                        $available_addons = ProductAddOn::whereIn('id',$add_on_ids)->where('product_id',$product_id)->get();
                        if(count($add_on_ids) != count($available_addons)){
                            $response['status'] = false;
                            $response['message'] = 'Product addons are not exists';  
                             return $response;
                        }
                    }

                    $same_restaurant = Cart::where('user_id',$user_id)->where('order_id',0)->first();
                    
                    if(isset($same_restaurant)){
                        
                        if($vendor_id != $same_restaurant['vendor_id']){
                            //delete all carts
                            
                            //find cart_add_on_ids
                            
                            $cart_products_exists = CartProducts::where('cart_id',$same_restaurant['id'])->get();
                            if(count($cart_products_exists) > 0){
                                $cart_products_exists = $cart_products_exists->toArray();
                                $cart_add_on_ids = array_column($cart_products_exists,'id'); 
                                
                                // $this->response($cart_add_on_ids);
                                //delete cart_add_on
                                CartProductAddons::whereIn('cart_product_id',$cart_add_on_ids)->delete();
                                CartProducts::whereIn('id',$cart_add_on_ids)->delete();
                                Cart::where('user_id',$user_id)->where('order_id',0)->delete();

                            }
                            
                            
                        }
                    }
                    
                    $existing_cart = CartProducts::select("cart.user_id","cart.vendor_id","cart_products.*")
                                        ->join('cart', 'cart.id','=','cart_products.cart_id')
                                        ->where('cart.user_id',$user_id)
                                        ->where('cart.vendor_id',$vendor_id)
                                        ->where('cart.order_id',0)
                                        ->where('cart_products.product_id',$product_id)
                                        ->with(['cartproductaddon'])
                                        ->when(!empty($addon_id), function ($query) {
                                                $query->join('cart_product_addons', 'cart_product_addons.cart_product_id','=','cart_products.id');
                                        })
                                        ->when(!empty($addon_id), function ($query) use ($add_on_ids){
                                            $query->whereIn('cart_product_addons.addon_id',$add_on_ids);
                                        })
                                        //->groupBy('cart.id')
                                        ->get();

                    $quantity = false;
                    // $this->response($existing_cart);

                    if(isset($existing_cart) && count($existing_cart) > 0){
                        $existing_cart = $existing_cart->toArray();

                        if(isset($addon_id) && $addon_id != ''){

                            foreach ($existing_cart as $ekey => $evalue) {
                                if(!empty($evalue['cartproductaddon']) && $evalue['cartproductaddon'] != ''){
                                    foreach ($evalue['cartproductaddon'] as $akey => $avalue) {
                                        $cart_product_add_on[] = $avalue;
                                    }
                                }
                                if(!empty($cart_product_add_on)){
                                    $cart_ids = array_column($cart_product_add_on,'cart_product_id');
                                    $cart_ids = array_unique($cart_ids);
                                    $cart_ids = array_values($cart_ids);

                                    foreach ($cart_product_add_on as $ckey => $cvalue) {
                                        foreach ($cart_ids as $cart_key => $cart_value) {
                                            if($cart_value == $cvalue['cart_product_id']){
                                                $cart[$cart_key]['id'] = $cart_value;
                                                $cart[$cart_key]['add_on'][] = $cvalue['addon_id'];
                                                
                                            }
                                        }
                                    } 

                                    foreach ($cart as $key => $value) {
                                        $cart[$key]['add_on'] = array_unique($value['add_on']);
                                        $cart[$key]['total_add_on'] = count($cart[$key]['add_on']);
                                    }
                                    $quantity = false;
                                    foreach ($cart as $key => $value) {
                                        if(count($add_on_ids) == $value['total_add_on']){
                                            if($value['add_on'] == $add_on_ids){
                                                $quantity = true;
                                                $quantity_id = $value['id'];
                                            }else{
                                                $quantity = false;
                                            }
                                        }
                                    }

                                }

                            }
                        }else{
                            $quantity = false;
                            foreach ($existing_cart as $ekey => $evalue) {
                                if(count($evalue['cartproductaddon']) == 0){
                                    $quantity = true;
                                    $quantity_id = $evalue['id'];
                                }
                            }
                        }
                    }
                    // $this->response($quantity);

                    if($quantity == false){


                        $add_on = true;
                        if(isset($addon_id) && $addon_id != ''){
                            if(!empty($product_data['addon']) && $product_data['addon'] != ''){
                                $product_data_add_on_ids = array_column($product_data['addon'],'id');
                                
                                foreach ($add_on_ids as $add_key => $add_value) {
                                    if(in_array($add_value,$product_data_add_on_ids)){
                                        $add_on = true;
                                    }else{
                                        $add_on = false;
                                        break;
                                    } 
                                }
                                
                            }else{
                                $add_on = false;
                            }
                        }

                        if($add_on == true){
                            // $this->response($same_restaurant);
                            if(isset($same_restaurant) && $vendor_id == $same_restaurant['vendor_id'] ){
    
                                $cart_products['cart_id'] = $same_restaurant['id'];
                                $cart_products['product_id'] = $product_id;
                                $cart_products['qty'] = $qty;
    
                                $cart_products_insert_id = CartProducts::insertGetId($cart_products);
    
                                if(isset($addon_id) && $addon_id != ''){
                                    foreach ($add_on_ids as $key => $value) {
    
                                        $temp_add_on = array(
                                            'cart_product_id' => $cart_products_insert_id,
                                            'addon_id' => $value,
                                        );   
                                        $cart_product_addons[] = $temp_add_on;
                                    }
                                    $cart_product_addons_insert_id = CartProductAddons::insert($cart_product_addons);
                                }
                            }else{
                                $cart_data['user_id'] = $user_id;
                                $cart_data['vendor_id'] = $vendor_id;
                                $cart_data['order_id'] = 0;
    
                                $cart_insert_id = Cart::insertGetId($cart_data);
    
                                $cart_products['cart_id'] = $cart_insert_id;
                                $cart_products['product_id'] = $product_id;
                                $cart_products['qty'] = $qty;
    
                                $cart_products_insert_id = CartProducts::insertGetId($cart_products);
    
                                if(isset($addon_id) && $addon_id != ''){
                                    foreach ($add_on_ids as $key => $value) {
    
                                        $temp_add_on = array(
                                            'cart_product_id' => $cart_products_insert_id,
                                            'addon_id' => $value,
                                        );   
                                        $cart_product_addons[] = $temp_add_on;
                                    }
                                    $cart_product_addons_insert_id = CartProductAddons::insert($cart_product_addons);
                                }
                            }

                            $response['status'] = true;
                            $response['message'] = 'Added to cart successfully';  


                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Product add on is not available!'; 
                            return $response;
                        }
                    }else{

                        //Quantity+1
                        if(isset($quantity_id) && $quantity_id != ''){
                            $cart_products_details = CartProducts::whereId($quantity_id)->first();
                            if(isset($cart_products_details)){
                                $updated_data['qty'] = $cart_products_details['qty'] + $qty;
                                CartProducts::whereId($quantity_id)->update($updated_data);

                                $response['status'] = true;
                                $response['message'] = 'Added to cart successfully'; 
                            }
                        }
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Product not found.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Restaurant not found.';
                return $response;
            }
        return $response;
    }

    public function web_push_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            /*_pre($request['order_id']);*/
            $orders = Orders::where('id', $request['order_id'])->first();
            if (count($orders) > 0) {
                $orders = $orders->toArray();

                $vendor_channels_data = VendorChannels::where('vendor_id',$request['user_id'])->get();
                $description = 'You received a new order #'. $request['order_id'].'.Please assign Delivery Person for the order.';
                $title = "New Order Arrived";
                if(count($vendor_channels_data) > 0){
                    $vendor_channels_data = $vendor_channels_data->toArray();
                    $player_ids = array_column($vendor_channels_data, 'channel_id');
                    $send_web_push = send_web_push($player_ids, $description, $title,$type = 1);                    
                }

                $user=User::where('id',$request['user_id'])->first();
                $userPhone=$user->phone;
                $userEmail=$user->email;  
                $phone_no =  $userPhone;
                $sms_text = 'Order ID: #'.$request['order_id'].' requires delivery person to be assigned. Kindly assign a RAD Driver from the Assign Delivery Person located in the Admin Panel.';
                send_SMS($phone_no,$sms_text); 
        
                $template = file_get_contents(base_url('email_templates/order_assignment.html'));
                $message  = create_email_template_new($template);
                $orderiduser = $request["order_id"];
                $message = str_replace('##ORDERIDUSER##', $orderiduser, $message);
                /*_pre($message);*/
                $subject = $this->config->item('site_title', 'ion_auth').' Delivery Person';
                $to = $userEmail;
                $sent = send_mail_new($to,$subject, $message);


                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No any order found.';
            }
              
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }
    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
   
}
?>