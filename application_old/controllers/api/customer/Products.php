<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);
class Products extends REST_Controller {
    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Banner');
        $this->load->model('Devices');
        $this->load->model('Product');
        $this->load->model('Promocode');
        $this->load->model('Orders');
        $this->load->model('TrackOrder');
        $this->load->model('Category');
        $this->load->model('Vendor');
        $this->load->model('Favourite');
        $this->load->model('ion_auth_model');
        $this->load->model('System_setting');
        $this->load->model('Cart');
        $this->load->model('CartProducts');
        $this->load->model('CartProductAddons');
        $this->load->model('DeliveryAddress');
        $this->load->library(['ion_auth', 'form_validation']);
    }
    public function customAlpha($str) {
        if($str != ''){
            if (!preg_match('/^[0-9a-z .,\-]+$/i', $str)) {
                $this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
                return false;
            }
        }
        
        return TRUE;
    }
    public function validateDateFormat($str) {
        if($str != ''){
            if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$str)) {
                $this->form_validation->set_message('validateDateFormat', 'The {field} field contain invalid date format.');
                return false;
            }
        }
        return TRUE;
    }

    public function validate_latlong($str){
        if (!preg_match('/([0-9.-]+).+?([0-9.-]+)/', $str)) {
                $this->form_validation->set_message('validate_latlong', 'The {field} field is invalid.');
                return false;
        }
        
        return TRUE;
    }

    public function category_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $category = $data = [];
            $category = Category::select('*')->where('status',1)->orderBy('id', 'desc')->get();

            $response['data'] = $category;
            $response['status'] = TRUE;
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }
    public function distance($lat1, $lon1, $lat2, $lon2) {
        $google_key = $this->config->item("google_key");
        $lat1 = '38.0255579';
        $lon1 ='-84.4938638';
        $lat2 = '54541'; 
        $lon2 ='-84.488372';
        $api = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$lon1."&destinations=".$lat2.",".$lon2."&mode=driving&language=pl-PL&key=".$google_key);
        $data = json_decode($api);
        
        if(isset($data->rows[0]->elements[0]->distance->value)){

            $distance = ((int)$data->rows[0]->elements[0]->distance->value);
            $min = ($data->rows[0]->elements[0]->duration->text);
            $distance = number_format((float)$distance / 1000, 2, '.', '');

            $result['status'] = true;
            $result['distance'] = $distance;
            $result['min'] = $min;
        }else{
            $result['status'] = false;
        }
        
        return $result;
    }

    public function nearby_restaurants_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('filter', 'filter', 'trim|required'); // 1 - delivery 2- takeaway 3-both
        $this->form_validation->set_rules('sorting', 'sorting', 'trim|required'); // 1 - within 3 miles 2- rating high to low 3-popularity
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $restaurants = $data = [];
            // print_r($request);exit;
            $restaurants = User::select("users.*","vendor.service_available")
                                
                                ->join('users_groups', 'users_groups.user_id','=','users.id')
                                ->join('vendor', 'vendor.user_id','=','users.id')
                                ->orderBy('id', 'desc')
                                ->with(['vendor'])
                                ->where(function ($query){
                                    $query->where('users.active',1)
                                    ->where('users_groups.group_id','2');
                                })
                                ->when($request['filter'] == 1, function ($query) {
                                    $query->where(function ($query){
                                        $query->where('vendor.service_available',1)
                                        ->orWhere('vendor.service_available',3);
                                    });
                                })
                                ->when($request['filter'] == 2, function ($query) {
                                    $query->where(function ($query){
                                        $query->where('vendor.service_available',2)
                                        ->orWhere('vendor.service_available',3);
                                    });
                                })
                                ->get();
                                // $this->response($restaurants);
            $store_ids = [];
            if (isset($request['user_id']) && $request['user_id'] != '') {
                $fav = Favourite::where('user_id', $request['user_id'])->orderBy('id', 'desc')->groupBy('store_id')->get();

                if (count($fav) > 0) {
                    $fav= $fav->toArray();
                    $store_ids = array_column($fav, 'store_id');
                }
            }
            $delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();
            if(count($restaurants) > 0 ){

                $restaurants = $restaurants->toArray();
                foreach ($restaurants as $key => $value) {
                    $restaurants[$key]['favourite'] = 0;   

                    if(in_array($value['id'],$store_ids)){
                        $restaurants[$key]['favourite'] = 1;
                    }
                    
                    $distance_array = get_delivery_miles($request['latitude'], $request['longitude'], $value['vendor']['latitude'], $value['vendor']['longitude']);

                    if($distance_array['status'] == true){
                        // echo round($distance_array['distance']).' '.$delivery_miles_available['value'];
                        if(round($distance_array['distance']) <=  $delivery_miles_available['value']){
                            $restaurants[$key]['distance'] = round($distance_array['distance']).' Km' ;
                            $restaurants[$key]['min'] = ($distance_array['min']) ;
                            $restaurants[$key]['dis'] = round($distance_array['distance']);
                        }else{
                            unset($restaurants[$key]); 
                        }
                    }else{
                        unset($restaurants[$key]); 
                    }
                }
                $restaurants = array_values($restaurants); 
                $rest = array_column($restaurants, 'dis');
                array_multisort($rest, SORT_ASC, $restaurants);

                if($request['sorting'] == 1){
                    //within 3 miles
                    $filtered_array = array_filter($restaurants, function ($var) {
                        return ($var['dis'] <= 3);
                    });
                    $filtered_array = array_values($filtered_array);
                    if(empty($filtered_array)){
                        $response['status'] = false;
                        $response['message'] = 'No restaurant found.';
                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                        return true;
                    }else{
                        $response['data'] = $filtered_array;
                        $response['status'] = true;
                    }
                }elseif ($request['sorting'] == 2) {
                    //rating high to low 
                    $rest = array_column($restaurants, 'rating');
                    array_multisort($rest, SORT_DESC, $restaurants);

                    if(empty($restaurants)){
                        $response['status'] = false;
                        $response['message'] = 'No restaurant found.';
                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                        return true;
                    }else{
                        $response['data'] = $restaurants;
                        $response['status'] = true;
                    }
                }elseif ($request['sorting'] == 3) {
                    //popularity
                    $ids = array_column($restaurants,'id');
                    $orders = Orders::whereIn('vendor_id',$ids)->where('status',10)->get();

                    if(count($orders) > 0){
                        $orders = $orders->toArray();
                        $total = 0;
                        foreach ($orders as $key => $value) {
                            foreach ($ids as $ikey => $ivalue) {
                                if($ivalue == $value['vendor_id']){
                                    $temp[$ikey]['id'] = $ivalue;
                                    $temp[$ikey]['total'] += 1;
                                }
                            }
                        }

                        foreach ($restaurants as $rkey => $rvalue) {
                           foreach ($temp as $tkey => $tvalue) {
                            $restaurants[$rkey]['total_orders'] = 0;
                               if($rvalue['id'] == $tvalue['id']){
                                    $restaurants[$rkey]['total_orders'] = $tvalue['total'];
                               }
                           }
                        }
                    }   

                    // $this->response($restaurants);
                    $rest = array_column($restaurants, 'total_orders');
                    array_multisort($rest, SORT_DESC, $restaurants);

                    if(empty($restaurants)){
                        $response['status'] = false;
                        $response['message'] = 'No restaurant found.';
                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                        return true;
                    }else{
                        $response['data'] = $restaurants;
                        $response['status'] = true;
                    }
                }else {
                    $response['data'] = $restaurants;
                }

                $response['status'] = TRUE;
            }else{
                $response['status'] = false;
                $response['message'] = 'No restaurant found.';
            }

        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function home_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $category = $data = [];
            $category = Category::select('*')->where('status',1)->orderBy('id', 'desc')->get();
            $banner = Banner::orderBy("sequence", 'ASC')->get();
            $restaurants = User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','2')->where('users.active',1)->orderBy('id', 'desc')->with(['vendor'])->get();

            $store_ids = [];
            if (isset($request['user_id']) && $request['user_id'] != '') {
                $fav = Favourite::where('user_id', $request['user_id'])->orderBy('id', 'desc')->groupBy('store_id')->get();

                if (count($fav) > 0) {
                    $fav= $fav->toArray();
                    $store_ids = array_column($fav, 'store_id');
                }

                $default_address = DeliveryAddress::whereUserId($request['user_id'])->where('default', '1')->first();

                if (isset($default_address) && count($default_address) > 0) {
                    $data['address'] = $default_address;
                }
            }

            $delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();

            if(count($restaurants) > 0 ){
                $restaurants = $restaurants->toArray();
                foreach ($restaurants as $key => $value) {
                    $restaurants[$key]['favourite'] = 0;   

                    if(in_array($value['id'],$store_ids)){
                        $restaurants[$key]['favourite'] = 1;
                    }
                    $distance_array = get_delivery_miles($request['latitude'], $request['longitude'], $value['vendor']['latitude'], $value['vendor']['longitude']);

                    if ($distance_array['status'] == true) {
                        if (round($distance_array['distance']) <=  $delivery_miles_available['value']) {
                            $restaurants[$key]['distance'] = round($distance_array['distance']).' Km' ;
                            $restaurants[$key]['min'] = ($distance_array['min']) ;
                            $restaurants[$key]['dis'] = round($distance_array['distance']);
                        }else{
                            unset($restaurants[$key]); 
                        }
                    }else{
                        unset($restaurants[$key]); 
                    }

                }
                $restaurants = array_values($restaurants); 
                $rest = array_column($restaurants, 'dis');
                array_multisort($rest, SORT_ASC, $restaurants);
                $data['restaurant_see_all'] = false;
                if(count($restaurants) > 6){
                    $data['restaurant_see_all'] = true;
                }
                $restaurants = array_slice($restaurants,0,6);
            }
            if (count($category) > 0) {
                $category = $category->toArray();
                $data['category_see_all'] = false;
                if (count($category) > 6) {
                    $data['category_see_all'] = true;
                }
                $category = array_slice($category, 0, 6);
            }
            $data['banner'] = $banner;
            $data['restaurants'] = $restaurants;
            $data['category'] = $category;

            $response['data'] = $data;
            $response['status'] = TRUE;
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function get_category_restaurants_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('category_id', 'category id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $restaurants = $data = [];
            
            $product = Product::where('status', 1)->where('category_id',$request['category_id'])->groupBy('user_id')->get();
            if(count($product) > 0){
                $product = $product->toArray();
                $user_ids = array_column($product,'user_id');
                $restaurants = User::with(['vendor'])->where('active',1)->whereIn('id',$user_ids)->get();

                $store_ids= [];
                if (isset($request['user_id']) && $request['user_id'] != '') {
                    $fav = Favourite::where('user_id', $request['user_id'])->orderBy('id', 'desc')->groupBy('store_id')->get();

                    if (count($fav) > 0) {
                        $fav= $fav->toArray();
                        $store_ids = array_column($fav, 'store_id');
                    }
                }

                $delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();

                if(count($restaurants) > 0 ){

                    $restaurants = $restaurants->toArray();
                    foreach ($restaurants as $key => $value) {
                        $restaurants[$key]['favourite'] = 0;   

                        if(in_array($value['id'],$store_ids)){
                            $restaurants[$key]['favourite'] = 1;
                        }
                        $distance_array = get_delivery_miles($request['latitude'], $request['longitude'], $value['vendor']['latitude'], $value['vendor']['longitude']);
                        
                        if($distance_array['status'] == true){
                            if (round($distance_array['distance']) <=  $delivery_miles_available['value']) {
                                $restaurants[$key]['distance'] = round($distance_array['distance']).' Km' ;
                                $restaurants[$key]['min'] = ($distance_array['min']) ;
                                $restaurants[$key]['dis'] = round($distance_array['distance']);
                            }else{
                                unset($restaurants[$key]);
                            }
                        }else{
                            unset($restaurants[$key]); 
                        }
                        
                    }
                    $restaurants = array_values($restaurants); 
                    $rest = array_column($restaurants, 'dis');
                    array_multisort($rest, SORT_ASC, $restaurants);
                }

                if(empty($restaurants)){
                    $response['status'] = false;
                    $response['message'] = 'No restaurant found.';
                }else{
                    $response['data'] = $restaurants;
                    $response['status'] = true;
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'No restaurant found.';
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function get_restaurant_details_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('restaurant_id', 'restaurant id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('alcoholic', 'alcoholic', 'trim|required');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{  
            $request = $this->post();
            $restaurant = [];

            $restaurant = User::with(['vendor'])->where('active',1)->find($request['restaurant_id']);
                
            if(isset($restaurant)){

                
                $restaurant['favourite'] = 0; 
                if (isset($request['user_id']) && $request['user_id'] != '') {
                    $fav = Favourite::where('user_id', $request['user_id'])->where('store_id', $request['restaurant_id'])->first();
                    if (count($fav)> 0) {
                        $restaurant['favourite'] = 1;
                    }
                }
                if($request['alcoholic'] == 1){
                    $product = Product::with(['variant','addon'])->where('status', 1)->where('user_id',$request['restaurant_id'])->orderBy('id','desc')->get();

                }else{
                    $product = Product::with(['variant','addon'])->where('status', 1)->where('type', 0)->where('user_id',$request['restaurant_id'])->orderBy('id','desc')->get();

                }

                if(isset($product) && count($product) > 0){

                    $product = $product->toArray();
                    $restaurant['cart_vendor_id'] = 0;

                    if (isset($request['user_id']) && $request['user_id'] != '') {
                        $existing_cart = Cart::where('user_id', $request['user_id'])->where('order_id', 0)->first();
                        // $restaurant['cart_vendor_id'] = 0;
                        if (isset($existing_cart) && count($existing_cart) > 0) {
                            $restaurant['cart_vendor_id'] = $existing_cart->vendor_id;
                        }
                    }
                    $category_ids = array_column($product, 'category_id');
                    $category_ids = array_unique($category_ids);
                    $category_ids = array_values($category_ids);
                    
                    $featured_product = $temp = [];
                    foreach ($product as $key => $value) {
                        foreach ($category_ids as $c_key => $c_value) {
                            if($c_value == $value['category_id']){
                                $value['cart'] = FALSE;
                                $value['cart_data'] = [];
                                $category_product[$c_key]['category_id'] = $c_value;
                                $category_product[$c_key]['category_name'] = $value['category']['name'];
                                $category_product[$c_key]['products'][] = $value;
                            }
                        }
                        if($value['featured'] == 1){
                            $temp = $value;
                            array_push($featured_product,$temp);
                        }
                    }

                    if(!empty($featured_product) && count($featured_product) > 0){
                        $featured_number = array_column($featured_product, 'featured_number');
                        array_multisort($featured_number, SORT_ASC, $featured_product);
                    }
                    if (isset($request['user_id']) && $request['user_id'] != '') {
                        $cart_data = CartProducts::select("cart_products.*", "cart.user_id", "cart.vendor_id")
                                    ->join('cart', 'cart.id', '=', 'cart_products.cart_id')
                                    ->where('cart.user_id', $request['user_id'])
                                    ->where('cart.vendor_id', $request['restaurant_id'])
                                    ->where('cart.order_id', 0)
                                    ->with(['cartproductaddon'])
                                    ->get();
                    }
                    // $this->response($cart_data);
                
                    if(isset($cart_data) && count($cart_data) > 0){
                        $cart_data = $cart_data->toArray();

                        foreach ($category_product as $cat_key => $cat_value) {
                            foreach ($cat_value['products'] as $pkey => $pvalue) {
                                foreach ($cart_data as $cart_key => $cart_value) {

                                    if($cart_value['product_id'] == $pvalue['id']){
                                        $category_product[$cat_key]['products'][$pkey]['cart'] = true;
                                        $category_product[$cat_key]['products'][$pkey]['cart_data'][] = $cart_value;
                                    }
                                } 
                            }
                        }

                        if(!empty($featured_product) && count($featured_product) > 0){
                            foreach ($featured_product as $f_key => $f_value) {
                                foreach ($cart_data as $cart_key => $cart_value) {
                                    if($cart_value['product_id'] == $f_value['id']){
                                        $featured_product[$f_key]['cart'] = true;
                                        $featured_product[$f_key]['cart_data'][] = $cart_value;
                                    }
                                } 
                            }
                        }
                        
                    }

                    $restaurant['category_product'] = $category_product;
                    if(!empty($featured_product) && count($featured_product) > 0){
                        $restaurant['featured_product'] = $featured_product;
                    }else{
                        $restaurant['featured_product'] = [];
                    }


                }else{
                    $restaurant['featured_product'] = [];
                    $restaurant['category_product'] = [];
                }

                
                $distance_array = get_delivery_miles($request['latitude'], $request['longitude'], $restaurant['vendor']['latitude'], $restaurant['vendor']['longitude']);

                if($distance_array['status'] == true){
                    $restaurant['distance'] = round($distance_array['distance']).' Km' ;
                    $restaurant['min'] = ($distance_array['min']) ;
                    $restaurant['dis'] = round($distance_array['distance']);

                    $response['data'] = $restaurant;
                    // $response['distance_array'] = $distance_array;
                    $response['status'] = TRUE;

                }else{
                    $response['status'] = false;
                    $response['message'] = 'No restaurant found.';
                }
                
            }else{
                $response['status'] = false;
                $response['message'] = 'No restaurant found.';
            }
        }
        // $this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }
    public function get_promocode_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('restaurant_id', 'restaurant id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required|greater_than[0]');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $data = $promocode = $promoids = [];
            $user_ids = [0,$request['restaurant_id']];
            //$promocode = Promocode::where('promo_min_order_amount','<=',intval($request['amount']))->where('usage_limit' ,'>',0)->where('status','=',1)->whereIn('user_id',$user_ids)->get();
            


            $promocode_all = Promocode::where('usage_limit' ,'>',0)->whereIn('user_id',$user_ids)->where('status','=',1)->get();
            $promocode_all_array = $promocode_all->toArray();
            foreach($promocode_all_array as $key => $value){
                $max_usage = $value['usage_limit'];
                $orders = Orders::where(['user_id'=>$request['user_id'],'promo_id'=>$value['id']])->get();
          /*  _pre($max_usage);*/
                if(count($orders->toArray()) >= $max_usage){
                // _pre('max used');
                    unset($promocode_all_array[$key]);
                }
            }

             $promocode_all = $promocode_all_array;
           /* _pre($promocode_all->toArray());*/
            if (count($promocode_all) > 0){
                // $promocode_all = $promocode_all->toArray();
                foreach ($promocode_all as $key => $value) {
                    if($value['promo_min_order_amount'] <= intval($request['amount'])){
                        $promocode_all[$key]['valid'] = 1;
                    }else{

                        $promocode_all[$key]['valid'] = 0;
                        $minimum = $value['promo_min_order_amount'] - $request['amount'];
                        $minimum = number_format((float)$minimum, 2, '.', '');
                        $promocode_all[$key]['error_msg'] = 'Add items worth $'.$minimum.' to apply this offer.';
                    }
                    if($value['discount_type']  == 1){
                        $promocode_all[$key]['discount_string'] = "Get ".$value['amount']."% OFF upto $".$value['max_disc'];
                        $request['amount'] = (float) $request['amount'];
                        $value['max_disc'] = (float) $value['max_disc'];
                        $promo_amount = ($request['amount'] / 100) * $value['amount'];
                        $promo_amount = (float) $promo_amount;
                        if($value['max_disc'] > $promo_amount){
                            $promo_amount = number_format((float)$promo_amount, 2, '.', '');
                        }else{
                            $promo_amount =  number_format((float)$value['max_disc'], 2, '.', ''); 
                        }

                        $promocode_all[$key]['promo_amount'] = $promo_amount;
                        
                    }else{
                        $promocode_all[$key]['discount_string'] = "Get $".$value['amount']." OFF";
                        $promocode_all[$key]['promo_amount'] = number_format((float)$value['amount'], 2, '.', '');
                    }
                    $promocode_all[$key]['maximum_string'] = "Valid on order placed above $".$value['promo_min_order_amount'];
                }

                $promo_amount = array_column($promocode_all, 'promo_amount');
                array_multisort($promo_amount, SORT_DESC, $promocode_all);
                    

                $response['data'] = $promocode_all;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No any promocode available.';
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }
    public function favorite_unfavorite_restaurant_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('restaurant_id', 'restaurant id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post(); 
            if($request['status'] == 2 ) {
                Favourite::where('user_id',$request['user_id'])->where('store_id',$request['restaurant_id'])->delete();
                $response['status'] = true;
                $response['message'] = 'Restaurant unfavorite successfully.';
            }else{
                $fav = Favourite::where('user_id',$request['user_id'])->where('store_id',$request['restaurant_id'])->first();
                if (isset($fav)){
                    $response['status'] = true;
                    $response['message'] = 'Restaurant favorite successfully.';
                }else{
                    $data['user_id'] = $request['user_id']; 
                    $data['store_id'] = $request['restaurant_id']; 
                    Favourite::insert($data);
                    $response['status'] = true;
                    $response['message'] = 'Restaurant favorite successfully.';
                }
            }
            
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function get_favorite_restaurant_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post(); 
            $restaurants = [];
            $fav = Favourite::where('user_id',$request['user_id'])->orderBy('id', 'desc')->groupBy('store_id')->get();
            if (count($fav) > 0){
                $fav= $fav->toArray();
                $store_ids = array_column($fav,'store_id');
                $restaurants = User::with(['vendor'])->whereIn('id',$store_ids)->get();

                $delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();

                if(count($restaurants) > 0 ){

                    $restaurants = $restaurants->toArray();
                    foreach ($restaurants as $key => $value) {
                        $restaurants[$key]['favourite'] = 1;   
                        $distance_array = get_delivery_miles($request['latitude'], $request['longitude'], $value['vendor']['latitude'], $value['vendor']['longitude']);
                        if($distance_array['status'] == true){
                            if (round($distance_array['distance']) <=  $delivery_miles_available['value']) {
                                $restaurants[$key]['distance'] = round($distance_array['distance']).' Km' ;
                                $restaurants[$key]['min'] = ($distance_array['min']) ;
                                $restaurants[$key]['dis'] = round($distance_array['distance']);
                            }else{
                                unset($restaurants[$key]); 
                            }
                        }else{
                            unset($restaurants[$key]); 
                        }
                    }
                    $restaurants = array_values($restaurants);
                    $rest = array_column($restaurants, 'dis');
                    array_multisort($rest, SORT_ASC, $restaurants);
                }

                if(empty($restaurants)){
                    $response['status'] = false;
                    $response['message'] = 'No restaurant found.';
                }else{
                    $response['data'] = $restaurants;
                    $response['status'] = true;
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'No restaurant found.';
            }
            
            
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function search_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('search_text', 'search text', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required|callback_validate_latlong');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required|callback_validate_latlong');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $category = Category::where('name', 'like', '%' . $request['search_text'] . '%')->where('status', 1)->orderBy('id', 'desc')->get();

          /*  $productsss = Product::where('status', 1)->where('title', 'like', '%' . $request['search_text'] . '%')->orderBy('id','desc')->get();*/

            $products = Product::where('status', 1)->where('title', 'like', '%' . $request['search_text'] . '%')->whereHas('user',function($query){
                $query->whereNull('deleted_at')->where('active',1);
            })->orderBy('id','desc')->get();


/* $restaurants = Vendor::select("vendor.*")->where('name', 'like', '%' . $request['search_text'] . '%')->with(['user'])->join('users', 'users.id','=','vendor.user_id')->where('users.active','1')->orderBy('vendor.id', 'desc')->get();*/
           /* _pre($products->toArray());*/
           /* print_r($products->toArray());*/

            // $restaurants = Vendor::select("vendor.*")->where('name', 'like', '%' . $request['search_text'] . '%')->with(['user'])->join('users', 'users.id','=','vendor.user_id')->where('users.active','1')->orderBy('vendor.id', 'desc')->get();
           $restaurants = Vendor::select("vendor.*")->where('name', 'like', '%' . $request['search_text'] . '%')->with(['user'])->join('users', 'users.id','=','vendor.user_id')->whereHas('user',function($query){
                $query->whereNull('deleted_at')->where('active',1);
            })->orderBy('vendor.id', 'desc')->get();

           /*_pre($restaurants->toArray());*/
            if(count($category) > 0){
                $category = $category->toArray();
                foreach ($category as $key => $value) {
                    $category[$key]['result_type'] = 'category';
                }
            }else{
                $category = array();
            }
            if(count($products) > 0){
                $products = $products->toArray();
                foreach ($products as $key => $value) {
                    $products[$key]['result_type'] = 'product';
                }
            }else{
                $products = array();
            }
            $delivery_miles_available = System_setting::select('value')->where('path','delivery_miles_available')->first();
            if(count($restaurants) > 0){
                // _pre($restaurants->toArray());
                $restaurants = $restaurants->toArray();
                foreach ($restaurants as $key => $value) {
                    $restaurants[$key]['result_type'] = 'restaurants';

                    $distance_array = get_delivery_miles($request['latitude'], $request['longitude'], $value['latitude'], $value['longitude']);

                    
                    if($distance_array['status'] == true){
                        if (round($distance_array['distance']) <=  $delivery_miles_available['value']) {
                            $restaurants[$key]['distance'] = round($distance_array['distance']).' Km' ;
                            $restaurants[$key]['min'] = ($distance_array['min']) ;
                            $restaurants[$key]['dis'] = round($distance_array['distance']);
                        }else{
                            unset($restaurants[$key]); 
                        }
                    }else{
                        unset($restaurants[$key]); 
                    }
                }
                $restaurants = array_values($restaurants);
                $rest = array_column($restaurants, 'dis');
                array_multisort($rest, SORT_ASC, $restaurants);
            }else{
                $restaurants = array();
            }

            $result = array_merge($category, $products, $restaurants);

            if(!empty($result) && count($result) > 0){
                $response['data'] = $result;
                // $response['category'] = $category;
                // $response['products'] = $products;
                // $response['restaurants'] = $restaurants;
                $response['status'] = TRUE;
            }else{
                $response['status'] = false;
                $response['message'] = 'No data found.';
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
   
}
?>