<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
error_reporting(0);

class Common extends REST_Controller {

	function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Faq');
		$this->load->model('ion_auth_model');
		$this->load->library(['ion_auth', 'form_validation']);

	}

	public function faq_get(){
        $faq = Faq::select('id','question','answer')->where('status',1)->orderBy('id', 'desc')->get();
        if (count($faq) > 0){
            $response['data'] = $faq;
            $response['status'] = true;
        }else{
            $response['data'] = array();
            $response['message'] = 'No any FAQ found';
            $response['status'] = false;
        }
        // $this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }


	public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
}
?>