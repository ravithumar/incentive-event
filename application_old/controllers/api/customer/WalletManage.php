<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class WalletManage extends REST_Controller {

	function __construct() {
        header('Content-Type: text/html; charset=utf-8');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
        $this->load->model('PaymentCard');
        $this->load->model('Wallet');
        $this->load->model('WalletHistory');
        $this->load->model('Devices');
		$this->load->model('ion_auth_model');
		$this->load->library(['ion_auth', 'form_validation']);

	}
    public function customAlpha($str) {
        if($str != ''){
            if (!preg_match('/^[a-z \-]+$/i', $str)) {
                $this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
                return false;
            }
        }
        
        return TRUE;
    }
    public function add_to_wallet_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('payment_card_id', 'payment card id', 'trim|is_natural_no_zero');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required');

        $this->form_validation->set_rules('type', 'type', 'trim|required'); // 1-payment_card_id 2- add_payment_card

        $this->form_validation->set_rules('save', 'save', 'trim'); // 1-save 2- Dont save

        $this->form_validation->set_rules('card_holder_name', 'card holder name', 'trim');
        $this->form_validation->set_rules('card_number', 'card number', 'trim');
        $this->form_validation->set_rules('expiry_date', 'expiry date', 'trim');
        $this->form_validation->set_rules('cvv', 'cvv', 'trim');
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            if($request['type'] == 1){
                if($request['payment_card_id'] != '' && isset($request['payment_card_id'])) {
                    $payment_card = PaymentCard::where('user_id',$request['user_id'])->whereId($request['payment_card_id'])->first();
                    if(isset($payment_card)){

                        $this->load->helper("stripe_helper");
                        $card = $payment_card;
                        $card_array['name'] = $card['card_holder_name'];
                        $card_array['card_number'] = decrypt($card['card_number']);
                        $monthyear = explode('/',decrypt($card['expiry_date']));
                        $card_array['month'] = $monthyear[0];
                        $card_array['year'] = $monthyear[1];
                        $card_array['cvc_number'] = decrypt($card['cvv']);

                        $stripeCardObject = create_token($card_array);
                        if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true){
                            $stripe_token = $stripeCardObject['token'];

                            $transact = array();
                            $transact['description'] = "Add to wallet for user id ".$request['user_id'];
                            $transact['source'] = $stripe_token;
                            $transact['amount'] = $request['amount'] * 100;
                            $paymentResponse = addCharge($transact);

                            if(isset($paymentResponse['status']) && $paymentResponse['status'] == true){

                                $res['status'] = $paymentResponse['status'];
                                $res['reference_id'] = $paymentResponse['success']->id;
                                $res['id'] = $paymentResponse['success']->id;

                                if($res['status'] == 1){
                                    
                                    $wallet = Wallet::where('user_id',$request['user_id'])->first();
                                    $wallet = $wallet->toArray();
                        
                                    $update_data = [];
                                    $balance = $request['amount'] + $wallet['balance'] ;
                                    $update_data['balance'] = number_format((float)$balance, 2, '.', '');
        
                                    Wallet::where('user_id', $request['user_id'])->update($update_data);
                                    
                                    $wallet_balance = Wallet::where('user_id',$request['user_id'])->first();
                                        
                                    $transaction['user_id'] = $request['user_id'];
                                    $transaction['amount'] = number_format((float)$request['amount'], 2, '.', '');
                                    $transaction['type'] = 'plus';
                                    $transaction['message'] = 'Money added to wallet';
        
                                    WalletHistory::insert($transaction);
        
                                    $response['wallet_balance'] = $wallet_balance['balance'];
                                    $response['status'] = true;
                                    $response['message'] = 'Amount has been added in wallet successfully';

                                }else{

                                    if(isset($paymentResponse['error']['message'])){
                                        $error = $paymentResponse['error']['message'].' '.$paymentResponse['error']['code'];
                                        // $error = $paymentResponse['error']['message'];
                                        // $error = 'Please use testing payment card to make payment.';
                                    }else{
                                        $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                    }

                                    $response['message'] = $error;
                                    $response['status'] = false;
                                    //$this->response($response);
                                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                    return TRUE;

                                }

                            }else{
                                if(isset($paymentResponse['error']['message'])){
                                    $error = $paymentResponse['error']['message'];
                                    // $error = 'Please use testing payment card to make payment.';
                                }else{
                                    $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                }

                                $response['message'] = $error;
                                $response['status'] = false;
                                //$this->response($response);
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                            }

                        }else{
                            $error = $stripeCardObject['error'];
                            $response['message'] = $error['message'];
                            $response['status'] = false;
                            // $response['card_array'] = $card_array;
                            //$this->response($response);
                            echo json_encode($response, JSON_UNESCAPED_UNICODE);
                            return TRUE;
                            
                        }

                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Payment card not found.';
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Payment card id not found.';
                }
            }elseif($request['type'] == 2){
                if($request['card_holder_name'] != '' && $request['card_number'] != '' && $request['expiry_date'] != '' && $request['cvv'] != ''){
                    $card_number = $request['card_number'];
                    $card_number = str_replace(' ', '', $card_number);
        
                    $card_type = validate_customer_card($card_number);
                    // $card_type = 1;
        
                    if($card_type != ''){
        
                            $payment_card_data = array(
                                'user_id' => $request['user_id'],
                                'card_holder_name' => $request['card_holder_name'],
                                'card_number' => encrypt($card_number),
                                'display_number' => 'XXXX XXXX XXXX '.substr($card_number, -4),
                                'expiry_date' => encrypt($request['expiry_date']),
                                'cvv' => encrypt($request['cvv']),
                                'card_type' => $card_type,
                            );
        
                            $card_data = array(
                                'Fullname' => $request['card_holder_name'],
                                'credit_card_no' => $card_number,
                                'security_code' => $request['cvv'],
                                'expiry_date' => $request['expiry_date']
                                    );
        
                            $this->load->helper("stripe_helper");
                            $card = $payment_card_data;
                            $card_array['name'] = $card['card_holder_name'];
                            $card_array['card_number'] = decrypt($card['card_number']);
                            $monthyear = explode('/',decrypt($card['expiry_date']));
                            $card_array['month'] = $monthyear[0];
                            $card_array['year'] = $monthyear[1];
                            $card_array['cvc_number'] = decrypt($card['cvv']);
    
                            $stripeCardObject = create_token($card_array);
                            if (isset($stripeCardObject['status']) && $stripeCardObject['status'] == true) {
                                $stripe_token = $stripeCardObject['token'];

                                $transact = array();
                                $transact['description'] = "Add to wallet for user id ".$request['user_id'];
                                $transact['source'] = $stripe_token;
                                $transact['amount'] = $request['amount'] * 100;
                                $paymentResponse = addCharge($transact);

                                if(isset($paymentResponse['status']) && $paymentResponse['status'] == true){
                                    $res['status'] = $paymentResponse['status'];
                                    $res['reference_id'] = $paymentResponse['success']->id;
                                    $res['id'] = $paymentResponse['success']->id;
    
                                    if ($res['status'] == 1) {
                                        //for saving card
                                       
                                        if($request['save'] ==  1){
                                            PaymentCard::insert($payment_card_data);
                                        }
                                        $wallet = Wallet::where('user_id',$request['user_id'])->first();
                                        if(isset($wallet)){
                                            $wallet = $wallet->toArray();
                                            
                                            $update_data = [];
                                            $balance = $request['amount'] + $wallet['balance'] ;
                                            $update_data['balance'] = number_format((float)$balance, 2, '.', '');
                
                                            Wallet::where('user_id', $request['user_id'])->update($update_data);
                                            
                                            $wallet_balance = Wallet::where('user_id',$request['user_id'])->first();
                                                
                                            $transaction['user_id'] = $request['user_id'];
                                            $transaction['amount'] = number_format((float)$request['amount'], 2, '.', '');
                                            $transaction['type'] = 'plus';
                                            $transaction['message'] = 'Money added to wallet';
                
                                            WalletHistory::insert($transaction);
                
                                            $response['wallet_balance'] = $wallet_balance['balance'];
                                            $response['status'] = true;
                                            $response['message'] = 'Amount added to wallet successfully';
                                            
                                            
                                        }else{
                                            $response['status'] = false;
                                            $response['message'] = 'Wallet not found.';
                                        }
                                       
                                    }else{

                                        if(isset($paymentResponse['error']['message'])){
                                            $error = $paymentResponse['error']['message'].' '.$paymentResponse['error']['code'];
                                            // $error = $paymentResponse['error']['message'];
                                            // $error = 'Please use testing payment card to make payment.';
                                        }else{
                                            $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                        }
    
                                        $response['message'] = $error;
                                        $response['status'] = false;
                                        //$this->response($response);
                                        echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                        return TRUE;
        
                                        }  
                                }else{
                                    if(isset($paymentResponse['error']['message'])){
                                        $error = $paymentResponse['error']['message'];
                                        // $error = 'Please use testing payment card to make payment.';
                                    }else{
                                        $error = "Invalid Card detail. Recheck payment card detail or use another payment card.";
                                    }
    
                                    $response['message'] = $error;
                                    $response['status'] = false;
                                    //$this->response($response);
                                    echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                    return TRUE;
                                }    
                            }else{
                                $error = $stripeCardObject['error'];
                                $response['message'] = $error['message'];
                                $response['status'] = false;
                                // $response['card_array'] = $card_array;
                                //$this->response($response);
                                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                                return TRUE;
                                
                            }
                            
                            
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Invalid card number';
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Please enter all the details to add the card';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'Please enter valid type.';
            }
            

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function get_name_send_money_post()
    {
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $user = User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','5')->where('phone',$request['phone'])->first();
            $user_sender = User::whereId($request['user_id'])->first();

            if(isset($user)){
                $response['data'] = $user['first_name'].' '.$user['last_name'];
                $response['status'] = true;

            }else{
                $response['status'] = false;
                $response['message'] = 'User not found.';

            }
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function send_money_post(){
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required|greater_than_equal_to[1]');
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $user = User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','5')->where('phone',$request['phone'])->first();
            $user_sender = User::whereId($request['user_id'])->first();

            if(isset($user)){

                if($user['id'] != $request['user_id']){
                    $wallet = Wallet::where('user_id',$request['user_id'])->first();
                    $userwallet = Wallet::where('user_id',$user['id'])->first();

                    if(isset($wallet) && isset($userwallet)){
                        $wallet = $wallet->toArray();

                        if($wallet['balance'] >= $request['amount']){
                            $userwallet = $userwallet->toArray();
    
                            //for sender
                            $update_data = [];
                            $balance = $wallet['balance'] - $request['amount'] ;
                            $update_data['balance'] = number_format((float)$balance, 2, '.', '');
        
                            Wallet::where('user_id', $request['user_id'])->update($update_data);
                            
                            $wallet_balance = Wallet::where('user_id',$request['user_id'])->first();
                                
                            $transaction['user_id'] = $request['user_id'];
                            $transaction['amount'] = number_format((float)$request['amount'], 2, '.', '');
                            $transaction['type'] = 'minus';
                            $transaction['message'] = 'Sent to '.$request['phone'];
        
                            WalletHistory::insert($transaction);
        
                            //for reciever
                            $update_data_receiver = [];
                            $balance = $userwallet['balance'] + $request['amount'] ;
                            $update_data_receiver['balance'] = number_format((float)$balance, 2, '.', '');
        
                            Wallet::where('user_id', $user['id'])->update($update_data_receiver);
                            
                            $transaction_receiver['user_id'] = $user['id'];
                            $transaction_receiver['amount'] = number_format((float)$request['amount'], 2, '.', '');
                            $transaction_receiver['type'] = 'plus';
                            $transaction_receiver['message'] = 'Received from '.$user_sender['phone'];
        
                            WalletHistory::insert($transaction_receiver);

                            //send notification to receiver
                            $device = Devices::where('user_id', $user['id'])->first();

                            $sender = User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','5')->where('users.id',$request['user_id'])->first();
                    
                            // $this->response($device);
                            $message = $sender->first_name.' '.$sender->last_name.' sent you $'.$request['amount'].'.';
                            $push_data['message'] = $message;
                            $push_data['user_id'] = $request['user_id'];
                            $push_type = 'send_money';
                            
                            if(!empty($device) && count($device) > 0){
                                if ($user['notification_status'] == 1 && $device['token'] != '') {
                                    $SendNotification = SendNotification($device['type'], $device['token'], 'Money Received', $push_data, $push_type);
                                }
                                $response['SendNotification'] = $SendNotification;
                                $response['push_data'] = $push_data;
                            }
                            $success_data = notification_add($user['id'], $message,$push_type,$request['user_id'],0,0);

                            $reciever = $user['first_name'].' '.$user['last_name'];
        
                            $response['wallet_balance'] = $wallet_balance['balance'];
                            $response['SendNotification'] = $SendNotification;
                            $response['success_data'] = $success_data;
                            $response['status'] = true;
                            $response['message'] = 'Money has been sent to '.$reciever.' successfully';
                        }else{
                            $response['status'] = false;
                            $response['message'] = 'Insufficient balance.';
                        }
                    
                    }else{
                        $response['status'] = false;
                        $response['message'] = 'Wallet not found.';
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'You can not send money to self.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'User not found.';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function wallet_history_post(){ 
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
           $response['status'] = false;
           $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $data = [];
            $wallet_history = WalletHistory::where('user_id',$request['user_id'])->orderBy('id', 'desc')->get();
            if (count($wallet_history) > 0){
                $wallet_history = $wallet_history->toArray();
                
                $wallet = Wallet::where('user_id',$request['user_id'])->first();
                $data['wallet_balance'] = $wallet['balance'];
                $data['wallet_history'] = $wallet_history;

                $response['data'] = $data;
                $response['status'] = true;
            }else{
                $response['status'] = false;
                $response['message'] = 'No history found.';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

	public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }
}
?>