<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class Auth extends REST_Controller {

	function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Devices');
        $this->load->model('Wallet');
		$this->load->model('ion_auth_model');
        $this->load->model('System_setting');
		$this->load->library(['ion_auth', 'form_validation']);

	}

    /* public function init_get($app_version = '',$type = '', $user_id = ''){

        $postFields['app_version'] = $app_version;        
        $type = ($type == 'ios')?'customer_ios':'customer_android';               
        $postFields['type'] = $type;           
        $errorPost = $this->ValidatePostFields($postFields);
        if(empty($errorPost)){

            $MaintenanceMode = (array)$this->db->get_where('appsetting',array('app_name' => 'maintenance_mode'))->row();
            $AppVersion = (array)$this->db->get_where('appsetting',array('app_name' => $type))->row();
            $current_version = (Int)str_replace('.', '',$AppVersion['app_version']);
            $app_version = (Int)str_replace('.', '', $app_version);

            if($MaintenanceMode['updates'] == 1){
                $response['status'] = false;
                $response['update'] = false;
                $response['maintenance'] = true;
                $response['message'] = 'Server under maintenance, please try again after some time';
            }

            else if($app_version < $current_version && $AppVersion['updates'] == 0){
                $response['status'] = true;
                $response['update'] = false;
                $response['maintenance'] = false;
                $response['message'] = 'RAD app new version available';
            }

            else if($app_version < $current_version && $AppVersion['updates'] == 1){
                $response['status'] = false;
                $response['update'] = true;
                $response['maintenance'] = false;
                $response['message'] = 'RAD app new version available, please upgrade your application';
            }
            else{
                $response['status'] = true;
                $response['update'] = false;
                $response['maintenance'] = false;
                $response['message'] = '';
            }

            if($response['status'] == TRUE && $user_id != ''){

                $user = User::whereId($user_id)->where("active",1)->first();
                $tax = System_setting::where('path','tax')->first();
                $service_charge = System_setting::where('path','service_charge')->first();
                $response['tax'] = $tax['value'];
                $response['service_charge'] = $service_charge['value'];

                if (isset($user) && count($user) > 0){

                    $response['status'] = true;
                   
                }else{
                    $response['status'] = true;
                    $response['blocked'] = true;
                    $response['message'] = 'User is blocked';
                }
            }

        }else{
            $response['status'] = false;
            $response['message'] = $errorPost;
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    } */

    public function init_get($app_version = '',$type = '', $user_id = ''){

        $postFields['app_version'] = $app_version;        
        $type = ($type == 'ios')?'customer_ios':'customer_android';               
        $postFields['type'] = $type;                  
        $errorPost = $this->ValidatePostFields($postFields);
        if(empty($errorPost)){

            $MaintenanceMode = (array)$this->db->get_where('appsetting',array('app_name' => 'maintenance_mode'))->row();
            $AppVersion = (array)$this->db->get_where('appsetting',array('app_name' => $type))->row();
            $current_version = (Int)str_replace('.', '',$AppVersion['app_version']);
            $app_version = (Int)str_replace('.', '', $app_version);

            if($MaintenanceMode['updates'] == 1){
                $response['status'] = false;
                $response['state'] = 2;
                // $response['update'] = false;
                // $response['maintenance'] = true;
                $response['message'] = 'Server under maintenance, please try again after some time';
            }

            else if($app_version < $current_version && $AppVersion['updates'] == 0){
                $response['status'] = true;
                $response['state'] = 0;

                // $response['update'] = true; 
                // $response['maintenance'] = false;
                $response['message'] = 'RAD app new version available';
            }

            else if($app_version < $current_version && $AppVersion['updates'] == 1){
                $response['status'] = false;
                $response['state'] = 1;

                // $response['update'] = true;
                // $response['maintenance'] = false;
                $response['message'] = 'RAD app new version available, please upgrade your application';
            }
            else{
                $response['status'] = true;
                $response['state'] = 3;

                // $response['update'] = false;
                // $response['maintenance'] = false;
                $response['message'] = '';
            }

            if($response['status'] == TRUE && $user_id != ''){

                $user = User::whereId($user_id)->where("active",1)->first();

                if (isset($user) && count($user) > 0){

                    $response['status'] = true;
                }else{
                    $response['status'] = true;
                    $response['blocked'] = true;
                    $response['message'] = 'RAD app new version available';
                }
            }

        }else{
            $response['status'] = false;
            $response['message'] = $errorPost;
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }
    
    public function login_post(){

        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
        $this->form_validation->set_rules('device_name', 'device name', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim|required');
        $this->form_validation->set_rules('lng', 'longitude', 'trim|required');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{

            $request = $this->post();
           /* $user = User::select('users.*')->where('phone', $request['phone'])->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[5])->first();*/

            $user = User::select('users.*')->where(array('users.phone'=> $request['phone'], "users_groups.group_id" => "5"))->join('users_groups', 'users_groups.user_id', 'users.id')
            ->join('groups', 'users_groups.group_id', 'groups.id')->first();
            // echo json_encode($user, JSON_UNESCAPED_UNICODE);
            // return TRUE;
            if(isset($user) && !empty($user)){
                $user = $user->toArray();
            	if($user['active'] == '1'){
            		$check = $this->ion_auth->api_login($request['phone'], $request['password'], $request['device_token'], $request['device_type'], $request['lat'], $request['lng'], 5, FALSE);
                    
                    if ($check){
            		/*if ($this->ion_auth->login($request['phone'], $request['password'], 5, FALSE)){*/
                        /*_pre($check);*/
                        $check = (array)$check;
            			$messages = $this->ion_auth->messages();

            			$messages = str_ireplace('<p>', '', $messages);
						$messages = str_ireplace('</p>', '', $messages); 

            			$response['message'] = $messages;

            			$user['api_key'] = $this->_generate_key($check['id']);
            			
                        $user_data = array(
                            'latitude' => $request['lat'],
                            'longitude' => $request['lng'],
                        );
                        
                        User::where('id',$check['id'])->update($user_data);
                        $check_device = Devices::where('user_id', $check['id'])->first();
						
                        if($check_device->token != '' && $check_device->token != $request['device_token']){
                            $push_title = 'logout';
                            $push_data = array(
                                'user_id' => $check['id'],
                                'message' => 'logout'
                            );
                            $push_type = 'silent';
                            $SendNotification = SendNotification($check_device->type,$check_device->token, $push_title, $push_data, $push_type);
                        }
						if (isset($check_device)) {
							$check_device->token = $request['device_token'];
							$check_device->type = $request['device_type'];
							$check_device->name = $request['device_name'];
							$check_device->lat = $request['lat'];
							$check_device->lng = $request['lng'];
							$check_device->update();
						} else {
							$device = new Devices;
							$device->user_id = $check['id'];
							$device->token = $request['device_token'];
							$device->type = $request['device_type'];
							$device->name = $request['device_name'];
							$device->lat = $request['lat'];
							$device->lng = $request['lng'];
							$device->save();
						}

                		$response['status'] = TRUE;
            			$response['user'] = $user;
            		}else{
            			$errors = $this->ion_auth->errors();

            			$errors = str_ireplace('<p>', '', $errors);
						$errors = str_ireplace('</p>', '', $errors);    

            			$response['message'] = $errors;
            			$response['status'] = FALSE;
            		}
                   
            	}else if($user['active'] == '0'){
            		$response['message'] = 'Sorry! Your account is not verified, please activate it from your registered phone number';
            		$response['status'] = FALSE;
            	}else if($user['active'] == '2'){
            		$response['message'] = 'Account deactivated.';
            		$response['status'] = FALSE;
            	}else{
            		$response['message'] = 'Something went wrong';
            		$response['status'] = FALSE;
            	}
            }else{
            	$response['user'] = $user;
            	$response['message'] = 'The phone number or password you have entered is incorrect.';
            	$response['status'] = FALSE;
            }
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function social_check_exists_post(){
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
        $this->form_validation->set_rules('device_name', 'device name', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim|required');
        $this->form_validation->set_rules('lng', 'longitude', 'trim|required');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $user = User::select('users.*')->where('users.email', $request['email'])
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->first();

            if (isset($user) && count($user) > 0) {

            	if($user['active'] == '1'){

            		$update_data = array(
            			'token' => $request['device_token'],
            			'type' => $request['device_type'],
            			'name' => $request['device_name'],
            			'lat' => $request['lat'],
            			'lng' => $request['lng']
            		);
            		Devices::where('user_id', $user['id'])->update($update_data);

            		if($user['password'] == ''){
                        $user->reset_password = false;
                    }else{
                        $user->reset_password = true;
                    }

                    $user['api_key'] = $this->_generate_key($user['id']);

                    $response['user'] = $user;
                    $response['status'] = true;

            	}else{
            		$response['status'] = false;
                    $response['is_deactivated'] = 1;
                    $response['message'] = 'Your account has been deactivated';
            	}

            }else{
                $response['status'] = false;
                $response['is_deactivated'] = 0;
                $response['message'] = 'Your email does not exist.';
            }

           
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function email_exist_check($str){
        if($str != ''){
            $user = User::select('users.*')->where('users.email', $str)
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->first();
            if (count($user) == 0) {
                return TRUE;
            }
            $this->form_validation->set_message('email_exist_check', "Email is already exist.");
            return FALSE;
        }else{
            return TRUE; 
        }
    }

    function phone_exist_check($str) {
        if (!preg_match('/^\((\d)\d{2}\) \d{3}-\d{4}$/', $str)) {
            $this->form_validation->set_message('phone_exist_check', 'The {field} field is invalid.');
            return false;
        }else{
            $user = User::select('users.*')->where('users.phone', $str)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[5])
            ->first();
            if (count($user) == 0) {
                return TRUE;
            }
            $this->form_validation->set_message('phone_exist_check', "Phone number is already exist.");
            return FALSE;
        }
    }

    function can_use_phone_check_post(){
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|callback_email_exist_check');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            
            $user = User::select('users.*')->where('users.phone', $request['phone'])
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->first();
            if (count($user) == 0) {

                //send otp
                $response['status'] = true;
                $response['message'] = 'User can use this phone number';
            }else{
                $response['status'] = false;
                $response['message'] = 'Phone number is already exist.';
            }

            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }

    public function register_old_post()
    {

        $this->form_validation->set_rules('first_name', 'first name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'last name', 'trim|required');
        
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');

        $this->form_validation->set_rules('phone', 'phone', 'trim|required|callback_phone_exist_check');
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|callback_email_exist_check');

        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
        $this->form_validation->set_rules('device_name', 'device name', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim|required');
        $this->form_validation->set_rules('lng', 'longitude', 'trim|required');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{


            $request = $this->post();
            // $this->load->library('Twilio');

            // $response = $this->twilio->validate_phone('+13069922861');


            // $this->response($response);
            
            $phone = $request['phone'];
            $password = $request['password'];

            $names = explode(' ', $request['full_name'], 2);

            $user_data = array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],

                'phone' => $request['phone'],
                'referral_code'=> generate_referral_code()
            );

            if(isset($request['email'])){
                $email = $user_data['email'] = $request['email'];
            }else{
                $email = '';
            }
            
            $id = $this->ion_auth->register($phone, $password, $email, $user_data, [5]);

            //$this->response($id);
            if(isset($id) && !is_null($id) && !$id == false){

                // OTP Send
                // send_SMS($request['phone']);

                //Wallet Balance
                $wallet['user_id'] = $id;
                $wallet['balance'] = '0';
                Wallet::insert($wallet);
                
                Devices::insert(['user_id' => $id, 'name' => $request['device_name'], 'token' => $request['device_token'], 'type' => $request['device_type'], 'lat' => $request['lat'],'lng' => $request['lng'] ]);

                // $otp = generate_OTP();

                $user = User::find($id)->toArray();
                $user['api_key'] = $this->_generate_key($id);
                unset($user['password']);
                $response['status'] = TRUE;
                $response['user'] = $user;
                $response['message'] = 'Registered successfully';
            }else{
                $response['status'] = false;
                $errors = $this->ion_auth->errors();

                $errors = str_ireplace('<p>', '', $errors);
                $errors = str_ireplace('</p>', '. ', $errors);    

                $response['message'] = $errors;
            }
        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function send_otp_post(){
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            // $user = User::where('users.phone', $request['phone'])
            //     ->join('users_groups', 'users_groups.user_id', 'users.id')
            //     ->whereIn('users_groups.group_id',[5])
            //     ->first();
            // if (count($user) == 0) {

            //     $otp = generate_OTP();
            //     $response['otp'] = $otp;
            //     $response['status'] = true;
            //     $response['message'] = 'OTP sent successfully.';

            // }else{
            //     $response['status'] = false;
            //     $response['message'] = 'Phone number is already exist.';
            // }
			
            $otp = generate_OTP();
            $msg = 'Your RAD OTP is '.$otp.'.';
            send_SMS($request['phone'],$msg);
			
			$this->db->insert("sended_otp", array("type" => "customer", "phone" => $_POST['phone'], "otp" => $otp));
			
            $response['otp'] = $otp;
            $response['status'] = true;
            $response['message'] = 'OTP has been sent to the registered phone number';
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }


    public function logout_post(){
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $data['token'] = '';
            Devices::where('token',$request['device_token'])->update($data);

            $response['status'] = true;
            $response['message'] = 'Logged out successfully';
        }
        
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        return TRUE;
    }   

    public function change_password_post(){


        $this->form_validation->set_rules('old', 'current password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']');
        $this->form_validation->set_rules('new', 'new password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']');
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required|is_natural_no_zero');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            // $user = User::find($request['user_id']);
            $user = User::select('users.*')->where('users.id', $request['user_id'])
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->first();
            // $this->response($user);
            if (count($user) > 0) {
                $change = $this->ion_auth->change_password($user->phone, $this->input->post('old'), $this->input->post('new'), $user->id);
            
                if($change){
                    $response['status'] = true;
                    $response['message'] = 'Password changed successfully';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Current password is incorrect';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'This account does not exists';
            }

        }

        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function reset_password_post(){
        $this->form_validation->set_rules('password', 'password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required|callback_phone_check');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $user = User::select('users.*')->where('users.phone', $request['phone'])
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->first();
 
            if (count($user) > 0) {
                $change = $this->ion_auth->reset_password($user->phone, $this->input->post('password'),$user->id);
           
                if($change){
                    $response['status'] = true;
                    $response['message'] = 'New password has been set successfully!';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Something went wrong.';
                }
            }else{
                $response['status'] = false;
                $response['message'] = 'This account does not exists';
            }

        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }
    public function phone_check($str){

        $user = User::select('users.*')->where('users.phone', $str)
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->first();

        if (count($user) > 0) {
            return TRUE;
        }else{
            $this->form_validation->set_message('phone_check', "This account does not exists");
            return FALSE; 
        }
        
    }
    
    public function forgot_password_post() {
        $this->form_validation->set_rules('phone', 'phone', 'trim|required|callback_phone_check');

        if ($this->form_validation->run() == FALSE) {
            $response['status'] = false;
            $response['message'] = validation_errors_response();
        } else {
            $phone = $this->post('phone');

            $user = User::select('users.*')->where('users.phone', $phone)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[5])
            ->first();

            if (count($user) > 0) {

                if($user['active'] == 1){
                    //send otp
                    $result = true;

                    //$result = $this->ion_auth->forgotten_password($phone);

                    $otp = generate_OTP();
                    $msg = 'Your RAD OTP is '.$otp.'.';
                    send_SMS($phone,$msg);
                    

                    if ($result == TRUE) {
                        $response['status'] = true;
                        $response['message'] = "OTP has been sent to the registered phone number";
                        $response['otp'] = $otp;
						$this->db->insert("sended_otp", array("type" => "customer", "phone" => $_POST['phone'], "otp" => $otp));
                    } else {
                        $response['status'] = false;
                        $response['message'] = strip_tags($this->ion_auth->errors());
                    }
                }else{
                    $response['status'] = false;
                    $response['message'] = 'The account with this phone number is not activated yet.';
                }
               
            }else{
                $response['status'] = false;
                $response['message'] = 'This account does not exists';
            }
           
        }
        //$this->response($data);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function register_post()
    {

        $this->form_validation->set_rules('first_name', 'first name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'last name', 'trim|required');
        
        if (empty($_POST['social_type']) && empty($_POST['social_id']) && $_POST['social_type'] == "" && $_POST['social_id'] == "") {
			$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');
		} else{
            $this->form_validation->set_rules('password', 'password', 'trim|min_length[8]');
        }

        $this->form_validation->set_rules('phone', 'phone', 'trim|required|callback_phone_exist_check');
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|callback_email_exist_check');
      /*  $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');*/
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
        $this->form_validation->set_rules('device_name', 'device name', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim|required');
        $this->form_validation->set_rules('lng', 'longitude', 'trim|required');
        
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }
        else
        {
            $request = $this->post();
            
            $phone = $request['phone'];
            $password = $request['password'];
            if (!empty($_POST['social_type']) && !empty($_POST['social_id']) && $_POST['social_type'] != "" && $_POST['social_id'] != "") {
            	$password = '12345678';
            }
            $names = explode(' ', $request['full_name'], 2);

            $social_type = isset($request['social_type']) ? $request['social_type'] : '';
			$social_id = isset($request['social_id']) ? $request['social_id'] : '';

            $user_data = array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'phone' => $request['phone'],
                'latitude' => $request['lat'],
                'longitude' => $request['lng'],
                'social_type' => $social_type,
                'social_id' => $social_id,
                'referral_code'=> generate_referral_code()
            );

            if(isset($request['email'])){
                $email = $user_data['email'] = $request['email'];
            }else{
                $email = '';
            }
           
            $id = $this->ion_auth->register($phone, $password, $email, $user_data, [5]);
          //  _pre("check");


            if (!empty($_POST['social_type']) && !empty($_POST['social_id']) && $_POST['social_type'] != "" && $_POST['social_id'] != "") {
            	$this->db->where('id', $id)->update('users', ['password' => '']);
            }
            if(isset($id) && !is_null($id) && !$id == false)
            {

                $wallet['user_id'] = $id;
                $wallet['balance'] = '0';
                Wallet::insert($wallet);
                
                Devices::insert(['user_id' => $id, 'name' => $request['device_name'], 'token' => $request['device_token'], 'type' => $request['device_type'], 'lat' => $request['lat'],'lng' => $request['lng'] ]);

                $user = User::find($id)->toArray();
                $user['api_key'] = $this->_generate_key($id);
                unset($user['password']);
                $response['status'] = TRUE;
                $response['user'] = $user;
                $response['message'] = 'Registered successfully';
            }
            else
            {
                $response['status'] = false;
                $errors = $this->ion_auth->errors();

                $errors = str_ireplace('<p>', '', $errors);
                $errors = str_ireplace('</p>', '. ', $errors);    

                $response['message'] = $errors;
            }                                    
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function social_login_post() 
    {
        if($_POST['social_type'] != "3")
        {  
            $this->form_validation->set_rules('user_name', 'user name', 'trim|required');
        }
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
        $this->form_validation->set_rules('lat', 'latitude', 'trim|required');
        $this->form_validation->set_rules('lng', 'longitude', 'trim|required');
        $this->form_validation->set_rules('social_type', 'social type', 'trim|required');
        $this->form_validation->set_rules('social_id', 'social id', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
        {
            $data['status'] = false;
            $data['message'] = validation_errors_response();
        } 
        else 
        {
            $request = $this->post();
            $grp = 5;

            $this->db->select('t1.*')->from('users t1');
            $this->db->join('users_groups t2', 't1.id = t2.user_id');
            if($request['social_type'] != "3")
            { 
                $this->db->group_start();
                $this->db->where('t1.email', $request['user_name']);
                $this->db->or_where('t1.phone', $request['user_name']);
                $this->db->group_end();
                $this->db->or_group_start(); 
            }
            $this->db->where('t1.social_type', $request['social_type']);
            $this->db->where('t1.social_id', $request['social_id']);
            if($request['social_type'] != "3"){
                $this->db->group_end();
            }
            $this->db->where('t1.deleted_at', null);
            $query = $this->db->where('t2.group_id', $grp)->get();

            if ($query->num_rows() === 1) 
            {
                $user = $query->row();

                if ($user->active == 1) 
                {
                    $device =  $this->db->get_where('devices', array('user_id' => $user->id))->row_array();

                    if ($request['device_token'] != $device['token']) 
                    {
                        $push_title = 'logout';
                        $push_data = array(
                            'user_id' => $user->id,
                            'message' => 'logout'
                        );
                        $push_type = 'silent';

                        $SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type); 
                    }

                    $this->db->where('id', $user->id)->update('users', ['social_type' => $request['social_type'], 'social_id' => $request['social_id']]);

                    $type = ($request['device_type'] == 1 ? 'android':'ios');
                    $this->db->where('user_id', $user->id)->update('devices', ['token' => $request['device_token'], 'type' => $type, 'lat' => $request['lat'], 'lng' => $request['lng']]);
                    $data['status'] = true;

                    $user = (array) $user;
                    $user['api_key'] = $this->_generate_key($user['id']);

                    $data['user'] = $user;
                    $data['is_user_exists'] = true;
                    $data['message'] = "Login successfully";
                } 
                else 
                {
                    $data['status'] = false;   
                    $data['is_user_exists'] = false;                         
                    $data['message'] = "Sorry! Your account is inactive. Please activate it from the link which we have sent on your registered email";                 
                }
            } 
            else 
            {
                $data['status'] = true;
                $data['is_user_exists'] = false;
                $data['message'] = "User does not exist";
            }
        }
        
        $this->response($data);
    }

    public function apple_login_post()
    {                  
        $postFields['apple_id']   = $_POST['apple_id'];                 
        $postFields['device_type']   = $_POST['device_type'];
        $postFields['device_token']   = $_POST['device_token'];
        $postFields['latitude']   = $_POST['latitude'];
        $postFields['longitude']   = $_POST['longitude'];

        $grp = '5';
        $check_apple_data = $this->db->get_where('appleaccountholder',array('apple_id' => $_POST['apple_id']))->row_array();
        if(empty($check_apple_data))
        {
            $postFields['first_name']   = $_POST['first_name'];  
            $postFields['last_name']   = $_POST['last_name'];                       
        }

        $errorPost = $this->ValidatePostFields($postFields);  
        $request = $this->post();

        if(empty($errorPost))
        {
            if(empty($check_apple_data))
            {               
                $insert_apple_data['apple_id'] = $_POST['apple_id'];
                $insert_apple_data['first_name'] = $_POST['first_name'];
                $insert_apple_data['last_name'] = $_POST['last_name'];
                if(isset($_POST['email']) && $_POST['email'] != "")
                {
                    $check_user = $this->db->get_where('users',array('email'=>$_POST['email']))->num_rows();
                    if($check_user > 0)
                    {
                        $response['status'] = false;                                           
                        $response['message'] = 'Email is already register';
                        $this->response($response);
                        exit;
                    }
                    $insert_apple_data['email'] = $_POST['email'];    
                }                        
                $insert_apple_data['created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('appleaccountholder',$insert_apple_data);

                $user_data = array(
                    'first_name' => $request['first_name'],
                    'last_name' => $request['last_name'],
                    'email' => strtolower($_POST['email']),
                    'phone' => '(000) 000-0000',
                    'active' => 1,
                    'password' => '12345678',
                    'social_id' => $_POST['apple_id'],
                    'social_type' => '3',
                    'referral_code'=> generate_referral_code()
                );

                $id = $this->ion_auth->register($user_data['phone'], $user_data['password'], $user_data['email'], $user_data, [5]);

                $device_data = array(
                	'user_id' => $id,
                	'token' => $request['device_token'],
                	'type' => $request['device_type'],
                	'lat' => $request['latitude'],
                	'lng' => $request['longitude'],
                );

                $this->db->insert('devices',$device_data);

                $this->db->where('id', $id)->update('users', ['phone' => '', 'password' => '']);

                $user_info = User::select('users.*')->where('users.id', $id)
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5])
                ->first();

            } 
            else
            {
                $user_check = $this->db->get_where('users',array('social_id' => $_POST['apple_id'],'social_type'=>'3'))->num_rows();

                if($user_check == 0)
                {
                    $check_user = $this->db->get_where('users',array('email'=>$_POST['email']))->num_rows();
                    if($check_user > 0)
                    {
                        $response['status'] = false;                                           
                        $response['message'] = 'Email is already register';
                        $this->response($response);
                        exit;
                    }

                    $user_data = array(
	                    'first_name' => $request['first_name'],
	                    'last_name' => $request['last_name'],
	                    'email' => strtolower($_POST['email']),
	                    'phone' => '(000) 000-0000',
	                    'active' => 1,
	                    'password' => '12345678',
	                    'social_id' => $_POST['apple_id'],
	                    'social_type' => '3',
	                    'referral_code'=> generate_referral_code()
	                );

	                $id = $this->ion_auth->register($user_data['phone'], $user_data['password'], $user_data['email'], $user_data, [5]);

	                $device_data = array(
	                	'user_id' => $id,
	                	'token' => $request['device_token'],
	                	'type' => $request['device_type'],
	                	'lat' => $request['latitude'],
	                	'lng' => $request['longitude'],
	                );

	                $this->db->insert('devices',$device_data);

	                $this->db->where('id', $id)->update('users', ['phone' => '', 'password' => '']);

	                $user_info = User::select('users.*')->where('users.id', $id)
	                ->join('users_groups', 'users_groups.user_id', 'users.id')
	                ->whereIn('users_groups.group_id',[5])
	                ->first();

                }
                else
                {
                    $user_info = User::select('users.*')->where('users.social_id', $_POST['apple_id'])
                    ->where('users.social_type', '3')
                    ->join('users_groups', 'users_groups.user_id', 'users.id')
                    ->whereIn('users_groups.group_id',[5])
                    ->first();
                }
            }

            $user_info['api_key'] = $this->_generate_key($user_info['id']);
                            
            $response['status'] = true;
            $response['user'] = $user_info;
            $response['message'] = "Login successfully";
        }
        else
        {
            $response['status']     = false;
            $response['message']    =  'There was an error connecting to the Apple ID server. Go to Setting -> Password & Security->App using your Apple ID->Naqsa->Stop using apple ID';
        }   

        $this->response($response);
    }

    public function apple_details_post() 
    {
		$this->form_validation->set_rules('apple_id', 'apple_id', 'trim|required');
		if ($this->form_validation->run() == FALSE) 
		{
			$data['status'] = false;
			$data['message'] = validation_errors_response();
		} 
		else 
		{
			$check_for_details = $this->db->select('first_name, last_name, email, apple_id')->get_where('appleaccountholder', array('apple_id' => $_POST['apple_id']))->row_array();
			if (!empty($check_for_details) && count($check_for_details) > 0) 
			{
				$response['status'] = TRUE;
				$response['message'] = $check_for_details;
			} 
			else 
			{
				if (isset($_POST['first_name']) && $_POST['first_name'] != '' && isset($_POST['last_name']) && $_POST['last_name'] != '') 
				{
					$post_fields['apple_id'] = $_POST['apple_id'];
					$post_fields['first_name'] = $_POST['first_name'];
					$post_fields['last_name'] = $_POST['last_name'];
					$post_fields['email'] = (isset($_POST['email']) && !empty($_POST['email'])) ? $_POST['email'] : '';
					$this->db->insert('appleaccountholder', $post_fields);

					$response['status'] = TRUE;
					$response['message'] = $post_fields;
				} 
				else 
				{
					$response['status'] = FALSE;
					$response['message'] = 'First name & Last name paramters missing';
				}
			}
		}

		$this->response($response);
	}

    public function ValidatePostFields($postFields){
        $error = array();        
        foreach ($postFields as $field => $value){            
            if(!isset($field) || $value == '' || is_null($value)){                
                $error[]= ucfirst(str_replace('_', ' ',$field)) ." field is required";             
            }        
        }    
        return $error;   
    }
    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array[0];
    }

    public function _generate_key($uid) {
		do {
			// Generate a random salt
			$salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);
			// If an error occurred, then fall back to the previous method
			if ($salt === FALSE) {
				$salt = hash('sha256', time() . mt_rand());
			}
			$new_key = substr($salt, 0, config_item('rest_key_length'));
		} while ($this->_key_exists($new_key));
		$this->_insert_key($new_key, ['level' => 1, 'ignore_limits' => 1, 'user_id' => $uid]);
		return $new_key;
	}

	private function _key_exists($key) {
		return $this->rest->db
			->where(config_item('rest_key_column'), $key)
			->count_all_results(config_item('rest_keys_table')) > 0;
	}
	private function _insert_key($key, $data) {
		$data[config_item('rest_key_column')] = $key;
		$data['date_created'] = function_exists('now') ? now() : time();
		$check_key = $this->db->get_where('tokens', array('user_id' => $data['user_id']))->row();
		if (empty($check_key)) {
			return $this->rest->db
				->set($data)
				->insert(config_item('rest_keys_table'));
		} else {
			$this->_update_key($key, $data['user_id']);
		}
	}

	private function _update_key($key, $uid) {
		return $this->rest->db
			->where('user_id', $uid)
			->update(config_item('rest_keys_table'), array('date_created' => time(), 'token' => $key));
	}
	private function _delete_key($key) {
		return $this->rest->db
			->where(config_item('rest_key_column'), $key)
			->delete(config_item('rest_keys_table'));
	}




}