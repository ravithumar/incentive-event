<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

class Chat extends REST_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Chats');
        $this->load->model('Orders');
        $this->load->model('ion_auth_model');
        $this->load->library(['ion_auth', 'form_validation']);

    }

    public function chat_messages_post(){
        $this->form_validation->set_rules('order_id', 'order id', 'trim|required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $user_id = $request['user_id'];

            $chats = Chats::where('order_id',$request['order_id'])->get();

            $organized_chat = [];
            if(count($chats) > 0){
                $chats = $chats->toArray();
                Chats::where('order_id',$request['order_id'])->update(['seen' => 1]);
                //1-Placed , 2-Store accepted & sent request to all db, 3-Store rejected , 4-Cancelled (refer reason) , 5-Order assign for dispatcher, 6-Delivery boy accepted, 7-Delivery boy rejected , 8-Delivery boy reached , 9-Picked & On the way / Ready to pick , 10-Delivered

                $today = new DateTime();

                foreach ($chats as $key => $value) {

                    $created_at = DateTime::createFromFormat('Y-m-d H:i:s', $value['created_at']);

                    if ($value['order_status'] == "4" || $value['order_status'] == "10") {
                        $response['can_send'] = 0;
                    }else{
                        $response['can_send'] = 1;
                    }

                    if($created_at->format('Y-m-d') == $today->format('Y-m-d')){
                        $created_at_dt = $created_at->format('h:i A');
                    }else{
                        $created_at_dt = $created_at->format('d-m-Y h:i A');
                    }

                    $chats[$key]['created_time'] = $created_at_dt;

                }

                $response['data'] = $chats;
                $response['status'] = TRUE;
            }else{
                $orders = Orders::where('id',$request['order_id'])->first();

                if(isset($orders)){
                    if ($orders['status'] == "4" || $orders['status'] == "10") {
                        $response['can_send'] = 0;
                    }else{
                        $response['can_send'] = 1;
                    }
                }
                $response['status'] = FALSE;
                $response['message'] = 'Chat history not found';
            }
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function test_push_post(){
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');

        if ($this->form_validation->run() == FALSE){
            $response['status'] = false;
            $response['message'] = $this->validation_errors_response();
        }else{
            $request = $this->post();

            $push_data['message'] = 'This is testing notification';
            $push_data['order_id'] = 9063;
            $push_type = 'request';
            
            $SendNotification = SendNotification($request['device_type'], $request['device_token'], 'Testing API Notification', $push_data, $push_type);
            $response['SendNotification'] = $SendNotification;
            $response['push_data'] = $push_data;
            
        }
        //$this->response($response);
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array;
    }
}
?>