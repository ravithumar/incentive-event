<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UsersController extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->model('User');
		// $this->load->model('Orders');
		
		$this->title = 'Users';
	}
	public function index(){

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('users.id, users.first_name, users.last_name, users.email, users.phone, users.active', false)
		->from('users')
		->join('users_groups', 'users_groups.user_id = users.id')
		->where('users_groups.group_id',3)
		->where('users.deleted_at', NULL);
		$action['edit'] = base_url('admin/user/edit/');
		$action['view'] = base_url('admin/user/view/');
		$dt_data
			->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Name', 'first_name',function($first_name,$row){
				if($first_name != ''){
					return ucfirst(stripslashes($first_name.' '.$row['last_name']));
				}else{
					return '-';
				}
			})
			
			->column('Phone', 'phone',function($phone,$row){
				if($phone != '' && $phone != '0'){
					return $phone;
				}else{
					return '-';
				}
			})
			->column('Email', 'email')
			->column('Status', 'active', function ($active, $row){
				$option = "";
				if ($active == 1) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Active</button>';
				} elseif ($active == 2) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Inactive</button>';
				}else{
					return '<button class="btn btn-warning btn-sm waves-effect waves-light">Pending</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light" >Edit</a>';
				$option .= '<a href="' . $action['view'] . $id . '" class="ml-1 btn btn-dark btn-sm waves-effect waves-light" >View</a>';
				$option .= " <button type='button' data-table='users' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});
			
		$dt_data->searchable('users.id,first_name,last_name,email,phone');
		$dt_data->datatable('users');
		$dt_data->init();
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/users');
		$data['datatable'] = true;
		$this->admin_render('users/index', $data);	
	}
	public function customAlpha($str) {
		if (!preg_match('/^[a-z \-]+$/i', $str)) {
			$this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
			return false;
		}
		return TRUE;
	}
	public function put($id = ''){
		$this->load->library(['ion_auth', 'form_validation']);
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
					if ($id == '') {
						$tables = $this->config->item('tables', 'ion_auth');
						$identity_column = $this->config->item('identity', 'ion_auth');
						$this->data['identity_column'] = $identity_column;
						$validation_rules = array(
							array('field' => 'first_name', 'label' => $this->lang->line('create_user_validation_fname_label'), 'rules' => 'trim|required|min_length[2]'),
							array('field' => 'last_name', 'label' => $this->lang->line('create_user_validation_lname_label'), 'rules' => 'trim|required|min_length[2]'), 
							array('field' => 'email', 'label' => $this->lang->line('create_user_validation_email_label'), 'rules' => 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]'),
							array('field' => 'phone', 'label' => $this->lang->line('create_user_validation_phone_label'), 'rules' => 'trim'),
							array('field' => 'password', 'label' => $this->lang->line('create_user_validation_password_label'), 'rules' => 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']'),
						); 
					}else{
						$validation_rules = array(
							array('field' => 'first_name', 'label' => $this->lang->line('create_user_validation_fname_label'), 'rules' => 'trim|required|min_length[2]'),
							array('field' => 'last_name', 'label' => $this->lang->line('create_user_validation_lname_label'), 'rules' => 'trim|required|min_length[2]'), 
						);
					}

				$this->form_validation->set_rules($validation_rules);

				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();
					
					$user_data = [
						'first_name' => $request['first_name'],
						'last_name' => $request['last_name'],
					];
					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
        		$img_name = 'user_' . time().rand(1000, 9999).'.jpg';
				    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
				    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
			        $user_data['profile_picture'] = $img_name;
			        if(isset($request["old_profile_picture"])){
			        if($request["old_profile_picture"] != 'assets/images/default.png'){
			        	unlink(FCPATH.$request["old_profile_picture"]);
			        }
			        }
					  }
				    $file_upload = true;
        	}
                	// _pre($user_data);

        	if ($id == '') {
        		$email = strtolower($this->input->post('email'));
						$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
						$password = $this->input->post('password');

						$additional_data = [
							'first_name' => $this->input->post('first_name'),
							'last_name' => $this->input->post('last_name'),
							'phone' => $this->input->post('phone'),
						];
						
						$data = [
							'first_name' => $this->input->post('first_name'),
							'last_name' => $this->input->post('last_name'),
							'password' => $this->input->post('password'),
							'phone' => $this->input->post('phone'),
							'email' => $this->input->post('email'),
						];
						// _pre($additional_data);

						$this->lang->load('ion_auth');

            $message = $this->load->view('email_templates/new_user', $data, true);
						$subject = $this->config->item('site_title', 'ion_auth') . ' -  Registration';
						$to_email = $email;

						//send_mail_new($to_email, $subject, $message);
            $this->ion_auth->register($identity, $password, $email, $additional_data,[3]);
						// _pre($id);

						if ($file_upload){
							// User::whereId($id)->update($user_data);
						$this->session->set_flashdata('success', 'User added successfully');
						redirect(base_url('admin/users'));
          	}
                		
          	}else{
          		if ($file_upload){
							User::whereId($id)->update($user_data);
							$this->session->set_flashdata('success', 'User updated successfully');
							redirect(base_url('admin/users'));
          	}
					}

				} 
			}
		}


		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/users');

		if ($id == '') {

	        $this->breadcrumbs->push('Add', '/', true);
			// $output_data['user'] = User::find($id); 
			$this->admin_render('users/add', $output_data);
		}else{

	        $this->breadcrumbs->push('Edit', '/', true);
			$output_data['user'] = User::find($id);
			if(!isset($output_data['user']) && count($output_data['user']) == 0){
				$this->session->set_flashdata('error', 'No any user found!');
				redirect(base_url('admin/users'));
			}
				$this->admin_render('users/put', $output_data);
		}
	}
	public function view($id = ''){

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('events.*', false)->from('events')->where('id',$id)->where('deleted_at', NULL);
		$action['view'] = base_url('admin/events/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Event name', 'name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($event_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/event/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){
				$string = ucfirst(htmlentities($first_name.' '.$row['last_name']));
				$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
				return '<u><a target="_blank" href="'.base_url('/admin/user/view/').$row['user_id'].'">'.$string.'</a></u>';

			})
			// ->column('Order Amount', 'total_amount',function($total_amount,$row){

			// 	return "$ ".$total_amount;

			// })
		
			// ->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				// if($order_type == 1){
				// 	return "Delivery";
				// }elseif($order_type == 2){
				// 	return "Takeout";
				// }elseif($order_type == 3){
				// 	return "Later Delivery";
				// }else{
				// 	return "Later Takeway";
				// }

			// }) 
			// ->column('Order Placed On', 'created_at', function ($created_at, $row){
			// 	$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
			// 	$created_at_date = $date->format('d M, Y');
   //              $created_at_time = $date->format('h:i A');
   //              return $created_at_date.' '.'at'.' '.$created_at_time;
				
			// })
			// ->column('Status', 'status',function($status,$row){

			// 	if($status == 1){
   //                  return '<span class="badge badge-primary">Pending</span>';
   //              }else if($status == 2){
   //                  return '<span class="badge badge-info">Accepted by store</span>';
   //              }else if($status == 3){
   //                  return '<span class="badge badge-danger">Rejected by store</span>';
   //              }else if($status == 4){
   //                  return '<span class="badge badge-danger">Cancelled</span>';
   //              }else if($status == 5){
   //                  return '<span class="badge badge-info">Delivery Person Assigned</span>';
   //              }else if($status == 6){
   //                  return '<span class="badge badge-info">Accepted by Delivery Person</span>';
   //              }else if($status == 7){
   //                  return '<span class="badge badge-danger">Rejected by Delivery Person</span>';
   //              }else if($status == 8){
   //                  return '<span class="badge badge-info">Reached at restaurant</span>';
   //              }else if($status == 9){
   //                  return '<span class="badge badge-info">On the Way</span>';
   //              }else if($status == 10){
   //                  return '<span class="badge badge-success">Delivered</span>';
   //              }else{
   //                  return ' ';
   //              }

			// })
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				// $option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';

				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id,vendor.name');
		$dt_data->datatable('orders');
		
		$dt_data->init();
		$output_data['datatable'] = true;

		$output_data['customer'] = User::find($id);
		if(isset($output_data['customer'])){
            $output_data['customer'] = $output_data['customer']->toArray();
        }
		//_pre($output_data);
		if(!isset($output_data['customer']) && count($output_data['customer']) == 0){
			$this->session->set_flashdata('error', 'No any user found!');
			redirect(base_url('admin/users'));
		}
		$net_profit = $revenue = 0;
		// $orders = Orders::where('status',10)->where('user_id',$id)->get();
  //       if(isset($orders)){
  //           $orders = $orders->toArray();
           
  //           foreach ($orders as $key => $value) {
  //               $net_profit += $value['admin_amount'];
  //               $revenue += $value['total_amount'];
  //           }
  //       }
        // $output_data['net_profit'] = $net_profit;
        // $output_data['revenue'] = $revenue;
        // $output_data['total_orders'] = count($orders);
		
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/users');
        $this->breadcrumbs->push('View', '/', true);
		$this->admin_render('users/view', $output_data);
	}
}