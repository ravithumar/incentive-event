<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class VendorController extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->helper('stripe_helper');
		$this->load->model('User');
		$this->load->model('Category');
		$this->load->model('VendorAvailibility');
		$this->load->model('Vendor');
		$this->load->model('ion_auth_model');
		$this->load->model('Orders');
		$this->load->library(['ion_auth', 'form_validation']);
		
		$this->title = 'Restaurants';
	}
	public function index(){
/* 
		$config['protocol'] = $this->config->item("protocol");
        $config['smtp_host'] = $this->config->item("smtp_host");
        $config['smtp_port'] = $this->config->item("smtp_port");
        $config['smtp_user'] = $this->config->item("smtp_user");
        $config['smtp_pass'] = $this->config->item("smtp_pass");
        $config['charset'] = $this->config->item("charset");
        $config['mailtype'] = $this->config->item("mailtype");


        $this->load->library('email');
		$message = 'sdfsdf';
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('ruralareadelivery@gmail.com',"RAD");
        $this->email->to('shivvv@yopmail.com');
        $this->email->subject('Account Activation'); 
        $this->email->message($message);

        var_dump($this->email->send());	
        var_dump($this->email->print_debugger());
        exit; */

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('users.id, users.first_name, users.last_name, users.email, users.phone, users.active,vendor.name', false)
		->from('users')
		->join('users_groups', 'users_groups.user_id = users.id')
		->join('vendor', 'vendor.user_id = users.id')
		->where('users_groups.group_id','2')
		->where('users.deleted_at', NULL);
		$action['edit'] = base_url('admin/vendor/edit/');
		$action['view'] = base_url('admin/vendor/view/');
		$dt_data
			->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Name', 'first_name',function($first_name,$row){
				if($first_name != ''){
					return ucfirst($first_name.' '.$row['last_name']);
				}else{
					return '-';
				}
			})
	
			->column('Store Name', 'name',function($name,$row){
				if($name != ''){
					return ucfirst($name);
				}else{
					return '-';
				}
			})
			->column('Phone', 'phone')
			->column('Email', 'email')
			->column('Status', 'active', function ($active, $row){
				$option = "";
				if ($active == 1) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Active</button>';
				} elseif ($active == 2) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Inactive</button>';
				}else{
					return '<button class="btn btn-warning btn-sm waves-effect waves-light">Pending</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light" >Edit</a>';
				$option .= '<a href="' . $action['view'] . $id . '" class="ml-1 btn btn-dark btn-sm waves-effect waves-light" >View</a>';
				$option .= " <button type='button' data-table='users' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});
			
		
		$dt_data->searchable('users.id,first_name,last_name,email,phone');
		$dt_data->datatable('users');
		$dt_data->init();
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/vendors');
		$data['datatable'] = true;
		$this->admin_render('vendor/index', $data);	
	}

	public function isexists_category($str = NULL, $id = NULL) {

		$this->db->select('*');
		if ($id != '') {
			$this->db->where('id !=', $id);
		}
		$this->db->where('name', $str);
		$this->db->where('deleted_at', NULL);
		$this->db->from('category');
		$sql_query = $this->db->get();
		if ($sql_query->num_rows() > 0) {
			$this->form_validation->set_message('isexists_category', "This name field is already exists.");
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function email_exist_check($str){
        if($str != ''){
            $user = User::where('users.email', $str)
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[2])
                ->first();
            if (!isset($user)) {
                return TRUE;
            }
            $this->form_validation->set_message('email_exist_check', "Email is already exist.");
            return FALSE;
        }else{
            return TRUE; 
        }
    }

    function phone_exist_check($str) {
        if (!preg_match('/^\((\d)\d{2}\) \d{3}-\d{4}$/', $str)) {
            $this->form_validation->set_message('phone_exist_check', 'The {field} field is invalid.');
            return false;
        }else{
            $user = User::where('users.phone', $str)
            ->join('users_groups', 'users_groups.user_id', 'users.id')
            ->whereIn('users_groups.group_id',[2])
            ->first();
            if (!isset($user)) {
                return TRUE;
            }
            $this->form_validation->set_message('phone_exist_check', "Phone number is already exist.");
            return FALSE;
        }
    }
	
	public function add(){

		if (isset($_POST) && !empty($_POST)) {
			if (isset($_POST['submit'])) {
				/* _pre($_POST);  */
				$validation_rules = array(
					array('field' => 'name', 'label' => 'name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'first_name', 'label' => 'first name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'last_name', 'label' => 'last name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'phone', 'label' => 'phone', 'rules' => 'trim|required|callback_phone_exist_check'),
					array('field' => 'email', 'label' => 'email', 'rules' => 'trim|required|callback_email_exist_check'),
				);
				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();

					$user_data = array(
						'first_name' => ucfirst($request["first_name"]),
						'last_name' => ucfirst($request["last_name"]),
						'phone' => $request["phone"],
						'email' => $request['email'],
						'deleted_at' => NULL,
					);

					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
                		$img_name = 'vendor_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
					        $user_data['profile_picture'] = $img_name;
					    }
					    $file_upload = true;
                	}

					$password = '12345678';
					$phone = $request["phone"];
					$email = $request["email"];
					
					$id = $this->ion_auth->register($phone, $password, $email, $user_data, [2]);
					
					/* bank account details */
					$user = User::where('id',$id)->where('account_id','')->first();
					if(isset($user)){
						$time=strtotime($request['dob']);
		                $month=date("m",$time);
		                $year=date("Y",$time);
		                $day=date("d",$time);

		                $info = [
		                    'type' => 'custom',
		                    'country' => 'CA',
		                    'email' => $request['email'],
		                    'business_type' => 'individual',
		                    'individual' => [
		                        'dob' => [
		                            'day' => $day,
		                            'month' => $month,
		                            'year' => $year
		                        ],
		                       'first_name' => $user['first_name'],
		                       'last_name' => $user['last_name'],
		                       'address' => [
		                            'city' => $request['bank_city'],
		                            'country' =>  'CA',
		                            'line1' =>  $request['line1'],
		                            'line2' =>  $request['line2'],
		                            'state' => $request['bank_state'],
		                            'postal_code' =>  $request['postal_code']
		                       ]
		                    ],
		                    'capabilities' => [
		                        'card_payments' => ['requested' => true],
		                        'transfers' => ['requested' => true]
		                    ],
		                    'external_account' => [
		                        'object' => 'bank_account',
		                        'country' => 'CA',
		                        'currency' => 'CAD',
		                        'routing_number' =>  $request['routing_number'],
		                        'account_number' =>  $request['account_number']
		                    ],
		                    'tos_acceptance' => [
		                        'date' => time(),
		                        'ip' => $_SERVER['REMOTE_ADDR']
		                    ]
		                ];
                		
                		$result = createCustomAccount($info);
                		if($result['status'] == TRUE){
		                    $update_data = array('account_id' => $result['success']['id']);
		                    User::where('id', $id)->update($update_data);
		                }else{
		                	User::where('id', $id)->delete();
		                    $this->session->set_flashdata('error',$result['error']['message']);
							redirect(base_url("admin/vendor/add"));
		                }
					}else{
						$this->session->set_flashdata('error','Error into adding data');
						redirect(base_url("admin/vendor/add"));
					}

					/* $this->response($id); */
					if(isset($id) && !is_null($id) && !$id == false){
						/* _pre($id); */
						$availability = array();
						foreach ($this->config->item("days") as $key => $value){
						
							if (!empty($_POST[$value]) && sizeof($_POST[$value]) != 0){
								/* If not closed */

								if($_POST[$value][0] == 'fullday'){
									/* If full day open */
									/* echo 'full day'; */
									$availability[$value]['full_day'] = '1';
									$availability[$value]['closed'] = '0';
									$availability[$value]['from'] = '';
									$availability[$value]['to'] = '';
								}else{
									/* If custom time open */
									$availability[$value]['full_day'] = '0';
									$availability[$value]['closed'] = '0';

									$availability[$value]['from'] = date('h:i A', strtotime($_POST[$value][0]));
									$availability[$value]['to'] = date('h:i A', strtotime($_POST[$value][1]));
								}
							}else{
								/* echo 'closed'; */
								/* If closed */
								$availability[$value]['full_day'] = '0';
								$availability[$value]['closed'] = '1';
								$availability[$value]['from'] = '';
								$availability[$value]['to'] = '';
							}
							
						}
						VendorAvailibility::where('user_id',$id)->delete();
						foreach ($availability as $key => $value) {

							$availability_data = array(
								'user_id' =>  $id,
								'day' =>  $key,
								'from_time' => $value['from'],
								'to_time' =>  $value['to'],
								'full_day' =>  $value['full_day'],
								'is_closed' =>  $value['closed'],
								'updated_at' =>  date('Y-m-d H:i:s')
								);
							VendorAvailibility::insert($availability_data);
						}
						$data = array(
							'user_id' =>  $id,
							'name' => ucfirst($request["name"]),
							'street' => $request["address"],
							'city' => $request["city"],
							'state' => $request["state"],
							'country' => $request["country"],
							'zipcode' => $request["zipcode"], 
							'latitude' => $request["latitude"],
							'longitude' => $request["longitude"],
							'minimum_order_amount' => $request["minimum_order_amount"],
							'percentage' => $request["percentage"],
						);
	
						if(isset($_FILES['bg_img']) && !empty($_FILES['bg_img']) && strlen($_FILES['bg_img']['name']) > 0) {
							$img_name = 'vendor_' . time().rand(1000, 9999).'.jpg';
							$destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
							if(compress($_FILES["bg_img"]["tmp_name"], $destination_url, 90)){
								$data['bg_img'] = $img_name;
								
							}
							$file_upload = true;
						}
	
						if(isset($_POST['service_available'])){
								if(count($_POST['service_available']) == 2){
									$service_available = 3;
								}else if($_POST['service_available'][0] == 2){
									$service_available = 2;
								}else{
									$service_available = 1;
								}
							}
						$data['service_available'] = $service_available;
	
						if(isset($_POST['phone_2']) && !empty($_POST['phone_2'])){
							$data['phone_2'] = $request["phone_2"];
						}else{
							$data['phone_2'] = '';
						}
	
						Vendor::insert($data);
						// $template = file_get_contents(base_url('email_templates/vendoraccount_activation.html'));
						// $message  = create_email_template_new($template);
						// $link = base_url() .'vendor/change-password/'.$id;
						// $message = str_replace('##LINK##', $link, $message);
						// $subject = $this->config->item('site_title', 'ion_auth').' User Detail';
						// $to = $request['email'];
						// $sent = send_mail_new($to,$subject, $message);
						$update_data['active'] = '1';

						$this->db->update('users',$update_data,array('id'=>$id));

						$forgotten = $this->ion_auth_model->forgotten_password_vendor($email, $id);
                        $data = [
							'identity'   => $email,
							'id'         => $id,
							'email'      => $email,
							'forgotten' => $forgotten ,
                    	];

						$this->lang->load('ion_auth');
                        $message = $this->load->view('email_templates/reset_password_vendor', $data, true);
						$subject = $this->config->item('site_title', 'ion_auth') . ' -  Reset Password';
						$to_email = $user->email;
						send_mail_new($to_email, $subject, $message);
						$this->session->set_flashdata('success', 'Reset password link sent to your email.');
						//_pre($sent);
						if ($file_upload){
							$this->session->set_flashdata('success','Restaurant added successfully');
							redirect(base_url("admin/vendors"));
						}

					} else {
						$this->session->set_flashdata('error','Error into adding data');
						redirect(base_url("admin/vendor/add"));
					}
				} 
			}
		}
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/vendor');
		$this->breadcrumbs->push('Add', '/', true);
		$data['state'] = $this->db->get_where('state')->result_array();
		
		$this->admin_render('vendor/post', $data);
	}
	
	public function account_create() {
		$time = strtotime("31 May, 1994");
		$month = date("m",$time);
		$year = date("Y",$time);
		$day = date("d",$time);

		$info = [
			'type' => 'custom',
			'country' => 'CA',
			'email' => 'developer.rad2810@gmail.com',
			'business_type' => 'individual',
			'individual' => [
				'dob' => [
					'day' => $day,
					'month' => $month,
					'year' => $year
				],
			   'first_name' => 'Developer',
			   'last_name' => 'Rad',
			   'address' => [
					'city' => 'Canada',
					'country' =>  'CA',
					'line1' =>  'Isquare Corporate Park, Panchamrut Bunglows II, Sola, Ahmedabad, Gujarat, India',
					'line2' =>  'Isquare Corporate Park, Panchamrut Bunglows II, Sola, Ahmedabad, Gujarat, India',
					'state' => 'MB',
					'postal_code' => 'K0K 2T0'
			   ]
			],
			'capabilities' => [
				'card_payments' => ['requested' => true],
				'transfers' => ['requested' => true]
			],
			'external_account' => [
				'object' => 'bank_account',
				'country' => 'CA',
				'currency' => 'CAD',
				'routing_number' => '11000000',
				'account_number' => '000999999991'
			],
			'tos_acceptance' => [
				'date' => time(),
				'ip' => $_SERVER['REMOTE_ADDR']
			]
		];
		
		_prea($info);
		
		$result = createCustomAccount($info);
		_prea($result);
	}

	public function customAlpha($str) {
		if (!preg_match('/^[a-z \-]+$/i', $str)) {
			$this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
			return false;
		}
		return TRUE;
	}
	public function put($id = ''){
		$this->load->library(['ion_auth', 'form_validation']);
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'name', 'label' => 'name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'first_name', 'label' => 'first name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'last_name', 'label' => 'last name', 'rules' => 'trim|required|min_length[2]|max_length[50]'),
					array('field' => 'phone', 'label' => 'phone', 'rules' => 'trim|required'),
					array('field' => 'email', 'label' => 'email', 'rules' => 'trim|required'),
				);
				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();
					
					/*$email_data = array()$this->db->get('email')->where("id = $id")->from('users')*/;
					$email_data = $this->db->select('email')->from('users')->where('id = '.$id.'')->get()->row_array();
					$vendor=User::whereId($id)->first();

					if($request["email"]){
						/*'active' => "0",*/
						}

					$user_data = [
							'first_name' => ucfirst($request["first_name"]),
							'last_name' => ucfirst($request["last_name"]),
							'phone' => $request["phone"],
							'email' => $request["email"],
							 
						];

					if($vendor->email!=$request['email']){
						$user_data['active'] = 0;
					}

					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
                		$img_name = 'vendor_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
					        $user_data['profile_picture'] = $img_name;
					        if($request["old_profile_picture"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_profile_picture"]);
					        }
					        
					    }
					    $file_upload = true;
                	}
                	

                	// _pre($user_data);

                	$availability = array();
					foreach ($this->config->item("days") as $key => $value){
					  
					  	if (!empty($_POST[$value]) && sizeof($_POST[$value]) != 0){
					  		// If not closed

						  	if($_POST[$value][0] == 'fullday'){
						  		// If full day open
						  		//echo 'full day';
						  		$availability[$value]['full_day'] = '1';
						  		$availability[$value]['closed'] = '0';
						  		$availability[$value]['from'] = '';
						  		$availability[$value]['to'] = '';
						  	}else{
						  		// If custom time open
						  		$availability[$value]['full_day'] = '0';
						  		$availability[$value]['closed'] = '0';

						  		$availability[$value]['from'] = date('h:i A', strtotime($_POST[$value][0]));
						  		$availability[$value]['to'] = date('h:i A', strtotime($_POST[$value][1]));
						  	}
					  	}else{
					  		// echo 'closed';
					  		// If closed
					  		$availability[$value]['full_day'] = '0';
					  		$availability[$value]['closed'] = '1';
					  		$availability[$value]['from'] = '';
					  		$availability[$value]['to'] = '';
					  	}
					  	
					}
					VendorAvailibility::where('user_id',$request["user_id"])->delete();
					//Add new availability
					foreach ($availability as $key => $value) {

						$availability_data = array(
							'user_id' =>  $request["user_id"],
							'day' =>  $key,
							'from_time' => $value['from'],
							'to_time' =>  $value['to'],
							'full_day' =>  $value['full_day'],
							'is_closed' =>  $value['closed'],
							'updated_at' =>  date('Y-m-d H:i:s')
							);
						VendorAvailibility::insert($availability_data);
					}

					User::whereId($request['user_id'])->update($user_data);

					$data = array(
						'user_id' =>  $request["user_id"],
						'name' => ucfirst($request["name"]),
						'street' => $request["address"],
						'city' => $request["city"],
						'state' => $request["state"],
						'country' => $request["country"],
						'zipcode' => $request["zipcode"],
						'minimum_order_amount' => $request["minimum_order_amount"],
						'percentage' => $request["percentage"],
						'latitude' => $request["latitude"],
						'longitude' => $request["longitude"],
						
						//'delivery_boy_percentage' => $request["delivery_boy_percentage"],
					);

					if(isset($_FILES['bg_img']) && !empty($_FILES['bg_img']) && strlen($_FILES['bg_img']['name']) > 0) {
                		$img_name = 'vendor_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["bg_img"]["tmp_name"], $destination_url, 90)){
					        $data['bg_img'] = $img_name;
					        if($request["old_bg_img"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_bg_img"]);
					        }
					        
					    }
					    $file_upload = true;
                	}

					if(isset($_POST['service_available'])){
							if(count($_POST['service_available']) == 2){
								$service_available = 3;
							}else if($_POST['service_available'][0] == 2){
								$service_available = 2;
							}else{
								$service_available = 1;
							}
						}
					$data['service_available'] = $service_available;
					/*if(isset($_POST['category_id']) && !empty($_POST['category_id'])){
			        	$data['category'] = implode(",", $_POST['category_id']);
			        	unset($_POST['category_id']);
			        }*/

					if(isset($_POST['phone_2']) && !empty($_POST['phone_2'])){
						$data['phone_2'] = $request["phone_2"];
					}else{
						$data['phone_2'] = '';
					}

					//_pre($data);

					Vendor::whereId($request['vendor_id'])->update($data);


					/* UsersCategory::where('user_id', $request['user_id'])->delete();
					if(isset($request['category_id'])){
					
						foreach ($request['category_id'] as $key => $value) {
							$insert_category_data[] = array('user_id'=> $id, 'category_id'=> $value);
						}
						UsersCategory::insert($insert_category_data);
					} */

					if($request['email'] != $request['old_email'])
					{
						$template = file_get_contents(base_url('email_templates/account_activation.html'));
						$message  = create_email_template_new($template);
						$link = base_url() .'admin/vendor-active/'.$id;
			            $message = str_replace('##LINK##', $link, $message);
			            $subject = $this->config->item('site_title', 'ion_auth').' User Detail';
			            $to = $request['email'];
			            $sent = send_mail_new($to,$subject, $message);

			            /**/
			            $message = $this->load->view('email_templates/reset_password_vendor', $data, true);
						$subject = $this->config->item('site_title', 'ion_auth') . ' -  Reset Password';
						$to_email = $user->email;
						send_mail_new($to_email, $subject, $message);
						$this->session->set_flashdata('success', 'Reset password link sent to your email.');
						/**/

					}
					if ($file_upload){
						$this->session->set_flashdata('success','Profile updated successfully');
						redirect(base_url("admin/vendors"));
					}else{
						
						$this->session->set_flashdata('error','Error into updating data');
						redirect(base_url("admin/vendor/edit/".$request["id"]));
					}
				} 
			}
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/vendors');
        $this->breadcrumbs->push('Edit', '/', true);
		$output_data['vendor'] = User::with(['vendor','vendor_availability'])->find($id);
		// $output_data['category'] = Category::get();

		if(isset($output_data['vendor'])){
            $output_data['vendor'] = $output_data['vendor']->toArray();
        }
        /* if(isset($output_data['category'])){
            $output_data['category'] = $output_data['category']->toArray();
        } */

		if(!isset($output_data['vendor']) && count($output_data['vendor']) == 0){
			$this->session->set_flashdata('error', 'No any user found!');
			redirect(base_url('admin/vendors'));
		}
		
		$this->admin_render('vendor/put', $output_data);
	}
	public function view($id = ''){

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('vendor_id',$id)->where('orders.deleted_at', NULL);
		$action['view'] = base_url('admin/all-orders/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){
				$string = ucfirst(htmlentities($first_name.' '.$row['last_name']));
				$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
				return '<u><a target="_blank" href="'.base_url('/admin/user/view/').$row['user_id'].'">'.$string.'</a></u>';

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Admin Amount', 'admin_amount',function($admin_amount,$row){

				return "$ ".$admin_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Status', 'status',function($status,$row){

				if($status == 1){
                    return '<span class="badge badge-primary">Pending</span>';
                }else if($status == 2){
                    return '<span class="badge badge-info">Accepted by store</span>';
                }else if($status == 3){
                    return '<span class="badge badge-danger">Rejected by store</span>';
                }else if($status == 4){
                    return '<span class="badge badge-danger">Cancelled</span>';
                }else if($status == 5){
                    return '<span class="badge badge-info">Delivery Person Assigned</span>';
                }else if($status == 6){
                    return '<span class="badge badge-info">Accepted by Delivery Person</span>';
                }else if($status == 7){
                    return '<span class="badge badge-danger">Rejected by Delivery Person</span>';
                }else if($status == 8){
                    return '<span class="badge badge-info">Reached at restaurant</span>';
                }else if($status == 9){
                    return '<span class="badge badge-info">On the Way</span>';
                }else if($status == 10){
                    return '<span class="badge badge-success">Delivered</span>';
                }else{
                    return ' ';
                }

			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';

				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id,vendor.name');
		$dt_data->datatable('orders');
		
		$dt_data->init();
		$output_data['datatable'] = true;

		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/vendors');
        $this->breadcrumbs->push('View', '/', true);
		$net_profit = $revenue = 0;
		$orders = Orders::where('status',10)->where('vendor_id',$id)->get();
        if(isset($orders)){
            $orders = $orders->toArray();
           
            foreach ($orders as $key => $value) {
                $net_profit += $value['admin_amount'];
                $revenue += $value['total_amount'];
            }
        }
        $output_data['net_profit'] = $net_profit;
        $output_data['revenue'] = $revenue;
        $output_data['total_orders'] = count($orders);

        $output_data['vendor'] = User::with(['product','vendor','vendor_availability'])->find($id);
		// $output_data['category'] = Category::get();

		if(isset($output_data['vendor'])){
            $output_data['vendor'] = $output_data['vendor']->toArray();
        }
        /* if(isset($output_data['category'])){
            $output_data['category'] = $output_data['category']->toArray();
        } */

		if(!isset($output_data['vendor']) && count($output_data['vendor']) == 0){
			$this->session->set_flashdata('error', 'No any user found!');
			redirect(base_url('admin/vendors'));
		}

		$user = User::where('id',$id)->where('account_id','!=','')->first();
        if (isset($user->account_id) && $user->account_id != "") {
            $output_data['stripe_detail'] = getCustomAccount($user->account_id);
        }
		$this->admin_render('vendor/view', $output_data);
	}


	publiC function vendor_active($id){
		_pre($id);
	    $details = (array)$this->db->get_where('users',array('id'=>$id))->row();
      	$data['active'] = '1';
        $this->db->update('users',$data,array('id' => $id));
        //redirect('vendor/login');
}
	
}