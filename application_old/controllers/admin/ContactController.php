<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->model('ContactUs');
		$this->title = 'Contact Us';
	}

	public function index()
	{
		
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('*', false)->from('contact_us')->where('deleted_at',NULL);
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Name', 'name',function($name,$row){

				return ucfirst(htmlentities($name));

			})
			->column('Email', 'email', function ($email, $row){
            	return '<a href="mailto:'.strtolower($email).'" class="text-info">'.strtolower($email).'</a>';
        	})
			->column('Submitted On','created_at', function ($created_at, $row){
            	$created_at = DateTime::createFromFormat('Y-m-d H:i:s', $created_at);
                return $created_at->format('d F, Y');
        	})
			->column('Action', 'id', function ($id, $row){
				$option = "";
				$option .= '<button type="button" data-id="'.$id.'" data-name="'.$row['name'].'" data-email="'.$row['email'].'" data-message="'.$row['message'].'" class="btn-edit btn btn-dark btn-sm waves-effect waves-light" id="view" data-toggle="modal" data-target="#exampleModal" >View</button>';
				$option .= " <button type='button' data-table='contact_us' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				//$option .= "<input type='button' id='btnReply' data-id='".$row['id']."' data-email='".$row['email']."' value='Reply' class='ml-1 btn btn-warning btn-sm waves-effect waves-light' />";
				return $option;
			});

		$dt_data->searchable('id,name,email');
		$dt_data->datatable('contact_us');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'admin/contact-us');
		$this->admin_render('contact_us/index',$data);	
	}
	public function reply(){
		$from = "";
		$subject = $this->input->post("email_subject");
		$email_message_string = $this->input->post("email_msg");
		$message = $this->load->view("email_templates/custom_mail", array("mail_body" => $email_message_string), TRUE);
		$to = $this->input->post("email_to");

		$mail = sendmail($from, $to, $subject, $message);
		echo json_encode(array("is_success" => $mail));
		return TRUE;
	}

}
?>
