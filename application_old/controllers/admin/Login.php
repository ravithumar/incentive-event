<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// $this->load->model("admin_model");
	}

	public function index(){

		if ($this->auth->is_admin() == TRUE){
			redirect(base_url() . "admin/dashboard");
		}

		
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

				$validation_rules = array(
					array('field' => 'email', 'label' => 'email', 'rules' => 'trim|required|max_length[225]|valid_email'),
					array('field' => 'password', 'label' => 'password', 'rules' => 'trim|required|min_length[6]')
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					if($this->auth->login($this->input->post('email'), $this->input->post('password'), 'admin')){
						$this->session->set_flashdata('success', 'You have been successfully logged in.');
						redirect(base_url() . "admin/dashboard");
					}else{
						$this->session->set_flashdata($this->auth->get_messages_array());
						redirect(base_url() . "admin/login");
					}			
				} 
			}
		}
		$data['user_type'] = 'admin';
		$this->load->view('login', $data);
	}

	function test(){
		$device_token = 'dWwbo-ms5ElkmLXfIYBOVf:APA91bFdKWl7HfnXq2mb9q4tnUvvIhw41xH8wUMNnqT3LN8WjGLh2YGj2SFgb38nJqNP4pCiBkAeiCuha_BNtjCMvSRQtsBIMrZFA_E_4beEJ-k0GKMCsJF-1JSKsnv7d1iq3CpuT_QV';
		$push_title = 'Hi cvb';
		$push_data = ['order' => 55, 'message' => 'How are you?'];
		$push_type = 'request';
		$SendNotification = SendNotification('2', $device_token, $push_title, $push_data, $push_type);
		echo "<pre>";
		print_r($SendNotification);
		exit;
	}

	public function logout(){
		if($this->auth->logout()){
			redirect(base_url("admin/login"));
		}
	}
}