<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require '/var/www/html/vendor/autoload.php';
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

class OrderController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
        
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		
        $this->load->model('User');

        $this->load->model('Orders');
        $this->load->model('OrderProductAddOn');
        $this->load->model('Devices');
        $this->load->model('OrderProducts');
		$this->title = 'Order';
		
	}

	public function order_arrived_index()
	{
	
		$this->load->library('Datatables');
		
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)
				->from('orders')
				->join('users','orders.user_id = users.id')
				->join('vendor','orders.vendor_id = vendor.user_id')
				->where('orders.deleted_at', NULL)
				->where('orders.status',1);
		$action['view'] = base_url('admin/order-arrived/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				/* $option .= '<button onclick="accept_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-success btn-sm waves-effect waves-light mr-1">Accept</button>';
				
				$option .= '<button onclick="reject_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-danger btn-sm waves-effect waves-light mr-1" >Reject</button>'; */

				// $option .= '<button type="button" id="'.$id.'" data-id ="'.$id.'" data-userid ="'.$row['user_id'].'"  data-deliveryid ="'.$row['delivery_boy_id'].'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1" onclick="send_push_notification('.$id.')" data-toggle="modal" data-target="#sendpush" >Push</button>';

				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				$order_status_allowed = array('1', '2', '5', '6', '7', '8', '9');
				if(in_array($row['status'], $order_status_allowed)){
					$option .= '<button onclick="admin_cancel_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-danger btn-sm waves-effect waves-light admin-cancel-order-btn mr-1" >Cancel</button>';
				}
				 
				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		//$data['user'] = $this->Order_model->list_user();
        $this->breadcrumbs->push($this->title , 'admin/order-arrived');
        $this->breadcrumbs->push('Arrived', '/', true);

		$this->admin_render('order/index',$data);	
	}

	public function order_arrived_view($id = ''){
		
		$output_data['title'] = $this->title;

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$output_data['order'] = $orders;
			$output_data['order_products'] = $order_products;

		}

		if(!isset($output_data['order_products']) && count($output_data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('admin/order-arrived'));
		}
		// _pre($output_data);
		$output_data['back_url'] = base_url('admin/order-arrived');		
		$this->breadcrumbs->push($this->title , 'admin/order-arrived');
        $this->breadcrumbs->push('View', '/', true);
		$this->admin_render('order/view', $output_data);
	}

	public function order_assigned_index(){
		
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		// $status = [2,5,6,7,8,9];
		$status = [5];
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('orders.deleted_at', NULL)->where_in('orders.status',$status);
		$action['view'] = base_url('admin/order-inprocess/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Assigned Delivery Person', 'delivery_boy_id',function($delivery_boy_id,$row){

				if($delivery_boy_id != 0){
					$this->db->select('first_name,last_name');
					$this->db->from('users');
					$this->db->where('id', $delivery_boy_id);
					$sql_query = $this->db->get();
					if ($sql_query->num_rows() > 0) {
						$delivery_boy = $sql_query->row_array(); 

						$string = ucfirst($delivery_boy['first_name'].' '.$delivery_boy['last_name']);
						$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
						return '<u><a target="_blank" href="'.base_url('/admin/delivery-boy/view/').$row['delivery_boy_id'].'">'.$string.'</a></u>';
					}
				}else{
					return '';
				}

			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";

				if($row['order_type'] == 1 || $row['order_type'] == 3){

					if($row['status'] == 2 || $row['status'] == 5 || $row['status'] == 7){
						if($row['delivery_boy_id'] != 0){
							$option .= '<button type="button" id="1'.$id.'" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-2 reassign assign-db-btn" onclick="assign_delivery_boy('.'1'.$id.','.$row['delivery_boy_id'].')" data-toggle="modal" data-target="#exampleModal" >Reassign</button>';
						}else{
							$option .= '<button type="button" id="1'.$id.'" data-id ="'.$id.'"  class="btn-edit btn btn-primary btn-sm waves-effect waves-light assign-db-btn mr-2" onclick="assign_delivery_boy('.'1'.$id.')" data-toggle="modal" data-target="#exampleModal" >Assign</button>';
						}
					}
				}else{
					//$option .= '<button onclick="completed(this);" data-alcoholic="'.$row['alcoholic'].'" data-id="' . $id . '"  data-userid="' . $row['user_id'] . '" class="btn btn-success btn-sm waves-effect waves-light mr-1 reassign">Complete</button>';
				}
				// $option .= '<button type="button" id="'.$id.'" data-id ="'.$id.'" data-userid ="'.$row['user_id'].'"  data-deliveryid ="'.$row['delivery_boy_id'].'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1" onclick="send_push_notification('.$id.')" data-toggle="modal" data-target="#sendpush" >Push</button>';

				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				$option .= '<button onclick="admin_cancel_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-danger btn-sm waves-effect waves-light mr-1" >Cancel</button>'; 
				return $option;
				

			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;

		$status = [6,8,9];
		$orders =  Orders::where('status',$status)->where('delivery_boy_id','!=',0)->get();
		$delivery_boy_ids = [];
		if(count($orders) > 0){
			$orders = $orders->toArray();
			$delivery_boy_ids = array_column($orders,'delivery_boy_id');
		}
		// _pre($delivery_boy_ids);

		$data['delivery_boy'] =  User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','4')->orderBy('id', 'desc')->where('users.active',1)->whereNotIn('users.id',$delivery_boy_ids)->get();
		// $data['delivery_boy'] = $this->Order_model->list_delivery_boy();
		$data['user'] = User::all();
		// _pre($data);

        $this->breadcrumbs->push($this->title , 'admin/order-inprocess');
        $this->breadcrumbs->push('In Process', '/', true);

		$this->admin_render('order/index',$data);	
	}

	public function order_assigned_view($id = ''){
		$data['title'] = $this->title;
		$data['back_url'] = base_url('admin/order-inprocess');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$data['order'] = $orders;
			$data['order_products'] = $order_products;

		}

		if(!isset($data['order_products']) && count($data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('admin/order-arrived'));
		}

        $this->breadcrumbs->push($this->title , 'admin/order-inprocess');
        $this->breadcrumbs->push('In Process', 'admin/order-inprocess');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->admin_render('order/view',$data);
	}

	public function order_inprocess_index(){
		
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$status = [2,5,6,7,8,9];
		// $status = [5];
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('orders.deleted_at', NULL)->where_in('orders.status',$status);
		$action['view'] = base_url('admin/order-inprocess/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){

				return ucfirst($first_name.' '.$row['last_name']);

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Assigned Delivery Person', 'delivery_boy_id',function($delivery_boy_id,$row){

				if($delivery_boy_id != 0){
					$this->db->select('first_name,last_name');
					$this->db->from('users');
					$this->db->where('id', $delivery_boy_id);
					$sql_query = $this->db->get();
					if ($sql_query->num_rows() > 0) {
						$delivery_boy = $sql_query->row_array();
						
						$string = ucfirst($delivery_boy['first_name'].' '.$delivery_boy['last_name']);
						$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
						return '<u><a target="_blank" href="'.base_url('/admin/delivery-boy/view/').$row['delivery_boy_id'].'">'.$string.'</a></u>';

						// return $delivery_boy['first_name'].' '.$delivery_boy['last_name'];
					}
				}else{
					return '';
				}

			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";

				if($row['order_type'] == 1 || $row['order_type'] == 3){

					/* if($row['status'] == 2 || $row['status'] == 5 || $row['status'] == 7){
						if($row['delivery_boy_id'] != 0){
							$option .= '<button type="button" id="1'.$id.'" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-2 reassign" onclick="assign_delivery_boy('.'1'.$id.','.$row['delivery_boy_id'].')" data-toggle="modal" data-target="#exampleModal" >Reassign</button>';
						}else{
							$option .= '<button type="button" id="1'.$id.'" data-id ="'.$id.'"  class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-2" onclick="assign_delivery_boy('.'1'.$id.')" data-toggle="modal" data-target="#exampleModal" >Assign</button>';
						}
					} */
				}else{
					//$option .= '<button onclick="completed(this);" data-alcoholic="'.$row['alcoholic'].'" data-id="' . $id . '"  data-userid="' . $row['user_id'] . '" class="btn btn-success btn-sm waves-effect waves-light mr-1 reassign">Complete</button>';
				}
				// $option .= '<button type="button" id="'.$id.'" data-id ="'.$id.'" data-userid ="'.$row['user_id'].'"  data-deliveryid ="'.$row['delivery_boy_id'].'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1" onclick="send_push_notification('.$id.')" data-toggle="modal" data-target="#sendpush" >Push</button>';

				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				$order_status_allowed = array('1', '2', '5', '6', '7', '8', '9');
				if(in_array($row['status'], $order_status_allowed)){
					$option .= '<button onclick="admin_cancel_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-danger btn-sm waves-effect waves-light admin-cancel-order-btn ml-1" >Cancel</button>';
				}
				return $option;
				

			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id');
		$dt_data->datatable('orders');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		
		$data['delivery_boy'] =  User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','4')->orderBy('id', 'desc')->where('users.active',1)->get();
		$data['user'] = User::all();
		// _pre($data);

        $this->breadcrumbs->push($this->title , 'admin/order-inprocess');
        $this->breadcrumbs->push('In Process', '/', true);

		$this->admin_render('order/index',$data);	
	}

	public function order_inprocess_view($id = ''){
		$data['title'] = $this->title;
		$data['back_url'] = base_url('admin/order-inprocess');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$data['order'] = $orders;
			$data['order_products'] = $order_products;

		}

		if(!isset($data['order_products']) && count($data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('admin/order-arrived'));
		}

        $this->breadcrumbs->push($this->title , 'admin/order-inprocess');
        $this->breadcrumbs->push('In Process', 'admin/order-inprocess');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->admin_render('order/view',$data);
	}

	public function assign_delivery_boy(){
		$request = $_POST;

		$result = $this->assign_db($request);
		if($result['status'] == TRUE){
			$this->session->set_flashdata('success',$result['message']);
		}else{
			$this->session->set_flashdata('error',$result['message']);
		}
		
		redirect(base_url('admin/order-assigned'),'refresh'); 
	}

	public function assign_db($request){
		$return = array('status' => false, 'message' => '');
		// _pre($request);
		$orders = Orders::with(['vendor','user_vendor','user','orderproducts'])->where('id', $request['order_id'])->where('status',5)->first();
		if (isset($orders)){
			$orders = $orders->toArray();
			$customer_id = $orders['user_id'];
			$orders = Orders::where('id',$request['order_id'])->first();
            $user = User::where('id',$orders['user_id'])->first();
            $user_phone = $user['phone'];
           
			if($orders['delivery_boy_id'] == $request['delivery_boy'] && $orders['status'] == 6){
				$return['status'] = TRUE;
				$return['message'] = 'Same Delivery Person already assigned.';
				return $return;
			}

			$delivery_boy =  User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','4')->where('users.active',1)->where("users.id", $request['delivery_boy'])->first();
            if (isset($delivery_boy)){
            	$delivery_boy = $delivery_boy->toArray();

            	if($delivery_boy['duty'] == '1'){
					$user_data['delivery_boy_id'] = $request['delivery_boy'];
					$user_data['status'] = 6;
					$user_data['db_assigned_at'] = date('Y-m-d H:i:s');
					Orders::where('id', $request['order_id'])->update($user_data);
					
					$orders['delivery_time']  = date('h:i A');
					$orders['items']	= $orders['orderproducts'];
					/*delivery boy notification start*/
					$push_title = 'New request of delivery';
					$message = 'Hey '.$delivery_boy['first_name'].'! ';
					$message .= 'You got new order delivery request.';

					$push_data = array(
						'order_id' => $request['order_id'],
						'message' => $message
					);

					$push_type = 'order_assigned';

					$device = Devices::where('user_id',$request['delivery_boy'])->first();


					if($delivery_boy['notification_status'] == 1 && $device['token'] != ''){
						$SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
					}
					notification_add($request['delivery_boy'],$message,$push_type,0,$request['order_id'],0);
					/*delivery boy notification end*/
					$customer_data =  User::select("users.*")->join('users_groups', 'users_groups.user_id','=','users.id')->where('users_groups.group_id','5')->where('users.active',1)->where("users.id", $customer_id)->first();

					/*customer  notification start*/
					$order_data = Orders::with(['user_device','delivery_boy'])->where('id',$request['order_id'])->first();
					
                    if (isset($order_data)) {
                        $order_data = $order_data->toArray();
                    }

                    $phone_no = $order_data['delivery_boy']['phone'];

                    $device_token = $order_data['user_device']['token'];
                    $device_type = $order_data['user_device']['type'];
                    
                    $sms_text = $order_data['delivery_boy']['first_name'].' '.$order_data['delivery_boy']['last_name'].' has been assigned as a delivery person for your order #'.$request['order_id'].'. Call'.$phone_no.' for a real-time update.';
                    /*john doe has been assigned as a delivery person for your order #123. Call 677678677676 for a real-time update.*/
                    $push_title = 'Delivery Person assigned';
                    $push_data = array(
                            'order_id' => $request['order_id'],
                            'message' => $sms_text
                        );
                    $push_type = 'delivery_boy_accepted';

                    $message = $sms_text; 
                    
                    if($user['notification_status'] == 1 && $device_token != ''){
                        $SendNotification = SendNotification($device_type,$device_token, $push_title, $push_data, $push_type);
                    }
                    notification_add($order_data['user_id'],$message,$push_type,0,$request['order_id'],0);

                    send_SMS($user_phone,$sms_text);

                    $emit_data = array("delivery_boy_id" => $request['delivery_boy'],"id" => $request['order_id']);
                    $client = new Client(new Version2X($this->config->item('socket_url')));
                    $client->initialize();

                    $client->emit('accept_booking_request',$emit_data);
                    $client->close();

					/*customer  notification end*/
					$return['status'] = TRUE;
					$return['message'] = 'Delivery person Assigned.';
					// $return['SendNotification'] = $SendNotification;
					$return['order'] = $orders;
						
					
            	}else{
            		$return['status'] = FALSE;
					$return['message'] = 'Delivery person offline';
            	}
            }else{
            	$return['status'] = FALSE;
				$return['message'] = 'Delivery person account not found';
            }

		}else{
			$return['status'] = FALSE;
			$return['message'] = 'Delivery person already accepted the order';
		}
		return $return;
	}

	public function order_rejected_index() {

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('orders.deleted_at', NULL)->where('orders.status',3);
		$action['view'] = base_url('admin/order-rejected/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){
				$string = ucfirst(htmlentities($first_name.' '.$row['last_name']));
				$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
				return '<u><a target="_blank" href="'.base_url('/admin/user/view/').$row['user_id'].'">'.$string.'</a></u>';

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Admin Amount', 'admin_amount',function($admin_amount,$row){

				return "$ ".$admin_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Reason', 'status', function ($status, $row){
	
				return '<span class="badge badge-danger">Store Rejected</span>';
				
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				return $option;

			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id,vendor.name');
		$dt_data->datatable('orders');
		
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'admin/order-rejected');
        $this->breadcrumbs->push('Rejected', '/', true);
		$this->admin_render('order/index',$data);	
	}

	public function order_rejected_view($id = ''){
		$output_data['title'] = $this->title;
		$output_data['back_url'] = base_url('admin/order-rejected');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$output_data['order'] = $orders;
			$output_data['order_products'] = $order_products;

		}

		if(!isset($output_data['order_products']) && count($output_data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('admin/order-rejected'));
		}

        $this->breadcrumbs->push($this->title , 'admin/order-rejected');
        $this->breadcrumbs->push('Rejected', 'admin/order-rejected');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->admin_render('order/view',$output_data);
	}

	public function order_completed_index() {

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('orders.deleted_at', NULL)->where('orders.status',10);
		$action['view'] = base_url('admin/order-completed/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){
				$string = ucfirst(htmlentities($first_name.' '.$row['last_name']));
				$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
				return '<u><a target="_blank" href="'.base_url('/admin/user/view/').$row['user_id'].'">'.$string.'</a></u>';

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Admin Amount', 'admin_amount',function($admin_amount,$row){

				return "$ ".$admin_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				return $option;

			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id,vendor.name');
		$dt_data->datatable('orders');
		
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'admin/order-completed');
        $this->breadcrumbs->push('Completed', '/', true);
		$this->admin_render('order/index',$data);	
	}

	public function order_completed_view($id = ''){

		$output_data['title'] = $this->title;
		$output_data['back_url'] = base_url('admin/order-completed');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$output_data['order'] = $orders;
			$output_data['order_products'] = $order_products;

		}

		if(!isset($output_data['order_products']) && count($output_data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('admin/order-completed'));
		}


        $this->breadcrumbs->push($this->title , 'admin/order-completed');
        $this->breadcrumbs->push('Completed', 'admin/order-completed');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->admin_render('order/view',$output_data);
	}

	public function all_orders_index() {

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('orders.deleted_at', NULL);
		$action['view'] = base_url('admin/all-orders/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){
				$string = ucfirst(htmlentities($first_name.' '.$row['last_name']));
				$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
				return '<u><a target="_blank" href="'.base_url('/admin/user/view/').$row['user_id'].'">'.$string.'</a></u>';

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Admin Amount', 'admin_amount',function($admin_amount,$row){

				return "$ ".$admin_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Status', 'status',function($status,$row){

				if($status == 1){
                    return '<span class="badge badge-primary">Pending</span>';
                }else if($status == 2){
                    return '<span class="badge badge-info">Accepted by store</span>';
                }else if($status == 3){
                    return '<span class="badge badge-danger">Rejected by store</span>';
                }else if($status == 4){
                    return '<span class="badge badge-danger">Cancelled</span>';
                }else if($status == 5){
                    return '<span class="badge badge-info">Assign Delivery Person</span>';
                }else if($status == 6){
                    return '<span class="badge badge-info">Accepted by Delivery Person</span>';
                }else if($status == 7){
                    return '<span class="badge badge-danger">Rejected by Delivery Person</span>';
                }else if($status == 8){
                    return '<span class="badge badge-info">Reached at restaurant</span>';
                }else if($status == 9){
                    return '<span class="badge badge-info">On the Way</span>';
                }else if($status == 10){
                    return '<span class="badge badge-success">Delivered</span>';
                }else{
                    return ' ';
                }

			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				if($row['status'] == 1 || $row['status'] == 2 || $row['status'] == 5 || $row['status'] == 6 || $row['status'] == 7 || $row['status'] == 8 || $row['status'] == 9  ){
					$option .= '<button type="button" id="'.$id.'" data-id ="'.$id.'" data-userid ="'.$row['user_id'].'"  data-deliveryid ="'.$row['delivery_boy_id'].'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light mr-1" onclick="send_push_notification('.$id.')" data-toggle="modal" data-target="#sendpush" >Push</button>';
				}
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';
				$order_status_allowed = array('1', '2', '5', '6', '7', '8', '9');
				if(in_array($row['status'], $order_status_allowed)){
					$option .= '<button onclick="admin_cancel_order(this);" data-ordertype = "'.$row['order_type'].'" data-id="' . $row['id'] . '" data-userid="' . $row['user_id'] . '" class="btn btn-danger btn-sm waves-effect waves-light admin-cancel-order-btn ml-1" >Cancel</button>';
				}

				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id,vendor.name');
		$dt_data->datatable('orders');
		
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$data['user'] = User::all();
		// _pre($data['user']->toArray());
        $this->breadcrumbs->push($this->title , 'admin/all-orders');
        $this->breadcrumbs->push('All Orders', '/', true);
		$this->admin_render('order/index',$data);	
	}

	public function send_push_notification(){
		$request = $_POST;
		/*_pre($request);*/
		$result =[];
		$user = User::where("id",$request['user'])->first();
        if (isset($user)){
            $user = $user->toArray();
            $user['order_id'] = $request['order_id_push'];
			
			$device = Devices::where('user_id',$request['user'])->first();

			$push_title = 'Order #'.$request['order_id_push'].' : '. $request['title'];
			$push_data_msg = 'Hey '.$user['first_name'].'! ';
			$push_data_msg .= $request['description'];

			$push_data = array(
				'order_id' => $request['order_id_push'],
				'message' => $push_data_msg
			);
			$push_type = 'order_detail';

			if($user['notification_status'] == 1 && $device['token'] != ''){
				$SendNotification = SendNotification($device['type'],$device['token'], $push_title, $push_data, $push_type);
			}
			// notification_add($request['user'],$push_data_msg,$push_type,0,$request['order_id'],0);

            $result['status'] = TRUE;
            $result['message'] = 'Notification send successfully.';
        }else{
            $result['status'] = false;
            $result['message'] = 'User not found.';
        }
		
		// $result = $this->Order_model->send_push_notification($request);

		if($result['status'] == TRUE){
			$this->session->set_flashdata('success',$result['message']);
		}else{
			$this->session->set_flashdata('error',$result['message']);
		}

		redirect($request['url'],'refresh'); 
		//redirect(base_url('admin/all-orders'),'refresh'); 
	}

	public function all_orders_view($id = ''){
		$output_data['title'] = $this->title;
		$output_data['back_url'] = base_url('admin/all-orders');

		$orders = Orders::with(['vendor','user_vendor','user','promocode','delivery_boy'])->where('id',$id)->first();
		if(isset($orders)){
			$orders = $orders->toArray();

			$order_products = OrderProducts::where('order_id',$id)->with(['orderproductsaddon'])->get();
			$subtotal = 0;
			if(count($order_products) > 0){
				$order_products = $order_products->toArray();
				foreach ($order_products as $key => $value) {
					$addon_total = 0;
					if(count($value['orderproductsaddon']) > 0){
						foreach ($value['orderproductsaddon'] as $ckey => $cvalue) {
							$addon_total += $cvalue['price'];
						}
					}
					$order_products[$key]['addon_total'] = $addon_total * $value['quantity'];
				}
				$total_price = 0;
				foreach ($order_products as $key => $value) {
					$subtotal = $value['total_amount'] + $value['addon_total'];
					$total_price  += $subtotal;  

					
				}

			}

			if($orders['payment_mode'] == 1){
				$orders['payment_type'] = 'Paid: Using card';
			}else{
				$orders['payment_type'] = 'Paid: Using wallet';
			}

			$orders['subtotal'] = number_format((float)$total_price, 2, '.', '');
			$output_data['order'] = $orders;
			$output_data['order_products'] = $order_products;

		}

		if(!isset($output_data['order_products']) && count($output_data['order_products']) == 0){
			$this->session->set_flashdata('error', 'No any orders found!');
			redirect(base_url('admin/all-orders'));
		}

        $this->breadcrumbs->push($this->title , 'admin/all-orders');
        $this->breadcrumbs->push('All Orders', 'admin/all-orders');
        $this->breadcrumbs->push('Order Detail', '/', true);
		$this->admin_render('order/view',$output_data);
	}

}
?>
