<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PromocodeController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		
		$this->load->model('Promocode');
		$this->load->model('User');
		$this->title = 'Promocode';
		
	}

	public function index()
	{
		$users = User::with(['vendor'])->get();
		if(count($users) > 0){
			$users = $users->toArray();
		}
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		/* $dt_data->select('promocode.*,vendor.name', false)->from('promocode')->join('vendor', 'vendor.user_id = promocode.user_id')->where('promocode.deleted_at', NULL); */
		$dt_data->select('promocode.*', false)->from('promocode')->where('promocode.deleted_at', NULL);
		$action['edit'] = base_url('admin/promocode/edit/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Promocode', 'promocode',function($promocode,$row){

				return ucfirst(htmlentities($promocode));

			})
			->column('Restaurant Name', 'user_id',function($user_id,$row) use ($users){
				$name = '';
				foreach ($users as $key => $value) {
					if($user_id == $value['id']){
						$name = '<u><a  target="_blank" href="'.base_url('admin/vendor/view/').$value['id'].'">'.$value['vendor']['name'].'</a></u>';
					}
				} 
				return $name; 
				

			})
			->column('Minimum Order Amount', 'promo_min_order_amount',function($promo_min_order_amount,$row){

				return '$ '.$promo_min_order_amount;

			})
			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="promocode" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="promocode" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">Edit</a>';
				$option .= " <button type='button' data-table='promocode' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('promocode.id,promocode');
		$dt_data->datatable('promocode');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$data['add_url'] = base_url('admin/promocode/add');
        $this->breadcrumbs->push($this->title , 'admin/promocode');
		$this->admin_render('promocode/index',$data);	
	}
	public function isexists_promo($str = NULL, $id = NULL) {

			$this->db->select('*');
			if ($id != '') {
				$this->db->where('id !=', $id);
			}
			$this->db->where('promocode', $str);
			$this->db->where('deleted_at', NULL);
			$this->db->from('promocode');
			$sql_query = $this->db->get();
			if ($sql_query->num_rows() > 0) {
				$this->form_validation->set_message('isexists_promo', "This promocode field is already exists.");
				return FALSE;
			} else {
				return TRUE;
			}
	}
	public function add(){
		
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$validation_rules = array(
					array('field' => 'promocode', 'label' => 'promocode', 'rules' => 'trim|required|min_length[2]|max_length[50]|callback_isexists_promo')
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){
					
					$request = $this->input->post();

					if(isset($_POST['user_id']) && !empty($_POST['user_id']))
			        {
						$promo_data['user_id'] = $request['user_id']; 
					}else{
						$promo_data['user_id'] = 0;
					}

					if(isset($_POST['from_date']) && !empty($_POST['from_date']))
			        {
			            $from_date = $request['from_date']; 
						$date = DateTime::createFromFormat('d F, Y', $from_date);
				        $promo_data['from_date'] =  $date->format('Y-m-d');
			        }
			        if(isset($_POST['to_date']) && !empty($_POST['to_date']))
			        {
			            $to_date = $request['to_date']; 
						$date = DateTime::createFromFormat('d F, Y', $to_date);
				        $promo_data['to_date'] =  $date->format('Y-m-d');
			        }

					$promo_data['promocode'] = strtoupper($request['promocode']);
					$promo_data['promo_min_order_amount'] = $request['promo_min_order_amount'];
					$promo_data['amount'] = $request['amount'];
					if(isset($_POST['discount_type']) && !empty($_POST['discount_type']))
			        {
						$promo_data['discount_type'] = $request['discount_type'];
					}
					if(isset($_POST['max_disc']) && !empty($_POST['max_disc']))
			        {
						$promo_data['max_disc'] = $request['max_disc'];
					}
					$promo_data['usage_limit'] = $request['usage_limit'];
					$promo_data['description'] = $request['description'];
         
                	Promocode::insert($promo_data);

                	$this->session->set_flashdata('success','Promocode added successfully');
					redirect(base_url("admin/promocode"));
					  	
				}
			}
		}

		$output_data['title'] = $this->title;
		$output_data['vendor'] = User::select('users.*')->join('users_groups','users_groups.user_id','users.id')->where('users.active',1)->where('users_groups.group_id',2)->with(['vendor'])->get();

		if(isset($output_data['vendor'])){
            $output_data['vendor'] = $output_data['vendor']->toArray();
        }
  
		$this->breadcrumbs->push($this->title , 'admin/promocode');
        $this->breadcrumbs->push('Add', '/', true);
		$this->admin_render('promocode/post', $output_data);
	}
	public function edit($id = ''){
		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'promocode', 'label' => 'promocode', 'rules' => 'trim|required|min_length[2]|max_length[50]|callback_isexists_promo[' . $this->input->post("id") . ']')
				);

				$this->form_validation->set_rules($validation_rules);
				 
				if ($this->form_validation->run() === true) {


					$request = $this->input->post();

                	$promo_data['promocode'] = strtoupper($request['promocode']);
					$promo_data['promo_min_order_amount'] = $request['promo_min_order_amount'];
					$promo_data['amount'] = $request['amount'];
					$promo_data['discount_type'] = isset($request['discount_type'])?0:1;

					$promo_data['max_disc'] = $request['max_disc'];
					$promo_data['usage_limit'] = $request['usage_limit'];
					$promo_data['description'] = $request['description'];

					if(isset($_POST['user_id']) && !empty($_POST['user_id']))
			        {
						$promo_data['user_id'] = $request['user_id']; 
					}else{
						$promo_data['user_id'] = 0;
					}

					if(isset($_POST['from_date']) && !empty($_POST['from_date']))
			        {
			            $from_date = $request['from_date']; 
						$date = DateTime::createFromFormat('d F, Y', $from_date);
				        $promo_data['from_date'] =  $date->format('Y-m-d');
			        }
			        if(isset($_POST['to_date']) && !empty($_POST['to_date']))
			        {
			            $to_date = $request['to_date']; 
						$date = DateTime::createFromFormat('d F, Y', $to_date);
				        $promo_data['to_date'] =  $date->format('Y-m-d');
			        }
                	Promocode::whereId($id)->update($promo_data);

                	$this->session->set_flashdata('success','Promocode updated successfully');
					redirect(base_url("admin/promocode"));
						
				} 
			}
		}
		$output_data['title'] = $this->title;
		$output_data['vendor'] = User::select('users.*')->join('users_groups','users_groups.user_id','users.id')->where('users.active',1)->where('users_groups.group_id',2)->with(['vendor'])->get();

		if(isset($output_data['vendor'])){
            $output_data['vendor'] = $output_data['vendor']->toArray();
        }
  
		if(!isset($output_data['vendor']) && count($output_data['vendor']) == 0){
			$this->session->set_flashdata('error', 'No any promocode found!');
			redirect(base_url('admin/promocode'));
		}
		
		$this->breadcrumbs->push($this->title , 'admin/promocode');
        $this->breadcrumbs->push('Edit', '/', true);
		$output_data['promocode'] = Promocode::find($id);
		$this->admin_render('promocode/put', $output_data);
	}

}
?>
