<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class AdminController extends MY_Controller {

	public function __construct() {
		parent::__construct();
		/* if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		} */
		$this->load->model('User');
		$this->title = 'Admin Dashboard';
	}

	public function change_password(){
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
				$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]');
				$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

				 
				if ($this->form_validation->run() === true){
					$request = $this->input->post();


					$identity =  $this->session->userdata['admin']['identity'];
					$id =  $this->session->userdata['admin']['user_id'];

					// _pre($this->session->userdata());
					$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'),$id);

					if($change){
						$this->session->set_flashdata('success', $this->ion_auth->messages());
					}else{
						$this->session->set_flashdata('error', $this->ion_auth->errors());
					}
					redirect(base_url('admin/change-password'));
				}

			}
		}

		$output_data['title'] = 'Change Password';
		$output_data['change_pw_link'] = base_url('admin/change-password');
		$this->breadcrumbs->push('Change Password' , 'admin/change-password');
		$this->admin_render('change_password',$output_data);
	}

	public function forgot_password(){

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item("form_field_error_prefix"), $this->config->item("form_field_error_suffix"));

		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){

				$this->form_validation->set_rules('identity', 'email', 'required');
				 
                if ($this->form_validation->run() === true) {
                    $request = $this->input->post();

                    $user = User::select('users.*')->where('email', $request['identity'])->join('users_groups', 'users_groups.user_id', 'users.id')->whereIn('users_groups.group_id', [1])->first();
                    if (isset($user)) {
                        //forgot pass
                        $forgotten = $this->ion_auth_model->forgotten_password_vendor($request['identity'], $user->id);
                        $data = [
							'identity'   => $user->phone,
							'id'         => $user->id,
							'email'      => $user->email,
							'forgotten' => $forgotten ,
						];
						
						$this->lang->load('ion_auth');
						$message = $this->load->view('email_templates/reset_password_vendor', $data, true);
						$subject = $this->config->item('site_title', 'ion_auth') . ' -  Reset Password';
						$to_email = $user->email;
						send_mail_new($to_email, $subject, $message);
						$this->session->set_flashdata('success', 'Reset password link sent to your email.');

                        /* $this->load->library(['email']);
                        $this->lang->load('ion_auth');
                    
                        $email_config = $this->config->item('email_config', 'ion_auth');

                        $this->email->initialize($email_config);
                        $message = $this->load->view('email_templates/reset_password_vendor', $data, true);
                    
                        $this->email->set_newline("\r\n");
                        $this->email->from('ruralareadelivery@gmail.com', "RAD");
                        $this->email->to($user->email);
                        $this->email->reply_to('info@radservices.ca', "RAD");
                        $this->email->subject($this->config->item('site_title', 'ion_auth') . ' -  Reset Password');
                        $this->email->message($message);

                        if ($this->email->send() === true) {
                            $this->session->set_flashdata('success', 'Reset password link sent to your email.');
                        } else {
                            $this->session->set_flashdata('error', 'Something went wrong');
                        } */
                        redirect(base_url('admin/forgot-password'));
                    }else{
						$this->session->set_flashdata('error', 'This account does not Exists!');
						redirect(base_url('admin/forgot-password'));
					}
                }
			}
		}
		$data['user_type'] = "admin";
		$this->load->view('auth/forgot_password',$data);
	}
}