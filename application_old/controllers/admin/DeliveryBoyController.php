<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DeliveryBoyController extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->model('User');
		$this->load->model('Orders');
		$this->load->helper('stripe_helper');
		
		$this->title = 'Delivery Person Management';
	}
	public function index(){

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('users.id, users.first_name, users.last_name, users.email, users.phone, users.active', false)
		->from('users')
		->join('users_groups', 'users_groups.user_id = users.id')
		->where('users_groups.group_id',4)
		->where('users.deleted_at', NULL)
		->where_in('users.active',[1,2]);
		$action['edit'] = base_url('admin/delivery-boy/edit/');
		$action['view'] = base_url('admin/delivery-boy/view/');
		$dt_data
			->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Name', 'first_name',function($first_name,$row){
				if($first_name != ''){
					$name = ucfirst($first_name.' '.$row['last_name']);
					$out = strlen($name) > 25 ? substr($name,0,25)."..." : $name;
					return $out;
				}else{
					return '-';
				}
			})
			
			->column('Phone', 'phone',function($phone,$row){
				if($phone != '' && $phone != '0'){
					return $phone;
				}else{
					return '-';
				}
			})
			//->column('Email', 'email')
			->column('Status', 'active', function ($active, $row){
				$option = "";
				if ($active == 1) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Active</button>';
				} elseif ($active == 2) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Inactive</button>';
				}else{
					return '<button class="btn btn-warning btn-sm waves-effect waves-light">Pending</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light" >Edit</a>';
				$option .= '<a href="' . $action['view'] . $id . '" class="ml-1 btn btn-dark btn-sm waves-effect waves-light" >View</a>';
				$option .= " <button type='button' data-table='users' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});
			
		$dt_data->searchable('users.id,first_name,last_name,email,phone');
		$dt_data->datatable('users');
		$dt_data->init();
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/delivery-boy');
		$data['datatable'] = true;
		$this->admin_render('delivery_boy/index', $data);	
	}
	public function customAlpha($str) {
		if (!preg_match('/^[a-z \-]+$/i', $str)) {
			$this->form_validation->set_message('customAlpha', 'The {field} field contain only alphabets and space.');
			return false;
		}
		return TRUE;
	}
	public function put($id = ''){
		$this->load->library(['ion_auth', 'form_validation']);
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'first_name', 'label' => 'first name', 'rules' => 'trim|required|min_length[2]'),
					array('field' => 'last_name', 'label' => 'last name', 'rules' => 'trim|required|min_length[2]'),
				);
				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();
					
					$user_data = [
							'first_name' => $request['first_name'],
							'last_name' => $request['last_name'],
						];
					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
                		$img_name = 'user_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
					        $user_data['profile_picture'] = $img_name;
					        if($request["old_profile_picture"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_profile_picture"]);
					        }
					        
					    }
					    $file_upload = true;
                	}

                	if(isset($_FILES['driving_license']) && !empty($_FILES['driving_license']) && strlen($_FILES['driving_license']['name']) > 0) {
                		$img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                		$destination_url = FCPATH . $this->config->item("document").$img_name;
					    if(compress($_FILES["driving_license"]["tmp_name"], $destination_url, 90)){
					        $user_data['driving_license'] = $img_name;
					        if($request["old_driving_license"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_driving_license"]);
					        }
					        
					    }
					    $file_upload = true;
                	}

                	if(isset($_FILES['insurance_certi']) && !empty($_FILES['insurance_certi']) && strlen($_FILES['insurance_certi']['name']) > 0) {
                		$img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                		$destination_url = FCPATH . $this->config->item("document").$img_name;
					    if(compress($_FILES["insurance_certi"]["tmp_name"], $destination_url, 90)){
					        $user_data['insurance_certi'] = $img_name;
					        if($request["old_insurance_certi"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_insurance_certi"]);
					        }
					        
					    }
					    $file_upload = true;
                	}
                	// _pre($user_data);
  
					if ($file_upload){
						User::whereId($id)->update($user_data);
						$this->session->set_flashdata('success', 'Delivery Boy updated successfully');
						redirect(base_url('admin/delivery-boy'));
					}
				} 
			}
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/delivery-boy');
        $this->breadcrumbs->push('Edit', '/', true);
		$output_data['user'] = User::find($id);
		if(!isset($output_data['user']) && count($output_data['user']) == 0){
			$this->session->set_flashdata('error', 'No any delivery boy found!');
			redirect(base_url('admin/delivery-boy'));
		}
		$this->admin_render('delivery_boy/put', $output_data);
	}
	public function view($id = ''){

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('orders.delivery_boy_id',$id)->where('orders.deleted_at', NULL);
		$action['view'] = base_url('admin/all-orders/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst($store_name);
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){
				$string = ucfirst($first_name.' '.$row['last_name']);
				$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
				return '<u><a target="_blank" href="'.base_url('/admin/user/view/').$row['user_id'].'">'.$string.'</a></u>';

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Admin Amount', 'admin_amount',function($admin_amount,$row){

				return "$ ".$admin_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			})
			->column('Status', 'status',function($status,$row){

				if($status == 1){
                    return '<span class="badge badge-primary">Pending</span>';
                }else if($status == 2){
                    return '<span class="badge badge-info">Accepted by store</span>';
                }else if($status == 3){
                    return '<span class="badge badge-danger">Rejected by store</span>';
                }else if($status == 4){
                    return '<span class="badge badge-danger">Cancelled</span>';
                }else if($status == 5){
                    return '<span class="badge badge-info">Delivery Person Assigned</span>';
                }else if($status == 6){
                    return '<span class="badge badge-info">Accepted by Delivery Person</span>';
                }else if($status == 7){
                    return '<span class="badge badge-danger">Rejected by Delivery Person</span>';
                }else if($status == 8){
                    return '<span class="badge badge-info">Reached at restaurant</span>';
                }else if($status == 9){
                    return '<span class="badge badge-info">On the Way</span>';
                }else if($status == 10){
                    return '<span class="badge badge-success">Delivered</span>';
                }else{
                    return ' ';
                }

			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-primary btn-sm waves-effect waves-light mr-1">View</a>';
				$option .= '<button type="button" id="track-order" data-id ="'.$id.'" class="btn-edit btn btn-primary btn-sm waves-effect waves-light"  data-toggle="modal" data-target="#trackmodal" >Track</button>';

				return $option;
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id,vendor.name');
		$dt_data->datatable('orders');
		
		$dt_data->init();
		$output_data['datatable'] = true;

		$orders = Orders::where('status',10)->where('delivery_boy_id',$id)->get();
        $output_data['total_orders'] = count($orders);

		$total_earnings = 0;
		if (count($orders) > 0) {
			foreach ($orders as $key => $value) {
				$total_earnings += $value['delivery_boy_amount'] + $value['tip'];
			}
		}
		$output_data['total_earnings'] = number_format((float)$total_earnings, 2, '.', '');

		$pending_orders = Orders::where('status','!=',10)->where('delivery_boy_id',$id)->get();
        $output_data['pending_orders'] = count($pending_orders);

		$output_data['db'] = User::with(['delivery_address','wallet_history'])->find($id);
		if(isset($output_data['db'])){
            $output_data['db'] = $output_data['db']->toArray();
        }
		$user = User::where('id',$id)->where('account_id','!=','')->first();
        if (isset($user->account_id) && $user->account_id != "") {
            $output_data['stripe_detail'] = getCustomAccount($user->account_id);
        }
		//_pre($output_data);
		if(!isset($output_data['db']) && count($output_data['db']) == 0){
			$this->session->set_flashdata('error', 'No any delivery boy found!');
			redirect(base_url('admin/delivery-boy'));
		}
		
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/delivery-boy');
        $this->breadcrumbs->push('View', '/', true);
		$this->admin_render('delivery_boy/view', $output_data);
	}

	public function pending_list(){

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('users.id, users.first_name, users.last_name, users.email, users.phone, users.active', false)
		->from('users')
		->join('users_groups', 'users_groups.user_id = users.id')
		->where('users_groups.group_id',4)
		->where('users.deleted_at', NULL)
		->where_in('users.active',[0,3]);
		$action['edit'] = base_url('admin/pending-delivery-boy/edit/');
		$action['view'] = base_url('admin/pending-delivery-boy/view/');
		$dt_data
			->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Name', 'first_name',function($first_name,$row){
				if($first_name != ''){
					$name = ucfirst($first_name.' '.$row['last_name']);
					$out = strlen($name) > 25 ? substr($name,0,25)."..." : $name;
					return $out;
				}else{
					return '-';
				}
			})
			
			->column('Phone', 'phone',function($phone,$row){
				if($phone != '' && $phone != '0'){
					return $phone;
				}else{
					return '-';
				}
			})
			//->column('Email', 'email')
			->column('Status', 'active', function ($active, $row){
				$option = "";
				if ($active == 1) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Active</button>';
				} elseif ($active == 2) {
					return '<button onclick="active_deactive(this);" data-table="users" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="users" data-status="' . $active . '">Inactive</button>';
				}elseif ($active == 3){
					return '<span class="badge badge-danger">Rejected</span>';
				}else{
					$option .= '<button onclick="accept_db(this);" data-id="' . $row['id'] . '"  data-phone="' . $row['phone'] . '" class="btn btn-success btn-sm waves-effect waves-light mr-1">Accept</button>';
				
					$option .= '<button onclick="reject_db(this);" data-id="' . $row['id'] . '"  data-phone="' . $row['phone'] . '" class="btn btn-danger btn-sm waves-effect waves-light mr-1" >Reject</button>';

					return $option;
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light" >Edit</a>';
				$option .= '<a href="' . $action['view'] . $id . '" class="ml-1 btn btn-dark btn-sm waves-effect waves-light" >View</a>';
				$option .= " <button type='button' data-table='users' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});
			
		$dt_data->searchable('users.id,first_name,last_name,email,phone');
		$dt_data->datatable('users');
		$dt_data->init();
		$data['title'] = 'Pending Requests';
		$this->breadcrumbs->push('Pending Requests' , 'admin/pending-delivery-boy');
		$data['datatable'] = true;
		$this->admin_render('pending_delivery_boy/index', $data);	
	}
	public function pending_list_put($id = ''){
		$this->load->library(['ion_auth', 'form_validation']);
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				$validation_rules = array(
					array('field' => 'first_name', 'label' => 'first name', 'rules' => 'trim|required|min_length[2]'),
					array('field' => 'last_name', 'label' => 'last name', 'rules' => 'trim|required|min_length[2]'),
				);
				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {
					$file_upload = true;
					$request = $this->input->post();
					
					$user_data = [
							'first_name' => $request['first_name'],
							'last_name' => $request['last_name'],
						];
					if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']) && strlen($_FILES['profile_picture']['name']) > 0) {
                		$img_name = 'user_' . time().rand(1000, 9999).'.jpg';
					    $destination_url = FCPATH . $this->config->item("user_profile_path").$img_name;
					    if(compress($_FILES["profile_picture"]["tmp_name"], $destination_url, 90)){
					        $user_data['profile_picture'] = $img_name;
					        if($request["old_profile_picture"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_profile_picture"]);
					        }
					        
					    }
					    $file_upload = true;
                	}

                	if(isset($_FILES['driving_license']) && !empty($_FILES['driving_license']) && strlen($_FILES['driving_license']['name']) > 0) {
                		$img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                		$destination_url = FCPATH . $this->config->item("document").$img_name;
					    if(compress($_FILES["driving_license"]["tmp_name"], $destination_url, 90)){
					        $user_data['driving_license'] = $img_name;
					        if($request["old_driving_license"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_driving_license"]);
					        }
					        
					    }
					    $file_upload = true;
                	}

                	if(isset($_FILES['insurance_certi']) && !empty($_FILES['insurance_certi']) && strlen($_FILES['insurance_certi']['name']) > 0) {
                		$img_name = 'document_' . time().rand(1000, 9999).'.jpg';
                		$destination_url = FCPATH . $this->config->item("document").$img_name;
					    if(compress($_FILES["insurance_certi"]["tmp_name"], $destination_url, 90)){
					        $user_data['insurance_certi'] = $img_name;
					        if($request["old_insurance_certi"] != 'assets/images/default.png'){
					        	unlink(FCPATH.$request["old_insurance_certi"]);
					        }
					        
					    }
					    $file_upload = true;
                	}
                	// _pre($user_data);
  
					if ($file_upload){
						User::whereId($id)->update($user_data);
						$this->session->set_flashdata('success', 'Delivery Boy updated successfully');
						redirect(base_url('admin/pending-delivery-boy'));
					}
				} 
			}
		}
		$output_data['title'] = 'Pending Requests';
		$this->breadcrumbs->push('Pending Requests' , 'admin/pending-delivery-boy');
        $this->breadcrumbs->push('Edit', '/', true);
		$output_data['user'] = User::find($id);
		if(!isset($output_data['user']) && count($output_data['user']) == 0){
			$this->session->set_flashdata('error', 'No any delivery person found!');
			redirect(base_url('admin/pending-delivery-boy'));
		}
		$this->admin_render('pending_delivery_boy/put', $output_data);
	}
	public function pending_list_view($id = ''){
		$output_data['db'] = User::with(['delivery_address','wallet_history'])->find($id);
		if(isset($output_data['db'])){
            $output_data['db'] = $output_data['db']->toArray();
        }
		//_pre($output_data);
		if(!isset($output_data['db']) && count($output_data['db']) == 0){
			$this->session->set_flashdata('error', 'No any delivery person found!');
			redirect(base_url('admin/pending-delivery-boy'));
		}
		
		$output_data['title'] = 'Pending Requests';
		$this->breadcrumbs->push('Pending Requests' , 'admin/pending-delivery-boy');
        $this->breadcrumbs->push('View', '/', true);
		$this->admin_render('pending_delivery_boy/view', $output_data);
	}
}