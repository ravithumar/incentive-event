<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EarningController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
        
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		
        $this->load->model('User');

        $this->load->model('Orders');
        $this->load->model('OrderProductAddOn');
        $this->load->model('Devices');
        $this->load->model('OrderProducts');
		$this->title = 'Earning';
		
	}


	public function index() {

		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('orders.*,users.first_name,users.last_name,vendor.name as store_name', false)->from('orders')->join('users','orders.user_id = users.id')->join('vendor','orders.vendor_id = vendor.user_id')->where('orders.deleted_at', NULL)->where('orders.status',10);
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Store name', 'store_name',function($store_name,$row){
				$store_name =ucfirst(htmlentities($store_name));
				$store_name = (strlen($store_name) > 13) ? substr($store_name,0,13).'...' : $store_name;
				return '<u><a target="_blank" href="'.base_url('/admin/vendor/view/').$row['vendor_id'].'">'.$store_name.'</a></u>';

			})
			->column('Customer Name', 'first_name',function($first_name,$row){
				$string = ucfirst(htmlentities($first_name.' '.$row['last_name']));
				$string = (strlen($string) > 13) ? substr($string,0,13).'...' : $string;
				return '<u><a target="_blank" href="'.base_url('/admin/user/view/').$row['user_id'].'">'.$string.'</a></u>';

			})
			->column('Order Amount', 'total_amount',function($total_amount,$row){

				return "$ ".$total_amount;

			})
			->column('Revenue', 'admin_amount',function($admin_amount,$row){

				return "$ ".$admin_amount;

			})
			->column('Order Type', 'order_type',function($order_type,$row){
				// 1-Delivery,2-Takeout, 3-Later Delivery 4- Later Takeway
				if($order_type == 1){
					return "Delivery";
				}elseif($order_type == 2){
					return "Takeout";
				}elseif($order_type == 3){
					return "Later Delivery";
				}else{
					return "Later Takeway";
				}

			}) 
			->column('Order Placed On', 'created_at', function ($created_at, $row){
				$date = DateTime::createFromFormat('Y-m-d H:i:s',$created_at);
				$created_at_date = $date->format('d M, Y');
                $created_at_time = $date->format('h:i A');
                return $created_at_date.' '.'at'.' '.$created_at_time;
				
			});

		$dt_data->searchable('users.first_name,users.last_name,orders.id,vendor.name');
		$dt_data->datatable('orders');
		
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'admin/earning');
		$this->admin_render('earning/index',$data);	
	}

}
?>
