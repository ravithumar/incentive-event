<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IssueController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		
		$this->load->model('Issue');
		$this->title = 'Issue Management';
		
	}

	public function index()
	{
		
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('issue.*', false)->from('issue')->where('issue.deleted_at', NULL);
		$action['edit'] = base_url('admin/issue/edit/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Issue', 'issue',function($issue,$row){
				 $out = strlen($issue) > 50 ? substr($issue,0,50)."..." : $issue;
				 return $out;

			})
			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="issue" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="issue" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['edit'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">Edit</a>';
				$option .= " <button type='button' data-table='issue' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('id,question');
		$dt_data->datatable('issue');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/issue');
		$data['add_url'] = base_url('admin/issue/add');
        
		$this->admin_render('issue/index',$data);	
	}
	public function add(){

		
		if (isset($_POST) && !empty($_POST)){
			if (isset($_POST['submit'])){
				
				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					array('field' => 'issue', 'label' => 'issue', 'rules' => 'trim|required|min_length[2]'),
				);

				$this->form_validation->set_rules($validation_rules);
				
				if ($this->form_validation->run() === true){

					$request = $this->input->post();
                	$user_data['issue'] = $request['issue'];
                	Issue::insert($user_data);

					$this->session->set_flashdata('success','Issue added successfully');
					redirect(base_url("admin/issue"));

				}
			}
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/issue');
        $this->breadcrumbs->push('Add', '/', true);
		$this->admin_render('issue/post', $output_data);
	}
	public function edit($id = ''){

		
		if (isset($_POST) && !empty($_POST)){

			if (isset($_POST['submit'])){

				$this->load->library(['ion_auth', 'form_validation']);

				$validation_rules = array(
					
					array('field' => 'issue', 'label' => 'issue', 'rules' => 'trim|required|min_length[2]'),
				);

				$this->form_validation->set_rules($validation_rules);
				if ($this->form_validation->run() === true) {

                    	$request = $this->input->post();

                    	$user_data['issue'] = $request['issue'];
                    	Issue::whereId($id)->update($user_data);

                    	$this->session->set_flashdata('success', 'Issue updated successfully');
						redirect(base_url('admin/issue'));

				} 
			}	
		}
		$output_data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/issue');
        $this->breadcrumbs->push('Edit', '/', true);

		$output_data['issue'] = Issue::find($id);
		$this->admin_render('issue/put', $output_data);
	}

}
?>
