<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ProductController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->model('Product');
		$this->load->model('Variant');
        $this->load->model('Category');
        $this->load->model('User');
        $this->load->model('ProductVariant');
        $this->load->model('ProductAddOn');
		$this->title = 'Product';
	}
	public function index()
	{
		$this->load->library('Datatables');
		$dt_data = new Datatables;
		$dt_data->select('product.*,vendor.name', false)->from('product')->join('vendor', 'vendor.user_id = product.user_id')->where('product.deleted_at', NULL);
		$action['view'] = base_url('admin/product/view/');
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Image', 'image',function($image,$row){
				
				if($image != ''){
					$src = BASE_URL().$this->config->item("product_picture_path").$image;
				}else{
					$src = BASE_URL().'/assets/images/default.png';
				}
				return '<img class="border rounded p-0"  src="'.$src.'" alt="your image" style="height: 75px;width: 75px;object-fit: cover;" id="blah"/>';

			})
			->column('Title', 'title',function($title,$row){

				return ucfirst($title);

			})

			->column('Restaurant Name', 'name',function($name,$row){
				return '<u><a  target="_blank" href="'.base_url('admin/vendor/view/').$row['user_id'].'">'.$name.'</a></u>';

			})
			->column('Featured', 'featured', function ($featured, $row){
				if ($featured == 1) {
					return '<button onclick="featured(this);" data-id="' . $row['id'] . '" data-user_id="' . $row['user_id'] . '" class="btn btn-success btn-sm waves-effect waves-light" data-status="' . $featured . '">On</button>';
				} else{
					return '<button onclick="featured(this);" data-id="' . $row['id'] . '" data-user_id="' . $row['user_id'] . '" class="btn btn-danger btn-sm waves-effect waves-light" data-status="' . $featured . '">Off</button>';
				}
				
			})
			->column('Status', 'status', function ($status, $row){
				$option = "";
				if ($status == 1) {
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-success btn-sm waves-effect waves-light" data-table="product" data-status="' . $status . '">Active</button>';
				} else{
					return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '"  class="btn btn-danger btn-sm waves-effect waves-light" data-table="product" data-status="' . $status . '">Inactive</button>';
				}
			})
			->column('Action', 'id', function ($id, $row) use ($action){
				$option = "";
				$option .= '<a href="' . $action['view'] . $id . '" class="btn btn-dark btn-sm waves-effect waves-light">View</a>';
				$option .= " <button type='button' data-table='product' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('product.id,title,name');
		$dt_data->datatable('product');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
		$this->breadcrumbs->push($this->title , 'admin/product');
		$this->admin_render('product/index',$data);	
	}
	
	public function view($id = ''){
		
		$output_data['title'] = $this->title;
		$output_data['product'] = Product::with(['variant','addon','vendor'])->find($id);

		if(isset($output_data['product'])){
            $output_data['product'] = $output_data['product']->toArray();
        }

		if(!isset($output_data['product']) && count($output_data['product']) == 0){
			$this->session->set_flashdata('error', 'No any product found!');
			redirect(base_url('admin/product'));
		}
		// /_pre($output_data);		
		$this->breadcrumbs->push($this->title , 'admin/product');
        $this->breadcrumbs->push('View', '/', true);
		$this->admin_render('product/view', $output_data);
	}
/*
	public function featured_products()
	{
		$data['title'] = 'Featured Products';

		$data['products'] = Product::with(['vendor'])->where("featured", 1)->groupBy("id")->orderBy("featured_number", 'ASC')->get();
		//_pre($data['products']->toArray());
        $this->breadcrumbs->push('Featured Products' , 'admin/products');
		$this->admin_render('product/featured',$data);	
	}

	public function set_sequence(){

		$i = 1;
		$new_sequence = array();

		foreach ($_POST['seq'] as $row) {
			$new_sequence[$i] = $row;
	 		$i++;
		}

		foreach ($new_sequence as $key => $value) {
			$data = array('featured_number' => $key);
			$this->db->where('id', $value);
			$this->db->update("product", $data);
		}
		echo 'success';
	}*/
}
?>
