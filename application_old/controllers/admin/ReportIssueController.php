<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportIssueController extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/admin/login');
		}
		$this->load->model('ReportIssue');
		$this->title = 'Report Issue List';
	}

	public function index()
	{
		
		$this->load->library('Datatables');
		$dt_data = new Datatables; 
		$dt_data->select('report_issue.*,issue.issue',false)->from('report_issue')->join('issue','issue.id = report_issue.issue_id')->where('report_issue.deleted_at',NULL);
		$dt_data->style(array(
				'class' => 'table table-hover dt-responsive nowrap',
				'style' => 'border-collapse: collapse; border-spacing: 0; width: 100%;'
			))
			->column('Id', 'id')
			->column('Order Id', 'order_id',function($order_id,$row){
                return '<u><a target="_blank" href="'.base_url('/admin/all-orders/view/').$row['order_id'].'">'.$order_id.'</a></u>';

			})
			->column('Issue', 'issue',function($issue,$row){
				return $issue;
			})
			->column('Submitted On','created_at', function ($created_at, $row){
            	$created_at = DateTime::createFromFormat('Y-m-d H:i:s', $created_at);
                return $created_at->format('d F, Y');
        	})
            ->column('Status', 'status',function($status,$row){
                $option ="";
                if($row['status'] == 1 ){
					$option .= '<button onclick="change_status(this);" data-status="'.$row['status'].'" data-id="' . $row['id'] . '" class="btn btn-warning btn-sm waves-effect waves-light mr-1 w-75">Pending</button>';
				}else if($row['status'] == 2 ){
					$option .= '<button onclick="change_status(this);" data-status="'.$row['status'].'" data-id="' . $row['id'] . '" class="btn btn-warning btn-sm waves-effect waves-light mr-1 w-75">In Process</button>';
				}else{
					$option .= '<span class="badge badge-success">Resolved</span>';
				}
                return $option;
            })
			->column('Action', 'id', function ($id, $row){
				$option = "";
                
				$option .= '<button type="button" data-id="'.$id.'" data-order_id="'.$row['order_id'].'" data-description="'.$row['description'].'" data-issue="'.$row['issue'].'" class="btn-edit btn btn-dark btn-sm waves-effect waves-light" id="view" data-toggle="modal" data-target="#exampleModal" >View</button>';
				$option .= " <button type='button' data-table='report_issue' data-id='".$row['id']."' class='btn btn-danger btn-sm waves-effect waves-light delete' title='Delete' data-popup='tooltip' onclick='delete_notiflix(this);'>Delete</button>";
				return $option;
			});

		$dt_data->searchable('id,order_id');
		$dt_data->datatable('report_issue');
		$dt_data->init();
		$data['datatable'] = true;
		$data['title'] = $this->title;
        $this->breadcrumbs->push($this->title , 'admin/contact-us');
		$this->admin_render('report_issue/index',$data);	
	}
}
?>
