<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: text/html; charset=utf-8');
class CronController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Orders');
        $this->load->model('ion_auth_model');
        $this->load->model('Devices');
        $this->load->model('TrackOrder');
        $this->load->model('WalletHistory');
        $this->load->model('Wallet');
        $this->load->model('PaymentCard');
        $this->load->model('Vendor');
        $this->load->model('Notifications');
        $this->load->model('System_setting');
        $this->load->model('Transaction');
    }

    public function delete_users(){
        $user = User::select('users.*')
                ->join('users_groups', 'users_groups.user_id', 'users.id')
                ->whereIn('users_groups.group_id',[5,4])
                ->get();
                if (count($user) > 0 && isset($user)) {
                    $user = $user->toArray();
                    $ids = array_column($user,'id');
                    User::whereIn('id',$ids)->delete();
                }
        echo "<pre>";
        print_r($ids);
        exit();
    }
    public function order_assign()
    {

        // $date = date('Y-m-d H:i:s', strtotime('+10 minute'));
        $current_datetime = date("Y-m-d H:i:s");
        $RAD_number = System_setting::select('value')->where('path', 'RAD_number')->first();

        $delivery_boy_response_time = System_setting::select('value')->where('path', 'delivery_person_response_time')->first();
        $minutes_to_add = $delivery_boy_response_time['value'];
       
        $orders = Orders::where('status', 2)->where('order_type',1)->get();

        if (count($orders) > 0 && isset($orders)) {
            $orders = $orders->toArray();
            foreach ($orders as $key => $value) {
                $time = new DateTime($value['db_request_sent_at']);
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $created_time = $time->format('Y-m-d H:i:s');

                $orders[$key]['current_datetime'] = $current_datetime;
                $orders[$key]['created_time'] = $created_time;

                if ($created_time <= $current_datetime) {
                    $update_ids[] = array(
                        'id' => $value['id'],
                    );
                    //send SMS to Admin
                    $phone_no = $RAD_number['value'];
                    // $sms_text = 'Order #'.$value['id'].' - does not find any delivery person. Please assign delivery person to this Order!';
                    $sms_text = 'Order #'.$value['id'].' - No delivery person available. Please assign delivery person to this Order!';
                    /*Order #123 - No delivery person available. Please assign delivery person to this Order!*/
                    send_SMS($phone_no, $sms_text);
                }
            }

            if (isset($update_ids) && !empty($update_ids)) {
                $ids = array_column($update_ids, 'id');
                Orders::whereIn('id', $ids)->update(array('status' => 5));
            }

            
        }

        // echo $current_datetime;
        // echo "<pre>";
        // print_r($ids);
        // print_r($orders);
        // print_r($update_ids);
        // exit();
    }

    public function store_not_responde_expire()
    {
        /*echo date("Y-m-d H:i:s");*/
        $return_data = array();
        //$minutes_to_add = 10;
        $store_confirmation_minute = System_setting::select('value')->where('path', 'store_confirmation_minute')->first();
        $minutes_to_add = $store_confirmation_minute['value'];
        $current_datetime = date("Y-m-d H:i:s");

        $orders = Orders::where('status', 1)->with(['user_device','user'])->get();
        $SendNotification = $notification_result_pro = $update_data = $track_data = $transaction_data = [];
        if (isset($orders)) {
            $orders = $orders->toArray();
            
            $push_type = 'order_expire';
            $this->load->helper("stripe_helper");
            foreach ($orders as $key => $value) {
                $time = new DateTime($value['created_at']);
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $created_time = $time->format('Y-m-d H:i:s');
                $orders[$key]['current_datetime'] = $current_datetime;
                $orders[$key]['created_time'] = $created_time;
                
                $v_id = $value['vendor_id'];
                $vendor_detail = Vendor::where('user_id',$v_id)->first()->toArray();
                if(!empty($vendor_detail)) {

                    $restaurant_name = $vendor_detail["name"];
                    if ($current_datetime > $created_time) {
                        if ($value['payment_mode'] == 1) {
                            $refund_stripe = refund_stripe($value['transaction_id'],$value['total_amount']);
                        } else {
                            $update_wallet = [];
                            $balance = $value['total_amount'] + $value['user_balance'] ;
                            $update_wallet['balance'] = number_format((float)$balance, 2, '.', '');

                            Wallet::where('user_id', $value['user_id'])->update($update_wallet);

                            $temp_trans = array(
                                    'user_id' => $value['user_id'],
                                    'order_id' => $value['id'],
                                    'amount' => number_format((float)$value['total_amount'], 2, '.', ''),
                                    'type' => 'plus',
                                    'message' => 'Payment refunded to wallet for order #'.$value['id']

                                );
                                
                            $transaction_data[] = $temp_trans;
                        }

    					$update_ids[] = array(
    						'id' => $value['id'],
    					);
                        $push_title = 'Order #'.$value['id'].' Has Expired';
                        $message = 'Your order #'.$value['id'].' has been expired because '.$restaurant_name.'did not respond.You will recievce your refund within 3-4 working days.';
                        /*Your order #123 has expired because restaurant name did not respond.You will recievce your refund within 3-4 working days.*/
     
                        $push_data = array(
                             'order_id' => $value['id'],
                             'message' => $message
                         );

                         
                        if ($value['user']['notification_status'] == 1 && $value['user_device']['token'] != '') {

                            $SendNotification[] = SendNotification($value['user_device']['type'], $value['user_device']['token'], $push_title, $push_data, $push_type);
                        }
                        $temp = array(
                                 'user_id' => $value['user_id'],
                                 'user_id_2' => 0,
                                 'product_id' => 0,
                                 'order_id' => $value['id'],
                                 'message' => $message,
                                 'type' => $push_type
                             );
                              
                        $notification_result_pro[] = $temp;
     
                        $phone_no = $value['user']['phone'];
                        $sms_text = 'We apologize for the inconvenience caused and thank you for your cooperation. Any amount you paid for #'.$value['id'].' Order will be refunded.';
                         
                        send_SMS($phone_no, $sms_text);
     
                        //for order tracking
                        $current_datetime = date("Y-m-d H:i:s");
                        $temp_track = array(
                                 'order_id' => $value['id'],
                                 'status' => 4,
                                 'status_title' => 'Order Expired',
                                 'status_text' => 'Order expired because store did not respond.',
                                 'created_at'   => $current_datetime
                             );
                             
                        $track_data[] = $temp_track;
                    }
                }
            }
            if (isset($update_ids) && !empty($update_ids)) {
				$ids = array_column($update_ids,'id');
				$update_data = array(
					'status'  => 4, //expire status
					'reason'  => 'Order Expire', //expire status
				);
				Orders::whereIn('id', $ids)->update($update_data);
            } 
            if (isset($notification_result_pro) && !empty($notification_result_pro)) {
				Notifications::insert($notification_result_pro);
            }
            if (isset($track_data) && !empty($track_data)) {
				TrackOrder::insert($track_data);
            }
            if (isset($transaction_data) && !empty($transaction_data)) {
				WalletHistory::insert($transaction_data);
            }
        }
        return array('SendNotification' => $SendNotification, 'notification_result_insert' => $notification_result_pro, 'update_data' => $update_data ,'track_data' => $track_data ,'transaction_data' => $transaction_data );
    }

    public function test_push()
    {
        // $this->load->library('Twilio');

        // $response = $this->twilio->validate_phone('+14378863183');
        // // $response = $this->twilio->send('+14378863183', 'Testing 1');
        // _pre($response);
        // exit;


        // $this->load->library('Stripe');
        // $this->load->helper("stripe_helper");

        // $result = TransferExpressAccount(0, 554);
        // _pre($result);
        // exit;
        // // succeeded

        // echo "Testing";
        $device_token = 'dJqEJKQMY0BKi5c7yPFmtO:APA91bGJggJVn6Ic3FUqpl2wYEzwmzKKh2nJuVg7xy-1QXe1Qr8cKgarlQrWUVpeQx9FDjZvbuLVm4bME6NEGG2B7R6JpSCQXXpMEFff6kIZx6B8kkvu8RCmgY6tq172RJR9P5ozlsJr'; //vaibhavi
        // $device_token = 'ceq1wgRdpk-znpoEZqkWBd:APA91bHI9Gldfga36nEYriJSS5bdZ-C5WXVKr-fXl96iLhmxG5jgWcrGACpFRyUjptPMAY_LwJPGAzO9XD44_AE3UCQloSHJtTon7mxcVpJEG56qH2fB2yglb89RqXKe8CusAaF_XZtL'; //Dhrumi
        // $device_token = 'eercPqX7TOeDntRTCRtkX9:APA91bGwYuzM-ATbkTFL8uzDrGssk1KvfJbeuetkdfXAlI22Q52xDtTCdzdWiLPBU0qP8jr71zHkIjTHdvpZA968w20tDTowJYQXsapQ25YoVY-dPeoPelz7j_-EsouM-ooqTy7lbBub'; //SHivani
        $push_title = "Hello User ".date('His');
        $push_data = array(
            'message' => 'How are you',
            'order_id' => '4'
        );
        $push_type = 'request';

        $result = SendNotification('ios', $device_token, $push_title, $push_data, $push_type);
        echo "<pre>";
        print_r($result);
        exit;
    }

    public function connected_account_payout(){

        $orders = Orders::where('status', 10)->where('delivery_boy_id','!=',0)->where('db_withdraw_id',0)->with(['delivery_boy','deliveryboy_device'])->get();
        if(count($orders) > 0){
            $orders = $orders->toArray();
            $total_amount = $amount = 0;
            $earn=$earnings=[];
            $delivery_boy_ids = array_column($orders,'delivery_boy_id');
            $delivery_boy_ids = array_unique($delivery_boy_ids);
            $delivery_boy_ids = array_values($delivery_boy_ids);

            foreach ($delivery_boy_ids as $ddkey => $ddvalue) {
               $temp['id'] = $ddvalue;
               $temp['earning'] = 0;
               $temp['order_id'] = 0;
               $temp['delivery_boy'] = [];
               $temp['deliveryboy_device'] = [];
               array_push($earnings,$temp);
            }
            foreach ($orders as $key => $value) {
                foreach ($earnings as $dkey => $dvalue) {
                    if ($dvalue['id'] == $value['delivery_boy_id']) {
                        $earnings[$dkey]['earning'] += $value['delivery_boy_amount'] + $value['tip'];
                        $earnings[$dkey]['delivery_boy'] += $value['delivery_boy'];
                        $earnings[$dkey]['order_id'] += $value['id'];
                        $earnings[$dkey]['deliveryboy_device'] += $value['deliveryboy_device'];
                    }
                }
			}

            foreach ($earnings as $ekey => $evalue) {
                $earnings[$ekey]['earning'] = number_format((float)$evalue['earning'], 2, '.', '');
            }
            $this->load->helper("stripe_helper");

            foreach ($earnings as $ekey => $evalue) {
                $update_ids[] = array(
                    'id' => $evalue['id'],
                );
                if($evalue['delivery_boy']['account_id'] != ""){
                    $transferToConnectedAccount = transferToConnectedAccount($evalue['earning'] * 100,$evalue['delivery_boy']['account_id']);
                }

                $phone_no = $evalue['delivery_boy']['phone'];
                $sms_text = 'Earning of $'.$evalue['earning']." sent to your account from Rad Team.";
                    
                send_SMS($phone_no, $sms_text);
                $temp_trans = array(
                    'user_id' => $evalue['id'],
                    'amount' =>$evalue['earning'],
                    'date' => date('Y-m-d'),
                );
                $transaction_data[] = $temp_trans;

            }
            if (isset($update_ids) && !empty($update_ids)) {
				$ids = array_column($update_ids,'id');
				$update_data = array(
					'db_withdraw_id'  => 1, //withdraw
				);
				Orders::whereIn('delivery_boy_id', $ids)->update($update_data);
            } 
            if (isset($transaction_data) && !empty($transaction_data)) {
                Transaction::insert($transaction_data);
            }

        }
        echo $total_amount;
        echo "<pre>";
        print_r($earnings);
        exit;

    }
    public function connected_account_vendor_payout(){

        $orders = Orders::where('status', 10)->where('store_withdraw_id',0)->with(['user_vendor'])->get();
        if(count($orders) > 0){
            $orders = $orders->toArray();
            $total_amount = $amount = 0;
            $earn=$earnings=[];
            $vendor_ids = array_column($orders,'vendor_id');
            $vendor_ids = array_unique($vendor_ids);
            $vendor_ids = array_values($vendor_ids);

            foreach ($vendor_ids as $ddkey => $ddvalue) {
               $temp['id'] = $ddvalue;
               $temp['order_id'] = 0;
               $temp['earning'] = 0;
               $temp['user_vendor'] = [];
               array_push($earnings,$temp);
            }
            foreach ($orders as $key => $value) {
                foreach ($earnings as $dkey => $dvalue) {
                    if ($dvalue['id'] == $value['vendor_id']) {
                        $earnings[$dkey]['earning'] += $value['store_amount'] + $value['vendor_tip'];
                        $earnings[$dkey]['user_vendor'] += $value['user_vendor'];
                        $earnings[$dkey]['order_id'] += $value['id'];
                    }
                }
			}

            foreach ($earnings as $ekey => $evalue) {
                $earnings[$ekey]['earning'] = number_format((float)$evalue['earning'], 2, '.', '');
            }
            $this->load->helper("stripe_helper");

            foreach ($earnings as $ekey => $evalue) {
                $update_ids[] = array(
                    'id' => $evalue['id'],
                );
                if($evalue['user_vendor']['account_id'] != ''){
                    $transferToConnectedAccount = transferToConnectedAccount($evalue['earning'] * 100,$evalue['user_vendor']['account_id']);
                }

                $temp_trans = array(
                    'user_id' => $evalue['id'],
                    'amount' =>$evalue['earning'],
                    'date' => date('Y-m-d'),
                );
                $transaction_data[] = $temp_trans;

                $phone_no = $evalue['user_vendor']['phone'];
                $sms_text = 'Earning of $'.$evalue['earning']." sent to your account from Rad Team.";
                    
                send_SMS($phone_no, $sms_text);

            }
            if (isset($update_ids) && !empty($update_ids)) {
				$ids = array_column($update_ids,'id');
				$update_data = array(
					'store_withdraw_id'  => 1, //withdraw
				);
				Orders::whereIn('vendor_id', $ids)->update($update_data);
            } 
            if (isset($transaction_data) && !empty($transaction_data)) {
                Transaction::insert($transaction_data);
            }

        }
        echo $total_amount;
        echo "<pre>";
        print_r($earnings);
        exit;

    }
}
