<?php 

/**
 * summary
 */
abstract class MY_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Ion_auth');
        // $this->db->query("SET time_zone  = 'Asia/Riyadh'");
        // if (!$this->ion_auth->logged_in()) {
        //     redirect('admin/login');
        // }  
        //echo '<pre>';print_r($this->session->all_userdata());exit;
        if ($this->uri->segment(1) == "admin"){
            $this->breadcrumbs->push(config('site_title'), '/admin/dashboard');
        }elseif($this->uri->segment(1) == "organizer"){
            $this->breadcrumbs->push(config('site_title'), '/organizer/dashboard');
        } else{
            $this->breadcrumbs->push(config('site_title'), '/events');
        }
    }

    public function render($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('layout/header',$params);
        if($page != null){
            $this->load->view('pages/'.$page,$params,$return);
        }
        $this->load->view('layout/footer',$params);
    }

    public function web_render($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('web/layout/header',$params);
        // $this->load->view('web/layout/menu',$params);
        if($page != null){
            $this->load->view('web/pages/'.$page,$params,$return);
        }
        $this->load->view('web/layout/footer',$params);
    }

    public function admin_render($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('admin/layout/header',$params);
        if($page != null){
            $this->load->view('admin/pages/'.$page,$params,$return);
        }
        $this->load->view('admin/layout/footer',$params);
    }

    public function vendor_render($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('vendor/layout/header',$params);
        if($page != null){
            $this->load->view('vendor/pages/'.$page,$params,$return);
        }
        $this->load->view('vendor/layout/footer',$params);
    }

    public function organizer_render($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('organizer/layout/header',$params);
        if($page != null){
            $this->load->view('organizer/pages/'.$page,$params,$return);
        }
        $this->load->view('organizer/layout/footer',$params);
    }

    public function dispatcher_render($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('dispatcher/layout/header',$params);
        if($page != null){
            $this->load->view('dispatcher/pages/'.$page,$params,$return);
        }
        $this->load->view('dispatcher/layout/footer',$params);
    }


    public function response($data = null,$http = null,$format = 'json')
    {
        echo json_encode($data);
    }
    

    public function _generate_body_class()
    {
        if($this->uri->segment_array() == null){
            $uri = array('index');
        }else{
            $uri = $this->uri->segment_array();
            if(end($uri) == 'index'){
                array_pop($uri);
            }
        }
        return implode('-', $uri);
    }

}